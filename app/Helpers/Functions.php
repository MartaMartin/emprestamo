<?php

namespace App\Helpers;

class Functions {

	public static function prueba() {
		return 'hola';
	}

	public static function beautyDate($date) {
		return strftime(_('%a, %e de %B de %G'), strtotime($date));
	}

	public static function beautyDateTime($date) {
		return strftime(_('%a, %e de %B de %G a las %H:%M'), strtotime($date));
	}

	public static function beautyTime($date) {
		return strftime(_('%H:%M'), strtotime($date));
	}

	public static function shortDate($date) {
		return date('d.m.Y', strtotime($date));
	}

	public static function shortDateTime($date) {
		return date('d.m.Y H:i', strtotime($date));
	}

    public static function generateRandomString($length = 10) {
        // $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}
