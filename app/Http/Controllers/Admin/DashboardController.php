<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Notification;
use App\Models\Operation;
use App\Models\User;

class DashboardController extends Controller
{
    public function index() {
		$user = Auth::user();

        $opened_operations = Operation::where('status', 3)->get();

        $data['d1'] = Operation::count();
        $data['d2'] = $opened_operations->count();
        $data['d3'] = User::where('role', 'investor')->count();
        $data['d4'] = User::where('role', 'transferor_company')->count();

        $data['tasks'] = [];

        foreach ($opened_operations as $opened_operation) {
            $percent = round($opened_operation->getCurrentInvested() / $opened_operation->invoice->price * 100, 2);

            if ($percent < 33.33) {
                $color = 'danger';
            } elseif ($percent < 66.66) {
                $color = 'warning';
            } else {
                $color = 'success';
            }

            $data['tasks'][] = [
                'name' => $opened_operation->invoice->name . ' <small>' . number_format($opened_operation->getCurrentInvested(), 2, ',', '.') . ' / ' . number_format($opened_operation->invoice->price, 2, ',', '.') . ' €</small>',
                'progress' => $percent,
                'color' => $color
            ];
        }

        return view('backend.pages.test-admin')->with($data);
    }

    public function eventsTemp() {
        return view('backend.pages.events');
    }
}
