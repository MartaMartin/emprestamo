<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\DebtorCompany;
use App\Models\User;
use DB;
use Auth;
use Validator;
use Input;
use Redirect;

class DebtorCompanyController extends Controller
{

    /**
     * Show the form for editing a new resource.
     *
     * @param  Request  $request
     * @return Response
     */
    public function edit(Request $request, $id)
    {
		if ($debtor_company = DebtorCompany::where('user_id', $id)->first()) {
			$this->authorize($debtor_company);
		} else {
			$this->authorize(new DebtorCompany);
			$debtor_company = new DebtorCompany;
			$debtor_company->create(array(
				'user_id' => $id
			));
			$debtor_company = DebtorCompany::where('user_id', $id)->first();
		}
		$user = User::find($id);
        return view('backend.debtor_companies.edit', compact('debtor_company', 'user'));
    }

	/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
	    $user = Auth::user();
        $debtor_company = DebtorCompany::findOrFail($id);
        $this->authorize($debtor_company);

		$rules = array(
			'name' => 'required',
		);

		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator->messages())->withInput();
			// return Redirect::back()->with('error', _('No se pudo actualizar el evento'));
		}

        $debtor_company->update($request->all());
        return Redirect::back()->with('message', sprintf(_('Se han actualizado los datos de la empresa deudora %s'), $debtor_company->name));
    }

}
