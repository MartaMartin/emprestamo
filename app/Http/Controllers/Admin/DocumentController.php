<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Document;
use App\Models\User;
use DB;
use Auth;
use Validator;
use Input;
use Redirect;

class DocumentController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param  Request  $request
     * @return Response
     */
    public function index(Request $request, $id)
    {
		$user = Auth::user();
		$this->authorize(new Document);

        $documents = DB::table('documents')
            ->whereNull('deleted_at')
			->where('model', 'User')
			->where('foreign_id', $id)
            ->orderBy('created_at', 'desc')
            ->paginate(config('admin.pagination'));

		$user = User::find($id);

        return view('backend.documents.index', compact('documents', 'user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  Request  $request
     * @return Response
     */
    public function create(Request $request, $id)
    {
        $this->authorize(new Document);
		$user = User::find($id);
        return view('backend.documents.create', compact('user'));
    }

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$user = Auth::user();
        $this->authorize(new Document);

		$data = $request->all();

		if (!empty($data['user_document_names'])) {
			$names = $data['user_document_names'];
			$files = $data['user_document_files'];
			$ids = $data['ids'];

			for ($i = 0; $i < count($files); $i ++) {
				if ($ids[$i]) {
					$document = Document::find($ids[$i]);
				} else {
					$document = new Document;
				}
				if ($files[$i]) {
					$document->filename = $files[$i];
				}
				$document->foreign_id = $data['foreign_id'];
				$document->model = $data['model'];
				if ($names[$i]) {
					$document->name = $names[$i];
					$document->save();
				}
			}
		}

		return Redirect::back()->with('message', _('Documentos añadidos'));
	}

    /**
     * Show the form for editing a new resource.
     *
     * @param  Request  $request
     * @return Response
     */
    public function edit(Request $request, $id)
    {
		if ($document = Document::where('user_id', $id)->first()) {
			$this->authorize($document);
		} else {
			$this->authorize(new Document);
			$document = new Document;
			$document->create(array(
				'user_id' => $id
			));
			$document = Document::where('user_id', $id)->first();
		}
		$user = User::find($id);
        return view('backend.documents.edit', compact('document', 'user'));
    }

	/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
	    $user = Auth::user();
        $document = Document::findOrFail($id);
        $this->authorize($document);

		$rules = array(
			'type' => 'required',
		);

		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator->messages())->withInput();
			// return Redirect::back()->with('error', _('No se pudo actualizar el evento'));
		}

        $document->update($request->all());
        return Redirect::back()->with('message', sprintf(_('Se han actualizado los datos del documento %s'), $document->name));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $document = Document::findOrFail($id);
        $this->authorize($document);
        $document->delete();
    }

}
