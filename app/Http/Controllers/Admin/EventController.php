<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Circuit;
use App\Country;
use App\CountryState;

use App\Event;
use App\EventCategory;
use App\EventDocument;
use App\EventFeeType;
use App\EventGiveaway;
use App\EventIntermediatePoint;
use App\EventPaymentMethod;
use App\EventRaceType;
//use App\EventParent;
//use App\EventCategoriesRegistrationPrice;
//use App\EventRaceTypesRegistrationPrice;

//use App\Events\Event;
use App\Federation;
use App\Organization;
use App\RaceType;
use App\RegistrationFile;
use App\RegistrationTextfield;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

//use Input;
use Validator;
//use Redirect;

use Auth;


class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
	    $user = Auth::user();
        $this->authorize(new Event);

        $events = Event::whereNull('deleted_at');

        if ($user->role === 'organizer')
        {
            $events->whereIn('organizations_id', $user->userOrganizations->lists('organizations_id')->toArray());
        }

        $events = $events->orderBy('created_at', 'desc')->paginate(config('admin.pagination'));

        return view('backend.events.index', compact('events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  Request  $request
     * @return Response
     */
    public function create(Request $request)
    {
        $this->authorize(new Event);
	    extract($this->formData(Auth::user()));

        return view('backend.events.create', compact(
            'event',
            'countries',
            'country_states',
            'organizations',
            'federations',
            'circuits',
            'categories',
            'race_types'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
//        $this->authorize($event);

        $rules = array(
            'name' => 'required',
            'edition' => 'required',
            'seats' => 'required',
            'start_time' => 'required',
            'city' => 'required',
            'state' => 'required',
            'start_location' => 'required',
            'registration_start_time' => 'required',
            'registration_end_time' => 'required',
            'early_registration_start_time' => 'required',
            'early_registration_end_time' => 'required',
            'late_registration_end_time' => 'required',
        );

        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator->messages())->withInput();
        }

        $event = new Event;
        $event->create($request->all());
//        return Redirect::back()->with('message', sprintf(_('Se ha creado el evento evento %s'), $event->name));
//        return view('backend.events.edit', compact('event'))->with('message', sprintf(_('Se ha creado el evento evento %s'), $event->name));
//        return view('backend.events.index')->with('message', sprintf(_('Se ha creado el evento evento %s'), $event->name));
//        return view('backend.events.index')->with('message', sprintf(_('Se ha creado el evento evento %s'), $event->name));
        return redirect()->route('admin.events.index')->with('message', sprintf(_('Se ha creado el evento evento %s'), $event->name));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $event = Event::findOrFail($id);
        $this->authorize($event);
	    extract($this->formData(Auth::user(), $event));

//        $event_race_types = DB::table('race_types')
//            ->join('event_race_types', 'race_types.id', '=', 'event_race_types.race_types_id')
//            ->select('race_types.name', 'event_race_types.id')
//            ->get();/*WIP*/


//        $event_race_types = EventRaceType::lists('seats', 'id')->sortByDesc('name')->reverse(); /*WIP*/
//        dd($event_race_types);
//        $organizations_id = '';
//        foreach ($event->userOrganizations as $user_organization)
//        {
//            $organizations_id = $user_organization->pivot->id;
//            dd($organizations_id);
//        }
//        $this->authorize($event);

//        $event_categories = EventCategory::all()->where('events_id', $event->id);
//        $event_documents = EventDocument::all()->where('events_id', $event->id);
//        $event_fee_types = EventFeeType::all()->where('events_id', $event->id);
//        $event_giveaways = EventGiveaway::all()->where('events_id', $event->id);
//        $event_intermediate_points = EventIntermediatePoint::all()->where('events_id', $event->id);
//        $event_payment_methods = EventPaymentMethod::all()->where('events_id', $event->id);
//        $event_race_types = EventRaceType::all()->where('events_id', $event->id);

        return view('backend.events.edit', compact(
            'event',
            'countries',
            'country_states',
            'organizations',
            'federations',
            'circuits',
            'categories',
            'race_types',
            'event_race_types'
//            'event_categories',
//            'event_documents'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
	    $user = Auth::user();
        $event = Event::findOrFail($id);
        $this->authorize($event);
        $input = Input::all();

        if ($user->cannot('admin_edit', $event))
        {
            if (!$input = $event->checkAdminFields($input, $user))
            {
                    return Redirect::back()->with('error', _('No se pudo actualizar el evento'));
            }
        }

        $event->update($request->all());
        return Redirect::back()->with('message', sprintf(_('Se han actualizado los datos del evento %s'), $event->fullname));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event = Event::findOrFail($id);
        $this->authorize($event);
        $event_fullname = $event->name.($event->edition ? ' '.$event->edition : '');
        $event->delete();

        return redirect()->route('admin.events.index')->with('message', _("El evento $event_fullname ha sido eliminado."));
    }

    /**
     * Helper function for formData treatment.
     *
     * @param  \App\User $user
     * @param  \App\Event $event
     * @return \Illuminate\Http\Response
     */
    protected function formData($user, $event = null)
    {
	    $countries = Country::orderBy('name')->lists('name', 'id');
        $country_states = CountryState::orderBy('name')->lists('name', 'id');
        $federations = Federation::orderBy('name')->lists('name', 'id');
        $categories = Category::orderBy('name')->lists('name', 'id');
        $race_types = RaceType::orderBy('name')->lists('name', 'id');

        if ($user->role === 'admin')
        {
                $organizations = Organization::orderBy('name')->lists('name', 'id');
        }
        elseif ($user->role === 'organizer')
        {

	    $organizationIds = $user->organizationsWithLevel('gold', $event);
            $organizations = Organization::whereIn('id', $organizationIds)
                ->orderBy('name')
                ->lists('name', 'id');
        }

        $circuits = [];
	foreach ($organizations as $organizations_id => $organizations_name)
	{
		$circuits[$organizations_id] = Organization::find($organizations_id)->circuits()->lists('name', 'id')->toArray();
	}

        if($event)
        {
            $event_race_types = EventRaceType::where('events_id', $event->id)
                ->lists('race_types_id', 'id');
            $_race_types = RaceType::whereIn('id', $event_race_types)
                ->lists('name', 'id');
            foreach ($event_race_types as $id => $race_type_id)
            {
                $event_race_types[$id] = $_race_types[$race_type_id];
            }
        }

        if($event)
        {
            return compact(
                'countries',
                'country_states',
                'federations',
                'categories',
                'race_types',
                'organizations',
                'circuits',
                'event_race_types'
            );
        }
        else
        {
            return compact(
                'countries',
                'country_states',
                'federations',
                'categories',
                'race_types',
                'organizations',
                'circuits'
            );
        }

    }
}
