<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\FinancialEntity;
use DB;
use Auth;
use Validator;
use Input;
use Redirect;

class FinancialEntityController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param  Request  $request
     * @return Response
     */
    public function index(Request $request)
    {
		$user = Auth::user();
		$this->authorize(new FinancialEntity);

        $financial_entities = DB::table('financial_entities')
            ->whereNull('deleted_at')
            ->orderBy('created_at', 'desc')
            ->paginate(config('admin.pagination'));

        return view('backend.financial_entities.index', compact('financial_entities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  Request  $request
     * @return Response
     */
    public function create(Request $request)
    {
        $this->authorize(new FinancialEntity);
        return view('backend.financial_entities.create');
    }

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$user = Auth::user();
        $this->authorize(new FinancialEntity);

		$rules = array(
			'name' => 'required',
		);

		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator->messages())->withInput();
		}

		$financial_entity = new FinancialEntity;
		$financial_entity->create($request->all());

		return redirect()->route('admin.financial_entities.index')->with('message', sprintf(_('Se ha creado la entidad financiera %s'), $financial_entity->name));
	}

    /**
     * Show the form for editing a new resource.
     *
     * @param  Request  $request
     * @return Response
     */
    public function edit(Request $request, $id)
    {
		$financial_entity = FinancialEntity::findOrFail($id);
        $this->authorize($financial_entity);
        return view('backend.financial_entities.edit', compact('financial_entity'));
    }

	/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
	    $user = Auth::user();
        $financial_entity = FinancialEntity::findOrFail($id);
        $this->authorize($financial_entity);

		$rules = array(
			'name' => 'required',
		);

		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator->messages())->withInput();
			// return Redirect::back()->with('error', _('No se pudo actualizar el evento'));
		}

        $financial_entity->update($request->all());
        return Redirect::back()->with('message', sprintf(_('Se han actualizado los datos de la entidad financiera %s'), $financial_entity->name));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $financial_entity = FinancialEntity::findOrFail($id);
        $this->authorize($financial_entity);
		$name = $financial_entity->name;
        $financial_entity->delete();

        return redirect()->route('admin.financial_entities.index')->with('message', _("La entidad financiera $name ha sido eliminada."));
    }

}
