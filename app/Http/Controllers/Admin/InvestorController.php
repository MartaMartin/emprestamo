<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Investor;
use App\Models\User;
use DB;
use Auth;
use Validator;
use Input;
use Redirect;

class InvestorController extends Controller
{

    /**
     * Show the form for editing a new resource.
     *
     * @param  Request  $request
     * @return Response
     */
    public function edit(Request $request, $id)
    {
		if ($investor = Investor::where('user_id', $id)->first()) {
			$this->authorize($investor);
		} else {
			$this->authorize(new Investor);
			$investor = new Investor;
			$investor->create([
				'user_id' => $id
			]);
			$investor = Investor::where('user_id', $id)->first();
		}
		$user = User::find($id);
        return view('backend.investors.edit', compact('investor', 'user'));
    }

	/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
	    $user = Auth::user();
        $investor = Investor::findOrFail($id);
        $this->authorize($investor);

		$rules = array(
			'type' => 'required',
		);

		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator->messages())->withInput();
			// return Redirect::back()->with('error', _('No se pudo actualizar el evento'));
		}

        $investor->update($request->all());
        return Redirect::back()->with('message', sprintf(_('Se han actualizado los datos del inversor %s'), $investor->name));
    }

}
