<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Invoice;
use App\Models\TransferorCompany;
use App\Models\Operation;
use App\Models\DebtorCompany;
use App\Models\Document;
use App\Models\Notification;
use App\Models\User;
use DB;
use Auth;
use Validator;
use Input;
use Redirect;

class InvoiceController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param  Request  $request
     * @return Response
     */
    public function index(Request $request)
    {
		$user = Auth::user();
		$this->authorize(new Invoice);

        $invoices = Invoice::whereDoesntHave('operations')
            ->orderBy('created_at', 'desc')
            ->paginate(config('admin.pagination'));

		$status = Operation::getStatus();

        return view('backend.invoices.index', compact('invoices', 'status'));
    }

    /**
     * Show the form for editing a new resource.
     *
     * @param  Request  $request
     * @return Response
     */
    public function edit(Request $request, $id)
    {
		$invoice = Invoice::findOrFail($id);
        $this->authorize($invoice);
		if (!$operation = Operation::where('invoice_id', $id)->first()) {
			$operation = null;
		} else {
			// return redirect(route('admin.operations.show', $operation->id));
		}
		$debtor_companies = DebtorCompany::lists('name', 'id');
		$status = Operation::getStatus();
		$documents = Document::where('foreign_id', $id)->where('model', 'Invoice')->get();

		if ($notification_id = Notification::where('read', 0)
			->where('params', 'like', '%' . $id . '%')
			->where('url', 'admin.invoices.edit')
			->lists('id')
			->first()) {
				Notification::markAsRead($notification_id);
		}

        return view('backend.invoices.edit', compact('invoice', 'operation', 'debtor_companies', 'status', 'documents'));
    }

	/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
	    $user = Auth::user();
        $invoice = Invoice::findOrFail($id);
        $this->authorize($invoice);

		$invoice->debtor_company_id = $request->get('debtor_company_id');
		$invoice->save();

		$data = $request->all();

		if (!$operation = Operation::where('invoice_id', $id)->first()) {
			if ($data['status'] == 1 || $data['status'] == 2) {
				$transferor_company = TransferorCompany::whereId($invoice['transferor_company_id'])->first();
				if ($data['status'] == 1) { // Aprobado
					$message = _('Han aprobado tu presupuesto, entra para aceptar las condiciones del contrato');
				} elseif ($data['stauts'] == 2) { // Rechazado
					$message = _('Han rechazado tu presupuesto');
				}
				Notification::sendNotification($transferor_company->user_id, $message, 'private.invoices.view', json_encode([$id]));
			}
			$operation = new Operation;
			$operation->create(['invoice_id' => $id]);
			$operation = Operation::where('invoice_id', $id)->first();
		}

		$data['invoice_id'] = $id;
		$operation->update($data);

        return Redirect::back()->with('message', sprintf(_('Se han actualizado los datos de el presupuesto %s'), $invoice->name));
    }

}
