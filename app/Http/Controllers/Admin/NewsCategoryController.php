<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\NewsCategory;
use DB;
use Auth;
use Validator;
use Input;
use Redirect;

class NewsCategoryController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param  Request  $request
     * @return Response
     */
    public function index(Request $request)
    {
		$user = Auth::user();
		$this->authorize(new NewsCategory);

        $categories = DB::table('news_categories')
            ->whereNull('deleted_at')
            ->orderBy('created_at', 'desc')
            ->paginate(config('admin.pagination'));

        return view('backend.news_categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  Request  $request
     * @return Response
     */
    public function create(Request $request)
    {
        $this->authorize(new NewsCategory);
        return view('backend.news_categories.create');
    }

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$user = Auth::user();
        $this->authorize(new NewsCategory);

		$rules = array(
			'name' => 'required',
		);

		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator->messages())->withInput();
		}

		$category = new NewsCategory;
		$category->create($request->all());

		return redirect()->route('admin.news_categories.index')->with('message', sprintf(_('Se ha creado la categoría %s'), $category->name));
	}

    /**
     * Show the form for editing a new resource.
     *
     * @param  Request  $request
     * @return Response
     */
    public function edit(Request $request, $id)
    {
		$category = NewsCategory::findOrFail($id);
        $this->authorize($category);
        return view('backend.news_categories.edit', compact('category'));
    }

	/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
	    $user = Auth::user();
        $category = NewsCategory::findOrFail($id);
        $this->authorize($category);

		$rules = array(
			'name' => 'required',
		);

		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator->messages())->withInput();
			// return Redirect::back()->with('error', _('No se pudo actualizar el evento'));
		}

        $category->update($request->all());
        return Redirect::back()->with('message', sprintf(_('Se han actualizado los datos de la categoría %s'), $category->name));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = NewsCategory::findOrFail($id);
        $this->authorize($category);
		$name = $category->name;
        $category->delete();

        return redirect()->route('admin.news_categories.index')->with('message', _("La categoría $name ha sido eliminada."));
    }

}
