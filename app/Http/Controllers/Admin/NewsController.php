<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\News;
use App\Models\Document;
use DB;
use Auth;
use Validator;
use Input;
use Redirect;
use Illuminate\Support\Str;

class NewsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param  Request  $request
     * @return Response
     */
    public function index(Request $request)
    {
		$user = Auth::user();
		$this->authorize(new News);

        $news = DB::table('news')
			->select('news.*', 'news_categories.name as category_name')
			->leftJoin('news_categories', 'news.news_category_id', '=', 'news_categories.id')
            ->whereNull('news.deleted_at')
            ->orderBy('news.created_at', 'desc')
            ->paginate(config('admin.pagination'));

        return view('backend.news.index', compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  Request  $request
     * @return Response
     */
    public function create(Request $request)
    {
        $this->authorize(new News);
		extract($this->selects());
        return view('backend.news.create', compact('categories'));
    }

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$user = Auth::user();
        $this->authorize(new News);

		$rules = array(
			'title' => 'required',
			'content' => 'required',
			'news_category_id' => 'required',
		);

		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator->messages())->withInput();
		}

        $slug = Str::slug($request->get('title'));
        if (News::whereSlug($slug)->first()) {
			return Redirect::back()->withErrors(_('Ya existe una noticia con este título'))->withInput();
        }

		$news = new News;
		$news->title = $request->get('title');
		$news->news_category_id = $request->get('news_category_id');
		$news->content = $request->get('content');
		$news->slug = $slug;
		$news->save();

		$data = $request->all();
        if (!empty($data['filename'])) {
            $file = $data['filename'];
            $document = new Document;
            $document->name = '';
            $document->model = 'News';
            $document->foreign_id = $news->id;
            $document->filename = $file;
            $document->type = 'image';
            $document->save();
        }

		return redirect()->route('admin.news.index')->with('message', sprintf(_('Se ha creado la noticia %s'), $news->title));
	}

    /**
     * Show the form for editing a new resource.
     *
     * @param  Request  $request
     * @return Response
     */
    public function edit(Request $request, $id)
    {
		$news = News::findOrFail($id);
        $this->authorize($news);
		extract($this->selects());

		$document = Document::where('model', 'News')->where('foreign_id', $id)->first();
        return view('backend.news.edit', compact('news', 'categories', 'document'));
    }

	/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
	    $user = Auth::user();
        $news = News::findOrFail($id);
        $this->authorize($news);

		$rules = array(
			'title' => 'required',
			'content' => 'required',
		);

		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator->messages())->withInput();
			// return Redirect::back()->with('error', _('No se pudo actualizar el evento'));
		}

		$news = new News;
		$news->title = $request->get('title');
		$news->news_category_id = $request->get('news_category_id');
		$news->content = $request->get('content');
		// $news->slug = Str::slug($request->get('title'));
		$news->save();

		$file = $request->get('filename');
		$document = new Document;
		$document->name = '';
		$document->model = 'News';
		$document->foreign_id = $news->id;
		$document->filename = $file;
		$document->type = 'image';
		$document->save();

        // $news->update($request->all());
        return Redirect::back()->with('message', sprintf(_('Se han actualizado los datos de la noticia %s'), $news->title));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $news = News::findOrFail($id);
        $this->authorize($news);
		$title = $news->title;
        $news->delete();

        return redirect()->route('admin.news.index')->with('message', _("La noticia $title ha sido eliminada."));
    }

    /**
     * Returns list of tags and categories
     *
     * @return Array
     */
	private function selects() {
		/*
		$tags_list = DB::table('tags')->where('deleted_at', null)->get();
		$tags = array();
		foreach ($tags_list as $tag) {
			$tags[$tag->id] = $tag->name;
		}
		*/

		$category_list = DB::table('news_categories')->where('deleted_at', null)->get();
		$categories = array();
		foreach ($category_list as $category) {
			$categories[$category->id] = $category->name;
		}

		// return compact('tags', 'categories');
		return compact('categories');
	}

}
