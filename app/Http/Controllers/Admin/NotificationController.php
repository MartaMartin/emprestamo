<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Notification;
use DB;
use Auth;
use Validator;
use Input;
use Redirect;

class NotificationController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param  Request  $request
     * @return Response
     */
    public function index(Request $request)
    {
		$user = Auth::user();
        $types = Notification::getTypes();

        $requests = Notification::
            whereIn('url', $types['requests'])
            ->orderBy('created_at', 'desc')
            ->paginate(config('admin.pagination'));

        $accepted = Notification::
            whereIn('url', $types['accepted'])
            ->orderBy('created_at', 'desc')
            ->paginate(config('admin.pagination'));

        $rejected = Notification::
            whereIn('url', $types['rejected'])
            ->orderBy('created_at', 'desc')
            ->paginate(config('admin.pagination'));

        $no_response = Notification::
            whereIn('url', $types['no_response'])
            ->orderBy('created_at', 'desc')
            ->paginate(config('admin.pagination'));

        $invests = Notification::
            whereIn('url', $types['invests'])
            ->orderBy('created_at', 'desc')
            ->paginate(config('admin.pagination'));

        return view('backend.notifications.index', compact('requests', 'accepted', 'rejected', 'no_response', 'invests'));
    }

}
