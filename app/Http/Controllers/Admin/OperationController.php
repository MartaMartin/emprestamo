<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Operation;
use App\Models\Transfer;
use DB;
use Auth;
use Validator;
use Input;
use Redirect;

class OperationController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param  Request  $request
     * @return Response
     */
    public function index(Request $request)
    {
		$user = Auth::user();
		$this->authorize(new Operation);

        $operations = Operation::
            orderBy('created_at', 'desc')
            ->paginate(config('admin.pagination'));

		$status = Operation::getStatus();

        return view('backend.operations.index', compact('operations', 'status'));
    }

    /**
     * Show the form for editing a new resource.
     *
     * @param  Request  $request
     * @return Response
     */
    public function edit(Request $request, $id)
    {
		$operation = Operation::find($id);
        $this->authorize($operation);
		$operation->active = 1;
		$operation->save();
		foreach ($operation->transfers as $transfer) {
			$transfer->active = 1;
			$transfer->save();
		}
		return redirect()->route('admin.operations.index')->with('message', _('Operationencia validada'));
    }

    public function show($id) {
		$operation = Operation::find($id);
		$status = Operation::getStatus();
		$transfer_types = Transfer::getTypes();
        return view('backend.operations.show', compact('operation', 'status', 'transfer_types'));
    }

    public function updateStatus(Request $request, $operation_id) {
        if ($request->has('loan_status')) {
            $operation = Operation::find($operation_id);
            $operation->loan_status = $request->get('loan_status');
            $operation->save();
        }
        die;
    }

}
