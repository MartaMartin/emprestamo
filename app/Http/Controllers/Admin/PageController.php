<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Page;
use DB;
use Auth;
use Validator;
use Input;
use Redirect;

class PageController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param  Request  $request
     * @return Response
     */
    public function index(Request $request)
    {
		$user = Auth::user();
		$this->authorize(new Page);

        $pages = DB::table('pages')
            ->whereNull('deleted_at')
            ->orderBy('created_at', 'desc')
            ->paginate(config('admin.pagination'));

        return view('backend.pages.index', compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  Request  $request
     * @return Response
     */
    public function create(Request $request)
    {
        $this->authorize(new Page);
        return view('backend.pages.create');
    }

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$user = Auth::user();
        $this->authorize(new Page);

		$rules = array(
			'title' => 'required',
			'content' => 'required',
		);

		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator->messages())->withInput();
		}

		$page = new Page;
		$page->create($request->all());

		return redirect()->route('admin.pages.index')->with('message', sprintf(_('Se ha creado la página %s'), $page->title));
	}

    /**
     * Show the form for editing a new resource.
     *
     * @param  Request  $request
     * @return Response
     */
    public function edit(Request $request, $id)
    {
		$page = Page::findOrFail($id);
        $this->authorize($page);
        return view('backend.pages.edit', compact('page'));
    }

	/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
	    $user = Auth::user();
        $page = Page::findOrFail($id);
        $this->authorize($page);

		$rules = array(
			'title' => 'required',
			'content' => 'required',
		);

		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator->messages())->withInput();
			// return Redirect::back()->with('error', _('No se pudo actualizar el evento'));
		}

        $page->update($request->all());
        return Redirect::back()->with('message', sprintf(_('Se han actualizado los datos de la página %s'), $page->title));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $page = Page::findOrFail($id);
        $this->authorize($page);
		$title = $page->title;
        $page->delete();

        return redirect()->route('admin.pages.index')->with('message', _("La página $title ha sido eliminada."));
    }

}
