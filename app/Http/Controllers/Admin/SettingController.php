<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Setting;
use DB;
use Auth;
use Validator;
use Input;
use Redirect;

class SettingController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param  Request  $request
     * @return Response
     */
    public function index(Request $request)
    {
		$user = Auth::user();
		$this->authorize(new Setting);

        $setting_list = Setting::
            orderBy('created_at', 'desc')
            ->paginate(config('admin.pagination'));

        return view('backend.settings.index', compact('setting_list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  Request  $request
     * @return Response
     */
    public function create(Request $request)
    {
        $this->authorize(new Setting);
        return view('backend.settings.create');
    }

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$user = Auth::user();
        $this->authorize(new Setting);

		$rules = array(
			'name' => 'required',
		);

		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator->messages())->withInput();
		}

		$setting = new Setting;
		$setting->create($request->all());

		return redirect()->route('admin.settings.index')->with('message', sprintf(_('Se ha creado la etiqueta %s'), $setting->name));
	}

    /**
     * Show the form for editing a new resource.
     *
     * @param  Request  $request
     * @return Response
     */
    public function edit(Request $request, $id)
    {
		$setting = Setting::findOrFail($id);
        $this->authorize($setting);
        return view('backend.settings.edit', compact('setting'));
    }

	/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
	    $user = Auth::user();
        $setting = Setting::findOrFail($id);
        $this->authorize($setting);

		$rules = array(
			'name' => 'required',
		);

		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator->messages())->withInput();
			// return Redirect::back()->with('error', _('No se pudo actualizar el evento'));
		}

        $setting->update($request->all());
        return Redirect::back()->with('message', sprintf(_('Se han actualizado los datos de la configuración %s'), $setting->name));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $setting = Setting::findOrFail($id);
        $this->authorize($setting);
		$name = $setting->name;
        $setting->delete();

        return redirect()->route('admin.settings.index')->with('message', _("La configuración $name ha sido eliminada."));
    }

}
