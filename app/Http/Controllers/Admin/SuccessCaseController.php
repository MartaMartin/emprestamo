<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\SuccessCase;
use DB;
use Auth;
use Validator;
use Input;
use Redirect;

class SuccessCaseController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param  Request  $request
     * @return Response
     */
    public function index(Request $request)
    {
		$user = Auth::user();
		$this->authorize(new SuccessCase);

        $success_cases = DB::table('success_cases')
            ->whereNull('deleted_at')
            ->orderBy('created_at', 'desc')
            ->paginate(config('admin.pagination'));

        return view('backend.success_cases.index', compact('success_cases'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  Request  $request
     * @return Response
     */
    public function create(Request $request)
    {
        $this->authorize(new SuccessCase);
        return view('backend.success_cases.create');
    }

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$user = Auth::user();
        $this->authorize(new SuccessCase);

		$rules = array(
			'name' => 'required',
		);

		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator->messages())->withInput();
		}

		$success_case = new SuccessCase;
		$success_case->create($request->all());

		return redirect()->route('admin.success_cases.index')->with('message', sprintf(_('Se ha creado el caso de éxito %s'), $success_case->name));
	}

    /**
     * Show the form for editing a new resource.
     *
     * @param  Request  $request
     * @return Response
     */
    public function edit(Request $request, $id)
    {
		$success_case = SuccessCase::findOrFail($id);
        $this->authorize($success_case);
        return view('backend.success_cases.edit', compact('success_case'));
    }

	/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
	    $user = Auth::user();
        $success_case = SuccessCase::findOrFail($id);
        $this->authorize($success_case);

		$rules = array(
			'name' => 'required',
		);

		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator->messages())->withInput();
			// return Redirect::back()->with('error', _('No se pudo actualizar el evento'));
		}

        $success_case->update($request->all());
        return Redirect::back()->with('message', sprintf(_('Se han actualizado los datos de el caso de éxito %s'), $success_case->name));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $success_case = SuccessCase::findOrFail($id);
        $this->authorize($success_case);
		$name = $success_case->name;
        $success_case->delete();

        return redirect()->route('admin.success_cases.index')->with('message', _("El caso de éxito $name ha sido eliminada."));
    }

}
