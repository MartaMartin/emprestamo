<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Tag;
use DB;
use Auth;
use Validator;
use Input;
use Redirect;

class TagController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param  Request  $request
     * @return Response
     */
    public function index(Request $request)
    {
		$user = Auth::user();
		$this->authorize(new Tag);

        $tags = DB::table('tags')
            ->whereNull('deleted_at')
            ->orderBy('created_at', 'desc')
            ->paginate(config('admin.pagination'));

        return view('backend.tags.index', compact('tags'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  Request  $request
     * @return Response
     */
    public function create(Request $request)
    {
        $this->authorize(new Tag);
        return view('backend.tags.create');
    }

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$user = Auth::user();
        $this->authorize(new Tag);

		$rules = array(
			'name' => 'required',
		);

		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator->messages())->withInput();
		}

		$tag = new Tag;
		$tag->create($request->all());

		return redirect()->route('admin.tags.index')->with('message', sprintf(_('Se ha creado la etiqueta %s'), $tag->name));
	}

    /**
     * Show the form for editing a new resource.
     *
     * @param  Request  $request
     * @return Response
     */
    public function edit(Request $request, $id)
    {
		$tag = Tag::findOrFail($id);
        $this->authorize($tag);
        return view('backend.tags.edit', compact('tag'));
    }

	/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
	    $user = Auth::user();
        $tag = Tag::findOrFail($id);
        $this->authorize($tag);

		$rules = array(
			'name' => 'required',
		);

		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator->messages())->withInput();
			// return Redirect::back()->with('error', _('No se pudo actualizar el evento'));
		}

        $tag->update($request->all());
        return Redirect::back()->with('message', sprintf(_('Se han actualizado los datos de la etiqueta %s'), $tag->name));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tag = Tag::findOrFail($id);
        $this->authorize($tag);
		$name = $tag->name;
        $tag->delete();

        return redirect()->route('admin.tags.index')->with('message', _("La etiqueta $name ha sido eliminada."));
    }

}
