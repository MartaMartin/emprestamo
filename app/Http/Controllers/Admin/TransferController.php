<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Transfer;
use App\Models\Investor;
use App\Models\Notification;
use DB;
use Auth;
use Validator;
use Input;
use Redirect;

class TransferController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param  Request  $request
     * @return Response
     */
    public function index(Request $request)
    {
		$user = Auth::user();
		$this->authorize(new Transfer);

        $transfers = Transfer::
            orderBy('created_at', 'desc')
            ->where('active', 0)
            ->whereNotNull('investor_id')
            ->paginate(config('admin.pagination'));

		$types = Transfer::getTypes();

        $notifications = Notification::where('read', 0)->where('url', 'admin.transfers.index')->get();
        foreach ($notifications as $notification) {
            $notification->update(['read' => 1]);
        }

        return view('backend.transfers.index', compact('transfers', 'types'));
    }

    /**
     * Show the form for editing a new resource.
     *
     * @param  Request  $request
     * @return Response
     */
    public function edit(Request $request, $id, $delete = false)
    {
		$transfer = Transfer::find($id);
        $this->authorize($transfer);

        if ($delete) {
            $transfer->delete();
            Notification::sendNotification($transfer->investor->user_id, _('Transferencia rechazada'), 'private.investors.wallet');
            Investor::updateWallet($transfer->investor_id);
            return redirect()->route('admin.transfers.index')->with('message', _('Transferencia rechazada'));
        }

		$transfer->active = 1;
		$transfer->save();
		Investor::updateWallet($transfer->investor_id);

        Notification::sendNotification($transfer->investor->user_id, _('Transferencia validada'), 'private.investors.wallet');

		return redirect()->route('admin.transfers.index')->with('message', _('Transferencia validada'));
    }

}
