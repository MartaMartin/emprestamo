<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\TransferorCompany;
use App\Models\User;
use DB;
use Auth;
use Validator;
use Input;
use Redirect;

class TransferorCompanyController extends Controller
{

    /**
     * Show the form for editing a new resource.
     *
     * @param  Request  $request
     * @return Response
     */
    public function edit(Request $request, $id)
    {
		if ($transferor_company = TransferorCompany::where('user_id', $id)->first()) {
			$this->authorize($transferor_company);
		} else {
			$this->authorize(new TransferorCompany);
			$transferor_company = new TransferorCompany;
			$transferor_company->create(array(
				'user_id' => $id
			));
			$transferor_company = TransferorCompany::where('user_id', $id)->first();
		}
		$user = User::find($id);
        return view('backend.transferor_companies.edit', compact('transferor_company', 'user'));
    }

	/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
	    $user = Auth::user();
        $transferor_company = TransferorCompany::findOrFail($id);
        $this->authorize($transferor_company);

		$rules = array(
			'name' => 'required',
		);

		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator->messages())->withInput();
			// return Redirect::back()->with('error', _('No se pudo actualizar el evento'));
		}

        $transferor_company->update($request->all());
        return Redirect::back()->with('message', sprintf(_('Se han actualizado los datos de la empresa cedente %s'), $transferor_company->name));
    }

}
