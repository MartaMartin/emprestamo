<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\User;
use App\Models\Operation;
use App\Models\Investor;
use App\Models\DebtorCompany;
use App\Models\TransferorCompany;
use App\Models\Document;
use App\Models\Transfer;
use App\Models\RejectedInvoice;

use Illuminate\Support\Facades\Hash;
use Input;
use Validator;
use Redirect;
use Auth;
use Gate;

use Log;

use Event;
// use App\Events\NewUserCreated;

use LaravelGettext;

use DB;

class UserController extends Controller
{
    /**
     * Ban a selected user.
     *
     * @param int $id
     * @return redirect
	 */
    public function ban($id)
    {
        $user = User::findOrFail($id);

        $user->status = 'banned';
        $user->save();

        return Redirect::back()->with('message', _("El usuario con email $user->email ha sido bloqueado."));
    }

    /**
     * Unban a selected user.
     *
     * @return redirect
	 */
    public function unban($id)
    {
        $user = User::findOrFail($id);

        $user->status = '';
        $user->save();

        return Redirect::back()->with('message', _("El usuario con email $user->email ha sido desbloqueado."));
    }

    /**
     * Display a listing of the resource.
     *
     * @param  Request  $request
     * @return Response
     */
    public function index(Request $request, $type = null)
    {
        $user = Auth::user();
        $users = User::
            where(function($q) use ($type) {
                if ($type) {
                    $q->where('role', $type);
                }
            })
            ->orderBy('created_at', 'desc');

        $aux = $users->get();

		$roles = User::getRoles();

        $acredited = $not_acredited = $with_wallet = $total_wallet = $total_automatic_wallet = 0;

        if ($type == 'transferor_company') {
                $companies = User::whereRole('transferor_company')->count();
                $requests = Operation::whereStatus(0)->count();
                $approved_requests = Operation::whereStatus(1)->count();
                $accepted_requests = Operation::whereStatus(3)->count();
                $rejected_requests = Operation::whereStatus(4)->count();
                $cancelled_requests = Operation::whereStatus(2)->count();
                $operations_finished = Operation::whereStatus(5)->get();
                $completed_requests = $operations_finished->count();
                $total_money = 0;
                $automatic_money = 0;
                $failed = Operation::whereStatus(6)->get();
                $failed_operations = $failed->count();
                $failed_money = 0;
                foreach ($failed as $op) {
                        $failed_money += $op->invoice->price;
                }
                foreach ($operations_finished as $op) {
                        $total_money += $op->invoice->price;
                }
                $automatic_transfers = Transfer::whereType(5)->get();
                $auto_ids = [];
                foreach ($automatic_transfers as $automatic_transfer) {
                    $auto_ids[] = $automatic_transfer->operation_id;
                }
                $auto_status5_ids = Operation::whereIn('id', $auto_ids)->whereStatus(5)->lists('id')->toArray();
                foreach ($automatic_transfers as $automatic_transfer) {
                    if (in_array($automatic_transfer->operation_id, $auto_status5_ids)) {
                        $automatic_money += $automatic_transfer->quantity;
                    }
                    // if ($automatic_transfer->operation->status == 5) {
                    //     $automatic_money += $automatic_transfer->quantity;
                    // }
                }
        }

		foreach ($aux as &$user) {
			switch ($user->role) {
				case 'investor':
					$user->path = 'admin.investors.edit';
					$user->role_name = _('Inversor');
                    if ($user->investor->type) {
                        $acredited ++;
                    } else {
                        $not_acredited ++;
                    }
                    if ($user->investor->wallet > 0) {
                        $with_wallet ++;
                        $total_wallet += $user->investor->wallet;
                    }
					break;
				case 'transferor_company':
					$user->path = 'admin.transferor_companies.edit';
					$user->role_name = _('Empresa cedente');
					break;
				case 'debtor_company':
					$user->path = 'admin.debtor_companies.edit';
					$user->role_name = _('Empresta deudora');
					break;
				case 'admin':
				case 'analyst':
				default:
					$user->path = '';
					break;
			}
		}

        $users = $users->paginate(config('admin.pagination'));

        return view('backend.users.index', compact('users', 'roles', 'type', 'acredited', 'not_acredited', 'with_wallet', 'total_wallet', 'total_automatic_wallet', 'requests', 'approved_requests', 'accepted_requests', 'rejected_requests', 'cancelled_requests', 'companies', 'completed_requests', 'total_money', 'automatic_money', 'failed_operations', 'failed_money'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  Request  $request
     * @return Response
     */
    public function create(Request $request)
    {
        // $this->authorize(Auth::user());
        // $countries = Country::lists('name', 'id')->sortByDesc('name')->reverse();
        // $country_states = CountryState::lists('name', 'id')->sortByDesc('name')->reverse();

        // return view('backend.users.create', compact('user', 'countries','country_states'));
		$roles = User::getRoles();
        return view('backend.users.create', compact('user', 'roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
		$this->authorize(Auth::user());

        $exists = User::whereEmail($request->input('email'))->first();
        if ($exists)
        {
            return Redirect::back()->withInput()->withErrors(_('El email de usuario ya existe.'));
        }

        $rules = array(
            'email' => 'required|email',
            'name' => 'required',
            'password' => 'required',
        );

        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator->messages())->withInput();
        }

        $user = new User;
        $user->password = Hash::make($request->input('password'));
        $user->name = $request->input('name');
        $user->family_name = $request->input('family_name');
        $user->email = $request->input('email');
        $user->real_id_type = $request->input('real_id_type');
        $user->real_id_number = $request->input('real_id_number');
        $user->role = $request->input('role');

        try
        {
            $user->save();
        }
        catch (Exception $e)
        {
            return redirect()->route('admin.users.create')->withErrors(_("Ha habido el siguiente error: $e.") );
        }

        // Event::fire(new NewUserCreated($user));
		if ($user->role == 'admin' || $user->role == 'analyst') {
			return redirect()->route('admin.users.index')->with('message', _("El usuario con email $user->email ha sido creado."));
		} else {
			return redirect()->route('admin.users.edit', $user->id)->with('message', _("El usuario con email $user->email ha sido creado."));
		}
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit(Request $request, $id)
    {
        $user = User::findOrFail($id);

		$this->authorize($user);

		$documents = Document::where('foreign_id', $id)->where('model', 'User')->get();

		$roles = User::getRoles();

		switch ($user->role) {
			case 'investor':
				$investor = Investor::where('user_id', $id)->first();
				if (!$investor) {
					Investor::create(array(
						'user_id' => $id
					));
					$investor = Investor::where('user_id', $id)->first();
				}
                $transfer_types = Transfer::getTypes();
                $transfers = Transfer::where('active', 1)->where('investor_id', $investor->id)->orderBy('created_at', 'asc')->orderBy('type', 'asc')->get();
                $aux = [];
                $type = null;
                $quantity = 0;
                $subtransfers = [];
                $old_transfer = null;
                foreach ($transfers as $transfer) {
                    if ($type !== $transfer->type) {
                        if ($quantity) {
                            $old_transfer->original_quantity = $old_transfer->quantity;
                            $old_transfer->quantity = $quantity;
                            $old_transfer->subtransfers = $subtransfers;
                            $param = $type . '-' . ($old_transfer->operation_id ? $old_transfer->operation_id : mt_rand(100, 999999999));
                            $aux[$param] = $old_transfer;
                            $quantity = 0;
                            $subtransfers = [];
                        }
                        $type = $transfer->type;
                    }
                    $quantity += $transfer->quantity;
                    $subtransfers[] = $transfer;
                    $old_transfer = $transfer;
                }

                if ($old_transfer) {
                    $old_transfer->original_quantity = $old_transfer->quantity;
                    $old_transfer->quantity = $quantity;
                    $old_transfer->subtransfers = $subtransfers;
                    $param = $type . '-' . ($old_transfer->operation_id ? $old_transfer->operation_id : mt_rand(100, 999999999));
                    $aux[$param] = $old_transfer;
                    $transfers = $aux;
                }

                $transfer_ids = Transfer::where('investor_id', $investor->id)
                    ->whereIn('type', [2, 3, 4, 5])->orderBy('created_at', 'asc')->lists('operation_id', 'id');
                $operations = Operation::whereIn('id', $transfer_ids)->get();
                list($totals, $donutgraphs, $operations) = $investor->getTotals($operations);
                $status = Operation::getStatus();
                $rejected_operations = RejectedInvoice::whereInvestorId($investor->id)->with('invoice.operations')->get();
				return view('backend.users.edit', compact('user', 'roles', 'documents', 'investor', 'transfers', 'transfer_types', 'totals', 'donutgraphs', 'operations', 'status', 'rejected_operations'));

				break;
			case 'debtor_company':
				$debtor_company = DebtorCompany::where('user_id', $id)->first();
				if (!$debtor_company) {
					DebtorCompany::create(array(
						'user_id' => $id
					));
					$debtor_company = DebtorCompany::where('user_id', $id)->first();
				}
                $status = Operation::getStatus();
                $invoices = $debtor_company->invoices;
                $operations = 0;
                $failed_operations = 0;
                $success_operations = 0;
                foreach ($invoices as $invoice) {
                    if (!empty($invoice->operations[0])) {
                        $operations ++;
                        if (in_array($invoice->operations[0]->status, [2, 4, 6])) {
                            $failed_operations ++;
                        } elseif (in_array($invoice->operations[0]->status, [5])) {
                            $success_operations ++;
                        }
                    }
                }
				return view('backend.users.edit', compact('user', 'roles', 'documents', 'debtor_company', 'status', 'operations', 'failed_operations', 'success_operations'));
				break;
			case 'transferor_company':
				$transferor_company = TransferorCompany::where('user_id', $id)->first();
				if (!$transferor_company) {
					TransferorCompany::create(array(
						'user_id' => $id
					));
					$transferor_company = TransferorCompany::where('user_id', $id)->first();
				}
                $status = Operation::getStatus();
                $invoices = $transferor_company->invoices;
                $operations = 0;
                $failed_operations = 0;
                $success_operations = 0;
                foreach ($invoices as $invoice) {
                    if (!empty($invoice->operations[0])) {
                        $operations ++;
                        if (in_array($invoice->operations[0]->status, [2, 4, 6])) {
                            $failed_operations ++;
                        } elseif (in_array($invoice->operations[0]->status, [5])) {
                            $success_operations ++;
                        }
                    }
                }
				return view('backend.users.edit', compact('user', 'roles', 'documents', 'transferor_company', 'status', 'operations', 'failed_operations', 'success_operations'));
				break;
			default:
				return view('backend.users.edit', compact('user', 'roles'));
				break;
		}
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);

		$this->authorize($user);

		$data = $request->all();
		if (!empty($data['password'])) {
			$data['password'] = bcrypt($data['password']);
		} else {
			unset($data['password']);
		}

        if (!isset($data['dni_verified'])) {
            $data['dni_verified'] = 0;
        }

        $user->update($data);

        return Redirect::back()->with('message', _("Se han actualizado los datos del usuario correctamente"));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
		$this->authorize($user);

		if ($user->role != 'analyst' && $user->role != 'admin') {
			$aux = $user->{$user->role};
			$aux->delete();
		}

        $user_email = $user->email;
        $user->delete();

        return redirect()->route('admin.users.index')->with('message', _("El usuario con email $user_email ha sido eliminado."));
    }
}
