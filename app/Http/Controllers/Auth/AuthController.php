<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\Request;
use App\Models\User;
use App\Models\Investor;
use App\Models\TransferorCompany;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

use Socialite;

use Log;

use Event;
// use App\Events\NewUserCreated;
use Auth;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    protected $redirectPath = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            //'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
		if (empty($data['id']) && $user = User::whereEmail($data['email'])->first()) {
			$url = $data['role'] == 'transferor_company' ? '/registro-empresa' : '/registro-inversor';
			return redirect()->route($url)->withErrors(_('El email ya está en uso'));
		}

        $name = empty($data['name']) ? null : $data['name'];

        $user = User::create([
            'name' => $name,
            'family_name' => $data['family_name'],
            'role' => $data['role'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);

		if ($data['role'] == 'investor') {
			$investor = new Investor;
			$investor->create([
				'user_id' => $user->id,
				'name' => $user->name
			]);
		} elseif ($data['role'] == 'transferor_company') {
			$transferor_company = new TransferorCompany;
			$transferor_company->create([
				'user_id' => $user->id,
				'name' => $user->name
			]);
		}

        // Event::fire(new NewUserCreated($user));
        return $user;
    }

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    public function redirectToGithubProvider()
    {
        return Socialite::driver('github')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return Response
     */
    public function handleGithubProviderCallback()
    {
        $user = Socialite::driver('github')->user();
        return $this->handle3rdPartyCallback($user, 'github');
    }

    /**
     * Redirect the user to the Google authentication page.
     *
     * @return Response
     */
    public function redirectToGoogleProvider()
    {
        return Socialite::driver('google')->redirect();
    }

    /**
     * Obtain the user information from Google.
     *
     * @return Response
     */
    public function handleGoogleProviderCallback()
    {
        $user = Socialite::driver('google')->user();
        /*try {
            $user = Socialite::driver('google')->user();
        }
        catch (GuzzleHttp\Exception\ClientException $e) {
             dd($e->response);
        }*/
        return $this->handle3rdPartyCallback($user, 'google');
    }

    /**
     * Redirect the user to the Facebook authentication page.
     *
     * @return Response
     */
    public function redirectToFacebookProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Obtain the user information from Facebook.
     *
     * @return Response
     */
    public function handleFacebookProviderCallback()
    {
        $user = Socialite::driver('facebook')->user();
        return $this->handle3rdPartyCallback($user, 'facebook');
    }

    /**
     * Redirect the user to the Twitter authentication page.
     *
     * @return Response
     */
    public function redirectToTwitterProvider()
    {
        return Socialite::driver('twitter')->redirect();
    }

    /**
     * Obtain the user information from Twitter.
     *
     * @return Response
     */
    public function handleTwitterProviderCallback()
    {
        $user = Socialite::driver('twitter')->user();
        return $this->handle3rdPartyCallback($user, 'twitter');
    }

    /**
     * Obtain the user information from 3rd party and start login/signup procedure.
     *
     * @return Response
     */
    private function handle3rdPartyCallback($user, $provider = false)
    {
        $name = $user->getName();
        $email = $user->getEmail();

        Log::debug('AuthController handle3rdPartyCallback: '
            . json_encode($name)
            . ' '
            . json_encode($email)
            . ' '
            . json_encode($provider)
            );

        if(!$name) $name = $email;
        if($email) self::loginOrSignup($email, $name);

        return redirect()->intended($this->redirectPath());
    }

    /**
     * Log in user from 3rd party provider, create user if necessary.
     *
     */
    static private function loginOrSignup($email, $name = null)
    {
        try {
            $user = User::whereEmail($email)->first();
        } catch (Exception $e) {
            $user = false;
        }

        if(!$user)
        {
            $user = new User;
            $user->email = $email;
            $user->name = $name;
            $user->save();
            // Event::fire(new NewUserCreated($user));
        }

        \Auth::login($user);
    }

    /**
     * Redirect $user when authenticated based in role.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function authenticated()
    {
		if (Auth::user()->status == 'banned') {
			Auth::logout();
			return redirect('/');
		}
        if (in_array(\Auth::user()->role, ['admin', 'analyst'])) {
            return redirect('/admin');
        }
        return redirect('/area-privada');
    }
}
