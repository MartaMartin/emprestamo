<?php

namespace App\Http\Controllers;
use App\Models\News;
use App\Models\NewsCategory;

class NewsController extends Controller
{

	public function index() {
		$news = News::get();
		$categories = NewsCategory::get();
		return view('frontend.news.index', compact('news', 'categories'));
	}

    public function show($id) {
        $news = News::whereSlug($id)->first();

        if (!$news) {
            $news = News::find($id);
        }

        return view('frontend.news.show', compact('news'));
    }

}
