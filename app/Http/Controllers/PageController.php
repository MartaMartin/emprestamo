<?php

namespace App\Http\Controllers;
use App\Models\Page;
use App\Models\SuccessCase;

class PageController extends Controller
{

	public function index() {
		$success_cases = SuccessCase::get();
		$num = $success_cases->count();
		$pages = ceil($num / 3);
		return view('frontend.pages.index', compact('success_cases', 'num', 'pages'));
	}

    public function success_cases($id = null) {
        if ($id) {
            $success_cases = SuccessCase::where('id', $id)->get();
        } else {
            $success_cases = SuccessCase::get();
        }
		return view('frontend.pages.success_cases', compact('success_cases', 'id'));
    }

}
