<?php

namespace App\Http\Controllers\PrivateArea;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Investor;
use App\Models\Notification;
use App\Models\RejectedInvoice;
use App\Models\Invoice;
use App\Models\Operation;
use App\Models\Transfer;
use App\Models\User;
use App\Models\Document;
use DB;
use Auth;
use Validator;
use Input;
use Redirect;

class InvestorController extends Controller
{

    /**
     * Display user's profile
     *
     * @return Response
     */
    public function profile(Request $request)
    {
        $user = Auth::user();
		$investor = Investor::where('user_id', $user->id)->first();
		$document = Document::where('foreign_id', $user->id)->where('model', 'User')->where('name', 'DNI')->first();
        return view('private.investors.profile', compact('user', 'investor', 'document'));
    }

    /**
     * Save user's profile
     *
     * @return Response
     */
    public function update(Request $request)
    {
        $user = Auth::user();
		$investor = Investor::where('user_id', $user->id)->first();
		$data = $request->all();

		$investor_data = [];
		foreach ($data as $key => $value) {
			if (strpos($key, 'investor_') === 0) {
				$investor_data[str_replace('investor_', '', $key)] = $value;
				unset($data[$key]);
			}
		}

		if (!empty($data['password'])) {
			$data['password'] = bcrypt($data['password']);
		}

		if (!empty($data['filename'])) {
			$document = Document::where('foreign_id', $user->id)
				->where('name', 'DNI')
				->where('model', 'User')->first();

			if (!$document) {
				$document = new Document;
			}
			$document->filename = $data['filename'];
			$document->foreign_id = $user->id;
			$document->model = 'User';
			$document->name = 'DNI';
			$document->save();
			unset($data['filename']);
		}

		if (!$data['password']) {
			unset($data['password']);
		}

		if (empty($investor_data['calification'])) {
			$investor_data['calification'] = 0;
		}
		if (empty($investor_data['fee'])) {
			$investor_data['fee'] = 1;
		}
		if (empty($investor_data['max_days'])) {
			$investor_data['max_days'] = 365;
		}

		$user->update($data);
		$investor->update($investor_data);
        return Redirect::back()->with('message', _("Se han actualizado los datos del usuario correctamente"));
    }

    /**
     * Show Wallet
     *
     * @param  Request  $request
     * @return Response
     */
    public function wallet(Request $request, $substract = false)
    {
	    $user = Auth::user();
		$transfer_types = Transfer::getTypes();
		$investor = Investor::where('user_id', $user->id)->first();
        $transfers = Transfer::where('active', 1)->where('investor_id', $investor->id)->orderBy('created_at', 'asc')->orderBy('type', 'asc')->limit(100)->get();
        $aux = [];
        $type = null;
        $quantity = 0;
        $subtransfers = [];
        $old_transfer = null;
        foreach ($transfers as $transfer) {
            $transfer->original_quantity = $transfer->quantity;
            if ($type !== $transfer->type) {
                if ($quantity) {
                    $old_transfer->original_quantity = $old_transfer->quantity;
                    $old_transfer->quantity = $quantity;
                    $old_transfer->subtransfers = $subtransfers;
                    $param = $type . '-' . ($old_transfer->operation_id ? $old_transfer->operation_id : mt_rand(100, 999999999));
                    $aux[$param] = $old_transfer;
                    $quantity = 0;
                    $subtransfers = [];
                }
                $type = $transfer->type;
            }
            $quantity += $transfer->quantity;
            $subtransfers[] = $transfer;
            $old_transfer = $transfer;
        }
        $old_transfer->original_quantity = $old_transfer->quantity;
        $old_transfer->quantity = $quantity;
        $old_transfer->subtransfers = $subtransfers;
        $param = $type . '-' . ($old_transfer->operation_id ? $old_transfer->operation_id : mt_rand(100, 999999999));
        $aux[$param] = $old_transfer;

        $transfers = $aux;
		// $graphs = array_reverse($transfers->toArray());
		// $graphs = array_reverse($transfers);
        $graphs = $transfers;
		$operation_ids = Transfer::where('investor_id', $investor->id)->
			whereIn('type', [2, 3, 4, 5])->orderBy('created_at', 'asc')->lists('operation_id', 'id');
		$aux_operations = Operation::whereIn('id', $operation_ids)->get();
		$operations = [];
		foreach ($aux_operations as $operation) {
			$invoice = Invoice::where('id', $operation->invoice_id)->first();
			$operations[$operation->id] = $invoice->price;
		}
		$amount = Investor::updateWallet($investor->id);

        $notifications = Notification::where('user_id', $user->id)->where('read', 0)->where('url', 'private.investors.wallet')->get();
        foreach ($notifications as $notification) {
            $notification->update(['read' => 1]);
        }

        $future_transfers = Transfer::where('active', 0)->whereIn('type', [3, 4])->where('investor_id', $investor->id)->orderBy('created_at', 'asc')->orderBy('type', 'asc')->get();
        $aux = [];
        $type = null;
        $quantity = 0;
        $old_transfer = null;
        foreach ($future_transfers as $transfer) {
            if ($type !== $transfer->type) {
                if ($quantity) {
                    $old_transfer->quantity = $quantity;
                    $param = $type . '-' . ($old_transfer->operation_id ? $old_transfer->operation_id : mt_rand(100, 999999999));
                    $old_transfer->created_at = $old_transfer->operation->invoice->expires;
                    $aux[$param] = $old_transfer;
                    $quantity = 0;
                }
                $type = $transfer->type;
            }
            $quantity += $transfer->quantity;
            $old_transfer = $transfer;
        }
        $old_transfer->created_at = $old_transfer->operation->invoice->expires;
        $old_transfer->quantity = $quantity;
        $param = $type . '-' . ($old_transfer->operation_id ? $old_transfer->operation_id : mt_rand(100, 999999999));
        $aux[$param] = $old_transfer;
        $graphs += $aux;

        return view('private.investors.wallet', compact('user', 'investor', 'transfers', 'amount', 'transfer_types', 'operations', 'graphs', 'substract'));
    }

    /**
     * Show Proposals
     *
     * @param  Request  $request
     * @return Response
     */
    public function proposals(Request $request)
    {
	    $user = Auth::user();
		$investor = Investor::where('user_id', $user->id)->first();
		$rejected = RejectedInvoice::where('investor_id', $investor->id)->get();
		$rejected_ids = [];
		foreach ($rejected as $r) {
			$rejected_ids[] = $r->invoice_id;
		}

        $operations = Operation::whereNotIn('invoice_id', $rejected_ids)->where('status', 3)->orderBy('created_at', 'desc')->get();

		$transfer_types = Transfer::getTypes();

		foreach ($operations as $operation) {
			$sum = 0;
			foreach ($operation->transfers as $transfer) {
				if ($transfer->investor_id == $investor->id) {
                    if ($transfer->type == 5 || $transfer->type == 2) {
                        $sum += $transfer->quantity;
                    }
				}
			}
			$operation->invested = $sum;
		}

		$operations_rejected = Operation::whereIn('invoice_id', $rejected_ids)->orderBy('created_at', 'desc')->get();

        return view('private.investors.proposals', compact('user', 'operations', 'rejected', 'rejected_ids', 'transfer_types', 'investor', 'operations_rejected'));
    }

    /**
     * Show Historical
     *
     * @param  Request  $request
     * @return Response
     */
    public function historical(Request $request)
    {
	    $user = Auth::user();
		$transfer_types = Transfer::getTypes();
		$investor = Investor::where('user_id', $user->id)->first();
		$data = $request->all();
		if (!empty($data['ids'])) {
			$operations = Operation::whereIn('id', $data['ids']);
		} else {
            $transfer_ids = Transfer::where('investor_id', $investor->id)
                ->whereIn('type', [2, 3, 4, 5])->orderBy('created_at', 'asc')->lists('operation_id', 'id');
			$operations = Operation::whereIn('id', $transfer_ids);
		}

        $status_selected = null;
        if (!empty($data['status'])) {
            $operations = $operations->where('status', $data['status']);
            $status_selected = $data['status'];
        }

        $operations = $operations->get();
        list($totals, $donutgraphs, $operations) = $investor->getTotals($operations);
        $status = Operation::getStatus();
        unset($status[0]);
        unset($status[1]);
        unset($status[2]);
        unset($status[4]);
        return view('private.investors.historical', compact('user', 'operations', 'totals', 'donutgraphs', 'status', 'status_selected'));
    }

}
