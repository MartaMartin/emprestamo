<?php

namespace App\Http\Controllers\PrivateArea;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Invoice;
use App\Models\Operation;
use App\Models\Transfer;
use App\Models\User;
use App\Models\Document;
use App\Models\InvoiceUser;
use App\Models\Notification;
use DB;
use Auth;
use Validator;
use Input;
use Redirect;

class InvoiceController extends Controller
{

    /**
     * Store data
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
		$data = $request->all();
		$files = $data['filenames'];
		$names = $data['names'];

		unset($data['filenames']);
		unset($data['names']);

		$data['debtor_company_id'] = null;

		$invoice = Invoice::create($data);

		$analyst = User::where('role', 'analyst')->orderByRaw('RAND()')->first();
		$invoices_users = new InvoiceUser;
		$invoices_users->create([
			'user_id' => $analyst->id,
			'invoice_id' => $invoice->id
		]);

		for ($i = 0; $i < count($files); $i ++) {
			if (!$files[$i]) continue;
			$mime_type = $files[$i]->getMimeType();
			$mime_type = explode('/', $mime_type);
			$document = new Document;
			$document->filename = $files[$i];
			$document->type = $mime_type[0] == 'image' ? 'image' : 'document';
			$document->name = $names[$i];
			$document->model = 'Invoice';
			$document->foreign_id = $invoice->id;
			$document->save();
		}

		Notification::sendNotification(['admin', 'analyst'], _('Hay una nueva factura pendiente de aprobación'), 'admin.invoices.edit', json_encode([$invoice->id]));

        return Redirect::back()->with('message', _("Se ha enviado el presupuesto"));
    }

    /**
     * View data
     *
     * @return Response
     */
    public function view(Request $request, $id)
    {
        $user = Auth::user();
		$invoice = Invoice::find($id);
		$transfer_types = Transfer::getTypes();
		$operation_status = Operation::getStatus();

		if (count($invoice->operations) > 0) {
            $total = $invoice->operations[0]->getCurrentInvested();
			$invoice->invested = $total;
		}

        $notification = Notification::where('user_id', $user->id)->where('read', 0)->where('url', 'private.invoices.view')->where('params', '["' . $id . '"]')->first();
        if ($notification) {
            $notification->update(['read' => 1]);
        }

        $own_notifications = Notification::where('user_id', $user->id)
            ->orderBy('read', 'asc')
            ->orderBy('created_at', 'desc')
            ->where('params', '["' . $invoice->id . '"]')
            ->groupBy('message')
            ->get();

        return view('private.invoices.view', compact('user', 'invoice', 'transfer_types', 'operation_status', 'own_notifications'));
    }

}
