<?php

namespace App\Http\Controllers\PrivateArea;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Notification;
use DB;
use Auth;
use Validator;
use Input;
use Redirect;

class NotificationController extends Controller
{

	/**
	 * Show notifications
	 * @param Request $request
	 * @return Response
	 */
	public function index(Request $request) {
		$user = Auth::user();
		$notifications = Notification::where('user_id', $user->id)->orderBy('created_at', 'desc')->get();
		return view('private.notifications.index', compact('user', 'notifications'));
	}

}
