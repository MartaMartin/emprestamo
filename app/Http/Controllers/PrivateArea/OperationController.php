<?php

namespace App\Http\Controllers\PrivateArea;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Operation;
use App\Models\Investor;
use App\Models\RejectedInvoice;
use App\Models\Transfer;
use App\Models\TransferorCompany;
use App\Models\Notification;
use App\Models\Setting;
use DB;
use Auth;
use Validator;
use Input;
use Redirect;
use Carbon\Carbon;

class OperationController extends Controller
{

    /**
     * Accept proposal
     * @return back
     */
    public function acceptProposal(Request $request, $id)
    {
        $user = Auth::user();
        $operation = Operation::find($id);
        $this->authorize($operation);

        $operation->update(['status' => 3, 'accepted_date' => Carbon::now()]);

        list($operation, $investors) = Investor::automaticInvest($operation);
        while ($operation->status == 3 && count($investors) > 0) {
            list($operation, $investors) = Investor::automaticInvest($operation);
        }

        Notification::sendNotification(['admin', 'analyst'], _('El promotor ha aceptado la propuesta'), 'admin.operations.edit', json_encode([$operation->id]));

        return Redirect::back()->with('message', _("Se ha aceptado la operación"));
    }

    /**
     * Reject proposal
     * @return back
     */
    public function rejectProposal(Request $request, $id)
    {
        $user = Auth::user();
        $operation = Operation::find($id);
        $this->authorize($operation);

        if ($request->all()) {
            $operation->update(['status' => 4, 'reject_explain' => $request->get('reject_explain')]);
            return Redirect::route('private.transferor_companies.invoices')->with('message', _("Se ha rechazado la operación"));
        }

        return view('private.transferor_companies.reject', compact('id', 'user'));
    }

    /**
     * Invest in an operation
     */
    public function invest(Request $request, $id)
    {
        if ($request->all()) {

            $user = Auth::user();
            $investor = Investor::where('user_id', $user->id)->first();
            $operation = Operation::find($id);
            $this->authorize($operation);

            $settings = Setting::lists('value', 'key');

            if ($operation->status != 3) { // Ronda abierta
                return Redirect::back()->withErrors(_('No puedes invertir en esta operación'));
            }

            if ($operation->hasInvested($user)) {
                // return Redirect::back()->withErrors(_('Ya has invertido en esta operación'));
            }

            list($quantity, $completed) = Operation::getTicket($operation, $request->get('amount'));

            if ($quantity < $request->get('amount')) {
                return Redirect::back()->withErrors(sprintf(_('La inversión tiene que ser como máximo de %s€'), $quantity));
            }
            if ($quantity > $request->get('amount')) {
                return Redirect::back()->withErrors(sprintf(_('La inversión tiene que ser como mínimo de %s€'), $quantity));
            }
            if ($user->investor->type == 0) {
                if ($settings['max_invest_not_accredited'] < $request->get('amount')) {
                    return Redirect::back()->withErrors(sprintf(_('Tu límite de inversión es como máximo de %s€'), $settings['max_invest_not_accredited']));
                }
                if ($settings['max_invest_year'] < $request->get('amount') + $user->totalInvested()) {
                    return Redirect::back()->withErrors(sprintf(_('Tu límite de inversión anual es como máximo de %s€, y con esta inversión lo superas. Te quedan %s'), $settings['max_invest_year'], $settings['max_invest_year'] - $user->totalInvested()));
                }
            }

            $transferor_company = TransferorCompany::whereId($operation->invoice->transferor_company_id)->first();

            if ($quantity && $investor->wallet >= $quantity) {

                $transfer = new Transfer;
                $transfer->create([
                    'operation_id' => $id,
                    'investor_id' => $investor->id,
                    'investor_type' => $investor->type,
                    'quantity' => $quantity,
                    'active' => 1,
                    'type' => 2,
                    'validate' => 1
                ]);

                // Si estaba rechazado, borrarlo de rechazados
                if ($rejected = RejectedInvoice::where('invoice_id', $operation->invoice_id)->first()) {
                    $rejected->delete();
                }

                Notification::sendNotification($transferor_company->user_id, _('Se ha invertido de manera manual en una operación'), 'private.invoices.view', json_encode([$operation->invoice_id]));
                Notification::sendNotification(['admin', 'analyst'], _('Han invertido en una operación'), 'admin.operations.show', json_encode([$operation->id]));

            } else {
                return Redirect::back()->withErrors(_('No tienes suficiente dinero en tu wallet para realizar esta operación. Puedes ingresar más en la sección Wallet'));
            }

            if ($completed) {
                Operation::operationCompleted($operation);
                Notification::sendNotification($transferor_company->user_id, _('Ya se ha invertido el total del presupuesto'), 'private.invoices.view', json_encode([$operation->invoice_id]));
            }
        }

        // return Redirect::back()->with('message', _("Se han actualizado los datos de la operación correctamente"));
        return Redirect::route('private.investors.historical')->with('message', _("Se han actualizado los datos de la operación correctamente"));
    }

    /*
     * Reject an operation
     */
    public function reject(Request $request, $id)
    {
        $user = Auth::user();
        $investor = Investor::where('user_id', $user->id)->first();
        $operation = Operation::where('invoice_id', $id)->first();
        $this->authorize($operation);
        $rejected = new RejectedInvoice;
        $rejected->create([
            'invoice_id' => $id,
            'investor_id' => $investor->id,
            'status' => 0 // TODO no tengo muy claro para qué sirve
        ]);
        return Redirect::back()->with('message', _("Presupuesto rechazado"));
    }

}
