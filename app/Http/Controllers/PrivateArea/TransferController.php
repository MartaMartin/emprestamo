<?php

namespace App\Http\Controllers\PrivateArea;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Transfer;
use App\Models\Investor;
use App\Models\User;
use App\Models\Notification;
use DB;
use Auth;
use Validator;
use Input;
use Redirect;
use Functions;

class TransferController extends Controller
{

    /**
     * Save transfer
     *
     * @return Response
     */
    public function store(Request $request, $substraact = false)
    {
        $user = Auth::user();
		$investor = Investor::where('user_id', $user->id)->first();
		$data = $request->all();

        if ($substraact) {
            $data['type'] = 1;
            if ($data['quantity'] <= $investor->wallet) {
                Notification::sendNotification(['admin'], _('Se ha realizado una solicitud de retirada de dinero'), 'admin.transfers.index');
            } else {
                return Redirect::back()->withErrors(_('No tienes suficiente dinero para retirar esa cantidad'));
            }
        } else {
            $data['type'] = 0;
            Notification::sendNotification(['admin'], _('Se ha realizado un ingreso'), 'admin.transfers.index');
        }

		if (($data['type'] == 2 || $data['type'] == 3) && empty($data['operation_id'])) {
			return Redirect::back()->with('message', _("Falta Operación"));
		}

		$transfer = new Transfer;
		$transfer->create([
			'operation_id' => !empty($data['operation_id']) ? $data['operation_id'] : null,
			'investor_id' => $investor->id,
			'type' => $data['type'],
			'investor_type' => $investor->type,
			'quantity' => abs($data['quantity']),
            'ref' => Functions::generateRandomString(6),
			'validate' => 1
		]);

        return Redirect::back()->with('message', _('Se han actualizado tu wallet correctamente'));
    }

}
