<?php

namespace App\Http\Controllers\PrivateArea;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\TransferorCompany;
use App\Models\DebtorCompany;
use App\Models\Transfer;
use App\Models\Invoice;
use App\Models\User;
use App\Models\Document;
use App\Models\Notification;
use App\Models\Operation;
use DB;
use Auth;
use Validator;
use Input;
use Redirect;

class TransferorCompanyController extends Controller
{

    /**
     * Display user's profile 
     *
     * @return Response
     */
    public function profile(Request $request)
    {
        $user = Auth::user();
		$transferor_company = TransferorCompany::where('user_id', $user->id)->first();
		$document = Document::where('foreign_id', $user->id)->where('model', 'User')->where('name', 'DNI')->first();
        return view('private.transferor_companies.profile', compact('user', 'transferor_company', 'document'));
    }

    /**
     * Save user's profile
     *
     * @return Response
     */
    public function update(Request $request)
    {
        $user = Auth::user();
		$transferor_company = TransferorCompany::where('user_id', $user->id)->first();
		$data = $request->all();
		$transferor_company_data = array();
		foreach ($data as $key => $value) {
			if (strpos($key, 'transferor_company_') === 0) {
				$transferor_company_data[str_replace('transferor_company_', '', $key)] = $value;
				unset($data[$key]);
			}
		}

		if (!empty($data['password'])) {
			$data['password'] = bcrypt($data['password']);
		}

		if (!empty($data['filename'])) {
			$document = Document::where('foreign_id', $user->id)
				->where('name', 'DNI')
				->where('model', 'User')->first();
			if (!$document) {
				$document = new Document;
			}
			$document->filename = $data['filename'];
			$document->foreign_id = $user->id;
			$document->model = 'User';
			$document->name = 'DNI';
			$document->save();
			unset($data['filename']);
		}

		$user->update($data);
		$transferor_company->update($transferor_company_data);
        return Redirect::back()->with('message', _("Se han actualizado los datos del usuario correctamente"));
    }

    /**
     * Show Invoices
     *
     * @param  Request  $request
     * @return Response
     */
    public function invoices(Request $request)
    {
	    $user = Auth::user();
		$transferor_company = TransferorCompany::where('user_id', $user->id)->first();
		$invoices = Invoice::where('transferor_company_id', $transferor_company->id)->orderBy('created_at', 'desc')->get();
		$debtor_companies = DebtorCompany::lists('name', 'id');

		$status = Operation::getStatus();

        return view('private.transferor_companies.invoices', compact('user', 'transferor_company', 'invoices', 'debtor_companies', 'status'));
    }

}
