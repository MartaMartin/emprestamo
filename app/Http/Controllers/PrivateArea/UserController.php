<?php

namespace App\Http\Controllers\PrivateArea;

use Illuminate\Http\Request;
//use Illuminate\Routing\Controller;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\User;
//use Input;
use Validator;
use Redirect;
use Auth;
use Gate;

use Log;

use Event;
use App\Events\NewUserCreated;

use LaravelGettext;

use DB;

class UserController extends Controller
{

    /**
     * Display user's home
     *
     * @return Response
     */
	public function index(Request $request) {
        $user = Auth::user();
        return view('private.users.index', compact('user'));
	}

    /**
     * User profile edit
     *
     * @return Response
     */
    public function profileEdit()
    {
        $user = Auth::user();
        return view('frontend.users.profile-edit')
            ->withUser($user);
    }


    public function updateProfile(Request $request)
    {
        $user = $request->user();
        $input = $request->all();

        $user->update($input);

        return redirect()->back();
    }

}
