<?php

// TODO no se usa para nada

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Http\Request;

use App\Models\Investor;
use App\Models\TransferorCompany;

use Auth;

class UserController extends Controller
{

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function register(Request $request)
    {
		// TODO no se usa
		return;
		$data = $request->all();

		$name = empty($data['name']) ? null : $data['name'];

		$user = User::create([
			'name' => $name,
			'family_name' => $data['family_name'],
			'role' => $data['role'],
			'email' => $data['email'],
			'password' => bcrypt($data['password']),
		]);

		if ($data['role'] == 'investor') {
			$investor = new Investor;
			$investor->create([
				'user_id' => $user->id,
				'name' => $user->name
			]);
		} elseif ($data['role'] == 'transferor_company') {
			$transferor_company = new TransferorCompany;
			$transferor_company->create([
				'user_id' => $user->id,
				'name' => $user->name
			]);
		}

		Auth::login($user);

		return redirect('/');

	}

}
