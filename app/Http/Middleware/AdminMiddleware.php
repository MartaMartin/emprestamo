<?php

namespace App\Http\Middleware;

use Closure;
use App\User;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!\Auth::user() || !in_array(\Auth::user()->role, ['admin', 'analyst'])) {
            if ($request->ajax()) {
                return abort(401, _('Acceso restringido'));
            } else {
                return redirect()->guest('auth/login');
            }
        }
        return $next($request);
    }
}
