<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/generate/models', '\\Jimbolino\\Laravel\\ModelBuilder\\ModelGenerator5@start');

//-- Frontend Routes --//
Route::get('/', [
    'as' => 'index',
	'uses' => 'PageController@index'
]);
Route::get('/blog', [
    'as' => 'blog',
	'uses' => 'NewsController@index'
]);
Route::get('/blog/{new}', [
    'as' => 'news.show',
	'uses' => 'NewsController@show'
]);

	/*************************/
	/*	PACO */
	/*************************/

	//COMO FUNCIONA
	Route::get('/como-funciona', function(){
		return view('frontend.pages.como');
	});

	//EMRPESA
	Route::get('/empresas', function(){
		return view('frontend.pages.empresas');
	});

	//INVERSORES
	Route::get('/inversores', function(){
		return view('frontend.pages.inversores');
	});

	//REGISTRO INVERSORES
	Route::get('/registro-inversor', function() {
		return view('auth.registro-inversor');
	});
	Route::post('/registro-inversor', [
		'as' => 'register_investor',
		'uses' => 'UserController@registerInvestor'
	]);

	//REGISTRO EMPRESAS
	Route::get('/registro-empresa', function() {
		return view('auth.registro-empresa');
	});
	Route::post('/registro-empresa', [
		'as' => 'register_company',
		'uses' => 'UserController@registerComapny'
	]);

	//CASOS EXITO INVERSORES
	Route::get('/casos-de-exito-inversores', function(){
		return view('frontend.pages.casos-exito-inversores');
	});

	//CASOS EXITO EMPRESASA
	Route::get('/casos-de-exito-empresa', function(){
		return view('frontend.pages.casos-exito-empresa');
	});

	//BLOG DETALLE (temporal para maquetarlo)
	Route::get('/blog/view', function(){
		return view('frontend.news.view');
	});

	//CASOS EXITO
	Route::get('/casos-de-exito', [
			'as' => 'success_cases.index',
			'uses' => 'PageController@success_cases'
	]);
	Route::get('/caso-de-exito/{case}', [
			'as' => 'success_cases.show',
			'uses' => 'PageController@success_cases'
	]);

	/*************************/
	/*	END PACO */
	/*************************/

///-- Profile Routes --///
Route::group(['middleware' => 'auth'], function () {
	Route::get('/perfil/editar', [
		'as' => 'profileEdit',
		'uses' => 'UserController@profileEdit'
	]);

	Route::get('perfil', [
		'as' => 'profile',
		'uses' => 'UserController@profile'
	]);

	Route::post('perfil/editar', [
		'as' => 'profile.update',
		'uses' => 'UserController@updateProfile'
	]);
});
///-- /Profile Routes --///
//-- /Frontend Routes --//

//-- Auth Routes --//
Route::get('auth/login', [
	'as' => 'auth.login',
	'uses' => 'Auth\AuthController@getLogin'
]);
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', [
	'as' => 'auth.logout',
	'uses' => 'Auth\AuthController@getLogout'
]);

Route::get('auth/register', [
    'as' => 'auth.register',
	'uses' => 'Auth\AuthController@getRegister'
]);

// Route::post('auth/register', 'Auth\AuthController@postRegister');
Route::post('auth/registro', [
	'as' => 'auth.register.postRegister',
	'uses' => 'Auth\AuthController@postRegister'
	// 'uses' => 'UserController@register'
]);

Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');
//-- /Auth Routes --//

Route::group([
	'prefix' => 'area-privada',
	'namespace' => 'PrivateArea',
	'middleware' => ['auth']
], function () {

	Route::get('/', [
		'as' => 'private.users.index',
		'uses' => 'UserController@index'
	]);

	//- Inversor - //
	Route::get('inversor', [
		'as' => 'private.investors.profile',
		'uses' => 'InvestorController@profile'
	]);
	Route::put('inversor', [
		'as' => 'private.investors.update',
		'uses' => 'InvestorController@update'
	]);
	Route::get('wallet/{substract?}', [
		'as' => 'private.investors.wallet',
		'uses' => 'InvestorController@wallet'
	]);
	Route::get('propuestas-de-inversion', [
		'as' => 'private.investors.proposals',
		'uses' => 'InvestorController@proposals'
	]);
	Route::get('historico', [
		'as' => 'private.investors.historical',
		'uses' => 'InvestorController@historical'
	]);
	Route::post('historico', [
		'as' => 'private.investors.historical',
		'uses' => 'InvestorController@historical'
	]);
	//- /Inversor - //

	//- Transfers - //
	Route::put('transfer/{substract?}', [
		'as' => 'private.transfers.store',
		'uses' => 'TransferController@store'
	]);
	//- /Trasnfers - //

	//- Notification - //
	Route::get('notificaciones', [
		'as' => 'private.notifications.index',
		'uses' => 'NotificationController@index'
	]);
	//- /Notification - //

	//- Inversor - //
	Route::get('empresa', [
		'as' => 'private.transferor_companies.profile',
		'uses' => 'TransferorCompanyController@profile'
	]);
	Route::put('empresa', [
		'as' => 'private.transferor_companies.update',
		'uses' => 'TransferorCompanyController@update'
	]);
	Route::get('presupuestos', [
		'as' => 'private.transferor_companies.invoices',
		'uses' => 'TransferorCompanyController@invoices'
	]);
	//- /Inversor - //

	//- Invoices - //
	Route::put('presupuestos', [
		'as' => 'private.invoices.store',
		'uses' => 'InvoiceController@store'
	]);
	Route::get('presupuesto/{invoice}', [
		'as' => 'private.invoices.view',
		'uses' => 'InvoiceController@view'
	]);
	//- /Invoices - //

	//- Operations - //
	Route::get('operacion/aceptar-propuesta/{operation}', [
		'as' => 'private.operations.accept-proposal',
		'uses' => 'OperationController@acceptProposal'
	]);
	Route::get('operacion/rechazar-propuesta/{operation}', [
		'as' => 'private.operations.reject-proposal',
		'uses' => 'OperationController@rejectProposal'
	]);
	Route::post('operacion/rechazando-propuesta/{operation}', [
		'as' => 'private.operations.reject-proposal-post',
		'uses' => 'OperationController@rejectProposal'
	]);
	Route::get('operacion/invertir/{operation}', [
		'as' => 'private.operations.invest',
		'uses' => 'OperationController@invest'
	]);
	Route::post('operacion/invertir/{operation}', [
		'as' => 'private.operations.invest',
		'uses' => 'OperationController@invest'
	]);
	Route::get('operacion/rechazar/{operation}', [
		'as' => 'private.operations.reject',
		'uses' => 'OperationController@reject'
	]);
	//- /Operations - //

});

Route::group([
	'prefix' => 'admin',
	'namespace' => 'Admin',
	'middleware' => ['admin', 'auth']
], function () {

	Route::get('/', [
		'as' => 'admin.index',
		'uses' => 'DashboardController@index'
	]);

   //-- Users Routes --//
    Route::get('usuarios/{type?}', [
        'as' => 'admin.users.index',
        'uses' => 'UserController@index'
    ]);

    Route::get('usuario/crear', [
        'as' => 'admin.users.create',
        'uses' => 'UserController@create'
    ]);

    Route::get('usuario/{user}/editar', [
        'as' => 'admin.users.edit',
        'uses' => 'UserController@edit'
    ]);

    Route::post('usuarios', [
        'as' => 'admin.users.store',
        'uses' => 'UserController@store'
    ]);

    Route::delete('usuario/{user}', [
        'as' => 'admin.users.destroy',
        'uses' => 'UserController@destroy'
    ]);

    Route::put('usuario/{user}', [
        'as' => 'admin.users.update',
        'uses' => 'UserController@update'
    ]);

    Route::post('usuario/{user}/ban', [
        'as' => 'admin.users.ban',
        'uses' => 'UserController@ban'
    ]);

    Route::post('usuario/{user}/unban', [
        'as' => 'admin.users.unban',
        'uses' => 'UserController@unban'
    ]);
    //-- /Users Routes --//

    //-- Notifications Routes --//
	Route::get('notificaciones', [
		'as' => 'admin.notifications.index',
		'uses' => 'NotificationController@index'
	]);
    //-- /Notifications Routes --//

    //-- Transfers Routes --//
    Route::get('transferencias', [
        'as' => 'admin.transfers.index',
        'uses' => 'TransferController@index'
    ]);
    // Route::get('transferencia/crear', [
    //     'as' => 'admin.transfers.create',
    //     'uses' => 'TransferController@create'
    // ]);
    // Route::post('transferencias', [
    //     'as' => 'admin.transfers.store',
    //     'uses' => 'TransferController@store'
    // ]);
    // Route::put('transferencia/{transfer}', [
    //     'as' => 'admin.transfers.update',
    //     'uses' => 'TransferController@update'
    // ]);
    Route::get('transferencia/{transfer}/editar/{cancel?}', [
        'as' => 'admin.transfers.edit',
        'uses' => 'TransferController@edit'
    ]);
	// Route::delete('transferencia/{transfer}', [
	// 	'as' => 'admin.transfers.destroy',
	// 	'uses' => 'TransferController@destroy'
	// ]);
    //-- /Transfers Routes --//

	//-- Operations Routes --//
    Route::get('operaciones', [
        'as' => 'admin.operations.index',
        'uses' => 'OperationController@index'
    ]);
    Route::get('operacion/{operation}/editar', [
        'as' => 'admin.operations.edit',
        'uses' => 'OperationController@edit'
    ]);
    Route::get('operacion/{operation}', [
        'as' => 'admin.operations.show',
        'uses' => 'OperationController@show'
    ]);
    Route::post('operacion/loan_status/{operation}', [
        'as' => 'admin.operations.updateStatus',
        'uses' => 'OperationController@updateStatus'
    ]);
    //-- /Operations Routes --//


    //-- Pages Routes --//
    Route::get('paginas', [
        'as' => 'admin.pages.index',
        'uses' => 'PageController@index'
    ]);
    Route::get('pagina/crear', [
        'as' => 'admin.pages.create',
        'uses' => 'PageController@create'
    ]);
    Route::post('paginas', [
        'as' => 'admin.pages.store',
        'uses' => 'PageController@store'
    ]);
    Route::put('pagina/{page}', [
        'as' => 'admin.pages.update',
        'uses' => 'PageController@update'
    ]);
    Route::get('pagina/{page}/editar', [
        'as' => 'admin.pages.edit',
        'uses' => 'PageController@edit'
    ]);
	Route::delete('pagina/{page}', [
		'as' => 'admin.pages.destroy',
		'uses' => 'PageController@destroy'
	]);
    //-- /Pages Routes --//

    //-- News Routes --//
    Route::get('noticias', [
        'as' => 'admin.news.index',
        'uses' => 'NewsController@index'
    ]);
    Route::get('noticia/crear', [
        'as' => 'admin.news.create',
        'uses' => 'NewsController@create'
    ]);
    Route::post('noticias', [
        'as' => 'admin.news.store',
        'uses' => 'NewsController@store'
    ]);
    Route::put('noticia/{news}', [
        'as' => 'admin.news.update',
        'uses' => 'NewsController@update'
    ]);
    Route::get('noticia/{news}/editar', [
        'as' => 'admin.news.edit',
        'uses' => 'NewsController@edit'
    ]);
	Route::delete('noticia/{news}', [
		'as' => 'admin.news.destroy',
		'uses' => 'NewsController@destroy'
	]);
    //-- /News Routes --//

    //-- NewsCategory Routes --//
    Route::get('categorias', [
        'as' => 'admin.news_categories.index',
        'uses' => 'NewsCategoryController@index'
    ]);
    Route::get('categoria/crear', [
        'as' => 'admin.news_categories.create',
        'uses' => 'NewsCategoryController@create'
    ]);
    Route::post('categorias', [
        'as' => 'admin.news_categories.store',
        'uses' => 'NewsCategoryController@store'
    ]);
    Route::put('categoria/{news}', [
        'as' => 'admin.news_categories.update',
        'uses' => 'NewsCategoryController@update'
    ]);
    Route::get('categoria/{news}/editar', [
        'as' => 'admin.news_categories.edit',
        'uses' => 'NewsCategoryController@edit'
    ]);
	Route::delete('categoria/{news}', [
		'as' => 'admin.news_categories.destroy',
		'uses' => 'NewsCategoryController@destroy'
	]);
    //-- /NewsCategory Routes --//

    //-- Tags Routes --//
    Route::get('etiquetas', [
        'as' => 'admin.tags.index',
        'uses' => 'TagController@index'
    ]);
    Route::get('etiqueta/crear', [
        'as' => 'admin.tags.create',
        'uses' => 'TagController@create'
    ]);
    Route::post('etiquetas', [
        'as' => 'admin.tags.store',
        'uses' => 'TagController@store'
    ]);
    Route::put('etiqueta/{tag}', [
        'as' => 'admin.tags.update',
        'uses' => 'TagController@update'
    ]);
    Route::get('etiqueta/{tag}/editar', [
        'as' => 'admin.tags.edit',
        'uses' => 'TagController@edit'
    ]);
	Route::delete('etiqueta/{tag}', [
		'as' => 'admin.tags.destroy',
		'uses' => 'TagController@destroy'
	]);
    //-- /Tags Routes --//

    //-- FinancialEntities Routes --//
    Route::get('entidades-financieras', [
        'as' => 'admin.financial_entities.index',
        'uses' => 'FinancialEntityController@index'
    ]);
    Route::get('entidad-financiera/crear', [
        'as' => 'admin.financial_entities.create',
        'uses' => 'FinancialEntityController@create'
    ]);
    Route::post('entidades-financieras', [
        'as' => 'admin.financial_entities.store',
        'uses' => 'FinancialEntityController@store'
    ]);
    Route::put('entidad-financiera/{entity}', [
        'as' => 'admin.financial_entities.update',
        'uses' => 'FinancialEntityController@update'
    ]);
    Route::get('entidad-financiera/{entity}/editar', [
        'as' => 'admin.financial_entities.edit',
        'uses' => 'FinancialEntityController@edit'
    ]);
	Route::delete('entidad-financiera/{entity}', [
		'as' => 'admin.financial_entities.destroy',
		'uses' => 'FinancialEntityController@destroy'
	]);
    //-- /FinancialEntities Routes --//

    //-- SuccessCases Routes --//
    Route::get('casos-de-exito', [
        'as' => 'admin.success_cases.index',
        'uses' => 'SuccessCaseController@index'
    ]);
    Route::get('caso-de-exito/crear', [
        'as' => 'admin.success_cases.create',
        'uses' => 'SuccessCaseController@create'
    ]);
    Route::post('casos-de-exito', [
        'as' => 'admin.success_cases.store',
        'uses' => 'SuccessCaseController@store'
    ]);
    Route::put('caso-de-exito/{case}', [
        'as' => 'admin.success_cases.update',
        'uses' => 'SuccessCaseController@update'
    ]);
    Route::get('caso-de-exito/{case}/editar', [
        'as' => 'admin.success_cases.edit',
        'uses' => 'SuccessCaseController@edit'
    ]);
	Route::delete('caso-de-exito/{case}', [
		'as' => 'admin.success_cases.destroy',
		'uses' => 'SuccessCaseController@destroy'
	]);
    //-- /SuccessCases Routes --//

    //-- Investors Routes --//
    Route::put('inversor/{investor}', [
        'as' => 'admin.investors.update',
        'uses' => 'InvestorController@update'
    ]);
    Route::get('inversor/{investor}/editar', [
        'as' => 'admin.investors.edit',
        'uses' => 'InvestorController@edit'
    ]);
    //-- /Investors Routes --//

    //-- Debtors Routes --//
    Route::put('empresa-deudora/{debtor}', [
        'as' => 'admin.debtor_companies.update',
        'uses' => 'DebtorCompanyController@update'
    ]);
    Route::get('empresa-deudora/{debtor}/editar', [
        'as' => 'admin.debtor_companies.edit',
        'uses' => 'DebtorCompanyController@edit'
    ]);
    //-- /Debtors Routes --//

    //-- Transferor Routes --//
    Route::put('empresa-cedente/{transferor}', [
        'as' => 'admin.transferor_companies.update',
        'uses' => 'TransferorCompanyController@update'
    ]);
    Route::get('empresa-cedente/{transferor}/editar', [
        'as' => 'admin.transferor_companies.edit',
        'uses' => 'TransferorCompanyController@edit'
    ]);
    //-- /Debtors Routes --//

    //-- Documents Routes --//
    Route::get('documentos/{user}', [
        'as' => 'admin.documents.index',
        'uses' => 'DocumentController@index'
    ]);
    Route::get('documentos/{user}/crear', [
        'as' => 'admin.documents.create',
        'uses' => 'DocumentController@create'
    ]);
    Route::post('documentos', [
        'as' => 'admin.documents.store',
        'uses' => 'DocumentController@store'
    ]);
    Route::put('documentos/{document}', [
        'as' => 'admin.documents.update',
        'uses' => 'DocumentController@update'
    ]);
    Route::get('documentos/{document}/editar', [
        'as' => 'admin.documents.edit',
        'uses' => 'DocumentController@edit'
    ]);
	Route::get('documentos/{document}/delete', [
		'as' => 'admin.documents.destroy',
		'uses' => 'DocumentController@destroy'
	]);
    //-- /Documents Routes --//

    //-- Invoices Routes --//
	Route::get('presupuestos', [
		'as' => 'admin.invoices.index',
		'uses' => 'InvoiceController@index'
	]);
	Route::get('presupuesto/{invoice}', [
		'as' => 'admin.invoices.edit',
		'uses' => 'InvoiceController@edit'
	]);
	Route::put('presupuesto/{invoice}', [
		'as' => 'admin.invoices.update',
		'uses' => 'InvoiceController@update'
	]);
    //-- /Invoices Routes --//

    //-- Settings Routes --//
    Route::get('configuraciones', [
        'as' => 'admin.settings.index',
        'uses' => 'SettingController@index'
    ]);
    Route::get('configuracion/crear', [
        'as' => 'admin.settings.create',
        'uses' => 'SettingController@create'
    ]);
    Route::post('configuraciones', [
        'as' => 'admin.settings.store',
        'uses' => 'SettingController@store'
    ]);
    Route::put('configuracion/{setting}', [
        'as' => 'admin.settings.update',
        'uses' => 'SettingController@update'
    ]);
    Route::get('configuracion/{setting}/editar', [
        'as' => 'admin.settings.edit',
        'uses' => 'SettingController@edit'
    ]);
	Route::delete('configuracion/{setting}', [
		'as' => 'admin.settings.destroy',
		'uses' => 'SettingController@destroy'
	]);
    //-- /Settings Routes --//

});
