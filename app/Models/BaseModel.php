<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Artvisual\Tools\UuidTrait;

/**
 * Base model
 */
class BaseModel extends Model
{
    use UuidTrait;
    use SoftDeletes;

    /**
     * Indicates if the model should be incrementing.
     *
     * @var bool
     */
	public $incrementing = false;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
}
