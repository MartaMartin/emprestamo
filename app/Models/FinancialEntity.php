<?php

namespace App\Models;

use App\Models\BaseModel;
use Artvisual\Tools\FileUploadTrait;

/**
 * Eloquent class to describe the financial_entities table
 *
 * automatically generated by ModelGenerator.php
 */
class FinancialEntity extends BaseModel
{

	use FileUploadTrait;

	static protected $fileUploadFields = [
		'filename' => [
			'allowedTypes' => ['image'],
		]
	];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'financial_entities';

    /**
     * All date fields of the model.
     *
     * @return array
     */
    public function getDates()
    {
        return [
            'deleted_at'
        ];
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
		'filename',
        'deleted_at'
    ];

}

