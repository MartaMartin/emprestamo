<?php

namespace App\Models;

use App\Models\BaseModel;

/**
 * Eloquent class to describe the investment_values table
 *
 * automatically generated by ModelGenerator.php
 */
class InvestmentValue extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'investment_values';

    /**
     * All date fields of the model.
     *
     * @return array
     */
    public function getDates()
    {
        return [
            'deleted_at'
        ];
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'key',
        'value',
        'deleted_at'
    ];

    /**
     * hasMany InvestmentValueHistory
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function investmentValueHistory()
    {
        return $this->hasMany('App\InvestmentValueHistory', 'investment_value_id', 'id');
    }

}

