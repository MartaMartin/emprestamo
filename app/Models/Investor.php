<?php

namespace App\Models;

use App\Models\BaseModel;

/**
 * Eloquent class to describe the investors table
 *
 * automatically generated by ModelGenerator.php
 */
class Investor extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'investors';

    var $colors = [
        '#481B65',
        '#F39200',
        '#3e474c',
        '#98AB15',
        '#483BC5',
        '#081B95',
        '#B8CB65',
        '#183B05',
        '#A81BB5',
    ];

    /**
     * All date fields of the model.
     *
     * @return array
     */
    public function getDates()
    {
        return [
            'deleted_at'
        ];
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'type',
        'auto',
        'percent',
        'calification',
        'wallet',
		'user_id',
		'fee',
		'max_invest',
		'min_invest',
		'max_days',
        'deleted_at'
    ];

    /**
     * belongsTo Transfer
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    /**
     * hasMany Transfer
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transfers()
    {
        return $this->hasMany('App\Models\Transfer', 'investor_id', 'id');
    }

    /**
     * Update investor's wallet
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
	public static function updateWallet($investor_id) {
		$transfers = Transfer::where('investor_id', $investor_id)->orderBy('created_at', 'asc')->get();
		$total = 0;
		$pending_earned = $pending_ingresed = $pending_substracted = 0;
		foreach ($transfers as $transfer) {
			if (in_array($transfer->type, [3, 4])) { // Ganancia
				if ($transfer->active == 0) {
					$pending_earned += $transfer->quantity;
				} else {
					$total += $transfer->quantity;
				}
            } elseif (in_array($transfer->type, [0])) { // Ingreso
				if ($transfer->active == 0) {
					$pending_ingresed += $transfer->quantity;
				} else {
					$total += $transfer->quantity;
				}
			} else {
				if ($transfer->active == 0) {
					$pending_substracted -= $transfer->quantity;
				} else {
					$total -= $transfer->quantity;
				}
			}
		}
		$investor = Investor::find($investor_id);
		$investor->update(['wallet' => $total]);
		return [$total, $pending_earned, $pending_ingresed, $pending_substracted];
	}

	/**
	 * Busca inversores que se ajusten a los criterios de la operación
	 *
	 * Crea una transferencia para cada inversor
	 */
	public static function automaticInvest($operation) {
		$invoice = Invoice::where('id', $operation->invoice_id)->first();
		$time = (strtotime($invoice->expires) - time()) / 86400; // Días

		$percent_auto_invest = Setting::where('key', 'percent_auto_invest')->first();
		$price = floor($invoice->price * ($percent_auto_invest->value / 100));
        $total = $operation->getCurrentInvested();
		$price -= $total;

		if (!$min_quantity = $operation->ticket_min) {
			$min_invest = Setting::where('key', 'min_invest')->first();
			$min_quantity = $min_invest->value;
			// $min_quantity = $price * ($min_invest->value / 100);
		}

		$limit = floor($price / $min_quantity);

        $fee = $operation->fee * 365 / $time;

		$investors = Investor::where('type', 1)
			->where('auto', 1)
			->where('calification', '<=', $operation->calification)
			->where('max_days', '>=', $time)
			->where('fee', '<=', $fee)
			// ->where('max_invest', '>=', $min_quantity)
			->where('wallet', '>=', $min_quantity)
			->limit($limit)
			->orderBy('num_invest', 'asc')
			->get();

		if (count($investors)) {

			if (count($investors) == $limit) {
				$quantity = $min_quantity;
			} else {
				$max_quantity = 99999999999;
				foreach ($investors as $investor) {
					if ($investor->max_invest == 0 || $investor->max_invest > $investor->wallet) {
						$max_invest = $investor->wallet;
					}
					if ($max_invest < $max_quantity) {
						$max_quantity = $max_invest;
					}
				}
				$quantity = $max_quantity;
			}

			if ($operation->ticket_max != 0 && $quantity > $operation->ticket_max) {
				$quantity = $operation->ticket_max;
			}

			if ($quantity * count($investors) > $price) {
				$quantity = floor($price / count($investors));
			}

			foreach ($investors as $investor) {
				$transfer = new Transfer;
				$transfer->create([
					'investor_id' => $investor->id,
					'operation_id' => $operation->id,
					'investor_type' => $investor->type,
					'quantity' => $quantity,
					'active' => 1,
					'type' => 5,
					'validate' => 1
				]);
				Investor::updateWallet($investor->id);

				$investor->num_invest += 1;
				$investor->save();

				$total += $quantity;

				Notification::sendNotification($investor->user_id, _('Has invertido automáticamente en una operación'), 'private.invoices.view', json_encode([$operation->invoice_id]));
			}

			if ($total >= $invoice->price) {
				$transferor_company = TransferorCompany::whereId($invoice->transferor_company_id)->first();
				$operation = Operation::operationCompleted($operation);
				Notification::sendNotification($transferor_company->user_id, _('Ya se ha invertido el total del presupuesto'), 'private.invoices.view', json_encode([$operation->invoice_id]));
			}

		}

		return [$operation, $investors];
	}

    public function getTotals($operations) {
        $donutgraphs = [
            'invested' => [],
            'earned' => [],
            'returned' => [],
            'fee' => []
        ];
		$totals = [
			'invested' => 0,
			'earned' => 0,
            'returned' => 0,
            'fee' => 0
		];
        $i = 0;
		foreach ($operations as $operation) {
			if ($i >= 8) continue;
			foreach ($operation->transfers as $transfer) {
				if ($transfer->investor_id == $this->id) {
					switch ($transfer->type) {
						case 5: // Inversión automática
						case 2: // Inversión
							$operation->invested += $transfer->quantity;
							$totals['invested'] += $transfer->quantity;
							break;
						case 3: // Ganancias
							$operation->earned += $transfer->quantity;
                            $totals['earned'] += $transfer->quantity;
							break;
						case 4: // Devuelto
							$operation->returned += $transfer->quantity;
							$totals['returned'] += $transfer->quantity;
							break;
					}
				}
			}
            $totals['fee'] += $operation->fee;

            $donutgraphs['invested'][] = [
                $operation->invoice->name,
                $operation->invested,
                $this->colors[$i],
            ];
            $donutgraphs['earned'][] = [
                $operation->invoice->name,
                $operation->earned,
                $this->colors[$i],
            ];
            $donutgraphs['returned'][] = [
                $operation->invoice->name,
                $operation->returned,
                $this->colors[$i],
            ];
            $donutgraphs['fee'][] = [
                $operation->invoice->name,
                $operation->fee,
                $this->colors[$i],
            ];
            $i ++;
		}
        if ($operations->count()) {
                $totals['fee'] /= $operations->count();
        }
        $totals['only_earned'] = $totals['earned'];
        $totals['earned'] += $totals['invested'];

        return [$totals, $donutgraphs, $operations];
    }

}

