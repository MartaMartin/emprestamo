<?php

namespace App\Policies;

use App\Models\DebtorCompany;
use App\Models\User;

use Illuminate\Auth\Access\HandlesAuthorization;

class DebtorCompanyPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

	/**
	 * Determine if the given debtor_company can be edited by the user.
	 *
	 * @param  \App\Models\User $user
	 * @param  \App\Models\DebtorCompany $debtor_company
	 * @return bool
	 */
    public function edit(User $user, DebtorCompany $debtor_company)
    {
        return ($user->role === 'admin' || $user->role === 'analyst');
    }

	/**
	 * Determine if the given debtor_company can be deleted by the user.
	 *
	 * @param  \App\Models\User $user
	 * @param  \App\Models\DebtorCompany $debtor_company
	 * @return bool
	 */
    public function destroy(User $user, DebtorCompany $debtor_company)
    {
        return ($user->role === 'admin');
    }

	/**
	 * Determine if the given debtor_company can be updated by the user.
	 *
	 * @param  \App\Models\User $user
	 * @param  \App\Models\DebtorCompany $debtor_company
	 * @return bool
	 */
    public function update(User $user, DebtorCompany $debtor_company)
    {
        return ($user->role === 'admin' || $user->role === 'analyst');
    }

}
