<?php

namespace App\Policies;

use App\Models\Document;
use App\Models\User;

use Illuminate\Auth\Access\HandlesAuthorization;

class DocumentPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

	public function index(User $user, Document $document)
	{
        return ($user->role === 'admin');
	}  

	/**
	 * Determine if the given document can be created by the user.
	 *
	 * @param  \App\Models\User $user
	 * @param  \App\Models\Document $document
	 * @return bool
	 */
    public function create(User $user, Document $document)
    {
        return ($user->role === 'admin');
    }

	/**
	 * Determine if the given document can be edited by the user.
	 *
	 * @param  \App\Models\User $user
	 * @param  \App\Models\Document $document
	 * @return bool
	 */
    public function edit(User $user, Document $document)
    {
        return ($user->role === 'admin');
    }

	/**
	 * Determine if the given document can be deleted by the user.
	 *
	 * @param  \App\Models\User $user
	 * @param  \App\Models\Document $document
	 * @return bool
	 */
    public function destroy(User $user, Document $document)
    {
        return ($user->role === 'admin');
    }

	/**
	 * Determine if the given document can be stored by the user.
	 *
	 * @param  \App\Models\User $user
	 * @param  \App\Models\Document $document
	 * @return bool
	 */
    public function store(User $user, Document $document)
    {
        return ($user->role === 'admin');
    }

	/**
	 * Determine if the given document can be updated by the user.
	 *
	 * @param  \App\Models\User $user
	 * @param  \App\Models\Document $document
	 * @return bool
	 */
    public function update(User $user, Document $document)
    {
        return ($user->role === 'admin');
    }

}
