<?php

namespace App\Policies;

use App\User;
use App\Event;

class EventPolicy
{

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function before(User $user, $ability)
    {
        if ($user->role == 'admin')
        {
            return true;
        }
    }

    public function index(User $user, Event $event)
    {
        return true;
    }    

    public function create(User $user, Event $event)
    {
        return $user->hasLevel('gold');
    }

    public function store(User $user, Event $event)
    {
        return $user->hasLevel('gold');
    }

    public function edit(User $user, Event $event)
    {
        return $user->hasLevelOnEvent('gold', $event);
    }

    public function admin_edit(User $user, Event $event)
    {
        return ($user->role === 'admin');
    }

    public function update(User $user, Event $event)
    {
        return $user->hasLevelOnEvent('gold', $event);
    }

    public function delete(User $user, Event $event)
    {
        return ($user->role === 'admin');
    }
}
