<?php

namespace App\Policies;

use App\Models\FinancialEntity;
use App\Models\User;

use Illuminate\Auth\Access\HandlesAuthorization;

class FinancialEntityPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

	public function index(User $user, FinancialEntity $news)
	{
        return ($user->role === 'admin');
	}  

	/**
	 * Determine if the given news can be created by the user.
	 *
	 * @param  \App\Models\User $user
	 * @param  \App\Models\FinancialEntity $news
	 * @return bool
	 */
    public function create(User $user, FinancialEntity $news)
    {
        return ($user->role === 'admin');
    }

	/**
	 * Determine if the given news can be edited by the user.
	 *
	 * @param  \App\Models\User $user
	 * @param  \App\Models\FinancialEntity $news
	 * @return bool
	 */
    public function edit(User $user, FinancialEntity $news)
    {
        return ($user->role === 'admin');
    }

	/**
	 * Determine if the given news can be deleted by the user.
	 *
	 * @param  \App\Models\User $user
	 * @param  \App\Models\FinancialEntity $news
	 * @return bool
	 */
    public function destroy(User $user, FinancialEntity $news)
    {
        return ($user->role === 'admin');
    }

	/**
	 * Determine if the given news can be stored by the user.
	 *
	 * @param  \App\Models\User $user
	 * @param  \App\Models\FinancialEntity $news
	 * @return bool
	 */
    public function store(User $user, FinancialEntity $news)
    {
        return ($user->role === 'admin');
    }

	/**
	 * Determine if the given news can be updated by the user.
	 *
	 * @param  \App\Models\User $user
	 * @param  \App\Models\FinancialEntity $news
	 * @return bool
	 */
    public function update(User $user, FinancialEntity $news)
    {
        return ($user->role === 'admin');
    }

}
