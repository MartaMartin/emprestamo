<?php

namespace App\Policies;

use App\Models\Investor;
use App\Models\User;

use Illuminate\Auth\Access\HandlesAuthorization;

class InvestorPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

	/**
	 * Determine if the given investor can be edited by the user.
	 *
	 * @param  \App\Models\User $user
	 * @param  \App\Models\Investor $investor
	 * @return bool
	 */
    public function edit(User $user, Investor $investor)
    {
        return ($user->role === 'admin');
    }

	/**
	 * Determine if the given investor can be deleted by the user.
	 *
	 * @param  \App\Models\User $user
	 * @param  \App\Models\Investor $investor
	 * @return bool
	 */
    public function destroy(User $user, Investor $investor)
    {
        return ($user->role === 'admin');
    }

	/**
	 * Determine if the given investor can be updated by the user.
	 *
	 * @param  \App\Models\User $user
	 * @param  \App\Models\Investor $investor
	 * @return bool
	 */
    public function update(User $user, Investor $investor)
    {
        return ($user->role === 'admin');
    }

}
