<?php

namespace App\Policies;

use App\Models\Invoice;
use App\Models\User;

use Illuminate\Auth\Access\HandlesAuthorization;

class InvoicePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

	public function index(User $user, Invoice $invoice)
	{
		return ($user->role === 'analyst' || $user->role === 'admin');
	}

	/**
	 * Determine if the given invoice can be edited by the user.
	 *
	 * @param  \App\Models\User $user
	 * @param  \App\Models\Invoice $invoice
	 * @return bool
	 */
    public function edit(User $user, Invoice $invoice)
    {
		return ($user->role === 'analyst' || $user->role === 'admin');
    }

	/**
	 * Determine if the given invoice can be updated by the user.
	 *
	 * @param  \App\Models\User $user
	 * @param  \App\Models\Invoice $invoice
	 * @return bool
	 */
    public function update(User $user, Invoice $invoice)
    {
		return ($user->role === 'analyst' || $user->role === 'admin');
    }

}
