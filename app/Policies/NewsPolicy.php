<?php

namespace App\Policies;

use App\Models\News;
use App\Models\User;

use Illuminate\Auth\Access\HandlesAuthorization;

class NewsPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

	public function index(User $user, News $news)
	{
        return ($user->role === 'admin');
	}  

	/**
	 * Determine if the given news can be created by the user.
	 *
	 * @param  \App\Models\User $user
	 * @param  \App\Models\News $news
	 * @return bool
	 */
    public function create(User $user, News $news)
    {
        return ($user->role === 'admin');
    }

	/**
	 * Determine if the given news can be edited by the user.
	 *
	 * @param  \App\Models\User $user
	 * @param  \App\Models\News $news
	 * @return bool
	 */
    public function edit(User $user, News $news)
    {
        return ($user->role === 'admin');
    }

	/**
	 * Determine if the given news can be deleted by the user.
	 *
	 * @param  \App\Models\User $user
	 * @param  \App\Models\News $news
	 * @return bool
	 */
    public function destroy(User $user, News $news)
    {
        return ($user->role === 'admin');
    }

	/**
	 * Determine if the given news can be stored by the user.
	 *
	 * @param  \App\Models\User $user
	 * @param  \App\Models\News $news
	 * @return bool
	 */
    public function store(User $user, News $news)
    {
        return ($user->role === 'admin');
    }

	/**
	 * Determine if the given news can be updated by the user.
	 *
	 * @param  \App\Models\User $user
	 * @param  \App\Models\News $news
	 * @return bool
	 */
    public function update(User $user, News $news)
    {
        return ($user->role === 'admin');
    }

}
