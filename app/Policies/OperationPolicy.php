<?php

namespace App\Policies;

use App\Models\Operation;
use App\Models\Invoice;
use App\Models\TransferorCompany;
use App\Models\User;

use Illuminate\Auth\Access\HandlesAuthorization;

class OperationPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

	public function index(User $user, Operation $operation)
	{
		return ($user->role === 'admin' || $user->role == 'analyst');
	}

	public function edit(User $user, Operation $operation)
	{
		return ($user->role === 'admin' || $user->role == 'analyst');
	}

	public function acceptProposal(User $user, Operation $operation)
	{
		if ($user->role === 'analyst') {
			return true;
		} elseif ($user->role === 'transferor_company') {
			$invoice = Invoice::where('id', $operation->invoice_id)->first();
			$transferor_company = TransferorCompany::where('id', $invoice->transferor_company_id)->first();
			return $transferor_company['user_id'] == $user->id;
		}
		return false;
	}

	public function rejectProposal(User $user, Operation $operation)
	{
		if ($user->role === 'analyst') {
			return true;
		} elseif ($user->role === 'transferor_company') {
			$invoice = Invoice::where('id', $operation->invoice_id)->first();
			$transferor_company = TransferorCompany::where('id', $invoice->transferor_company_id)->first();
			return $transferor_company['user_id'] == $user->id;
		}
		return false;
	}

	public function invest(User $user, Operation $operation)
	{
		return ($user->role === 'investor');
	}

	public function reject(User $user, Operation $operation)
	{
		return ($user->role === 'investor');
	}

}
