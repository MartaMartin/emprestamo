<?php

namespace App\Policies;

use App\Models\Page;
use App\Models\User;

use Illuminate\Auth\Access\HandlesAuthorization;

class PagePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

	public function index(User $user, Page $page)
	{
        return ($user->role === 'admin');
	}  

	/**
	 * Determine if the given page can be created by the user.
	 *
	 * @param  \App\Models\User $user
	 * @param  \App\Models\Page $page
	 * @return bool
	 */
    public function create(User $user, Page $page)
    {
        return ($user->role === 'admin');
    }

	/**
	 * Determine if the given page can be edited by the user.
	 *
	 * @param  \App\Models\User $user
	 * @param  \App\Models\Page $page
	 * @return bool
	 */
    public function edit(User $user, Page $page)
    {
        return ($user->role === 'admin');
    }

	/**
	 * Determine if the given page can be deleted by the user.
	 *
	 * @param  \App\Models\User $user
	 * @param  \App\Models\Page $page
	 * @return bool
	 */
    public function destroy(User $user, Page $page)
    {
        return ($user->role === 'admin');
    }

	/**
	 * Determine if the given page can be stored by the user.
	 *
	 * @param  \App\Models\User $user
	 * @param  \App\Models\Page $page
	 * @return bool
	 */
    public function store(User $user, Page $page)
    {
        return ($user->role === 'admin');
    }

	/**
	 * Determine if the given page can be updated by the user.
	 *
	 * @param  \App\Models\User $user
	 * @param  \App\Models\Page $page
	 * @return bool
	 */
    public function update(User $user, Page $page)
    {
        return ($user->role === 'admin');
    }

}
