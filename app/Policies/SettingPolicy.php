<?php

namespace App\Policies;

use App\Models\Setting;
use App\Models\User;

use Illuminate\Auth\Access\HandlesAuthorization;

class SettingPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

	public function index(User $user, Setting $setting)
	{
        return ($user->role === 'admin');
	}  

	/**
	 * Determine if the given setting can be created by the user.
	 *
	 * @param  \App\Models\User $user
	 * @param  \App\Models\Setting $setting
	 * @return bool
	 */
    public function create(User $user, Setting $setting)
    {
        return ($user->role === 'admin');
    }

	/**
	 * Determine if the given setting can be edited by the user.
	 *
	 * @param  \App\Models\User $user
	 * @param  \App\Models\Setting $setting
	 * @return bool
	 */
    public function edit(User $user, Setting $setting)
    {
        return ($user->role === 'admin');
    }

	/**
	 * Determine if the given setting can be deleted by the user.
	 *
	 * @param  \App\Models\User $user
	 * @param  \App\Models\Setting $setting
	 * @return bool
	 */
    public function destroy(User $user, Setting $setting)
    {
        return ($user->role === 'admin');
    }

	/**
	 * Determine if the given setting can be stored by the user.
	 *
	 * @param  \App\Models\User $user
	 * @param  \App\Models\Setting $setting
	 * @return bool
	 */
    public function store(User $user, Setting $setting)
    {
        return ($user->role === 'admin');
    }

	/**
	 * Determine if the given setting can be updated by the user.
	 *
	 * @param  \App\Models\User $user
	 * @param  \App\Models\Setting $setting
	 * @return bool
	 */
    public function update(User $user, Setting $setting)
    {
        return ($user->role === 'admin');
    }

}
