<?php

namespace App\Policies;

use App\Models\SuccessCase;
use App\Models\User;

use Illuminate\Auth\Access\HandlesAuthorization;

class SuccessCasePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

	public function index(User $user, SuccessCase $news)
	{
        return ($user->role === 'admin');
	}  

	/**
	 * Determine if the given news can be created by the user.
	 *
	 * @param  \App\Models\User $user
	 * @param  \App\Models\SuccessCase $news
	 * @return bool
	 */
    public function create(User $user, SuccessCase $news)
    {
        return ($user->role === 'admin');
    }

	/**
	 * Determine if the given news can be edited by the user.
	 *
	 * @param  \App\Models\User $user
	 * @param  \App\Models\SuccessCase $news
	 * @return bool
	 */
    public function edit(User $user, SuccessCase $news)
    {
        return ($user->role === 'admin');
    }

	/**
	 * Determine if the given news can be deleted by the user.
	 *
	 * @param  \App\Models\User $user
	 * @param  \App\Models\SuccessCase $news
	 * @return bool
	 */
    public function destroy(User $user, SuccessCase $news)
    {
        return ($user->role === 'admin');
    }

	/**
	 * Determine if the given news can be stored by the user.
	 *
	 * @param  \App\Models\User $user
	 * @param  \App\Models\SuccessCase $news
	 * @return bool
	 */
    public function store(User $user, SuccessCase $news)
    {
        return ($user->role === 'admin');
    }

	/**
	 * Determine if the given news can be updated by the user.
	 *
	 * @param  \App\Models\User $user
	 * @param  \App\Models\SuccessCase $news
	 * @return bool
	 */
    public function update(User $user, SuccessCase $news)
    {
        return ($user->role === 'admin');
    }

}
