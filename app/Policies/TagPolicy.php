<?php

namespace App\Policies;

use App\Models\Tag;
use App\Models\User;

use Illuminate\Auth\Access\HandlesAuthorization;

class TagPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

	public function index(User $user, Tag $tag)
	{
        return ($user->role === 'admin');
	}  

	/**
	 * Determine if the given tag can be created by the user.
	 *
	 * @param  \App\Models\User $user
	 * @param  \App\Models\Tag $tag
	 * @return bool
	 */
    public function create(User $user, Tag $tag)
    {
        return ($user->role === 'admin');
    }

	/**
	 * Determine if the given tag can be edited by the user.
	 *
	 * @param  \App\Models\User $user
	 * @param  \App\Models\Tag $tag
	 * @return bool
	 */
    public function edit(User $user, Tag $tag)
    {
        return ($user->role === 'admin');
    }

	/**
	 * Determine if the given tag can be deleted by the user.
	 *
	 * @param  \App\Models\User $user
	 * @param  \App\Models\Tag $tag
	 * @return bool
	 */
    public function destroy(User $user, Tag $tag)
    {
        return ($user->role === 'admin');
    }

	/**
	 * Determine if the given tag can be stored by the user.
	 *
	 * @param  \App\Models\User $user
	 * @param  \App\Models\Tag $tag
	 * @return bool
	 */
    public function store(User $user, Tag $tag)
    {
        return ($user->role === 'admin');
    }

	/**
	 * Determine if the given tag can be updated by the user.
	 *
	 * @param  \App\Models\User $user
	 * @param  \App\Models\Tag $tag
	 * @return bool
	 */
    public function update(User $user, Tag $tag)
    {
        return ($user->role === 'admin');
    }

}
