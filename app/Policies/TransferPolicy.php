<?php

namespace App\Policies;

use App\Models\Transfer;
use App\Models\User;

use Illuminate\Auth\Access\HandlesAuthorization;

class TransferPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

	public function index(User $user, Transfer $transfer)
	{
        return ($user->role === 'admin');
	}

	/**
	 * Determine if the given transfer can be created by the user.
	 *
	 * @param  \App\Models\User $user
	 * @param  \App\Models\Transfer $transfer
	 * @return bool
	 */
    public function create(User $user, Transfer $transfer)
    {
        return ($user->role === 'admin');
    }

	/**
	 * Determine if the given transfer can be edited by the user.
	 *
	 * @param  \App\Models\User $user
	 * @param  \App\Models\Transfer $transfer
	 * @return bool
	 */
    public function edit(User $user, Transfer $transfer)
    {
        return ($user->role === 'admin');
    }

	/**
	 * Determine if the given transfer can be deleted by the user.
	 *
	 * @param  \App\Models\User $user
	 * @param  \App\Models\Transfer $transfer
	 * @return bool
	 */
    public function destroy(User $user, Transfer $transfer)
    {
        return ($user->role === 'admin');
    }

	/**
	 * Determine if the given transfer can be stored by the user.
	 *
	 * @param  \App\Models\User $user
	 * @param  \App\Models\Transfer $transfer
	 * @return bool
	 */
    public function store(User $user, Transfer $transfer)
    {
        return ($user->role === 'admin');
    }

	/**
	 * Determine if the given transfer can be updated by the user.
	 *
	 * @param  \App\Models\User $user
	 * @param  \App\Models\Transfer $transfer
	 * @return bool
	 */
    public function update(User $user, Transfer $transfer)
    {
        return ($user->role === 'admin');
    }

}
