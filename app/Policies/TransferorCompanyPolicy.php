<?php

namespace App\Policies;

use App\Models\TransferorCompany;
use App\Models\User;

use Illuminate\Auth\Access\HandlesAuthorization;

class TransferorCompanyPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

	/**
	 * Determine if the given transferor_company can be edited by the user.
	 *
	 * @param  \App\Models\User $user
	 * @param  \App\Models\TransferorCompany $transferor_company
	 * @return bool
	 */
    public function edit(User $user, TransferorCompany $transferor_company)
    {
        return ($user->role === 'admin' || $user->role === 'analyst');
    }

	/**
	 * Determine if the given transferor_company can be deleted by the user.
	 *
	 * @param  \App\Models\User $user
	 * @param  \App\Models\TransferorCompany $transferor_company
	 * @return bool
	 */
    public function destroy(User $user, TransferorCompany $transferor_company)
    {
        return ($user->role === 'admin');
    }

	/**
	 * Determine if the given transferor_company can be updated by the user.
	 *
	 * @param  \App\Models\User $user
	 * @param  \App\Models\TransferorCompany $transferor_company
	 * @return bool
	 */
    public function update(User $user, TransferorCompany $transferor_company)
    {
        return ($user->role === 'admin' || $user->role === 'analyst');
    }

}
