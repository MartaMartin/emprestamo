<?php

namespace App\Policies;

use App\Models\User;

use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(User $user, User $user2)
    {
        return ($user->role === 'admin');
    }

    /**
     * Determine if the given user2 can be created by the user.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\User $user2
     * @return bool
     */
    public function create(User $user, User $user2)
    {
        return ($user->role === 'admin' || $user->role == 'analyst');
    }

    /**
     * Determine if the given user2 can be edited by the user.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\User $user2
     * @return bool
     */
    public function edit(User $user, User $user2)
    {
        return ($user->role === 'admin' || $user->role == 'analyst');
    }

    /**
     * Determine if the given user2 can be deleted by the user.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\User $user2
     * @return bool
     */
    public function destroy(User $user, User $user2)
    {
        return ($user->role === 'admin');
    }

    /**
     * Determine if the given user2 can be stored by the user.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\User $user2
     * @return bool
     */
    public function store(User $user, User $user2)
    {
        return ($user->role === 'admin' || $user->role == 'analyst');
    }

    /**
     * Determine if the given user2 can be updated by the user.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\User $user2
     * @return bool
     */
    public function update(User $user, User $user2)
    {
        return ($user->role === 'admin' || $user->role == 'analyst');
    }

}
