<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(['private.partials._investor-menu'], 'App\ViewComposers\WalletComposer');
        view()->composer(['private.partials.modules._sidebar'], 'App\ViewComposers\WalletComposer');
        view()->composer(['*'], 'App\ViewComposers\SettingComposer');
        view()->composer(['*'], 'App\ViewComposers\NotificationComposer');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
