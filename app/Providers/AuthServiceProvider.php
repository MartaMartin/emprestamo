<?php

namespace App\Providers;

use App\Models\Page;
use App\Models\News;
use App\Models\NewsCategory;
use App\Models\Tag;
use App\Models\FinancialEntity;
use App\Models\SuccessCase;
use App\Models\Investor;
use App\Models\Document;
use App\Models\DebtorCompany;
use App\Models\TransferorCompany;
use App\Models\Invoice;
use App\Models\Operation;
use App\Models\Setting;
use App\Models\Transfer;
use App\Models\User;

use App\Policies\PagePolicy;
use App\Policies\NewsPolicy;
use App\Policies\NewsCategoryPolicy;
use App\Policies\TagPolicy;
use App\Policies\FinancialEntityPolicy;
use App\Policies\SuccessCasePolicy;
use App\Policies\InvestorPolicy;
use App\Policies\DocumentPolicy;
use App\Policies\DebtorCompanyPolicy;
use App\Policies\TransferorCompanyPolicy;
use App\Policies\InvoicePolicy;
use App\Policies\OperationPolicy;
use App\Policies\SettingPolicy;
use App\Policies\TransferPolicy;
use App\Policies\UserPolicy;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
		Page::class => PagePolicy::class,
		News::class => NewsPolicy::class,
		NewsCategory::class => NewsCategoryPolicy::class,
		Tag::class => TagPolicy::class,
		FinancialEntity::class => FinancialEntityPolicy::class,
		SuccessCase::class => SuccessCasePolicy::class,
		User::class => UserPolicy::class,
		Investor::class => InvestorPolicy::class,
		DebtorCompany::class => DebtorCompanyPolicy::class,
		TransferorCompany::class => TransferorCompanyPolicy::class,
		Document::class => DocumentPolicy::class,
		Invoice::class => InvoicePolicy::class,
		Operation::class => OperationPolicy::class,
		Setting::class => SettingPolicy::class,
		Transfer::class => TransferPolicy::class,
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        parent::registerPolicies($gate);

        //
    }
}
