<?php
namespace App\ViewComposers;

use Illuminate\Contracts\View\View;

use App\Models\Notification;

use Auth;

class NotificationComposer {

	public function compose(View $view) {
        if ($user = Auth::user()) {
            $notifications = Notification::where('user_id', $user->id)
                ->orderBy('read', 'asc')
                ->orderBy('created_at', 'desc')
                ->limit(30)
                ->get();
        } else {
            $notifications = [];
        }
        $view->with(compact('notifications'));
	}

}
