<?php
namespace App\ViewComposers;

use Illuminate\Contracts\View\View;

use App\Models\Setting;

use Auth;

class SettingComposer {

	public function compose(View $view) {
		$settings = Setting::get();
		$return = [];
		foreach ($settings as $setting) {
			$return[$setting->key] = $setting->value;
		}

		$settings = $return;
		$view->with(compact('settings'));
	}

}
