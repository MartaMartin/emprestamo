<?php
namespace App\ViewComposers;

use Illuminate\Contracts\View\View;

use App\Models\Investor;

use Auth;

class WalletComposer {

	public function compose(View $view) {
		$user = Auth::user();
		$amount = 0;
		if ($user->role == 'investor') {
			list($amount, $pending_earned, $pending_ingresed, $pending_substracted) = Investor::updateWallet($user->investor->id);
		}
		$view->with(compact('amount', 'pending_earned', 'pending_ingresed', 'pending_substracted'));
	}

}
