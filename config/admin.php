<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Panel de control Admin y Web en general
    |--------------------------------------------------------------------------
    |
    | Configuraciones centralizadas de Admin y Web (TEMP hasta sección settings).
    |
    */

    /**
     * Admin aside menu sections
     */
    'sections_aside' => [

        'aside_1' => [
            'has_subs' => true,
            'has_level' => 'all',
            'label' => _('Usuarios'),
            'classes' => 'fa fa-users',
            'subs_aside' => [

                'sub_1' => [
                    'label' => _('Listado inversores'),
                    'route' => 'admin.users.index',
                    'params' => ['investor'],
                    'classes' => 'fa fa-list',
                ],

                'sub_2' => [
                    'label' => _('Listado cedentes'),
                    'route' => 'admin.users.index',
                    'params' => ['transferor_company'],
                    'classes' => 'fa fa-list',
                ],

                'sub_3' => [
                    'label' => _('Listado deudoras'),
                    'route' => 'admin.users.index',
                    'params' => ['debtor_company'],
                    'classes' => 'fa fa-list',
                ],

                'sub_4' => [
                    'label' => _('Crear usuario'),
                    'route' => 'admin.users.create',
                    'classes' => 'fa fa-plus text-aqua',
                ],

            ],
        ],

        'aside_2' => [
            'has_subs' => true,
            'has_level' => 'all',
            'label' => _('Transferencias'),
            'classes' => 'fa fa-euro',
            'subs_aside' => [

                'sub_1' => [
                    'label' => _('Listado transferencias'),
                    'route' => 'admin.transfers.index',
                    'classes' => 'fa fa-list',
                ],

            ],
        ],

        'aside_3' => [
            'has_subs' => true,
            'has_level' => 'all',
            'label' => _('Páginas'),
            'classes' => 'fa fa-file-text',
            'subs_aside' => [

                'sub_1' => [
                    'label' => _('Listado páginas'),
                    'route' => 'admin.pages.index',
                    'classes' => 'fa fa-list',
                ],

                'sub_2' => [
                    'label' => _('Crear página'),
                    'route' => 'admin.pages.create',
                    'classes' => 'fa fa-plus text-aqua',
                ],

            ],
        ],

        'aside_4' => [
            'has_subs' => true,
            'has_level' => 'all',
            'label' => _('Noticias'),
            'classes' => 'fa fa-newspaper-o',
            'subs_aside' => [

                'sub_1' => [
                    'label' => _('Listado noticias'),
                    'route' => 'admin.news.index',
                    'classes' => 'fa fa-list',
                ],

                'sub_2' => [
                    'label' => _('Crear noticia'),
                    'route' => 'admin.news.create',
                    'classes' => 'fa fa-plus text-aqua',
                ],

                'sub_3' => [
                    'label' => _('Categorías'),
                    'route' => 'admin.news_categories.index',
                    // 'classes' => 'fa fa-indent',
                    'classes' => 'fa fa-tag',
                ],

				/*
                'sub_4' => [
                    'label' => _('Etiquetas'),
                    'route' => 'admin.tags.index',
                    'classes' => 'fa fa-tag',
                ],
				*/

            ],
        ],

        'aside_5' => [
            'has_subs' => true,
            'has_level' => 'all',
            'label' => _('Entidades financieras'),
            'classes' => 'fa fa-eur',
            'subs_aside' => [

                'sub_1' => [
                    'label' => _('Listado de entidades'),
                    'route' => 'admin.financial_entities.index',
                    'classes' => 'fa fa-list',
                ],

                'sub_2' => [
                    'label' => _('Crear entidad'),
                    'route' => 'admin.financial_entities.create',
                    'classes' => 'fa fa-plus text-aqua',
                ],

            ],
        ],

        'aside_6' => [
            'has_subs' => true,
            'has_level' => 'all',
            'label' => _('Casos de éxito'),
            'classes' => 'fa fa-star',
            'subs_aside' => [

                'sub_1' => [
                    'label' => _('Listado de casos'),
                    'route' => 'admin.success_cases.index',
                    'classes' => 'fa fa-list',
                ],

                'sub_2' => [
                    'label' => _('Crear caso'),
                    'route' => 'admin.success_cases.create',
                    'classes' => 'fa fa-plus text-aqua',
                ],

            ],
        ],

        'aside_7' => [
            'has_subs' => true,
            'has_level' => 'all',
            'label' => _('Notificaciones'),
            'classes' => 'fa fa-bell',
            'subs_aside' => [

                'sub_1' => [
                    'label' => _('Listado de notificaciones'),
                    'route' => 'admin.notifications.index',
                    'classes' => 'fa fa-list',
                ],

            ],
        ],

        'aside_8' => [
            'has_subs' => true,
            'has_level' => 'all',
            'label' => _('Configuración'),
            'classes' => 'fa fa-gear',
            'subs_aside' => [

                'sub_1' => [
                    'label' => _('Listado de variables'),
                    'route' => 'admin.settings.index',
                    'classes' => 'fa fa-list',
                ],

                'sub_2' => [
                    'label' => _('Crear variable'),
                    'route' => 'admin.settings.create',
                    'classes' => 'fa fa-plus text-aqua',
                ],

            ],
        ],

    ],

    /**
     * Admin lists pagination
     */
    'pagination' => 20,

    /**
     * Web profile settings
     */
    // 'facebook_profile' => 'infomychip',
    // 'twitter_profile' => 'mychip_info',
];
