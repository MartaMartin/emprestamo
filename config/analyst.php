<?php

return [

/*
|--------------------------------------------------------------------------
| Panel de control Analista y Web en general
|--------------------------------------------------------------------------
|
 */

    /**
     * Admin aside menu sections
     */
    'sections_aside' => [

        'aside_0' => [
            'has_subs' => true,
            'has_level' => 'all',
            'label' => _('Usuarios'),
            'classes' => 'fa fa-users',
            'subs_aside' => [

                'sub_1' => [
                    'label' => _('Listado inversores'),
                    'route' => 'admin.users.index',
                    'params' => ['investor'],
                    'classes' => 'fa fa-list',
                ],

                'sub_2' => [
                    'label' => _('Listado cedentes'),
                    'route' => 'admin.users.index',
                    'params' => ['transferor_company'],
                    'classes' => 'fa fa-list',
                ],

                'sub_3' => [
                    'label' => _('Listado deudoras'),
                    'route' => 'admin.users.index',
                    'params' => ['debtor_company'],
                    'classes' => 'fa fa-list',
                ],

                'sub_4' => [
                    'label' => _('Crear usuario'),
                    'route' => 'admin.users.create',
                    'classes' => 'fa fa-plus text-aqua',
                ],

            ],
        ],

        'aside_1' => [
            'has_subs' => true,
            'has_level' => 'all',
            'label' => _('Facturas'),
            'classes' => 'fa fa-file-text',
            'subs_aside' => [

                'sub_1' => [
                    'label' => _('Listado facturas'),
                    'route' => 'admin.invoices.index',
                    'classes' => 'fa fa-list',
                ],

            ],
        ],

        'aside_2' => [
            'has_subs' => true,
            'has_level' => 'all',
            'label' => _('Operaciones'),
            'classes' => 'fa fa-file-text',
            'subs_aside' => [

                'sub_1' => [
                    'label' => _('Listado operaciones'),
                    'route' => 'admin.operations.index',
                    'classes' => 'fa fa-list',
                ],

            ],
        ],

    ],

    /**
     * Admin lists pagination
     */
    'pagination' => 20,

    /**
     * Web profile settings
     */
    // 'facebook_profile' => 'infomychip',
    // 'twitter_profile' => 'mychip_info',
];
