<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInvestmentValueHistoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('investment_value_history', function(Blueprint $table)
		{
			$table->char('id', 36)->primary();
			$table->char('investment_value_id', 36)->index('investment_value_id');
			$table->string('name');
			$table->string('key');
			$table->string('value');
			$table->nullableTimestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('investment_value_history');
	}

}
