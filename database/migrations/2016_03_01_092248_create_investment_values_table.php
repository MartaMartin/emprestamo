<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInvestmentValuesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('investment_values', function(Blueprint $table)
		{
			$table->char('id', 36)->primary();
			$table->string('name');
			$table->string('key')->unique('key');
			$table->string('value');
			$table->nullableTimestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('investment_values');
	}

}
