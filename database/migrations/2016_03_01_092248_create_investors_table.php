<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInvestorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('investors', function(Blueprint $table)
		{
			$table->char('id', 36)->primary();
			$table->string('name');
			$table->boolean('type')->default(0);
			$table->float('percent', 10, 2)->nullable();
			$table->float('wallet', 10, 2)->nullable();
			$table->integer('num_invest')->default(0);
			$table->integer('calification')->default(5);
			$table->nullableTimestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('investors');
	}

}
