<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInvoicesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('invoices', function(Blueprint $table)
		{
			$table->char('id', 36)->primary();
			$table->char('transferor_company_id', 36)->default('')->index('transferor_company_id');
			$table->char('debtor_company_id', 36)->default('')->index('debtor_company_id')->nullable();
			$table->float('price', 10, 2)->nullable();
			$table->timestamp('expires')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->nullableTimestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('invoices');
	}

}
