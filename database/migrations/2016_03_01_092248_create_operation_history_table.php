<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOperationHistoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('operation_history', function(Blueprint $table)
		{
			$table->char('id', 36)->primary();
			$table->char('operation_id', 36);
			$table->boolean('status')->default(0);
			$table->timestamp('close_date')->nullable();
			$table->nullableTimestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('operation_history');
	}

}
