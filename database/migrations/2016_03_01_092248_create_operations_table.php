<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOperationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('operations', function(Blueprint $table)
		{
			$table->char('id', 36)->primary();
			$table->char('invoice_id', 36)->default('')->index('invoice_id');
			$table->boolean('status')->default(0);
			$table->boolean('active')->default(0);
			$table->float('purpose', 10, 2)->nullable();
			$table->float('fee', 10, 2)->nullable();
			$table->float('additional_fee', 10, 2)->nullable();
			$table->float('comision', 10, 2)->nullable();
			$table->float('received_inversor', 10, 2)->nullable();
			$table->float('received_transferor', 10, 2)->nullable();
			$table->float('received_emprestamo', 10, 2)->nullable();
			$table->float('ticket_max', 10, 2)->nullable();
			$table->float('ticket_min', 10, 2)->nullable();
			$table->timestamp('close_date')->nullable();
			$table->integer('calification')->default(5);
			$table->nullableTimestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('operations');
	}

}
