<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTransfersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('transfers', function(Blueprint $table)
		{
			$table->char('id', 36)->primary();
			$table->char('investor_id', 36)->default('')->index('inversor_id');
			$table->char('operation_id', 36)->nullable()->index('operation_id');
			$table->boolean('investor_type')->default(0);
			$table->float('quantity', 10, 2);
			$table->boolean('active')->default(0);
			$table->boolean('type')->default(0);
			$table->boolean('validate')->default(0);
			$table->nullableTimestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('transfers');
	}

}
