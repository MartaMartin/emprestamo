<?php


use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->char('id', 36)->primary();
			$table->string('name');
			$table->string('email');
			$table->string('password');
			$table->string('remember_token', 100)->nullable();
			$table->string('family_name');
			$table->char('role', 10);
			$table->char('real_id_type', 10)->default('dni');
			$table->char('real_id_number', 30)->nullable();
			$table->nullableTimestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
