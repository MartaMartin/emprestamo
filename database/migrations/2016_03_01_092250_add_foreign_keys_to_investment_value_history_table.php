<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToInvestmentValueHistoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('investment_value_history', function(Blueprint $table)
		{
			$table->foreign('investment_value_id', 'investment_value_id')->references('id')->on('investment_values')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('investment_value_history', function(Blueprint $table)
		{
			$table->dropForeign('investment_value_id');
		});
	}

}
