<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToInvoicesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('invoices', function(Blueprint $table)
		{
			$table->foreign('debtor_company_id', 'debtor_company_id')->references('id')->on('debtor_companies')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('transferor_company_id', 'transferor_company_id')->references('id')->on('transferor_companies')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('invoices', function(Blueprint $table)
		{
			$table->dropForeign('debtor_company_id');
			$table->dropForeign('transferor_company_id');
		});
	}

}
