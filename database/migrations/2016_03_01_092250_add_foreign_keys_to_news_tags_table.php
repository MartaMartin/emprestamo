<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToNewsTagsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('news_tags', function(Blueprint $table)
		{
			$table->foreign('news_id', 'news_id')->references('id')->on('news')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('tag_id', 'tag_id')->references('id')->on('tags')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('news_tags', function(Blueprint $table)
		{
			$table->dropForeign('news_id');
			$table->dropForeign('tag_id');
		});
	}

}
