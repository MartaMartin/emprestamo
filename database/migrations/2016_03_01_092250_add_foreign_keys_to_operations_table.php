<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToOperationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('operations', function(Blueprint $table)
		{
			$table->foreign('invoice_id', 'invoice_id')->references('id')->on('invoices')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('operations', function(Blueprint $table)
		{
			$table->dropForeign('invoice_id');
		});
	}

}
