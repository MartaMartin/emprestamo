<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTransfersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('transfers', function(Blueprint $table)
		{
			$table->foreign('operation_id', 'operation_id')->references('id')->on('operations')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('investor_id', 'transfers_ibfk_1')->references('id')->on('investors')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('transfers', function(Blueprint $table)
		{
			$table->dropForeign('operation_id');
			$table->dropForeign('transfers_ibfk_1');
		});
	}

}
