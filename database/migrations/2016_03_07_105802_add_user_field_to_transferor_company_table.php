<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserFieldToTransferorCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
		Schema::table('transferor_companies', function(Blueprint $table)
		{
			$table->char('user_id', 36)->after('id');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
		Schema::table('transferor_companies', function(Blueprint $table)
		{
			$table->dropColumn('user_id');
		});
    }
}
