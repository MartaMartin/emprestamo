<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFilenameFieldIntoDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
		Schema::table('documents', function(Blueprint $table)
		{
			$table->string('filename')->nullable();
			$table->string('name')->nullable();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
		Schema::table('documents', function(Blueprint $table)
		{
			$table->dropColumn('filename');
			$table->dropColumn('name');
		});
    }
}
