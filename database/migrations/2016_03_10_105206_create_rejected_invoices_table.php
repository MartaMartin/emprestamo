<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRejectedInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
		Schema::create('rejected_invoices', function(Blueprint $table)
		{
			$table->char('id', 36)->primary();
			$table->char('investor_id', 36)->default('')->index('investor_id');
			$table->char('invoice_id', 36)->default('')->index('invoice_id');
			$table->boolean('status')->default(0);
			$table->nullableTimestamps();
			$table->softDeletes();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
		Schema::drop('rejected_invoices');
    }
}
