<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyToRejeectedInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
		Schema::table('rejected_invoices', function(Blueprint $table)
		{
			$table->foreign('invoice_id', 'fk_invoice_id')->references('id')->on('invoices')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('investor_id', 'fk_investor_id')->references('id')->on('investors')->onUpdate('NO ACTION')->onDelete('NO ACTION');

		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
		Schema::table('rejected_invoices', function(Blueprint $table)
		{
			$table->dropForeign('fk_invoice_id');
			$table->dropForeign('fk_investor_id');
		});
    }
}
