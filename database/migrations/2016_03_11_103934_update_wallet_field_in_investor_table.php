<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateWalletFieldInInvestorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
		Schema::table('investors', function ($table) {
			$table->float('wallet', 10, 2)->nullable(false)->change();;
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
		Schema::table('investors', function ($table) {
			$table->float('wallet', 10, 2)->nullable()->change();;
		});
    }
}
