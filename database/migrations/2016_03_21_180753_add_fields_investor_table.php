<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsInvestorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
		Schema::table('investors', function(Blueprint $table)
		{
			$table->float('max_invest')->nullable();
			$table->float('max_days')->nullable();
			$table->float('fee')->nullable();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
		Schema::table('investors', function(Blueprint $table)
		{
			$table->dropColumn('max_invest');
			$table->dropColumn('max_days');
			$table->dropColumn('fee');
		});
    }
}
