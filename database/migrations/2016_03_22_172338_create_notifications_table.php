<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
		Schema::create('notifications', function(Blueprint $table)
		{
			$table->char('id', 36)->primary();
			$table->char('user_id', 36);
			$table->text('message', 65535);
			$table->boolean('read')->default(0);
			$table->string('url')->nullable();
			$table->text('params', 65535)->nullable();
			$table->nullableTimestamps();
			$table->softDeletes();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
		Schema::drop('notifications');
    }
}
