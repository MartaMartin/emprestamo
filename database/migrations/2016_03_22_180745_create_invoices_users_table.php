<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
		Schema::create('invoices_users', function(Blueprint $table)
		{
			$table->char('id', 36)->primary();
			$table->char('invoice_id', 36)->default('')->index('invoice_id');
			$table->char('user_id', 36)->default('')->index('user_id');
			$table->nullableTimestamps();
			$table->softDeletes();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
		Schema::drop('invoices_users');
    }
}
