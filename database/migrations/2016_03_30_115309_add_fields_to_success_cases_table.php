<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToSuccessCasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
		Schema::table('success_cases', function(Blueprint $table)
		{
			$table->string('title')->nullable();
			$table->string('company_name')->nullable();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
		Schema::table('success_cases', function(Blueprint $table)
		{
			$table->dropColumn('title');
			$table->dropColumn('company_name');
		});
    }
}
