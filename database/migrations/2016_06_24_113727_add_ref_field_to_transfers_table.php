<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRefFieldToTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('transfers', function(Blueprint $table) {
            $table->string('ref', 20)->nullable()->after('validate');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('transfers', function(Blueprint $table) {
            $table->dropColumn('ref');
        });
    }
}
