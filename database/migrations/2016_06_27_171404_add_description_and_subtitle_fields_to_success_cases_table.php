<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDescriptionAndSubtitleFieldsToSuccessCasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('success_cases', function(Blueprint $table) {
            $table->string('subtitle')->nullable();
            $table->text('description')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('success_cases', function(Blueprint $table) {
            $table->dropColumn('subtitle');
            $table->dropColumn('description');
        });
    }
}
