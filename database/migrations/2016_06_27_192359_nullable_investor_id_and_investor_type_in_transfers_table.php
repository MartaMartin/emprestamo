<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NullableInvestorIdAndInvestorTypeInTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('transfers', function(Blueprint $table) {
            $table->string('investor_id', 36)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('transfers', function(Blueprint $table) {
            // PETA!!
            $table->string('investor_id', 36)->nullable(false)->change();
        });
    }
}
