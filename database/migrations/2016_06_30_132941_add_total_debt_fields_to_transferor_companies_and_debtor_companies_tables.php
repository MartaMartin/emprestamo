<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTotalDebtFieldsToTransferorCompaniesAndDebtorCompaniesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('transferor_companies', function(Blueprint $table) {
            $table->float('total_debt')->default(0);
        });
        Schema::table('debtor_companies', function(Blueprint $table) {
            $table->float('total_debt')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transferor_companies', function(Blueprint $table) {
            $table->dropColumn('total_debt');
        });
        Schema::table('debtor_companies', function(Blueprint $table) {
            $table->dropColumn('total_debt');
        });
    }
}
