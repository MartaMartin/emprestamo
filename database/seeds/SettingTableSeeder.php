<?php

use Illuminate\Database\Seeder;
use App\Models\Setting;

class SettingTableSeeder extends Seeder {

    /**
     * Run the event table seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('settings')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

		Setting::create([
			'name' => 'Plazo de inversión no automática (horas)',
			'key' => 'no_automatic_invest_hours',
			'value' => 48
		]);
		Setting::create([
			'name' => 'Porcentaje de precio a invertir automáticamente',
			'key' => 'percent_auto_invest',
			'value' => 100
		]);
		Setting::create([
			'name' => 'Porcentaje mínimo para éxito (manual)',
			'key' => 'percent_manual_invest',
			'value' => 90
		]);
		Setting::create([
			'name' => 'Precio para mínimo de inversión',
			'key' => 'min_invest',
			'value' => 1000
		]);
		Setting::create([
			'name' => 'Comisión de apertura',
			'key' => 'opening_fee',
			'value' => 1
		]);
		Setting::create([
			'name' => 'Comisión beneficio',
			'key' => 'profit_fee',
			'value' => 2
		]);
		Setting::create([
			'name' => 'Máximo de inversión no acreditado',
			'key' => 'max_invest_not_accredited',
			'value' => 2000
		]);
		Setting::create([
			'name' => 'Máximo de inversión anual',
			'key' => 'max_invest_year',
			'value' => 1000000
		]);

	}
}
