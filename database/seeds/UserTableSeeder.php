<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Investor;
use App\Models\Transfer;
use App\Models\TransferorCompany;
use App\Models\DebtorCompany;
use App\Models\Operation;
use App\Models\Invoice;

class UserTableSeeder extends Seeder {

    /**
     * Run the event table seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('users')->truncate();
        DB::table('investors')->truncate();
        DB::table('transferor_companies')->truncate();
        DB::table('debtor_companies')->truncate();
        DB::table('transfers')->truncate();
        DB::table('invoices')->truncate();
        DB::table('operations')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

		User::create([
			'name' => 'Admin',
			'email' => 'admin@emprestamo.dev',
			'password' => bcrypt('123123'),
			'family_name' => 'Emprestamo',
			'role' => 'admin',
			'real_id_type' => 'dni',
			'real_id_number' => 'xxxxxxxx',
			'status' => '',
		]);

		User::create([
			'name' => 'Analista',
			'email' => 'analista@emprestamo.dev',
			'password' => bcrypt('123123'),
			'family_name' => 'Emprestamo',
			'role' => 'analyst',
			'real_id_type' => 'dni',
			'real_id_number' => 'xxxxxxxx',
			'status' => '',
		]);

		for ($i = 1; $i <= 40; $i ++) {
			$user = new User;
			$user->name = 'Inversor ' . $i;
			$user->email = 'inversor' . $i . '@emprestamo.dev';
			$user->password = bcrypt('123123');
			$user->family_name = 'Emprestamo';
			$user->role = 'investor';
			$user->real_id_type = 'dni';
			$user->real_id_number = 'xxxxxxxx';
			$user->status = '';
			$user->save();

			$investor = new Investor;
			$investor->user_id = $user->id;
			$investor->auto = 1;
			$investor->fee = 10;
			$investor->max_days = 90;
			$investor->type = 1;
			$investor->save();

			Transfer::create([
				'investor_id' => $investor->id,
				'quantity' => mt_rand(1, 10) * 1000,
				'active' => 1
			]);

			Investor::updateWallet($investor->id);
		}

		$user = new User;
		$user->name = 'Deudora';
		$user->email = 'deudora@emprestamo.dev';
		$user->password = bcrypt('123123');
		$user->family_name = 'Emprestamo';
		$user->role = 'debtor_company';
		$user->real_id_type = 'dni';
		$user->real_id_number = 'xxxxxxxx';
		$user->status = '';
		$user->save();

		$debtor_company = new DebtorCompany;
		$debtor_company->user_id = $user->id;
		$debtor_company->name = 'Deudora';
		$debtor_company->save();

		$user = new User;
		$user->name = 'Cedente';
		$user->email = 'cedente@emprestamo.dev';
		$user->password = bcrypt('123123');
		$user->family_name = 'Emprestamo';
		$user->role = 'transferor_company';
		$user->real_id_type = 'dni';
		$user->real_id_number = 'xxxxxxxx';
		$user->status = '';
		$user->save();

		$transferor_company = new TransferorCompany;
		$transferor_company->user_id = $user->id;
		$transferor_company->name = 'Cedente';
		$transferor_company->save();

		$invoice = new Invoice;
		$invoice->transferor_company_id = $transferor_company->id;
		$invoice->price = 200000;
		$invoice->expires = '2016-07-01';
		$invoice->name = 'Proyecto';
		$invoice->debtor_company_id = $debtor_company->id;
		$invoice->save();

		$operation = new Operation;
		$operation->invoice_id = $invoice->id;
		$operation->status = 1;
		$operation->fee = 10;
		$operation->ticket_min = 1700;
		$operation->calification = 8;
		$operation->save();

	}
}
