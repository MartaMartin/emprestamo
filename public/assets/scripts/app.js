// FUNCION ENCARGADA DE INICIAR APP
var App = function () {

	// FUNCION QUE CARGA EL PLUGIN UNIFORM (ESTILOS RADIO BUTTONS)
	var cargaUniform = function () {
		if (!jQuery().uniform) {
			return;
		}
		var unif = $('input[type=checkbox]:not(.toggle), input[type=radio]:not(.toggle, .star)');
		if (unif.size() > 0) {
			unif.each(function () {
				if ($(this).parents('.checker').size() == 0) {
					$(this).show();
					$(this).uniform();
				}
			});
		}
	};

	$.fn.isOnScreen = function(){

		var win = $(window);

		var viewport = {
			top : win.scrollTop(),
			left : win.scrollLeft()
		};
		viewport.right = viewport.left + win.width();
		viewport.bottom = viewport.top + win.height();

		var bounds = this.offset();
		bounds.right = bounds.left + this.outerWidth();
		bounds.bottom = bounds.top + this.outerHeight();

		return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));

	};

	// FUNCIÃ“N QUE PREPARA EL MENU MOVIL
	var preparaMenuMovil = function() {
		var btn = $(".navbar-toggle"),
			btnclose = $(".close", "#navbar-mobile");

		btn.on('click', function(){
			var target = $($(this).data("target"));

			if (target.hasClass('oculto')){
				target.removeClass('oculto').addClass('in');
			}else{
				target.addClass('oculto').removeClass('in');
			}
		});

		btnclose.on('click', function(){
			var target = $($(this).data("target"));
			target.addClass('oculto').removeClass('in');
		})

	};

	// FUNCION QUE PREPARA LOS PORTLETS
	var preparaPortlets = function() {
		var b = $(".panel-toggle"),
			f = $(".panel-full-screen");

		b.on("click", function(){
			$(this).parent().parent().parent().find('.portlet-body').toggleClass('collapse');
			$(this).parent().parent().parent().find('.portlet-footer').toggleClass('collapse');

			var car = $(this).find('.text-active');

			if ( car.hasClass('fa-chevron-down')){
				car.removeClass('fa-chevron-down').addClass('fa-chevron-up');
			}else{
				car.removeClass('fa-chevron-up').addClass('fa-chevron-down');
			}
		});

		f.on("click", function(){
			var port = $(this).parent().parent().parent();
			port.toggleClass("portlet-fullscreen");

			if ($(this).children().hasClass("fa-expand")) {
				$(this).children().removeClass("fa-expand").addClass("fa-compress");
			}else{
				$(this).children().removeClass("fa-compress").addClass("fa-expand");
			}

		});


	};

	// FUNCION QUE PREPARA LOS TOOLTIPS
	var ponTooltips = function() {
		if (!jQuery().tooltip) {
			return;
		}

		$('[data-toggle="tooltip"]').tooltip();
	};

	// FUNCION QUE OCULTA EL LOADER
	var removeLoader = function() {
		var l = $("#loading");
		setTimeout(function(){
			l.fadeOut(500);
		}, 500);
		$(window).on('beforeunload', function(){
			l.stop(true, true).fadeIn(200);
		});
	};

	// FUNCION QUE MUESTRA MENSAJES DINAMICOS A LOS USUARIOS, CON TITULO OPCIONAL
	// (TIPO: SUCCESS / INFO / WARNING / ERROR)
	var muestraMensajes = function(tipo,txt,tit){

		toastr.options = {
			"closeButton": true,
			"debug": false,
			"newestOnTop": true,
			"progressBar": true,
			"positionClass": "toast-top-right",
			"preventDuplicates": false,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "6000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
		};

		if (tit != undefined){
			toastr[tipo](txt, tit);
		}else{
			toastr[tipo](txt);
		}
	};

	// FUNCION QUE PREPARA LOS SELECTS
	var preparaSelect2 = function(){
		if (!jQuery().select2) {
			return;
		}
		var dis = $(".select2diss"), //se permitirá sin valor
			sel = $(".select2");  // no se permite sin valor

		dis.select2({
			allowClear: true,
			tags: true,
			minimumResultsForSearch: Infinity
		});

		sel.select2({
			tags: true,
			allowClear:false,
			minimumResultsForSearch: Infinity
		});

	};

	// FUNCION QUE PREPARA LOS DATETIMEPICKER
	var preparaCalendarios = function(){
		if (!jQuery().datetimepicker) {
			return;
		}

		var opt = {
			locale: 'es',
			format: 'YYYY-MM-DD',
			useCurrent: true,
			icons: {
				time: 'fa fa-clock',
				date: 'fa fa-calendar',
				up: 'fa fa-chevron-up',
				down: 'fa fa-chevron-down',
				previous: 'fa fa-chevron-left',
				next: 'fa fa-chevron-right',
				today: 'fa fa-screenshot',
				clear: 'fa fa-trash',
				close: 'fa fa-remove'
			}
		};

		//sin limites de fechas
		var cal = $(".calendario"),
			callimitmax = $(".calendariolimitmax"),
			callimitmin = $(".calendariolimitmin");

		if (cal.length > 0){
			cal.datetimepicker(opt);
		}

		//fecha máxima hoy
		if (callimitmax.length > 0){
			opt.maxDate = 'now';
			callimitmax.datetimepicker(opt);
		}

		//fecha minima hoy
		if (callimitmin.length > 0){
			opt.minDate = 'now';
			callimitmin.datetimepicker(opt);
		}
	};

	return {

		// FUNCION PARA INICIALIZAR LA PLANTILLA
		init: function () {
			cargaUniform();
			preparaMenuMovil();
			preparaPortlets();
			ponTooltips();
			removeLoader();
			preparaSelect2();
			preparaCalendarios();
		},
		uniform: function() {
			cargaUniform();
		},
		ui_msg: function(tipo, txt,titulo){
			muestraMensajes(tipo, txt,titulo);
		}
	};
}();
