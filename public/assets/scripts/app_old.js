// FUNCION ENCARGADA DE INICIAR Pagina home
var App = function () {

	// FUNCION QUE CARGA EL PLUGIN UNIFORM (ESTILOS RADIO BUTTONS)
	var cargaUniform = function () {
		if (!jQuery().uniform) {
			return;
		}
		var unif = $('input[type=checkbox]:not(.toggle), input[type=radio]:not(.toggle, .star)');
		if (unif.size() > 0) {
			unif.each(function () {
				if ($(this).parents('.checker').size() == 0) {
					$(this).show();
					$(this).uniform();
				}
			});
		}
	};

	$.fn.isOnScreen = function(){

		var win = $(window);

		var viewport = {
			top : win.scrollTop(),
			left : win.scrollLeft()
		};
		viewport.right = viewport.left + win.width();
		viewport.bottom = viewport.top + win.height();

		var bounds = this.offset();
		bounds.right = bounds.left + this.outerWidth();
		bounds.bottom = bounds.top + this.outerHeight();

		return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));

	};

	// PREPARAMOS EL EFECTO DEL PORCETAJE
	var preparaScroll = function() {

		var por = $('.porcent'),
			cont = 0;

		$(window).bind('scroll', function() {
			if (por.isOnScreen && cont == 0) {
				por.each(function () {
					$(this).prop('Counter', 0).animate({
						Counter: $(this).text()
					}, {
						duration: 2000,
						easing: 'swing',
						step: function (now) {
							$(this).text(Math.ceil(now));
							cont++;
						}
					});
				});
			}
		});
	};

	// FUNCIÓN QUE PREPARA EL MENU MOVIL
	var preparaMenuMovil = function() {
		var btn = $(".navbar-toggle"),
			btnclose = $(".close", "#navbar-mobile");

		btn.on('click', function(){
			var target = $($(this).data("target"));

			if (target.hasClass('oculto')){
				target.removeClass('oculto').addClass('in');
			}else{
				target.addClass('oculto').removeClass('in');
			}
		});

		btnclose.on('click', function(){
			var target = $($(this).data("target"));
			target.addClass('oculto').removeClass('in');
		})

	};

	// FUNCION QUE PREPARA LA CARGA DE MÁS NOTICIAS EN EL BLOG
	var preparaBtnNoticias = function() {
		var b = $("#blog"),
			btn = $("#loadMoreNews");
			/*cont = $("#contNot"),*/


		if (!b.length > 0) {
			return;
		}else{
			btn.on("click", function(){
				var out = '';

				/*$.ajax({
					url: "test.html",
					context: document.body
				}).done(function() {
					$( this ).addClass( "done" );
				});*/

				$(this).addClass("disabled");

				var res = [
					{
						"titulo":		"Titulo de la noticia nueva",
						"categoria":	"Categoria",
						"fecha":		"02.03.2016",
						"img":			"/assets/img/testimonial/testimonial-1.jpg"
					},
					{
						"titulo":		"Titulo de la noticia nueva",
						"categoria":	"Categoria",
						"fecha":		"02.03.2016",
						"img":			"/assets/img/testimonial/testimonial-3.jpg"
					},
					{
						"titulo":		"Titulo de la noticia nueva",
						"categoria":	"Categoria",
						"fecha":		"02.03.2016",
						"img":			"/assets/img/testimonial/testimonial-3.jpg"
					},
					{
						"titulo":		"Titulo de la noticia nueva",
						"categoria":	"Categoria",
						"fecha":		"02.03.2016",
						"img":			"/assets/img/testimonial/testimonial-2.jpg"
					},
					{
						"titulo":		"Titulo de la noticia nueva",
						"categoria":	"Categoria",
						"fecha":		"02.03.2016",
						"img":			"/assets/img/testimonial/testimonial-3.jpg"
					},
					{
						"titulo":		"Titulo de la noticia nueva",
						"categoria":	"Categoria",
						"fecha":		"02.03.2016",
						"img":			"/assets/img/testimonial/testimonial-1.jpg"
					},
					{
						"titulo":		"Titulo de la noticia nueva",
						"categoria":	"Categoria",
						"fecha":		"02.03.2016",
						"img":			"/assets/img/testimonial/testimonial-1.jpg"
					},
					{
						"titulo":		"Titulo de la noticia nueva",
						"categoria":	"Categoria",
						"fecha":		"02.03.2016",
						"img":			"/assets/img/testimonial/testimonial-3.jpg"
					},
					{
						"titulo":		"Titulo de la noticia nueva",
						"categoria":	"Categoria",
						"fecha":		"02.03.2016",
						"img":			"/assets/img/testimonial/testimonial-2.jpg"
					}
				];

				//console.log(res);

				$.each(res, function(index, value){
					//alert(value.titulo);
					out += '	<div class="col-md-4 margBot40">'+
								'	<a href="http://emprestamo.lv/blog/view"><div style="background: url('+value.img+') no-repeat center center / cover" class="imagenmed"></div></a>'+
								'	<div class="latobold12 margTop30"><span class="naranja text-uppercase">'+value.categoria+'</span> <span class="negro">| '+value.fecha+'</span></div>'+
								'	<div class="latoligth34 margTop20"><a class="negro" href="http://emprestamo.lv/blog/view">'+value.titulo+'</a></div>'+
								'</div>';
				});

				/*cont.append(out);*/
				$(out).hide().appendTo("#contNot").fadeIn(1000);
				$(this).removeClass("disabled");
			});
		}
	};

	return {

		// FUNCION PARA INICIALIZAR LA PLANTILLA
		init: function () {
			cargaUniform();
			preparaMenuMovil();
			preparaBtnNoticias();
		}
	};
}();