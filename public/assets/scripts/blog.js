// FUNCION ENCARGADA DE INICIAR Pagina home
var Blog = function () {

	var preparaSocial = function () {
		if (!jQuery().jsSocials){
			return;
		}

		$("#social").jsSocials({
			showLabel: false,
			showCount: false,
			shares: ["email", "twitter", "facebook", "googleplus", "linkedin", "pinterest", "whatsapp"]
		});
	};

	return {

		// FUNCION PARA INICIALIZAR LA PLANTILLA
		init: function () {
			preparaSocial();
		}
	};
}();