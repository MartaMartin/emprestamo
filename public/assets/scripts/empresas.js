// FUNCION ENCARGADA DE INICIAR Pagina empresas
var Empresas = function () {

	//creamos un array con los porcetnajes que se aplican segun el dia financiacion
	var porcentajes = {
		"30"	: 2.54,
		"60"	: 3.83,
		"90"	: 5.09,
		"120"	: 6.48,
		"150"	: 7.75,
		"180"	: 8.08
	};


	var sldr = $("#slidr"), 	// elemento que mostrará el el slider
		hasta = $("#hasta"),	// elemento que mostrará el valor que se le dará (hasta X.XXX €)
		coste = $("#coste");	// elemento que mostrará el valor de coste

	//variables para almacenar variables y resultados de los calculos
	var sldrval, hastaval, costeval;

	// FUNCION QUE PREPARA EL SLIDER DE LA CALCULADORA (creo el slider)
	var preparaSlider = function () {
		//si no hay slider, salimos
		if (!jQuery().ionRangeSlider) {
			return;
		}

		//llamada al plguin para crear el slider
		sldr.ionRangeSlider({
			min: 1000,
			max: 9000,
			/*type: 'double',*/
			postfix: " €",
			decorate_both: false,
			step: 1000,
			grid: false,
			onStart: function (data) {
				//ponemos el valor del slider a 1000eu por defecto
				sldrval = 1000;
				//cogemos valor de dias (por defecto pusimos 30 en el html)
				var d = $("#dias").val();
				//calculamos el coste (será la inicial - al cargar pagian)
				calcula(sldrval, d);
			},
			onChange: function (data) {
				//sacamos  el valor del slider
				sldrval = data.from;
				//sacamos valor de los dias
				var d = $("#dias").val();
				//calculamos
				calcula(sldrval, d);
			},
			onFinish: function (data) {
				console.log("onFinish");
			},
			onUpdate: function (data) {
				console.log("onUpdate");
			}
		});
	};

	// FUNCION ENCARGADA DE CALCULAR LOS COSTES (m=dinero, d=dias
	var calcula = function(m, d) {
		hastaval = m*0.9;
		hasta.text(hastaval+ " €");

		costeval = (m*porcentajes[d])/100;
		coste.text(costeval+ " €")
	};

	// FUNCION PARA ACCIONES Y ESTILOS DE BOTONES DIAS (pone valores en input hidden dias)
	var preparaCalculadora = function(){

		//preparamos el slider
		preparaSlider();

		var btns = $(".btn","#minicalc"),
			firstbtn = $(".btn:first-child", "#minicalc"),
			dias = $("#dias");

		// activamos el primer boton
		firstbtn.button("toggle");

		//acciones cuando hacen click en el boton dia
		btns.on("click", function() {
			btns.each(function(){
				$(this).removeClass('active');
			});
			$(this).addClass('active');
			d = $(this).data("dias");
			dias.val(d);
			calcula(sldrval, d);
		});
	};

	return {

		// FUNCION PARA INICIALIZAR LA PLANTILLA
		init: function () {
			preparaCalculadora();
		}
	};
}();