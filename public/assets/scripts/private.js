// FUNCION ENCARGADA DE INICIAR AREA PRIVADA
var Private = function () {

	// var datosDemoDonut = [
	// 	['Inversiones', 95],['Beneficios', 5]
	// ];

	// FUNCION QUE PREPARA LOS CONTADORES
	var preparaCounters = function() {
		if (!jQuery().counterUp){
			return;
		}

		$('.counter').counterUp({
			delay: 10,
			time: 1500
		});
	};

	// FUNCION QUE PREPARA EL ESTILO DE LOS FILEINPUT
	var preparaUploadFiles = function() {
		if (!jQuery().fileinput) {
			return;
		}

		$('.fileupload').fileinput({
			// Uncomment the following to send cross-domain cookies:
			//xhrFields: {withCredentials: true},
			//url: 'assets/plugins/jquery-file-upload/server/php/'

		});
	};

	// FUNCION QUE MONTA LA GRAFICA DEL DONUT
	var preparaDonut = function(id, datosDonut, showLegend) {

		if (!$('#' + id).length) {
			return;
		}

		if (!jQuery().jqplot){
			return;
		}

		var btn = $(".jqplot-image-button");

        var colors = [];
        for (var i in datosDonut) {
            colors.push(datosDonut[i][2]);
        }

		var optionsDonut = {
			"responsive": {
				"enabled": true
			},
			animate: true,
			animateReplot: true,
			grid: {
				borderColor: 'transparent',
				borderWidth: 0,
				shadow: false,
				shadowAngle: 0,
				drawBorder: false,
				shadowColor: 'transparent',
				background: 'transparent'
			},
			legend: {
				show: showLegend, //location: ‘ne’, // compass direction, nw, n, ne, e, se, s, sw, w.
				fontSize: '12px',
				/*xoffset: 12, // pixel offset of the legend box from the x (or x2) axis.
				yoffset: 12 // pixel offset of the legend box from the y (or y2) axis.*/
			},
			seriesColors: colors,
			seriesDefaults: {
				// make this a donut chart.
				renderer:$.jqplot.DonutRenderer,
				rendererOptions:{
					// Donut's can be cut into slices like pies.
					sliceMargin: 3,
					// Pies and donuts can start at any arbitrary angle.
					startAngle: -90,
					showDataLabels: true,
					// By default, data labels show the percentage of the donut/pie.
					// You can show the data 'value' or data 'label' instead.
					dataLabels: 'value',
					// "totalLabel=true" uses the centre of the donut for the total amount
					dataLabelFormatString: '%s'+'%',
					totalLabel: false
				}
			}
		};

		var demodonut = $.jqplot(id, [datosDonut], optionsDonut);

		// CONVERTIMOS EL GRAFICO EN IMAGEN POR SI SE LO QUIEREN BAJAR
		var graphicImage = $('#imagenGraf');
		if(graphicImage.html() == ""){ //If the image was even generated, don't generate it again
			var divGraph = $('#' + id).jqplotToImageStr({});
			var divElem  = $('<img/>').attr('src', divGraph);
			graphicImage.html(divElem);

			btn.attr("href",divGraph).attr("target",'_blank');

		}

/*		btn.on("click", function(){
			//open(divGraph.toDataURL("image/png"));
			window.open(divGraph.toDataURL());
			graphicImage.toggleClass("hidden");
		});*/
	};

	// FUNCION QUE PREPARA EL CLICK DEL CHECKBOX DE LAS TABLAS
	var preparaCheckTablas = function () {

		var cs = $("#tblmsj .group-checkable"),
			bd = $("#deletetblmsj");

		cs.change(function () {
			var set = $(this).attr("data-set");
			var checked = $(this).is(":checked");
			$(set).each(function () {
				if (checked) {
					$(this).attr("checked", true);
					$(this).parent().addClass('checked');
				} else {
					$(this).attr("checked", false);
					$(this).parent().removeClass('checked');
				}
				//$(this).parents('tr').toggleClass("active");
			});
			//$.uniform.update();
			App.uniform();

		});

		bd.on("click", function(){
			var ref = $(this).data("ref");

			$(ref+" tbody span.checked input").each(function(indice, valor){
				var id = $(this).data("idmsj");
				alert(id);
			});
		});

	};

	// FUNCION QUE MONTA LA TABLA DE LAS GRAFICAS
	var preparaGrafica = function (datosGrafica) {
		var d = $("#chartdiv");
		AmCharts.translations[ "export" ][ "es" ] = {
			"fallback.save.text": "CTRL + C to copy the data into the clipboard.",
			"fallback.save.image": "Rightclick -> Save picture as... to save the image.",

			"capturing.delayed.menu.label": "{{duration}}",
			"capturing.delayed.menu.title": "Cancelar",

			"menu.label.print": "Imprimir",
			"menu.label.undo": "Deshacer",
			"menu.label.redo": "Rehacer",
			"menu.label.cancel": "Cancelar",

			"menu.label.save.image": "Descargar como ...",
			"menu.label.save.data": "Guardar como ...",

			"menu.label.draw": "Anotar ...",
			"menu.label.draw.change": "Cambiar ...",
			"menu.label.draw.add": "Añadir ...",
			"menu.label.draw.shapes": "Forma ...",
			"menu.label.draw.colors": "Color ...",
			"menu.label.draw.widths": "Tamaño ...",
			"menu.label.draw.opacities": "Opacidad ...",
			"menu.label.draw.text": "Texto",

			"menu.label.draw.modes": "Modo ...",
			"menu.label.draw.modes.pencil": "Pincel",
			"menu.label.draw.modes.line": "Linea",
			"menu.label.draw.modes.arrow": "Flecha",

			"label.saved.from": "Guardar desde: "
		};

		var chart = AmCharts.makeChart("chartdiv", {
			dataProvider: datosGrafica,
			labelPosition: 'left',
			language: 'es',
			type: "serial",
			theme: "light",
			autoDisplay:true,
			handDrawn:false,
			autoResize:true,
			sequencedAnimation: true,
			startAlpha: 0.5,
			startEffect: "easeOutSine",
			addClassNames: true,
			startDuration: 1,
			creditsPosition : 'bottom-left',
			fontFamily: "Lato",
			/*dataDateFormat: "YYYY-MM-DD HH:NN",*/
			dataDateFormat: "DD/MM/YYYY HH:NN",
			categoryField: "date",
			numberFormatter: {
				precision: 2,
				decimalSeparator: ',',
				thousandsSeparator: '.'
			},
			graphs: [{
				id: "wallet",
				category: 'cartera',
				labelPosition: 'left',
				animationPlayed: true,
				alphaField: "alpha",
				fillAlphas: 0,
				fontSize:'16',
				balloonText: "<h5>[[nomProyecto]]</h5><h3>[[trans]]</h3><h5>Total: [[total]]€</h5><div class='clr text-center'>----</div><small>[[date]]</small>",
				bullet: "round",
				bulletBorderAlpha: 1,
				useLineColorForBulletBorder: false,
				labelText: "[[cantidad]]€",
				dashLengthField: "dashLength",
				legendPeriodValueText: "", //"total: [[value.sum]] €",
				legendValueText: "[[value]] €",
				/*title: "cantidad",*/
				/*type: "column",*/
				valueField: "cantidad",
				valueAxis: "distanceAxis €",
				valuePosition: 'left'
			}],
			chartScrollbar: {
				oppositeAxis: false,
				offset: 30
			},
			chartCursor: {
				categoryBalloonDateFormat: "EEEE, DD de MMMM de YYYY",
				cursorAlpha: 0.5,
				pan: true,
				cursorColor:"#323232",
				fullWidth:false,
				valueBalloonsEnabled: true,
				zoomable: true,
				parseDates: true,
				valueZoomable:false
			},
			categoryAxis: {
				autoGridCount:true,
/*				autoWrap:true,*/
				axisAlpha: 0.1,
				axisColor:'#481b65',
				equalSpacing:true,
				minorGridEnabled: true,
				centerLabels:true,
				gridAlpha: 0.1,
				title: "Fechas de movimientos",
				gridPosition:"top",
				parseDates: true,
				twoLineMode:true,
				autoRotateCount:2,
				autoRotateAngle: -90/*,
				dateFormats: [{
					period: "fff",
					format: "DD-MM-YYYY"
				},{
					period: "ss",
					format: "DD-MM-YYYY"
				},{
					period: "mm",
					format: "DD-MM-YYYY"
				}, {
					period: "DD",
					format: "DD-MM-YYYY"
				}, {
					period: "MM",
					format: "DD-MM-YYYY"
				}, {
					period: "YYYY",
					format: "DD-MM-YYYY"
				}]
				autoWrap:true,
				gridPosition:"start",
				equalSpacing:true,
				parseDates: true,
				autoGridCount: false,
				axisColor: "#555555",
				gridAlpha: 0.05,
				gridColor: "#FFFFFF",
				gridCount: 1,
				fillAlpha:1,
				title: "Fechas de movimientos",
				dateFormats: [{
					period: "fff",
					format: "DD / MM / YYYY"
				},{
					period: "ss",
					format: "DD / MM / YYYY"
				},{
					period: "mm",
					format: "DD / MM / YYYY"
				}, {
					period: "DD",
					format: "DD / MM / YYYY"
				}, {
					period: "MM",
					format: "DD / MM / YYYY"
				}, {
					period: "YYYY",
					format: "DD / MM / YYYY"
				}]*/
			},

			export: {
				enabled: true,
				drawing: {
					shapes: false,
					modes:false
				}
			},
			valueAxes: [{
				id: "distanceAxis",
				axisAlpha: 0.1,
				gridAlpha: 0.1,
				position: "left",
				unit:"€"
			}],
			legend: {
				enabled:false,
				equalWidths: true,
				useGraphSettings: true,
				valueAlign: "left",
				valueWidth: 120
			},
			responsive: {
				"enabled": true
			}
		});

		chart.addListener("rendered", zoomChart);

		zoomChart();

		function zoomChart() {
			chart.zoomToIndexes(chart.dataProvider.length - 120, chart.dataProvider.length - 1);
		}
	};

	var cloneItems = function() {
		$('#clone').on('click', function() {
			var num = $('#dynamic_list > .clon').length + 1;
			var new_element = $('#clone_me').clone().attr('id', 'clone_me' + num);
			//console.log(new_element);
			new_element.html(new_element.html().replace(/1º/g, num + 'º')).removeClass("hidden").find('input').removeClass("novalidate");
			$('#dynamic_list').append(new_element);
			return false;
		});
		$('body').on('click', '.remove_item', function() {
			if ($(this).attr('item') != '1º') {
				$(this).closest('.clon').remove();
			} else {
				// alert('El primero no se puede eliminar');
			}
			return false;
		});
	};

	return {

		// FUNCION PARA INICIALIZAR LA PLANTILLA
		init: function () {
			preparaCounters();
			preparaUploadFiles();
			preparaCheckTablas();
			cloneItems();
			if (typeof datosGrafica != 'undefined') {
				preparaGrafica(datosGrafica);
			}
		},
        donuts: function() {
            preparaDonut('invested_donut', invested_data, false)
            preparaDonut('fee_donut', fee_data, false)
            preparaDonut('returned_donut', returned_data, false)
            preparaDonut('earned_donut', earned_data, false)
        }
	};
}();
