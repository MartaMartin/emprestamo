var validateCore = function(frm, reglas, msjs, ignora){
	frm.validate({
		errorClass: 'invalid',
		errorElement: 'span',
		rules: reglas,
		messages: msjs,
		ignore:ignora,
		/*onfocusout: false,
		focusCleanup: true,*/
		submitHandler: function(form) {
			form.submit();
		},
/*		showErrors: function(errorMap, errorList) {
			$("#modulos").html("Your form contains "
				+ this.numberOfInvalids()
				+ " errors, see details below.");
			this.defaultShowErrors();
		},*/
		highlight: function(element, errorClass) {
			$(element).fadeOut(function() {
				$(element).fadeIn().addClass(errorClass);
			});
		},
		invalidHandler: function(event, validator) {
			var errors = validator.numberOfInvalids(),
				errorstxt = 'errores',
				errorsmsg = 'Por favor, corriga los erorres para poder continuar.';
			if (errors) {
				if (errors == 1){
					errorstxt = 'error';
					errorsmsg = 'Por favor, corriga el error para poder continuar.';
				}
				App.ui_msg('error', errorsmsg, 'Tiene ' + errors + ' '+errorstxt);
			}
		},
		errorPlacement: function(error, element) {
			if (element.parent().is('.input-group')){
				error.insertAfter(element.parent());
			}else if (element.is(':checkbox')){
				error.appendTo(element.parent().closest('.form-group'));
			}else{
				error.appendTo(element.parent());
			}
		}
	});
};

var validateReg = function(){
	frm = $("#frmReg");
	var reglas = {
		name: {
			required: true,
			alfanumaccents:true
		},
		family_name:{
			required: true,
			alfanumaccents:true
		},
		email:{
			required: true,
			email:true
		},
		password:{
			required:true
		},
		password_confirmation:{
			required: true,
			equalTo: "#password"
		}
	};

	var msjs = {};

	validateCore(frm, reglas, msjs, []);
};

var validateInvWallet = function() {
	frm = $("#frmInvWallet");
	var reglas = {
		quantity: {
			required: true,
			dinerodecimal:true
		}
	};

	var msjs = {};

	validateCore(frm, reglas, msjs, []);

};

var validateCompanyInvoices = function(){
	if (!jQuery().validate){
		return;
	}
	var frm = $("#frmCompInv");
	var reglas = {
		name:{
			required: true,
			alfanumaccents:true
		},
		price: {
			required: true,
			dinerodecimal:true
		},
		expires:{
			required: true,
			fechabbdd: true
		},
		transferor_company_name:{
			required: true,
			alfanumaccents:true
		},
		"names[]":{
			alphanumeric:true
		}
	};
	var msjs = {};


	validateCore(frm, reglas, msjs, '.novalidate');
};

var validateCompanyProfile = function(){
	if (!jQuery().validate){
		return;
	}
	var frm = $("#frmCompProf");
	var reglas = {
		family_name: {
			required: true,
			alfanumaccents:true
		},
		name:{
			required: true,
			alfanumaccents:true
		},
		email:{
			required: true,
			email: true
		},
		transferor_company_name:{
			required: true,
			alfanumaccents:true
		}
	};
	var msjs = {
		family_name: {
			required: "Campo Obligatorio",
			alfanumaccents: "Solo carácteres alfanuméricos y con acentos"
		},
		name:{
			required: "Campo Obligatorio",
			alfanumaccents: "Solo carácteres alfanuméricos y con acentos"
		},
		email:{
			required: "Campo Obligatorio",
			email: "Formato Incorrecto"
		},
		transferor_company_name:{
			required: "Campo Obligatorio",
			alfanumaccents: "Solo carácteres alfanuméricos y con acentos"
		}
	};

	validateCore(frm, reglas, msjs, []);
};

var validateInvestorProfile = function() {
	if (!jQuery().validate){
		return;
	}
	var frm = $("#frmInvProf");
	var reglas = {
		family_name: {
			required: true,
			alfanumaccents:true
		},
		name:{
			required: true,
			alfanumaccents:true
		},
		email:{
			required: true,
			email: true
		}
	};
	var msjs = {
		family_name: {
			required: "Campo Obligatorio",
			alfanumaccents: "Solo carácteres alfanuméricos y con acentos"
		},
		name:{
			required: "Campo Obligatorio",
			alfanumaccents: "Solo carácteres alfanuméricos y con acentos"
		},
		email:{
			required: "Campo Obligatorio",
			email: "Formato Incorrecto"
		}
	};

	validateCore(frm, reglas, msjs, []);
};



