@section('pagina','login')								{{-- id de la pagina para el body --}}
@section('title','Accede a tu cuenta')                  {{-- titulo de la pagina --}}
@section('login_act','active')                          {{-- activacion menu top --}}
@section('content')                                     {{-- contendido pagina --}}
  <div id="sliderlogin" class="animatedParent animateOnce">
    <div class="col-md-6 full-height hidden-sm hidden-xs animated fadeIn" style="background: url(/assets/img/extra/login.jpg) no-repeat center center / cover"></div>
      <div class="col-md-6 col-sm-12 col-xs-12 full-height bg-morado-light formside animated fadeIn">
        <div class="row">
          <div class="col-md-7">
            <div class="latoligth34 margTop30">Bienvenido/a</div>
            <h1 class="black blanco text-uppercase margTop5">inicia sesión</h1>

            {{-- BEGIN FORM LOGIN --}}
            {!! Form::open([
              'url' => 'auth/login',
              'role' => 'form'
            ]) !!}

              <div class="form-group margTop30">
                {!! Form::text('email', '', ['id' => 'email', 'class' => 'form-control', 'placeholder' => 'Correo Electrónico']) !!}
              </div>

              <div class="form-group">
                {!! Form::password('password', ['id'=>'password', 'class' => 'form-control', 'placeholder' => 'Contraseña']) !!}
              </div>

              <div class="latoligth15">¡Vaya! <a href="{{ url('password/email') }}" class="dotted negro">He olvidado mi contraseña</a></div>

              <div class="clr margTop20"></div>

              {!! Form::submit('entrar', ['class' => 'btn noradio bg-naranja text-uppercase blanco alargado']) !!}

              <div class="clr"></div>

            {!! Form::close() !!}
            {{-- END FORM LOGIN --}}

            <div class="latoligth21 margTop60">¿No tienes cuenta?</div>
            <a href="#" data-target="#modal-reg" data-toggle="modal" class="btn btn-transparente sinradio morado text-uppercase latoblack12 margTop10">regístrate ahora</a>
          </div>
        </div>
      </div>
  </div>
@endsection

@extends('frontend.layouts.default')


<!--
  <section class="myc-intranet-panel clearfix">
    <h3 class="panel-title">Login</h3>
    <hr class="myc-hr-separator">
    <div class="row">
      <div class="col-sm-4 panel-picto-wrapper">
        <img src="{{--{{asset('img/intranet-picto-registro.svg')}}--}}">
      </div>
      <div class="col-sm-8">
    -->
{{--        {!! Form::open([
          'url' => 'auth/login',
          'class' => 'myc-form'
        ]) !!}--}}

          {{--{!! csrf_field() !!}--}}
<!--
          <div class="form-group">
            <label for="email">E-mail</label>
            <input type="email" class="form-control" name="email" value="{{--{{ old('email') }}--}}">
          </div>

          <div class="form-group">
            <label for="password">Contraseña</label>
            <input type="password" class="form-control" name="password" id="password">
          </div>

          <div class="checkbox">
            <label>
              <input type="checkbox" name="remember"> Recordar
            </label>
          </div>

          <div class="form-group">
            <button class="btn myc-btn-intranet pull-right" type="submit">Login</button>
            <a href="{{--{{ url('password/email') }}--}}" class="pull-left">He olvidado mi contraseña</a>
          </div>
-->
        {{--{!! Form::close() !!}--}}
<!--
      </div> {{-- /.col-sm-8 --}}
    </div> {{-- /.row --}}
  </section>
  -->



