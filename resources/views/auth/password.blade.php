{{-- Specify which layout this page inherits --}}
@extends('frontend.layouts.intranet')

{{-- Section title --}}
@section('title', _('Enviar contraseña por email'))

{{-- Section body classes --}}
@section('body-classes', 'page page-intranet reset-password')

@section('content')

  <section class="myc-intranet-panel clearfix">
    <h3 class="panel-title">Enviar contraseña por email</h3>
    <hr class="myc-hr-separator">
    <div class="row">
      <div class="col-sm-4 panel-picto-wrapper">
        <img src="{{asset('img/intranet-picto-registro.svg')}}"> 
      </div>
      <div class="col-sm-8">
        
        {!! Form::open([
          'url' => 'password/email',
          'class' => 'myc-form'
        ]) !!}

          {!! csrf_field() !!}

          <div class="form-group">
            <label for="email">E-mail</label>
            <input type="email" class="form-control" name="email" value="{{ old('email') }}">
          </div>

          <div class="form-group">
            <button class="btn myc-btn-intranet pull-right" type="submit">Send Password Reset Link</button>
          </div>

        {!! Form::close() !!}

      </div> {{-- /.col-sm-8 --}}
    </div> {{-- /.row --}} 
  </section>  

@endsection
