@section('pagina','reg-emp')								{{-- id de la pagina para el body --}}
@section('title','Registro Empresas')           			{{-- titulo de la pagina --}}
@section('reg_act','active')                    			{{-- activacion menu top --}}
@section('js_pagina','validateReg();')						{{--VALIDATION--}}
@section('content')                               			{{-- contendido pagina --}}
<div class="section bg-amarillo-claro" id="regempresa">
	<div class="text-center">
		<div class="latoligth34 margTop30">Crea tu cuenta de</div>
		<h1 class="black blanco text-uppercase margTop5">empresa</h1>
	</div>
	<div class="container">
		<div class="col-md-4 col-md-push-4">
			{{-- form start --}}
			{!! Form::open([
				'id' => 'frmReg',
				'route' => 'auth.register.postRegister',
				'role' => 'form'
				])
			!!}
				{!! Form::hidden('role', 'transferor_company') !!}

				<div class="form-group margTop30">
					<input type="text" class="form-control" name="name" id="nombre" placeholder="Nombre" required>
				</div>
				<div class="form-group">
					<input type="text" class="form-control" name="family_name" id="apellidos" placeholder="Apellidos" required>
				</div>
				<div class="form-group">
					<input type="email" class="form-control" name="email" id="email" placeholder="Correo Electrónico" required>
				</div>
				<div class="form-group">
					<input type="password" class="form-control" name="password" id="password" placeholder="Contraseña" required>
				</div>
				<div class="form-group">
					<input type="password" class="form-control" name="password_confirmation" id="confirm" placeholder="{{ _('Confirma contraseña') }}" required>
				</div>
				<div class="form-group">
					<input required type="checkbox" class="form-control pull-left" name="pcu" id="pcu"> Acepto la <a class="dotted negro" href="#">Política de Privacidad y Condiciones</a>
				</div>
				<div class="clr margTop20"></div>
				<div class="text-center margTop40">
					<button type="submit" class="btn noradio bg-amarillo text-uppercase blanco alargado">continuar</button>
					<div class="clr"></div>
					<div class="latoligth21 margTop60">Si tienes ya una cuenta</div>
					<a class="btn btn-transparente sinradio morado text-uppercase latoblack12 margTop10" href="{{ route('auth.login') }}">entra</a>
				</div>
			{!! Form::close() !!}{{-- /.form --}}
		</div>
	</div>
</div>
@endsection

@extends('frontend.layouts.default')

