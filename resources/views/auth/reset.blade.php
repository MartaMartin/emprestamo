{{-- Specify which layout this page inherits --}}
@extends('frontend.layouts.intranet')

{{-- Section title --}}
@section('title', _('Cambiar contraseña'))

{{-- Section body classes --}}
@section('body-classes', 'page page-intranet change-password')

@section('content')

  <section class="myc-intranet-panel clearfix">
    <h3 class="panel-title">Cambiar contraseña</h3>
    <hr class="myc-hr-separator">
    <div class="row">
      <div class="col-sm-4 panel-picto-wrapper">
        <img src="{{asset('img/intranet-picto-registro.svg')}}"> 
      </div>
      <div class="col-sm-8">
        
        {!! Form::open([
          'url' => 'password/reset',
          'class' => 'myc-form'
        ]) !!}

          {!! csrf_field() !!}
          <input type="hidden" name="token" value="{{ $token }}">

          @if (count($errors) > 0)
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
          @endif

          <div class="form-group">
            <label for="email">E-mail</label>
            <input type="email" class="form-control" name="email" value="{{ old('email') }}">
          </div>

          <div class="form-group">
            <label for="password">Contraseña</label>
            <input type="password" class="form-control" name="password">
          </div>

          <div class="form-group">
            <label for="password_confirmation">Repite la contraseña</label>
            <input type="password" class="form-control" name="password_confirmation">
          </div>

          <div class="form-group">
            <button class="btn myc-btn-intranet pull-right" type="submit">Reset password</button>
          </div>

        {!! Form::close() !!}

      </div> {{-- /.col-sm-8 --}}
    </div> {{-- /.row --}} 
  </section>  

@endsection