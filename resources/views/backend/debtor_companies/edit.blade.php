@extends('backend.layouts.dashboard')

@section('content')

    {{-- //////// News title ///////// --}}
    <?php $page_title = 'Editar empresa deudora ' . $user->name;  ?>
    {{-- //////// News title ///////// --}}

    {{-- *** Create debtor_companies row *** --}}
    <div class="row">

        <div class="col-xs-12">

            <div class="box box-primary">

                {{-- form start --}}
				{!! Form::model($debtor_company,
					[
					'method' => 'PUT',
					'route' => [
						'admin.debtor_companies.update',
						$debtor_company->id
					],
					'role' => 'form'
					])
                !!}

                {!! Form::hidden('password', 'pass_inicial') !!}

                <div class="box-body">

                    <div class="row">

                        <div class="col-md-12">

							<div class="form-group">

								{!! Form::label('name', _('Nombre de la empresa')) !!}
								{!! Form::text('name', $debtor_company->name,
									[
										'class' => 'form-control',
										'placeholder' => _('Nombre de la empresa'),
										'required'
									])
								!!}

							</div>{{-- /.form-group --}}

						</div>{{-- /.col-md-12 --}}

                    </div>{{-- /.row --}}

                </div>{{-- /.box-body --}}

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Actualizar empresa deudora</button>
                </div>{{-- /.box-footer --}}

                {!! Form::close() !!}{{-- /.form --}}

            </div>{{-- /.box --}}

			<a class="btn btn-danger" href="{{ route('admin.users.index') }}"><i class="fa fa-chevron-circle-left"></i> {{ _('Volver') }}</a>

        </div>{{-- /.col --}}

    </div>{{-- /.row --}}
    {{-- /*** Create debtor_companies row *** --}}

@endsection
