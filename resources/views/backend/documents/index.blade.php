@extends('backend.layouts.dashboard')

@section('content')

    {{-- //////// News title ///////// --}}
    <?php $page_title = 'Listado de documentos de ' . $user->name;  ?>
    {{-- //////// News title ///////// --}}

    <div class="row">

        <div class="col-xs-12">

            <div class="box">

                <div class="box-header">
                    <h3 class="box-title">{{ _('Documentos') }}</h3>
                </div>{{-- /.box-header --}}

                <div class="box-body">

                    {!! $documents->render() !!}

                    <table id="documents" class="table table-bordered table-striped data-table">

                        <thead>

                        <tr>
                            <th>Nombre</th>
                            <th class="no-sort">Acciones</th>
                        </tr>

                        </thead>

                        <tbody>

                        @foreach($documents as $document)
                            <tr>
                                <td>{{ $document->name }}</td>
                                <td class="text-right">
                                    @can ('edit', new \App\Models\Document)
                                        <a class="btn btn-sm btn-success" href="{{ route('admin.documents.edit', $document->id) }}">
                                            <i class="fa fa-pencil"></i> {{ _('Editar documento') }}
                                        </a>
                                    @endcan

                                    @can ('destroy', new \App\Models\Document)
									<button type="button" data-documents_id="{{ $document->id }}"
										data-documents_title="{{ $document->name }}"
										class="btn btn-danger btn-sm" data-toggle="modal" data-target="#confirmDelete">
                                        <i class="fa fa-trash"></i> {{ _('Eliminar') }}
                                     </button>
                                     @endcan

                                </td>

                            </tr>
                        @endforeach

                        </tbody>

                        <tfoot>

                        <tr>
                            <th>Nombre</th>
                            <th>Acciones</th>
                        </tr>

                        </tfoot>

                    </table>

                    {!! $documents->render() !!}

                </div>{{-- /.box-body --}}

            </div>{{-- /.box --}}

        </div>{{-- /.col --}}

    </div>{{-- /.row --}}

    <div class="row">

        <div class="col-xs-12">
	    @can ('create', new \App\Models\Document)
            <div class="box">

                <div class="box-header">
                    <h3 class="box-title">{{ _('Nuevas documentos') }}</h3>
                </div>{{-- /.box-header --}}

                <div class="box-body">

                    <a class="btn btn-success" href="{{ route('admin.documents.create', $user->id) }}"><i class="fa fa-plus"></i> Crear documento</a>

                </div>{{-- /.box-body --}}

            </div>{{-- /.box --}}
	    @endcan

        </div>{{-- /.col --}}

    </div>{{-- /.row --}}

    {{-- Modal delete --}}
    <div class="modal modal-danger fade" id="confirmDelete">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Eliminar documento</h4>
                </div>
                <div class="modal-body">
                    <p>¿Estás seguro que quieres eliminar la siguiente documento?</p>
                    <p>
                        <span class="txt-documents-name"></span>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cerrar</button>
                    {!! Form::open(['method' => 'DELETE', 'id'=>'delForm']) !!}
                        <button type="submit" class="btn btn-outline">Eliminar documento</button>
                    {!! Form::close() !!}
                </div>{{-- /.modal-footer --}}
            </div>{{-- /.modal-content --}}
        </div>{{-- /.modal-dialog --}}
    </div>{{-- /.modal --}}
    {{-- /Modal delete --}}
@endsection


@section('specific-footer-js')

    <script>
        {{-- Trigger Modal elete --}}
        $('#confirmDelete').on('show.bs.modal', function(e) {

            var documentsId = $(e.relatedTarget).data('documents_id');
            var documentsName = $(e.relatedTarget).data('documents_name');
            var fullUrl = "{{ url('admin/documento') }}" + '/' + documentsId;

            $("#confirmDelete .txt-documents-name").text(documentsName);
            $("#delForm").attr('action', fullUrl);
        });
    </script>
@endsection
