@extends('backend.layouts.dashboard')

@section('content')

    {{-- //////// Page title and subtitle ///////// --}}
    <?php $page_title = _('Editar evento')  ?>
    <?php $page_description = _('datos específicos del evento')  ?>
    {{-- //////// Page title and subtitle ///////// --}}

    {{-- *** Edit event row *** --}}
    <div class="row">

        <div class="col-xs-12">

            {{-- Tabs Contentainer --}}
            <div class="nav-tabs-custom">

                <ul class="nav nav-tabs nav-justified">
                    <li class="active"><a href="#event-data" data-toggle="tab">{{ _('Datos evento') }}</a></li>
                    <li><a href="#event-race-types" data-toggle="tab">{{ _('Modalidades') }}</a></li>
                    <li><a href="#event-categories" data-toggle="tab">{{ _('Categorías') }}</a></li>
                    <li><a href="#event-registration-fields" data-toggle="tab">{{ _('Campos Inscripciones') }}</a></li>
                </ul>{{-- /.nav .nav-tabs --}}

                {{--| Tabs Content |--}}
                <div class="tab-content">


                    {{--|| Tab pane - Event Data ||--}}
                    <div class="tab-pane active" id="event-data">
                        {{-- form start --}}
                        {!! Form::model($event,
                                        [
                                        'method' => 'PUT',
                                        'route' => [
                                                    'admin.events.update',
                                                    $event->id
                                        ],
                                        'role' => 'form'
                                        ])
                        !!}
                            @include('backend.partials.events._tab-event-edit-data')
                            @include('backend.partials.events._tab-event-edit-data-additional')

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Actualizar evento</button>
                            </div>{{-- /.box-footer --}}
                        {!! Form::close() !!}{{-- /.form --}}
                    </div>{{-- /#event-data  --}}
                    {{--|| /Tab pane - Event Data ||--}}

                    {{--|| Tab pane - Event Race Types ||--}}
                    <div class="tab-pane" id="event-race-types">
                        {{-- form start --}}
                        {!! Form::model($event,
                                        [
                                        'method' => 'PUT',
                                        'route' => [
                                                    'admin.eventRaceTypes.update',
                                                    $event->id
                                        ],
                                        'role' => 'form'
                                        ])
                        !!}
                            @include('backend.partials.events._tab-event-edit-race-types-list')
                            @include('backend.partials.events._tab-event-edit-race-types-assign')
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Asignar modalidades</button>
                            </div>{{-- /.box-footer --}}
                        {!! Form::close() !!}{{-- /.form --}}

                        @include('backend.partials.events._modal-event-race-types-delete')

                    </div>{{-- /#event-race-types  --}}
                    {{--||| /Tab pane - Event Race Types |||--}}

                    {{--||| Tab pane - Event Categories |||--}}
                    <div class="tab-pane" id="event-categories">
                        {{-- form start --}}
                        {!! Form::model($event,
                                        [
                                        'method' => 'PUT',
                                        'route' => [
                                                    'admin.eventCategories.update',
                                                    $event->id
                                        ],
                                        'role' => 'form'
                                        ])
                        !!}
                          @include('backend.partials.events._tab-event-edit-categories-list')       
			  @include('backend.partials.events._tab-event-edit-categories-create') 

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Actualizar categorias</button>
                        </div>{{-- /.box-footer --}}
                        {!! Form::close() !!}{{-- /.form --}}

                        {{-- Modal delete partials outside Form for correct funtioning --}}
                        @include('backend.partials.events._modal-event-category-delete')

                    </div>{{-- /#event-race-types  --}}
                    {{--||| /Tab pane - Event Categories |||--}}

                    {{--|| Tab pane - Event Data Aditional ||--}}
                    <div class="tab-pane" id="event-registration-fields">
                        {{-- form start --}}
                        {!! Form::model($event,
                                        [
                                        'method' => 'PUT',
                                        'route' => [
                                                    'admin.registrationTextfields.update',
                                                    $event->id
                                        ],
                                        'role' => 'form'
                                        ])
                        !!}
                            @include('backend.partials.events._tab-event-edit-registration-textfields-list')
                            @include('backend.partials.events._tab-event-edit-registration-files-list')
                            @include('backend.partials.events._tab-event-edit-registration-textfields')
                            @include('backend.partials.events._tab-event-edit-registration-files')

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Actualizar campos registro</button>
                            </div>{{-- /.box-footer --}}
                        {!! Form::close() !!}{{-- /.form --}}

                        {{-- Modal delete partials outside Form for correct funtioning --}}
                        @include('backend.partials.events._modal-event-textfield-delete')
                        @include('backend.partials.events._modal-event-file-delete')
                        {{-- /Modal delete partials outside Form for correct funtioning --}}

                    </div>{{-- /#event-data  --}}
                    {{--|| /Tab pane - Event Data Aditional ||--}}

                </div>{{-- /.tab-content  --}}
                {{--| /Tabs Content |--}}

            </div>{{-- /.nav-tabs-custom  --}}
            {{-- /Tabs Contentainer --}}

        </div>{{-- /.col --}}

    </div>{{-- /.row --}}
    {{-- /*** Edit event row *** --}}

    {{-- *** Other functions & Delete account row *** --}}
    <div class="row">


	@can ('delete', $event)
        {{-- *** Delete account row *** --}}
        <div class="col-xs-12 col-md-6">

            <div class="box box-primary">

                <div class="box-header with-border">
                    <h3 class="box-title">{{ _('Eliminar evento') }}</h3>
                </div>{{-- /.box-header --}}

                <div class="box-body">

                    <div class="row">

                        {{-- *** Delete account form *** --}}
                        <div class="col-xs-1">

                            {!! Form::open(['method' => 'DELETE', 'route' => ['admin.events.destroy', $event->id]]) !!}
                                {!! Form::hidden('_method', 'DELETE') !!}
                                {!! Form::submit(_('Borrar evento'), ['class' => 'btn btn-danger']) !!}
                            {!! Form::close() !!}

                        </div>{{-- /.col-xs-1 --}}
                        {{-- /*** Delete account form *** --}}

                    </div>{{-- /.row --}}

                </div>{{-- /.box-body --}}

            </div>{{-- /.box .box-primary --}}

        </div>{{-- /.col-xs-12 .col-md-6 --}}
        {{-- *** /Delete account row *** --}}
	@endcan

    </div>{{-- /.row --}}
    {{-- /*** Other functions & Delete account row *** --}}

    @can ('admin_edit', $event)
    {{-- *** New RaceType Form *** --}}
    <div class="row">
        <div class="col-xs-12 col-md-12">
            {{-- form start --}}
            {!! Form::open([
                                'route' => 'admin.race_types.store',
                                'role' => 'form'
                                ])
                !!}
            @include('backend.partials.events._global-event-race-types-create')
            {!! Form::close() !!}{{-- /.form --}}
        </div>{{-- /.col-xs-12 .col-md-6 --}}
    </div>{{-- /.row --}}
    {{-- *** /New RaceType Form *** --}}
    @endcan

@endsection

@section ('specific-footer-js')
<script>
var  organizationCircuits = {!! json_encode($circuits) !!};

$('#js-organizations-select').on('change', function () {
	var circuits = $('#js-circuits-select');
	var placeholder = $('#js-circuits-select option:first').text();
	
	circuits.empty();
	circuits.append($('<option></option>').attr('value', '').text(placeholder));
	$.each(organizationCircuits[this.value], function(value, key) {
		circuits.append($('<option></option>').attr('value', value).text(key));
	});
});
</script>

@endsection
