@extends('backend.layouts.dashboard')



@section('content')

    {{-- //////// Page title ///////// --}}
    <?php $page_title = 'Listado de eventos'  ?>
    {{-- //////// Page title ///////// --}}

    <div class="row">

        <div class="col-xs-12">

            <div class="box">

                <div class="box-header">
                    <h3 class="box-title">{{ _('Eventos') }}</h3>
                </div>{{-- /.box-header --}}

                <div class="box-body">

                    {!! $events->render() !!}

                    <table id="events" class="table table-bordered table-striped data-table">

                        <thead>

                        <tr>
                            <th>Evento</th>
                            <th>Edición</th>
                            <th>Plazas</th>
                            <th>Fecha evento</th>
                            <th class="no-sort">Fechas de inscripción</th>
                            <th>Ciudad</th>
                            <th class="no-sort">Acciones</th>
                        </tr>

                        </thead>

                        <tbody>

                        @foreach($events as $event)
                            <tr>
                                <td>{{ $event->name }}</td>
                                <td>{{ $event->edition }}</td>
                                <td>{{ $event->seats }}</td>
                                <td>{{ date('d/m/y (H:i)', strtotime($event->start_time)) }}</td>
                                <td>
                                    <strong>Temprana:</strong> {{ date('d/m/y (H:i)', strtotime($event->early_registration_start_time)) }} - {{ date('d/m/y (H:i)', strtotime($event->early_registration_end_time)) }}
                                    {{--<strong>Test:</strong> {{ $event->start_time }}--}}
                                    <br>
                                    <strong>Estándar:</strong> {{ date('d/m/y (H:i)', strtotime($event->registration_start_time)) }} - {{ date('d/m/y (H:i)', strtotime($event->registration_end_time)) }}
                                    <br>
                                    <strong>Tardía:</strong> {{ date('d/m/y (H:i)', strtotime($event->late_registration_end_time)) }}
                                    <br>
                                </td>
                                <td>

                                </td>
                                <td class="text-right">
                                    @can ('edit', $event)
                                        <a class="btn btn-sm btn-success" href="{{ route('admin.events.edit', $event->id) }}">
                                            <i class="fa fa-pencil"></i> {{ _('Editar evento') }}
                                        </a>
                                    @endcan

                                    {{--@if($event->status)--}}
                                        {{--{!! Form::open(['route' => ['admin.events.unban', $event->id], 'onsubmit' => $js_ban_alert, 'style' => 'display: inline;']) !!}--}}
                                        {{--{!! Form::submit(_('Unban'), ['class' => 'btn btn-sm btn-success']) !!}--}}
                                        {{--{!! Form::close() !!}--}}
                                    {{--@else--}}
                                        {{--{!! Form::open(['route' => ['admin.events.ban', $event->id], 'onsubmit' => $js_ban_alert, 'style' => 'display: inline;']) !!}--}}
                                        {{--{!! Form::submit(_('Ban'), ['class' => 'btn btn-sm btn-danger']) !!}--}}
                                        {{--{!! Form::close() !!}--}}
                                    {{--@endif--}}

                                    @can ('delete', $event)
                                                    <button type="button" data-event_id="{{ $event->id }}"
                                    data-event_name="{{ $event->name }}"
                                    data-event_start_time="{{ date('d/m/y (H:i)', strtotime($event->start_time)) }}"
                                    data-event_edition="{{ $event->edition }}"

                                    class="btn btn-danger btn-sm" data-toggle="modal" data-target="#confirmDelete">
                                        <i class="fa fa-trash"></i> {{ _('Eliminar') }}
                                     </button>
                                     @endcan

                                </td>

                            </tr>
                        @endforeach

                        </tbody>

                        <tfoot>

                        <tr>
                            <th>Evento</th>
                            <th>Edición</th>
                            <th>Plazas</th>
                            <th>Fecha evento</th>
                            <th>Fechas de inscripción</th>
                            <th>Ciudad</th>
                            <th>Acciones</th>
                        </tr>

                        </tfoot>

                    </table>

                    {!! $events->render() !!}

                </div>{{-- /.box-body --}}

            </div>{{-- /.box --}}

        </div>{{-- /.col --}}

    </div>{{-- /.row --}}

    <div class="row">

        <div class="col-xs-12">
	    @can ('create', new \App\Event)
            <div class="box">

                <div class="box-header">
                    <h3 class="box-title">{{ _('Nuevos eventos') }}</h3>
                </div>{{-- /.box-header --}}

                <div class="box-body">

                    <a class="btn btn-success" href="{{ route('admin.events.create') }}"><i class="fa fa-plus"></i> Crear evento</a>

                </div>{{-- /.box-body --}}

            </div>{{-- /.box --}}
	    @endcan

        </div>{{-- /.col --}}

    </div>{{-- /.row --}}

    {{-- Modal delete --}}
    <div class="modal modal-danger fade" id="confirmDelete">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Eliminar evento</h4>
                </div>
                <div class="modal-body">
                    <p>¿Estás seguro que quieres eliminar al siguiente evento?</p>
                    <p>
                        <span class="txt-event-name"></span> <span class="txt-event-edition"></span>
                        <br>
                        <span class="txt-event-state"></span> - <span class="txt-event-start-time"></span>
                        <br>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cerrar</button>
                    {!! Form::open(['method' => 'DELETE', 'id'=>'delForm']) !!}
                        <button type="submit" class="btn btn-outline">Eliminar evento</button>
                    {!! Form::close() !!}
                </div>{{-- /.modal-footer --}}
            </div>{{-- /.modal-content --}}
        </div>{{-- /.modal-dialog --}}
    </div>{{-- /.modal --}}
    {{-- /Modal delete --}}
@endsection


@section('specific-footer-js')

    <script>
        {{-- Trigger Modal elete --}}
        $('#confirmDelete').on('show.bs.modal', function(e) {

            var eventId = $(e.relatedTarget).data('event_id');
            var eventName = $(e.relatedTarget).data('event_name');
            var eventEdition = $(e.relatedTarget).data('event_edition');
            var eventState = $(e.relatedTarget).data('event_state');
            var eventStartTime = $(e.relatedTarget).data('event_start_time');
            var fullUrl = "{{ url('admin/evento') }}" + '/' + eventId;

            $("#confirmDelete .txt-event-name").text(eventName);
            $("#confirmDelete .txt-event-edition").text(eventEdition);
            $("#confirmDelete .txt-event-state").text(eventState);
            $("#confirmDelete .txt-event-start-time").text(eventStartTime);
            $("#delForm").attr('action', fullUrl);
        });
    </script>
@endsection
