@extends('layout')

@section('content')
    <div class="page-header">
        <h1>events / Show </h1>
    </div>


    <div class="row">
        <div class="col-md-12">

            <form action="#">
                <div class="form-group">
                    <label for="nome">ID</label>
                    <p class="form-control-static">{{$event->id}}</p>
                </div>
                <div class="form-group">
                     <label for="edition">EDITION</label>
                     <p class="form-control-static">{{$event->edition}}</p>
                </div>
                    <div class="form-group">
                     <label for="name">NAME</label>
                     <p class="form-control-static">{{$event->name}}</p>
                </div>
                    <div class="form-group">
                     <label for="seats">SEATS</label>
                     <p class="form-control-static">{{$event->seats}}</p>
                </div>
                    <div class="form-group">
                     <label for="start_time">START_TIME</label>
                     <p class="form-control-static">{{$event->start_time}}</p>
                </div>
                    <div class="form-group">
                     <label for="start_location">START_LOCATION</label>
                     <p class="form-control-static">{{$event->start_location}}</p>
                </div>
                    <div class="form-group">
                     <label for="finish_location">FINISH_LOCATION</label>
                     <p class="form-control-static">{{$event->finish_location}}</p>
                </div>
                    <div class="form-group">
                     <label for="early_registration_start_time">EARLY_REGISTRATION_START_TIME</label>
                     <p class="form-control-static">{{$event->early_registration_start_time}}</p>
                </div>
                    <div class="form-group">
                     <label for="early_registration_end_time">EARLY_REGISTRATION_END_TIME</label>
                     <p class="form-control-static">{{$event->early_registration_end_time}}</p>
                </div>
                    <div class="form-group">
                     <label for="registration_start_time">REGISTRATION_START_TIME</label>
                     <p class="form-control-static">{{$event->registration_start_time}}</p>
                </div>
                    <div class="form-group">
                     <label for="registration_end_time">REGISTRATION_END_TIME</label>
                     <p class="form-control-static">{{$event->registration_end_time}}</p>
                </div>
            </form>



            <a class="btn btn-default" href="{{ route('events.index') }}">Back</a>
            <a class="btn btn-warning" href="{{ route('events.edit', $event->id) }}">Edit</a>
            <form action="#/$event->id" method="DELETE" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };"><button class="btn btn-danger" type="submit">Delete</button></form>
        </div>
    </div>


@endsection