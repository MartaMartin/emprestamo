@extends('backend.layouts.dashboard')

@section('content')

    {{-- //////// News title ///////// --}}
    <?php $page_title = 'Crear entidad financiera'  ?>
    {{-- //////// News title ///////// --}}

    {{-- *** Create financial_entities row *** --}}
    <div class="row">

        <div class="col-xs-12">

            <div class="box box-primary">

                {{-- form start --}}
                {!! Form::open([
					'route' => 'admin.financial_entities.store',
					'role' => 'form'
				])
                !!}

                {!! Form::hidden('password', 'pass_inicial') !!}

                <div class="box-body">

                    <div class="row">

                        <div class="col-md-12">

							<div class="form-group">

								{!! Form::label('name', _('Nombre de la entidad financiera')) !!}
								{!! Form::text('name', null,
									[
										'class' => 'form-control',
										'placeholder' => _('Nombre de la entidad financiera'),
										'required'
									])
								!!}

							</div>{{-- /.form-group --}}

						</div>{{-- /.col-md-12 --}}

                    </div>{{-- /.row --}}

                </div>{{-- /.box-body --}}

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Crear entidad financiera</button>
                </div>{{-- /.box-footer --}}

                {!! Form::close() !!}{{-- /.form --}}

            </div>{{-- /.box --}}

        </div>{{-- /.col --}}

    </div>{{-- /.row --}}
    {{-- /*** Create financial_entities row *** --}}

@endsection
