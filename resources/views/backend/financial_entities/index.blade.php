@extends('backend.layouts.dashboard')

@section('content')

    {{-- //////// News title ///////// --}}
    <?php $page_title = 'Listado de entidades financieras'  ?>
    {{-- //////// News title ///////// --}}

    <div class="row">

        <div class="col-xs-12">

            <div class="box">

                <div class="box-header">
                    <h3 class="box-title">{{ _('Entidades financieras') }}</h3>
                </div>{{-- /.box-header --}}

                <div class="box-body">

                    {!! $financial_entities->render() !!}

                    <table id="financial_entities" class="table table-bordered table-striped data-table">

                        <thead>

                        <tr>
                            <th>Nombre</th>
                            <th class="no-sort">Acciones</th>
                        </tr>

                        </thead>

                        <tbody>

                        @foreach($financial_entities as $financial_entity)
                            <tr>
                                <td>{{ $financial_entity->name }}</td>
                                <td class="text-right">
                                    @can ('edit', new \App\Models\FinancialEntity)
                                        <a class="btn btn-sm btn-success" href="{{ route('admin.financial_entities.edit', $financial_entity->id) }}">
                                            <i class="fa fa-pencil"></i> {{ _('Editar entidad financiera') }}
                                        </a>
                                    @endcan

                                    @can ('destroy', new \App\Models\FinancialEntity)
									<button type="button" data-financial_entities_id="{{ $financial_entity->id }}"
										data-financial_entities_title="{{ $financial_entity->name }}"
										class="btn btn-danger btn-sm" data-toggle="modal" data-target="#confirmDelete">
                                        <i class="fa fa-trash"></i> {{ _('Eliminar') }}
                                     </button>
                                     @endcan

                                </td>

                            </tr>
                        @endforeach

                        </tbody>

                        <tfoot>

                        <tr>
                            <th>Nombre</th>
                            <th>Acciones</th>
                        </tr>

                        </tfoot>

                    </table>

                    {!! $financial_entities->render() !!}

                </div>{{-- /.box-body --}}

            </div>{{-- /.box --}}

        </div>{{-- /.col --}}

    </div>{{-- /.row --}}

    <div class="row">

        <div class="col-xs-12">
	    @can ('create', new \App\Models\FinancialEntity)
            <div class="box">

                <div class="box-header">
                    <h3 class="box-title">{{ _('Nuevas entidades financieras') }}</h3>
                </div>{{-- /.box-header --}}

                <div class="box-body">

                    <a class="btn btn-success" href="{{ route('admin.financial_entities.create') }}"><i class="fa fa-plus"></i> Crear entidad financiera</a>

                </div>{{-- /.box-body --}}

            </div>{{-- /.box --}}
	    @endcan

        </div>{{-- /.col --}}

    </div>{{-- /.row --}}

    {{-- Modal delete --}}
    <div class="modal modal-danger fade" id="confirmDelete">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Eliminar entidad financiera</h4>
                </div>
                <div class="modal-body">
                    <p>¿Estás seguro que quieres eliminar la siguiente entidad financiera?</p>
                    <p>
                        <span class="txt-financial_entities-name"></span>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cerrar</button>
                    {!! Form::open(['method' => 'DELETE', 'id'=>'delForm']) !!}
                        <button type="submit" class="btn btn-outline">Eliminar entidad financiera</button>
                    {!! Form::close() !!}
                </div>{{-- /.modal-footer --}}
            </div>{{-- /.modal-content --}}
        </div>{{-- /.modal-dialog --}}
    </div>{{-- /.modal --}}
    {{-- /Modal delete --}}
@endsection


@section('specific-footer-js')

    <script>
        {{-- Trigger Modal elete --}}
        $('#confirmDelete').on('show.bs.modal', function(e) {

            var financial_entitiesId = $(e.relatedTarget).data('financial_entities_id');
            var financial_entitiesName = $(e.relatedTarget).data('financial_entities_name');
            var fullUrl = "{{ url('admin/entidad-financiera') }}" + '/' + financial_entitiesId;

            $("#confirmDelete .txt-financial_entities-name").text(financial_entitiesName);
            $("#delForm").attr('action', fullUrl);
        });
    </script>
@endsection
