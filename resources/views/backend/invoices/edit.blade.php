@extends('backend.layouts.dashboard')

@section('page_title', 'Editar presupuesto de <a href="' . route('admin.users.edit', [$invoice->transferorCompany->user_id]) . '">' . $invoice->transferorCompany->name . '</a>')
@section('content')

    {{-- *** Create invoices row *** --}}
    <div class="row">

        <div class="col-xs-12">

            <div class="box box-primary">

                {{-- form start --}}
				{!! Form::model($invoice,
					[
					'method' => 'PUT',
					'route' => [
                        'admin.invoices.update',
                        $invoice->id
					],
					'role' => 'form'
					])
                !!}

                <div class="box-body">

                    <div class="row">

                        <div class="col-md-4">

							<div class="form-group">

								{!! Form::label('price', _('Precio del presupuesto')) !!}
								{!! Form::text('price', $invoice->price,
									[
										'class' => 'form-control',
										'placeholder' => _('Precio del presupuesto'),
										'required'
									])
								!!}

							</div>{{-- /.form-group --}}

						</div>{{-- /.col-md-4 --}}

                        <div class="col-md-4">

							<div class="form-group">

								{!! Form::label('expiers', _('Fecha de expiración')) !!}
								{!! Form::date('expires', $invoice->expires,
									[
										'class' => 'form-control datepicker',
										'data-inputmask' => "'alias': 'dd/mm/yyyy'",
										'placeholder' => _('Fecha de expiración'),
										'required',
									])
								!!}

							</div>{{-- /.form-group --}}

						</div>{{-- /.col-md-4 --}}

                        <div class="col-md-4">

							<div class="form-group">

								{!! Form::label('debtor_company_id', _('Empresa deudora')) !!}
								{!! Form::select('debtor_company_id', $debtor_companies, $invoice->debtor_company_id,
										[
										'class' => 'form-control',
										]
									)
								!!}

							</div>{{-- /.form-group --}}

						</div>{{-- /.col-md-4 --}}

                    </div>{{-- /.row --}}

                    <div class="row">

                        <div class="col-md-4">

							<div class="form-group">

								{!! Form::label('ticket_min', _('Ticket mínimo')) !!}
								{!! Form::text('ticket_min', $operation ? $operation->ticket_min : ($settings['min_invest'] ? $settings['min_invest'] : 0),
									[
										'class' => 'form-control',
										'placeholder' => _('Ticket mínimo'),
										'required'
									])
								!!}

							</div>{{-- /.form-group --}}

						</div>{{-- /.col-md-4 --}}

                        <div class="col-md-4">

							<div class="form-group">

								{!! Form::label('ticket_max', _('Ticket máximo')) !!}
								{!! Form::text('ticket_max', $operation ? $operation->ticket_max : 0,
									[
										'class' => 'form-control',
										'placeholder' => _('Ticket máximo'),
										'required'
									])
								!!}

							</div>{{-- /.form-group --}}

						</div>{{-- /.col-md-4 --}}

                        <div class="col-md-4">

							<div class="form-group">

								{!! Form::label('calification', _('Calificación por riesgo')) !!}
								{!! Form::select('calification', [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], $operation ? $operation->calification : 4,
										['class' => 'form-control']
									)
								!!}

							</div>{{-- /.form-group --}}

						</div>{{-- /.col-md-4 --}}

                    </div>{{-- /.row --}}

                    <div class="row">

                        <div class="col-md-4">

							<div class="form-group">

								{!! Form::label('fee', _('Interés')) !!}
								{!! Form::text('fee', !empty($operation->fee) ? $operation->fee : 0,
									[
										'class' => 'form-control',
										'placeholder' => _('Interés'),
										'required'
									])
								!!}

							</div>{{-- /.form-group --}}

						</div>{{-- /.col-md-4 --}}

                        <div class="col-md-4">

							<div class="form-group">

								{!! Form::label('status', _('Estado (Será no editable)')) !!}
								{!! Form::select('status', $status, $operation ? $operation->status : null,
										['class' => 'form-control']
									)
								!!}

							</div>{{-- /.form-group --}}

						</div>{{-- /.col-md-4 --}}

                    </div>{{-- /.row --}}

                    <div class="row">

                        <div class="col-md-12">

							<div class="form-group">

                                {!! '<strong>' . _('Fecha solicitud') . ':</strong> ' . Functions::beautyDate($invoice->created_at) !!}

							</div>{{-- /.form-group --}}

						</div>{{-- /.col-md-12 --}}

                    </div>{{-- /.row --}}

                    <div class="row">

                        <div class="col-md-12">

							<div class="form-group">

								{!! '<strong>' . _('Descripción') . ':</strong> ' . $invoice->description !!}

							</div>{{-- /.form-group --}}

						</div>{{-- /.col-md-12 --}}

                    </div>{{-- /.row --}}

					@if (count($documents))
                    <div class="row">

                        <div class="col-md-12">

							<div class="form-group">

								{{ _('Documentos adjuntos') }}
								<ul>
                                    @foreach ($documents as $document)
                                        <li><a href="{{ AssetUrl::get('Document', $document->type, $document->filename) }}" target="_blank"><i class="fa fa-file"></i> {{ $document->name }}</a></li>
                                    @endforeach
								</ul>

							</div>{{-- /.form-group --}}

						</div>{{-- /.col-md-12 --}}

                    </div>{{-- /.row --}}
					@endif

                </div>{{-- /.box-body --}}

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">{{ $operation ? _('Actualizar operación') : _('Crear operación') }}</button>
                    @if (!empty($invoice->operations[0]))
                        <a href="{{ route('admin.operations.show', [$invoice->operations[0]->id]) }}" class="btn btn-primary">{{ _('Volver a la operación') }}</a>
                    @endif
                </div>{{-- /.box-footer --}}

                {!! Form::close() !!}{{-- /.form --}}

            </div>{{-- /.box --}}

        </div>{{-- /.col --}}

    </div>{{-- /.row --}}
    {{-- /*** Create invoices row *** --}}

@endsection
