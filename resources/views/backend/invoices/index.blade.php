@extends('backend.layouts.dashboard')

@section('content')

    {{-- //////// News title ///////// --}}
    <?php $page_title = 'Listado de presupuestos'  ?>
    {{-- //////// News title ///////// --}}

    <div class="row">

        <div class="col-xs-12">

            <div class="box">

                <div class="box-header">
                    <h3 class="box-title">{{ _('Presupuestos') }}</h3>
                </div>{{-- /.box-header --}}

                <div class="box-body">

                    {!! $invoices->render() !!}

                    <table id="invoices" class="table table-bordered table-striped data-table">

                        <thead>

                        <tr>
                            <th>Nombre</th>
                            <th>Precio</th>
                            <th>Fecha expiración</th>
                            <th>Estado</th>
                            <th class="no-sort">Acciones</th>
                        </tr>

                        </thead>

                        <tbody>

                        @foreach($invoices as $invoice)
                            <tr>
                                <td>{{ $invoice->name }} (<a href="{{ route('admin.users.edit', [$invoice->transferorCompany->user_id]) }}">{{ $invoice->transferorCompany->name }}</a>)</td>
                                <td>{{ number_format($invoice->price, 2, ',', '.') }}</td>
                                <td>{{ Functions::beautyDate($invoice->expires) }}</td>
                                <td>
								@if (count($invoice->operations))
									{{ $status[$invoice->operations[0]->status] }}
								@else
									{{ $status[0] }}
								@endif
								</td>
                                <td class="text-right">
                                    @can ('edit', new \App\Models\Invoice)
                                        <a class="btn btn-sm btn-success" href="{{ route('admin.invoices.edit', $invoice->id) }}">
                                            {{ _('Ver presupuesto') }}
                                        </a>
                                    @endcan

                                </td>

                            </tr>
                        @endforeach

                        </tbody>

                        <tfoot>

                        <tr>
                            <th>Nombre</th>
                            <th>Precio</th>
                            <th>Fecha expiración</th>
                            <th>Estado</th>
                            <th>Acciones</th>
                        </tr>

                        </tfoot>

                    </table>

                    {!! $invoices->render() !!}

                </div>{{-- /.box-body --}}

            </div>{{-- /.box --}}

        </div>{{-- /.col --}}

    </div>{{-- /.row --}}

    <div class="row">

        <div class="col-xs-12">
	    @can ('create', new \App\Models\Invoice)
            <div class="box">

                <div class="box-header">
                    <h3 class="box-title">{{ _('Nuevas presupuestos') }}</h3>
                </div>{{-- /.box-header --}}

                <div class="box-body">

                    <a class="btn btn-success" href="{{ route('admin.invoices.create') }}"><i class="fa fa-plus"></i> Crear presupuesto</a>

                </div>{{-- /.box-body --}}

            </div>{{-- /.box --}}
	    @endcan

        </div>{{-- /.col --}}

    </div>{{-- /.row --}}

    {{-- Modal delete --}}
    <div class="modal modal-danger fade" id="confirmDelete">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Eliminar presupuesto</h4>
                </div>
                <div class="modal-body">
                    <p>¿Estás seguro que quieres eliminar la siguiente presupuesto?</p>
                    <p>
                        <span class="txt-invoices-name"></span>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cerrar</button>
                    {!! Form::open(['method' => 'DELETE', 'id'=>'delForm']) !!}
                        <button type="submit" class="btn btn-outline">Eliminar presupuesto</button>
                    {!! Form::close() !!}
                </div>{{-- /.modal-footer --}}
            </div>{{-- /.modal-content --}}
        </div>{{-- /.modal-dialog --}}
    </div>{{-- /.modal --}}
    {{-- /Modal delete --}}
@endsection


@section('specific-footer-js')

    <script>
        {{-- Trigger Modal elete --}}
        $('#confirmDelete').on('show.bs.modal', function(e) {

            var invoicesId = $(e.relatedTarget).data('invoices_id');
            var invoicesName = $(e.relatedTarget).data('invoices_name');
            var fullUrl = "{{ url('admin/presupuesto') }}" + '/' + invoicesId;

            $("#confirmDelete .txt-invoices-name").text(invoicesName);
            $("#delForm").attr('action', fullUrl);
        });
    </script>
@endsection
