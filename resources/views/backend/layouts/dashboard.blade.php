<!DOCTYPE html>
<html>
    <head>

        <meta charset="UTF-8">
        <title>{{ $page_title or "Emprestamo Admin" }}</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        {{-- CDN: Bootstrap 3.3.6 --}}
        <link href="//cdn.jsdelivr.net/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        {{-- CDN: Font Awesome Icons 4.5.0 --}}
        <link href="//cdn.jsdelivr.net/fontawesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        {{-- CDN: Ionicons 2.0.1 --}}
        <link href="//cdn.jsdelivr.net/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        {{--WIP--}}
        {{-- Datatables --}}
        <link rel="stylesheet" href="/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css">
        {{-- Bootstrap Switch --}}
        <link href="{{ asset("/backend/css/bootstrap-switch.min.css")}}" rel="stylesheet" type="text/css" />
        {{-- iCheck --}}
        {{--<link href="{{ asset("/bower_components/AdminLTE/plugins/iCheck/all.css")}}" rel="stylesheet" type="text/css" />--}}
        {{--WIP--}}

        <link rel="stylesheet" href="{{ asset('/backend/js/datetimepicker/build/css/bootstrap-datetimepicker.min.css')}}"/>

        {{-- Theme style --}}
        <link href="{{ asset("/backend/css/AdminLTE.min.css")}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset("/backend/css/skins/skin-blue.min.css")}}" rel="stylesheet" type="text/css" />

        {{-- Additional styles --}}
        <style>
            .admin-label-switch {
                display: block;
                margin-bottom: 4px;
            }
            .container-dynamic,
            .container-existing {
                border: 2px dashed #ddd;
                padding: 5px;
                margin-bottom: 5px;
                border-radius: 4px;
            }
            .container-existing {
                border: 1px dashed #00a65a;
            }
        </style>
        {{-- /Additional styles --}}

        {{-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries --}}
        {{-- WARNING: Respond.js doesn't work if you view the page via file:// --}}
        <!--[if lt IE 9]>
            <script src="//cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="//cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        @yield('specific-header-css')

    </head>

    <body class="hold-transition skin-blue sidebar-mini">

        <div class="wrapper">

            {{-- Header --}}
            @include('backend.partials.header')

            {{-- Sidebar --}}
            @include('backend.partials.sidebar')

            {{-- Content Wrapper. Contains page content --}}
            <div class="content-wrapper">

                {{-- Content Header (Page header) --}}
                <section class="content-header">

                    <h1>
                        @if (!empty($page_title))
                            {{ $page_title or "Emprestamo" }}
                        @else
                            @yield('page_title')
                        @endif
                        <small>{{ $page_description or null }}</small>
                    </h1>
                    {{--WIP--}}
                    {{-- You can dynamically generate breadcrumbs here --}}
                    {{--<ol class="breadcrumb">--}}
                        {{--<li><a href="#"><i class="fa fa-dashboard"></i> Panel</a></li>--}}
                        {{--<li class="active">Resultados</li>--}}
                    {{--</ol>--}}
                    {{--WIP--}}

                </section>

                {{-- Main content --}}
                <section class="content">
                    <!-- Go to www.addthis.com/dashboard to customize your tools -->
                    {{--<div class="addthis_sharing_toolbox"></div>--}}
                    {{--WIP--}}

                    {{-- Messages --}}
                    @include('backend.partials.errors')
                    @include('backend.partials.success')
                    @include('backend.partials.status')
                    @include('backend.partials.message')

                    {{-- Your Page Content Here --}}
                    @yield('content')

                </section>{{-- /.content --}}
            </div>{{-- /.content-wrapper --}}

            {{-- Footer --}}
            @include('backend.partials.footer')

        </div>{{-- ./wrapper --}}

        {{-- REQUIRED JS SCRIPTS --}}

        {{-- CDN: jQuery 2.1.4 --}}
        <script src="//cdn.jsdelivr.net/jquery/2.1.4/jquery.min.js"></script>
        {{-- CDN: Bootstrap 3.3.6 JS --}}
        <script src="//cdn.jsdelivr.net/bootstrap/3.3.6/js/bootstrap.min.js" type="text/javascript"></script>
        {{-- Bootstrap Switch --}}
        {{--WIP--}}
        <script src="{{ asset ("/backend/js/bootstrap-switch.min.js") }}" type="text/javascript"></script>
        {{-- AdminLTE App --}}
        <script src="{{ asset ("/backend/js/app.min.js") }}" type="text/javascript"></script>

        {{--<script src="{{ asset ("/bower_components/AdminLTE/plugins/iCheck/icheck.min.js") }}"></script>--}}

        {{-- Parent active in sidebar menu --}}
        <script>
            $('.active').parents('li.has-subs').addClass('active treeview');
        </script>

        <script src="/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js"></script>

        {{-- SlimScroll 1.3.0 --}}
        <script src="{{ asset ("/bower_components/AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js") }}"></script>
        {{-- ChartJS 1.0.1 --}}
        {{--<script src="{{ asset ("/bower_components/AdminLTE/plugins/chartjs/Chart.min.js") }}"></script>--}}
        {{-- AdminLTE dashboard demo (This is only for demo purposes) --}}
        {{--<script src="{{ asset ("/bower_components/AdminLTE/dist/js/pages/temp-data.js") }}"></script>--}}
        {{-- AdminLTE for demo purposes --}}

        {{--<script src="{{ asset ("/bower_components/AdminLTE/dist/js/demo.js") }}"></script>--}}

        {{-- Optionally, you can add Slimscroll and FastClick plugins.
              Both of these plugins are recommended to enhance the
              user experience --}}
        <!-- Go to www.addthis.com/dashboard to customize your tools -->
        {{--WIP--}}
        {{--<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-568b8421686a3f31" async="async"></script>--}}

        <script src="{{ asset ('/backend/js/moment.js') }}"></script>
        <script src="{{ asset ('/backend/js/datetimepicker/src/js/bootstrap-datetimepicker.js') }}"></script>

        <script>
            $(function () {

    			$('.datepicker').datetimepicker({
    				locale: 'es',
    				format: 'YYYY-MM-DD'
    			});

				setTimeout(function() {
					$('.remove_autocomplete').each(function() {
						$(this).attr('readonly', false);
					})
				}, 300); // Si lo ejecuto en el momento, no va

//                $("#events").DataTable();
                $('.data-table').DataTable({
                    "paging": false,
                    "lengthChange": true,
                    "searching": true,
                    "ordering": true,
                    "info": false,
                    "order": false, // TODO para que no ordene
                    "autoWidth": true,
                    "columnDefs": [{
                        "targets"  : 'no-sort',
                        "orderable": false,
                    }],
                    language: {
                        search: "Buscar:"
                    }
                });
            });
        </script>

        {{-- Specific footer JS --}}
        @yield('specific-footer-js')

    </body>

</html>
