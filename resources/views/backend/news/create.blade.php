@extends('backend.layouts.dashboard')

@section('content')
	<script src="{{ asset('/vendors/ckeditor/ckeditor.js') }}"></script>


    {{-- //////// News title ///////// --}}
    <?php $page_title = 'Crear noticia'  ?>
    {{-- //////// News title ///////// --}}

    {{-- *** Create news row *** --}}
    <div class="row">

        <div class="col-xs-12">

            <div class="box box-primary">

                {{-- form start --}}
                {!! Form::open([
					'route' => 'admin.news.store',
					'role' => 'form',
					'files' => true
				])
                !!}

                {!! Form::hidden('password', 'pass_inicial') !!}

                <div class="box-body">

                    <div class="row">

                        <div class="col-md-4">

							<div class="form-group">

								{!! Form::label('title', _('Título de la noticia')) !!}
								{!! Form::text('title', null,
									[
										'class' => 'form-control',
										'placeholder' => _('Título de la noticia'),
										'required'
									])
								!!}

							</div>{{-- /.form-group --}}

						</div>{{-- /.col-md-4 --}}

                        <div class="col-md-4">

							<div class="form-group">

								{!! Form::label('news_category_id', _('Categoría')) !!}
                                {!! Form::select('news_category_id', $categories, null, [
									'class' => 'form-control',
									'required'
								]) !!}

							</div>{{-- /.form-group --}}

						</div>{{-- /.col-md-4 --}}

                        <div class="col-md-4">

							<div class="form-group">

								{!! Form::label('filename', _('Foto')) !!}
                                {!! Form::file('filename', [
									'class' => 'form-control'
								]) !!}

							</div>{{-- /.form-group --}}

						</div>{{-- /.col-md-4 --}}

                    </div>{{-- /.row --}}

                    <div class="row">

                        <div class="col-md-12">

							<div class="form-group">

								{!! Form::label('content', _('Contenido')) !!}
								{!! Form::textarea('content', null,
									[
										'class' => 'form-control ckeditor',
										'placeholder' => _('Contenido de la noticia'),
										'required'
									])
								!!}

							</div>{{-- /.form-group --}}

						</div>{{-- /.col-md-12 --}}

                    </div>{{-- /.row --}}

                </div>{{-- /.box-body --}}

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Crear noticia</button>
                </div>{{-- /.box-footer --}}

                {!! Form::close() !!}{{-- /.form --}}

            </div>{{-- /.box --}}

        </div>{{-- /.col --}}

    </div>{{-- /.row --}}
    {{-- /*** Create news row *** --}}

@endsection
