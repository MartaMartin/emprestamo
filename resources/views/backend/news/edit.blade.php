@extends('backend.layouts.dashboard')

@section('content')
	<script src="{{ asset('/vendors/ckeditor/ckeditor.js') }}"></script>

    {{-- //////// News title ///////// --}}
    <?php $page_title = 'Editar noticia'  ?>
    {{-- //////// News title ///////// --}}

    {{-- *** Create news row *** --}}
    <div class="row">

        <div class="col-xs-12">

            <div class="box box-primary">

                {{-- form start --}}
				{!! Form::model($news,
					[
					'method' => 'PUT',
					'files' => true,
					'route' => [
						'admin.news.update',
						$news->id
					],
					'role' => 'form'
					])
                !!}

                {!! Form::hidden('password', 'pass_inicial') !!}

                <div class="box-body">

                    <div class="row">

                        <div class="col-md-4">

							<div class="form-group">

								{!! Form::label('title', _('Título de la noticia')) !!}
								{!! Form::text('title', $news->title,
									[
										'class' => 'form-control',
										'placeholder' => _('Título de la noticia'),
										'required'
									])
								!!}

							</div>{{-- /.form-group --}}

						</div>{{-- /.col-md-4 --}}

                        <div class="col-md-4">

							<div class="form-group">

								{!! Form::label('news_category_id', _('Categoría')) !!}
                                {!! Form::select('news_category_id', $categories, $news->news_category_id, [
									'class' => 'form-control',
									'required'
								]) !!}

							</div>{{-- /.form-group --}}

						</div>{{-- /.col-md-4 --}}

                        <div class="col-md-4">

							<div class="form-group">

								{!! Form::label('filename', _('Foto')) !!}
								@if ($document)
									<img src="{{ AssetUrl::get('Document', $document->type, $document->filename, 50, 50) }}" />
								@endif
                                {!! Form::file('filename', [
									'class' => 'form-control'
								]) !!}

							</div>{{-- /.form-group --}}

						</div>{{-- /.col-md-4 --}}

                    </div>{{-- /.row --}}

                    <div class="row">

                        <div class="col-md-12">

							<div class="form-group">

								{!! Form::label('content', _('Contenido')) !!}
								{!! Form::textarea('content', $news->content,
									[
										'class' => 'form-control ckeditor',
										'placeholder' => _('Contenido de la noticia'),
										'required'
									])
								!!}

							</div>{{-- /.form-group --}}

						</div>{{-- /.col-md-12 --}}

                    </div>{{-- /.row --}}

                </div>{{-- /.box-body --}}

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Actualizar noticia</button>
                </div>{{-- /.box-footer --}}

                {!! Form::close() !!}{{-- /.form --}}

            </div>{{-- /.box --}}

        </div>{{-- /.col --}}

    </div>{{-- /.row --}}
    {{-- /*** Create news row *** --}}

@endsection
