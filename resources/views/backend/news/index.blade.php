@extends('backend.layouts.dashboard')

@section('content')

    {{-- //////// News title ///////// --}}
    <?php $page_title = 'Listado de noticias'  ?>
    {{-- //////// News title ///////// --}}

    <div class="row">

        <div class="col-xs-12">

            <div class="box">

                <div class="box-header">
                    <h3 class="box-title">{{ _('Noticias') }}</h3>
                </div>{{-- /.box-header --}}

                <div class="box-body">

                    {!! $news->render() !!}

                    <table id="news" class="table table-bordered table-striped data-table">

                        <thead>

                        <tr>
                            <th>Nombre</th>
                            <th>Categoría</th>
                            <th>Contenido</th>
                            <th class="no-sort">Acciones</th>
                        </tr>

                        </thead>

                        <tbody>

                        @foreach($news as $n)
                            <tr>
                                <td>{{ $n->title }}</td>
                                <td>{{ $n->category_name }}</td>
                                <td>{{ substr(strip_tags($n->content), 0, 100) }}...</td>
                                <td class="text-right">
                                    @can ('edit', new \App\Models\News)
                                        <a class="btn btn-sm btn-success" href="{{ route('admin.news.edit', $n->id) }}">
                                            <i class="fa fa-pencil"></i> {{ _('Editar noticia') }}
                                        </a>
                                    @endcan

                                    @can ('destroy', new \App\Models\News)
									<button type="button" data-news_id="{{ $n->id }}"
										data-news_title="{{ $n->title }}"
										class="btn btn-danger btn-sm" data-toggle="modal" data-target="#confirmDelete">
                                        <i class="fa fa-trash"></i> {{ _('Eliminar') }}
                                     </button>
                                     @endcan

                                </td>

                            </tr>
                        @endforeach

                        </tbody>

                        <tfoot>

                        <tr>
                            <th>Nombre</th>
                            <th>Categoría</th>
                            <th>Contenido</th>
                            <th>Acciones</th>
                        </tr>

                        </tfoot>

                    </table>

                    {!! $news->render() !!}

                </div>{{-- /.box-body --}}

            </div>{{-- /.box --}}

        </div>{{-- /.col --}}

    </div>{{-- /.row --}}

    <div class="row">

        <div class="col-xs-12">
	    @can ('create', new \App\Models\News)
            <div class="box">

                <div class="box-header">
                    <h3 class="box-title">{{ _('Nuevas noticias') }}</h3>
                </div>{{-- /.box-header --}}

                <div class="box-body">

                    <a class="btn btn-success" href="{{ route('admin.news.create') }}"><i class="fa fa-plus"></i> Crear noticia</a>

                </div>{{-- /.box-body --}}

            </div>{{-- /.box --}}
	    @endcan

        </div>{{-- /.col --}}

    </div>{{-- /.row --}}

    {{-- Modal delete --}}
    <div class="modal modal-danger fade" id="confirmDelete">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Eliminar noticia</h4>
                </div>
                <div class="modal-body">
                    <p>¿Estás seguro que quieres eliminar la siguiente noticia?</p>
                    <p>
                        <span class="txt-news-name"></span>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cerrar</button>
                    {!! Form::open(['method' => 'DELETE', 'id'=>'delForm']) !!}
                        <button type="submit" class="btn btn-outline">Eliminar noticia</button>
                    {!! Form::close() !!}
                </div>{{-- /.modal-footer --}}
            </div>{{-- /.modal-content --}}
        </div>{{-- /.modal-dialog --}}
    </div>{{-- /.modal --}}
    {{-- /Modal delete --}}
@endsection


@section('specific-footer-js')

    <script>
        {{-- Trigger Modal elete --}}
        $('#confirmDelete').on('show.bs.modal', function(e) {

            var newsId = $(e.relatedTarget).data('news_id');
            var newsName = $(e.relatedTarget).data('news_name');
            var fullUrl = "{{ url('admin/noticia') }}" + '/' + newsId;

            $("#confirmDelete .txt-news-name").text(newsName);
            $("#delForm").attr('action', fullUrl);
        });
    </script>
@endsection
