@extends('backend.layouts.dashboard')

@section('content')

    {{-- //////// News title ///////// --}}
    <?php $page_title = 'Crear categoría'  ?>
    {{-- //////// News title ///////// --}}

    {{-- *** Create news_categories row *** --}}
    <div class="row">

        <div class="col-xs-12">

            <div class="box box-primary">

                {{-- form start --}}
                {!! Form::open([
					'route' => 'admin.news_categories.store',
					'role' => 'form'
				])
                !!}

                {!! Form::hidden('password', 'pass_inicial') !!}

                <div class="box-body">

                    <div class="row">

                        <div class="col-md-12">

							<div class="form-group">

								{!! Form::label('name', _('Título de la categoría')) !!}
								{!! Form::text('name', null,
									[
										'class' => 'form-control',
										'placeholder' => _('Título de la categoría'),
										'required'
									])
								!!}

							</div>{{-- /.form-group --}}

						</div>{{-- /.col-md-12 --}}

                    </div>{{-- /.row --}}

                </div>{{-- /.box-body --}}

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Crear categoría</button>
                </div>{{-- /.box-footer --}}

                {!! Form::close() !!}{{-- /.form --}}

            </div>{{-- /.box --}}

        </div>{{-- /.col --}}

    </div>{{-- /.row --}}
    {{-- /*** Create news_categories row *** --}}

@endsection
