@extends('backend.layouts.dashboard')

@section('content')

    {{-- //////// News title ///////// --}}
    <?php $page_title = 'Listado de notificaciones'  ?>
    {{-- //////// News title ///////// --}}

    <div class="row">

        <div class="col-xs-12">

            <div class="box">

                <div class="box-header">
                    <h3 class="box-title">{{ _('Notificaciones') }}</h3>
                </div>{{-- /.box-header --}}

                <div class="box-body">

                    <table class="table table-bordered table-striped data-table">
                        <thead>
                            <tr class="hide_table">
                                <th>Notificaciones: Nuevas solicitudes</th>
                                <th>Usuario</th>
                            </tr>
                        </thead>
                        <tbody class="hidden">
                            @foreach($requests as $notification)
                                <tr>
                                    @if ($notification->params)
                                        <td><a href="{{ route($notification->url, [implode(',', json_decode($notification->params))]) }}">{{ $notification->message }}</a></td>
                                    @else
                                        <td><a href="{{ route($notification->url) }}">{{ $notification->message }}</a></td>
                                    @endif
                                    <td><a href="{{ route('admin.users.edit', [$notification->user_id]) }}">{{ $notification->user ? $notification->user->full_name : _('Usuario eliminado') }}</a></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                    <table class="table table-bordered table-striped data-table">
                        <thead>
                            <tr class="hide_table">
                                <th>Notificaciones: Ofertas aceptadas</th>
                                <th>Usuario</th>
                            </tr>
                        </thead>
                        <tbody class="hidden">
                            @foreach($accepted as $notification)
                                <tr>
                                    @if ($notification->params)
                                        <td><a href="{{ route($notification->url, [implode(',', json_decode($notification->params))]) }}">{{ $notification->message }}</a></td>
                                    @else
                                        <td><a href="{{ route($notification->url) }}">{{ $notification->message }}</a></td>
                                    @endif
                                    <td><a href="{{ route('admin.users.edit', [$notification->user_id]) }}">{{ $notification->user ? $notification->user->full_name : _('Usuario eliminado') }}</a></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                    <table class="table table-bordered table-striped data-table">
                        <thead>
                            <tr class="hide_table">
                                <th>Notificaciones: Ofertas rechazadas</th>
                                <th>Usuario</th>
                            </tr>
                        </thead>
                        <tbody class="hidden">
                            @foreach($rejected as $notification)
                                <tr>
                                    @if ($notification->params)
                                        <td><a href="{{ route($notification->url, [implode(',', json_decode($notification->params))]) }}">{{ $notification->message }}</a></td>
                                    @else
                                        <td><a href="{{ route($notification->url) }}">{{ $notification->message }}</a></td>
                                    @endif
                                    <td><a href="{{ route('admin.users.edit', [$notification->user_id]) }}">{{ $notification->user ? $notification->user->full_name : _('Usuario eliminado') }}</a></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                    <table class="table table-bordered table-striped data-table">
                        <thead>
                            <tr class="hide_table">
                                <th>Notificaciones: Ofertas sin respuesta</th>
                                <th>Usuario</th>
                            </tr>
                        </thead>
                        <tbody class="hidden">
                            @foreach($no_response as $notification)
                                <tr>
                                    @if ($notification->params)
                                        <td><a href="{{ route($notification->url, [implode(',', json_decode($notification->params))]) }}">{{ $notification->message }}</a></td>
                                    @else
                                        <td><a href="{{ route($notification->url) }}">{{ $notification->message }}</a></td>
                                    @endif
                                    <td><a href="{{ route('admin.users.edit', [$notification->user_id]) }}">{{ $notification->user ? $notification->user->full_name : _('Usuario eliminado') }}</a></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                    <table class="table table-bordered table-striped data-table">
                        <thead>
                            <tr class="hide_table">
                                <th>Notificaciones: Inversiones</th>
                                <th>Usuario</th>
                            </tr>
                        </thead>
                        <tbody class="hidden">
                            @foreach($invests as $notification)
                                <tr>
                                    @if ($notification->params)
                                        <td><a href="{{ route($notification->url, [implode(',', json_decode($notification->params))]) }}">{{ $notification->message }}</a></td>
                                    @else
                                        <td><a href="{{ route($notification->url) }}">{{ $notification->message }}</a></td>
                                    @endif
                                    <td><a href="{{ route('admin.users.edit', [$notification->user_id]) }}">{{ $notification->user ? $notification->user->full_name : _('Usuario eliminado') }}</a></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>{{-- /.box-body --}}

            </div>{{-- /.box --}}

        </div>{{-- /.col --}}

    </div>{{-- /.row --}}

    <div class="row">

        <div class="col-xs-12">
	    @can ('create', new \App\Models\Notification)
            <div class="box">

                <div class="box-header">
                    <h3 class="box-title">{{ _('Nuevos casos de éxito') }}</h3>
                </div>{{-- /.box-header --}}

                <div class="box-body">

                    <a class="btn btn-success" href="{{ route('admin.notifications.create') }}"><i class="fa fa-plus"></i> Crear caso de éxito</a>

                </div>{{-- /.box-body --}}

            </div>{{-- /.box --}}
	    @endcan

        </div>{{-- /.col --}}

    </div>{{-- /.row --}}

    {{-- Modal delete --}}
    <div class="modal modal-danger fade" id="confirmDelete">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Eliminar caso de éxito</h4>
                </div>
                <div class="modal-body">
                    <p>¿Estás seguro que quieres eliminar la siguiente caso de éxito?</p>
                    <p>
                        <span class="txt-notifications-name"></span>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cerrar</button>
                    {!! Form::open(['method' => 'DELETE', 'id'=>'delForm']) !!}
                        <button type="submit" class="btn btn-outline">Eliminar caso de éxito</button>
                    {!! Form::close() !!}
                </div>{{-- /.modal-footer --}}
            </div>{{-- /.modal-content --}}
        </div>{{-- /.modal-dialog --}}
    </div>{{-- /.modal --}}
    {{-- /Modal delete --}}
@endsection

<style>
.hide_table {
        cursor: pointer;
}
table th {
        width: 50%;
}
table {
        margin-bottom: 10px !important;
}
</style>

@section('specific-footer-js')

    <script>
        $('.hide_table').click(function() {
            $(this).parents('table').find('tbody').toggleClass('hidden');
        });

        {{-- Trigger Modal elete --}}
        $('#confirmDelete').on('show.bs.modal', function(e) {

            var notificationsId = $(e.relatedTarget).data('notifications_id');
            var notificationsName = $(e.relatedTarget).data('notifications_name');
            var fullUrl = "{{ url('admin/caso-de-exito') }}" + '/' + notificationsId;

            $("#confirmDelete .txt-notifications-name").text(notificationsName);
            $("#delForm").attr('action', fullUrl);
        });
    </script>
@endsection
