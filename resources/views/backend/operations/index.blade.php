@extends('backend.layouts.dashboard')

@section('page_title', 'Listado de operaciones')

@section('content')

    <div class="row">

        <div class="col-xs-12">

            <div class="box">

                <div class="box-header">
                    <h3 class="box-title">{{ _('Operaciones') }}</h3>
                </div>{{-- /.box-header --}}

                <div class="box-body">

                    {!! $operations->render() !!}

                    <table id="operations" class="table table-bordered table-striped data-table">

                        <thead>

                        <tr>
                            <th>Operación</th>
                            <th style="text-align: right;">Precio</th>
                            <th style="text-align: right;">Recaudado</th>
                            <th>Estado</th>
                            <th>Estado del préstamo</th>
                            <th>Validada</th>
                            <th class="no-sort">Acciones</th>
                        </tr>

                        </thead>

                        <tbody>

                        @foreach($operations as $operation)
                            <tr>
                                <td><a href="{{ route('admin.operations.show', [$operation->id]) }}">{{ $operation->invoice->name }}</a></td>
                                <td style="text-align: right;">
                                    {{ number_format($operation->invoice->price, 2, ',', '.') }}
                                </td>
                                <td style="text-align: right;">
                                    {{ number_format($operation->getCurrentInvested(), 2, ',', '.') }}
                                </td>
								<td>{{ $status[$operation->status] }}</td>
								<td>{{ \App\Models\Operation::getLoanStatus($operation->loan_status) }}</td>
								<td>{{ $operation->active ? _('Validada') : _('Inactiva') }}</td>
                                <td class="text-right">
									@if (!$operation->active && $operation->status == 5)
										@can ('edit', new \App\Models\Operation)
											<a class="btn btn-sm btn-success" href="{{ route('admin.operations.edit', $operation->id) }}">
												<i class="fa fa-pencil"></i> {{ _('Validar') }}
											</a>
										@endcan
									@endif
                                </td>
                            </tr>
                        @endforeach

                        </tbody>

                        <tfoot>

                        <tr>
                            <th>Operación</th>
                            <th style="text-align: right;">Precio</th>
                            <th style="text-align: right;">Recaudado</th>
                            <th>Estado</th>
                            <th>Validada</th>
                            <th class="no-sort">Acciones</th>
                        </tr>

                        </tfoot>

                    </table>

                    {!! $operations->render() !!}

                </div>{{-- /.box-body --}}

            </div>{{-- /.box --}}

        </div>{{-- /.col --}}

    </div>{{-- /.row --}}

@endsection
