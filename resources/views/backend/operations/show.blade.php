@extends('backend.layouts.dashboard')

@section('page_title', $operation->invoice->name)

@section('content')

<div class="box box-primary">

    <div class="box-body">

        <p><strong>Empresa cedente</strong>: <a href="{{ route('admin.users.edit', [$operation->invoice->transferorCompany->user_id]) }}">{{ $operation->invoice->transferorCompany->name }}</a></p>
        <p><strong>Empresa deudora</strong>: <a href="{{ route('admin.users.edit', [$operation->invoice->debtorCompany->user_id]) }}">{{ $operation->invoice->debtorCompany->name }}</a></p>
        <p><strong>Formulario de registro del préstamo</strong>: <a href="{{ route('admin.invoices.edit', [$operation->invoice_id]) }}">{{ $operation->invoice->name }}</a></p>
        <p><strong>Precio</strong>: {{ number_format($operation->invoice->price, 2, ',', '.') }} €</p>
        <p><strong>Recaudado</strong>: {{ number_format($operation->getCurrentInvested(), 2, ',', '.') }} €</p>
        <p><strong>Estado</strong>: {{ $status[$operation->status] }}</p>
        <p><strong>Estado del préstamo</strong>:
            {!! Form::select('loan_status', \App\Models\Operation::getLoanStatus(), $operation->loan_status, ['id' => 'ajax_change_loan_status', 'url' => route('admin.operations.updateStatus', [$operation->id])]) !!}
        </p>
        <p><strong>Fecha devolución</strong>: {{ Functions::beautyDateTime($operation->invoice->expires) }}</p>
        <p><strong>Validado</strong>:
            @if ($operation->active)
                {{ _('Validada') }}
            @else
                {{ _('Inactiva') }}
                <a href="{{ route('admin.operations.edit', [$operation->id]) }}">Validar</a>
            @endif
        </p>

    </div>{{-- /.box-body --}}

</div>{{-- /.box --}}

<div class="box box-primary">

    <div class="box-body">

        <h3>Inversiones</h3>

        <table class="table table-responsive table-condensed">
            <thead>
                <tr>
                    <th>{{ _('Inversor') }}</th>
                    <th>{{ _('Tipo') }}</th>
                    <th>{{ _('Cantidad') }}</th>
                    <th>{{ _('Fecha') }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($operation->transfersGrouped as $type => $investors)
                    @foreach ($investors as $investor)
                        <tr>
                            <td>
                                @if (!empty($investor['investor']))
                                    <a href="{{ route('admin.users.edit', [$investor['investor']->user_id]) }}">{{ $investor['investor']->user->full_name }}</a>
                                @else
                                    {{ _('Emprestamo') }}
                                @endif
                            </td>
                            <td>
                                {{ $transfer_types[$type] }}
				@if (!$operation->active)
					@if ($type == 3 || $type == 4 || $type == 7)
						({{ _('Pendiente') }})
					@endif
				@endif
                            </td>
                            <td>
                                {{ number_format($investor['quantity'], 2, ',', '.') }} €
                            </td>
                            <td>
                                {{ Functions::beautyDateTime($investor['date']) }}
                            </td>
                        </tr>
                    @endforeach
                @endforeach
            </tbody>
        </table>

    </div>{{-- /.box-body --}}

</div>{{-- /.box --}}

@endsection

@section('specific-footer-js')
    <script>
        $('#ajax_change_loan_status').change(function() {
            $.post($(this).attr('url'), {loan_status: $(this).val()});
        });
    </script>
@endsection
