@extends('backend.layouts.dashboard')

@section('content')

        <!-- Info boxes -->
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-asterisk"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Financiaciones totales</span>
                    <span class="info-box-number">465</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa-edit"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Financiaciones abiertos</span>
                    <span class="info-box-number">40</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-green"><i class="fa fa-line-chart"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Solicitudes totales</span>
                    <span class="info-box-number">199.992</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="fa fa-group"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Usuarios registrados</span>
                    <span class="info-box-number">49.956</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

    {{--<div class="row">--}}
        {{--<div class="col-md-12">--}}
            {{--<div class="box">--}}
                {{--<div class="box-header with-border">--}}
                    {{--<h3 class="box-title">Informe Mensual Ventas</h3>--}}

                    {{--<div class="box-tools pull-right">--}}
                        {{--<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>--}}
                        {{--</button>--}}
                        {{--<div class="btn-group">--}}
                            {{--<button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">--}}
                                {{--<i class="fa fa-wrench"></i></button>--}}
                            {{--<ul class="dropdown-menu" role="menu">--}}
                                {{--<li><a href="#">Action</a></li>--}}
                                {{--<li><a href="#">Another action</a></li>--}}
                                {{--<li><a href="#">Something else here</a></li>--}}
                                {{--<li class="divider"></li>--}}
                                {{--<li><a href="#">Separated link</a></li>--}}
                            {{--</ul>--}}
                        {{--</div>--}}
                        {{--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<!-- /.box-header -->--}}
                {{--<div class="box-body">--}}
                    {{--<div class="row">--}}
                        {{--<div class="col-md-8">--}}
                            {{--<p class="text-center">--}}
                                {{--<strong>Ventas: 10 Oct, 2015 - 27 Nov, 2015</strong>--}}
                            {{--</p>--}}

                            {{--<div class="chart">--}}
                                {{--<!-- Sales Chart Canvas -->--}}
                                {{--<canvas id="salesChart" style="height: 180px;"></canvas>--}}
                            {{--</div>--}}
                            {{--<!-- /.chart-responsive -->--}}
                        {{--</div>--}}
                        {{--<!-- /.col -->--}}
                        {{--<div class="col-md-4">--}}
                            {{--<p class="text-center">--}}
                                {{--<strong>Goal Completion</strong>--}}
                            {{--</p>--}}

                            {{--<div class="progress-group">--}}
                                {{--<span class="progress-text">Add Products to Cart</span>--}}
                                {{--<span class="progress-number"><b>160</b>/200</span>--}}

                                {{--<div class="progress sm">--}}
                                    {{--<div class="progress-bar progress-bar-aqua" style="width: 80%"></div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<!-- /.progress-group -->--}}
                            {{--<div class="progress-group">--}}
                                {{--<span class="progress-text">Complete Purchase</span>--}}
                                {{--<span class="progress-number"><b>310</b>/400</span>--}}

                                {{--<div class="progress sm">--}}
                                    {{--<div class="progress-bar progress-bar-red" style="width: 80%"></div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<!-- /.progress-group -->--}}
                            {{--<div class="progress-group">--}}
                                {{--<span class="progress-text">Visit Premium Page</span>--}}
                                {{--<span class="progress-number"><b>480</b>/800</span>--}}

                                {{--<div class="progress sm">--}}
                                    {{--<div class="progress-bar progress-bar-green" style="width: 80%"></div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<!-- /.progress-group -->--}}
                            {{--<div class="progress-group">--}}
                                {{--<span class="progress-text">Send Inquiries</span>--}}
                                {{--<span class="progress-number"><b>250</b>/500</span>--}}

                                {{--<div class="progress sm">--}}
                                    {{--<div class="progress-bar progress-bar-yellow" style="width: 80%"></div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<!-- /.progress-group -->--}}
                        {{--</div>--}}
                        {{--<!-- /.col -->--}}
                    {{--</div>--}}
                    {{--<!-- /.row -->--}}
                {{--</div>--}}
                {{--<!-- ./box-body -->--}}
                {{--<div class="box-footer">--}}
                    {{--<div class="row">--}}
                        {{--<div class="col-sm-3 col-xs-6">--}}
                            {{--<div class="description-block border-right">--}}
                                {{--<span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 17%</span>--}}
                                {{--<h5 class="description-header">$35,210.43</h5>--}}
                                {{--<span class="description-text">INGRESOS TOTALES</span>--}}
                            {{--</div>--}}
                            {{--<!-- /.description-block -->--}}
                        {{--</div>--}}
                        {{--<!-- /.col -->--}}
                        {{--<div class="col-sm-3 col-xs-6">--}}
                            {{--<div class="description-block border-right">--}}
                                {{--<span class="description-percentage text-yellow"><i class="fa fa-caret-left"></i> 0%</span>--}}
                                {{--<h5 class="description-header">$10,390.90</h5>--}}
                                {{--<span class="description-text">COSTE TOTAL</span>--}}
                            {{--</div>--}}
                            {{--<!-- /.description-block -->--}}
                        {{--</div>--}}
                        {{--<!-- /.col -->--}}
                        {{--<div class="col-sm-3 col-xs-6">--}}
                            {{--<div class="description-block border-right">--}}
                                {{--<span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 20%</span>--}}
                                {{--<h5 class="description-header">$24,813.53</h5>--}}
                                {{--<span class="description-text">BENEFICIOS TOTALES</span>--}}
                            {{--</div>--}}
                            {{--<!-- /.description-block -->--}}
                        {{--</div>--}}
                        {{--<!-- /.col -->--}}
                        {{--<div class="col-sm-3 col-xs-6">--}}
                            {{--<div class="description-block">--}}
                                {{--<span class="description-percentage text-red"><i class="fa fa-caret-down"></i> 18%</span>--}}
                                {{--<h5 class="description-header">1200</h5>--}}
                                {{--<span class="description-text">OBJETIVOS ALCANZADOS</span>--}}
                            {{--</div>--}}
                            {{--<!-- /.description-block -->--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<!-- /.row -->--}}
                {{--</div>--}}
                {{--<!-- /.box-footer -->--}}
            {{--</div>--}}
            {{--<!-- /.box -->--}}
        {{--</div>--}}
        {{--<!-- /.col -->--}}
    {{--</div>--}}
    {{--<!-- /.row -->--}}



    <div class='row'>
        <div class='col-md-6'>
            <!-- Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Inversiones</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    @foreach($tasks as $task)
                        <h5>
                            {{ $task['name'] }}
                            <small class="label label-{{$task['color']}} pull-right">{{$task['progress']}}%</small>
                        </h5>
                        <div class="progress progress-xxs">
                            <div class="progress-bar progress-bar-{{$task['color']}}" style="width: {{$task['progress']}}%"></div>
                        </div>
                    @endforeach

                </div><!-- /.box-body -->
                <div class="box-footer">
                    <form action='#'>
                        <input type='text' placeholder='Buscar inversión' class='form-control input-sm' />
                    </form>
                </div><!-- /.box-footer-->
            </div><!-- /.box -->
        </div><!-- /.col -->
        <div class='col-md-6'>
            <!-- Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ _('Notificaciones') }}</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
					<ul>
					@foreach ($notifications as $notification)
						<li>
							@if ($notification->url)
								@if ($notification->params)
									<a href="{{ route($notification->url, [implode(',', json_decode($notification->params))]) }}">
								@else
									<a href="{{ route($notification->url) }}">
								@endif
							@endif

							@if ($notification->read)
								{{ $notification->message }}
							@else
								<strong>{{ $notification->message }}</strong>
							@endif

							{{ Functions::beautyDateTime($notification->created_at) }}

							@if ($notification->url)
								</a>
							@endif
						</li>
					@endforeach
					</ul>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->

    </div><!-- /.row -->
@endsection
