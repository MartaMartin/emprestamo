@extends('backend.layouts.dashboard')

@section('content')

    {{-- //////// Page title ///////// --}}
    <?php $page_title = 'Listado de páginas'  ?>
    {{-- //////// Page title ///////// --}}

    <div class="row">

        <div class="col-xs-12">

            <div class="box">

                <div class="box-header">
                    <h3 class="box-title">{{ _('Páginas') }}</h3>
                </div>{{-- /.box-header --}}

                <div class="box-body">

                    {!! $pages->render() !!}

                    <table id="pages" class="table table-bordered table-striped data-table">

                        <thead>

                        <tr>
                            <th>Nombre</th>
                            <th>Contenido</th>
                            <th class="no-sort">Acciones</th>
                        </tr>

                        </thead>

                        <tbody>

                        @foreach($pages as $page)
                            <tr>
                                <td>{{ $page->title }}</td>
                                <td>{{ substr(strip_tags($page->content), 0, 100) }}...</td>
                                <td class="text-right">
                                    @can ('edit', new \App\Models\Page)
                                        <a class="btn btn-sm btn-success" href="{{ route('admin.pages.edit', $page->id) }}">
                                            <i class="fa fa-pencil"></i> {{ _('Editar página') }}
                                        </a>
                                    @endcan

                                    @can ('destroy', new \App\Models\Page)
									<button type="button" data-page_id="{{ $page->id }}"
										data-page_title="{{ $page->title }}"
										class="btn btn-danger btn-sm" data-toggle="modal" data-target="#confirmDelete">
                                        <i class="fa fa-trash"></i> {{ _('Eliminar') }}
                                     </button>
                                     @endcan

                                </td>

                            </tr>
                        @endforeach

                        </tbody>

                        <tfoot>

                        <tr>
                            <th>Nombre</th>
                            <th>Contenido</th>
                            <th>Acciones</th>
                        </tr>

                        </tfoot>

                    </table>

                    {!! $pages->render() !!}

                </div>{{-- /.box-body --}}

            </div>{{-- /.box --}}

        </div>{{-- /.col --}}

    </div>{{-- /.row --}}

    <div class="row">

        <div class="col-xs-12">
	    @can ('create', new \App\Models\Page)
            <div class="box">

                <div class="box-header">
                    <h3 class="box-title">{{ _('Nuevas páginas') }}</h3>
                </div>{{-- /.box-header --}}

                <div class="box-body">

                    <a class="btn btn-success" href="{{ route('admin.pages.create') }}"><i class="fa fa-plus"></i> Crear página</a>

                </div>{{-- /.box-body --}}

            </div>{{-- /.box --}}
	    @endcan

        </div>{{-- /.col --}}

    </div>{{-- /.row --}}

    {{-- Modal delete --}}
    <div class="modal modal-danger fade" id="confirmDelete">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Eliminar página</h4>
                </div>
                <div class="modal-body">
                    <p>¿Estás seguro que quieres eliminar la siguiente página?</p>
                    <p>
                        <span class="txt-page-name"></span>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cerrar</button>
                    {!! Form::open(['method' => 'DELETE', 'id'=>'delForm']) !!}
                        <button type="submit" class="btn btn-outline">Eliminar página</button>
                    {!! Form::close() !!}
                </div>{{-- /.modal-footer --}}
            </div>{{-- /.modal-content --}}
        </div>{{-- /.modal-dialog --}}
    </div>{{-- /.modal --}}
    {{-- /Modal delete --}}
@endsection


@section('specific-footer-js')

    <script>
        {{-- Trigger Modal elete --}}
        $('#confirmDelete').on('show.bs.modal', function(e) {

            var pageId = $(e.relatedTarget).data('page_id');
            var pageName = $(e.relatedTarget).data('page_name');
            var fullUrl = "{{ url('admin/pagina') }}" + '/' + pageId;

            $("#confirmDelete .txt-page-name").text(pageName);
            $("#delForm").attr('action', fullUrl);
        });
    </script>
@endsection
