@extends('backend.layouts.dashboard')

@section('content')

        <!-- Info boxes -->
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <a href="{{ route('admin.operations.index') }}">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="fa fa-asterisk"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Financiaciones totales</span>
                        <span class="info-box-number">{{ $d1 }}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </a>
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <a href="{{ route('admin.operations.index') }}">
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="fa fa-edit"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Financiaciones abiertas</span>
                        <span class="info-box-number">{{ $d2 }}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </a>
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <a href="{{ route('admin.users.index') }}">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa fa-line-chart"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Inversores registrados</span>
                        <span class="info-box-number">{{ $d3 }}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </a>
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <a href="{{ route('admin.users.index') }}">
                <div class="info-box">
                    <span class="info-box-icon bg-yellow"><i class="fa fa-group"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Empresas registradas</span>
                        <span class="info-box-number">{{ $d4 }}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </a>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

    <div class='row'>
        <div class='col-md-6'>
            <!-- Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Inversiones</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    @if (count($tasks))
                        @foreach($tasks as $task)
                            <h5>
                                {!! $task['name'] !!}
                                <small class="label label-{{$task['color']}} pull-right">{{$task['progress']}}%</small>
                            </h5>
                            <div class="progress progress-xxs">
                                <div class="progress-bar progress-bar-{{$task['color']}}" style="width: {{$task['progress']}}%"></div>
                            </div>
                        @endforeach
                    @else
                        No hay ninguna inversión en curso ahora mismo
                    @endif
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <form action='#'>
                        <input type='text' placeholder='Buscar inversión' class='form-control input-sm' />
                    </form>
                </div><!-- /.box-footer-->
            </div><!-- /.box -->
        </div><!-- /.col -->
        <div class='col-md-6'>
            <!-- Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ _('Notificaciones') }}</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
					<ul>
					@foreach ($notifications as $notification)
						<li>
							@if ($notification->url)
								@if ($notification->params)
									<a href="{{ route($notification->url, [implode(',', json_decode($notification->params))]) }}">
								@else
									<a href="{{ route($notification->url) }}">
								@endif
							@endif

							@if ($notification->read)
								{{ $notification->message }}
							@else
								<strong>{{ $notification->message }}</strong>
							@endif

							{{ Functions::beautyDateTime($notification->created_at) }}

							@if ($notification->url)
								</a>
							@endif
						</li>
					@endforeach
					</ul>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->

    </div><!-- /.row -->
@endsection
