@if(count($errors) > 0)
    <div class="callout callout-danger lead">
        <h4><i class="fa fa-thumbs-down"></i> Ha habido un problema</h4>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif