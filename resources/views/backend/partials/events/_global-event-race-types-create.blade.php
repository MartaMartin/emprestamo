{{--|| New RaceType ||--}}
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">{{ _('Crear nueva modalidad global') }} </h3>
    </div>{{-- /.box-header --}}

    <div class="box-body" id="dynamic-race-types-create">

        <div class="row">
            {{--||| RaceType btn-add-race-type |||--}}
            <div class="col-xs-12 col-md-12">
                <div class="form-group">
                    <p>Haga uso de esta sección para <strong>Crear</strong> una nueva <strong>Modalidad</strong> global. Siempre podrá ir a la sección <a href="#">Gestionar Modalidades</a> para gestionar las Modalidades existentes.</p>
                    <button class="btn-add-race-type btn btn-success" type="button"><i class="fa fa-plus"></i> Nueva Modalidad</button>
                </div>{{-- /.form-group --}}
            </div>{{-- /.col-md-6 --}}
            {{--||| /RaceType btn-add-race-type |||--}}
        </div>

        {{--||| Dynamic content |||--}}
        <div class="race-types-create-wrap">

        </div>{{-- /.race-types-create-wrap --}}
        {{--||| Dynamic content |||--}}

        <div class="row btn-add-race-type-create-bottom">
            {{--||| RaceType btn-add-race-type |||--}}
            <div class="col-xs-12 col-md-6">
                <div class="form-group">
                    <button class="btn-add-race-type btn btn-success" type="button"><i class="fa fa-plus"></i> Nueva Modalidad</button>
                </div>{{-- /.form-group --}}
            </div>{{-- /.col-md-6 --}}
            {{--||| /RaceType btn-add-race-type |||--}}
        </div>

    </div>{{-- /.box-body --}}

    <div class="box-footer">
        <button type="submit" class="btn btn-primary">Guardar nuevas modalidades</button>
    </div>{{-- /.box-footer --}}

</div>
{{--|| /New RaceType ||--}}


@section('specific-footer-js')
    @parent
    <script>
        $(document).ready(function() {
            var max_fields         = 5; //maximum input boxes allowed
            var race_types_create_wrapper = $(".race-types-create-wrap"); //Fields wrapper
            var dynamic_race_types_create = $("#dynamic-race-types-create"); //Add button ID

            var x = 0; //initlal text box count

            $('.btn-add-race-type-create-bottom').hide(); // Hide del button bottom "Añadir campo de texto"

            $(dynamic_race_types_create).on('click', '.btn-add-race-type', function(e){ //on add input button click
                e.preventDefault();
                if(x < max_fields){ //max input box allowed
                    x++; //text box increment
                    $(race_types_create_wrapper).append(
                        '<div class="container-row container-dynamic">' +
                            '<div class="row">' +
                                '<div class="col-xs-12 col-md-9">' +
                                    '<div class="form-group">' +
                                        '<label for="registration_race_types_create_name[]">Modalidad</label>' +
                                        '<div class="input-group">' +
                                            '<span class="input-group-addon"><i class="fa fa-tag"></i></span>' +
                                            '<input type="text" class="form-control" placeholder="Nombre de la Modalidad" name="registration_race_types_create_name[]" maxlength="45" required>' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +

                                '<div class="col-xs-6 col-md-3">' +
                                    '<div class="form-group">' +
                                        '<label for="registration_race_types_create_length">Distancia de la modalidad <em>(metros)</em></label>' +
                                        '<div class="input-group">' +
                                            '<span class="input-group-addon"><i class="fa fa-flag-checkered"></i></span>' +
                                            '<input type="number" class="form-control" name="registration_race_types_create_length[]" required>' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                            '<div class="row">' +
                                '<div class="col-xs-12 col-md-6">' +
                                    '<div class="form-group">' +
                                        '<button class="btn-remove-race-type btn btn-danger" type="button"><i class="fa fa-minus"></i> Eliminar Modalidad</button>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>'
                    ); //add input box

                    // Mostramos botón Añadir campos en footer de Form
                    if(x > 0) {
                        $('.btn-add-race-type-create-bottom').show();
                    }
                }
            });

            $(race_types_create_wrapper).on("click",".btn-remove-race-type", function(e){ //user click on remove text
                e.preventDefault();
                $(this).parent('div').parent('div').parent('div').parent('div').remove();
                x--;
                // Ocultamos botón Añadir campos en footer de Form
                if(x == 0) {
                    $('.btn-add-race-type-create-bottom').hide();
                }
            });

        });
    </script>
@endsection
