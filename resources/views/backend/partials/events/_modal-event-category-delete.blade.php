{{-- Modal delete --}}
@if(count($event->eventCategories))
    <div class="modal modal-danger fade" id="confirmDeleteCategory">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Eliminar categoría del evento</h4>
                </div>
                <div class="modal-body">
                    <p>¿Estás seguro que quieres eliminar esta categoría del evento?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cerrar</button>  
                    <button type="submit" id="js-submit-category-modal" class="btn btn-outline">Eliminar categoría</button>
                   
                </div>{{-- /.modal-footer --}}
            </div>{{-- /.modal-content --}}
        </div>{{-- /.modal-dialog --}}
    </div>{{-- /.modal --}}
    {{-- /Modal delete --}}


    @section('specific-footer-js')
        @parent
        <script>
            {{-- Trigger Modal delete --}}
            $('#confirmDeleteCategory').on('show.bs.modal', function(e) {
                var registrationCategoryId = $(e.relatedTarget).data('category_id');
		var rowId = $(e.relatedTarget).data('row_id');
                var fullUrl = "{{ url('admin/eventCategory') }}" + '/' + registrationCategoryId + '/delete';
		$("body").on('click', '#js-submit-category-modal', function() {

			$.ajax({
				type: 'GET',
				url: fullUrl,
			        success: function() {
					$('#confirmDeleteCategory').modal('hide');		
	          			$('#' + rowId).fadeOut(600, function () {
						$(this).remove();
					});
				}
			});
		});
            });

            
            {{-- /Trigger Modal delete --}}
        </script>
    @endsection
@endif
