{{-- Modal delete --}}
@if(count($event->registrationFiles))
    <div class="modal modal-danger fade" id="confirmDeleteFile">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Eliminar campo de archivo</h4>
                </div>
                <div class="modal-body">
                    <p>¿Estás seguro que quieres eliminar el campo de archivo de inscripciones?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cerrar</button>
                    <button type="submit" id="js-submit-filefield-modal" class="btn btn-outline">Eliminar fichero</button>
                </div>{{-- /.modal-footer --}}
            </div>{{-- /.modal-content --}}
        </div>{{-- /.modal-dialog --}}
    </div>{{-- /.modal --}}
    {{-- /Modal delete --}}


    @section('specific-footer-js')
        @parent
        <script>
            {{-- Trigger Modal delete --}}
            $('#confirmDeleteFile').on('show.bs.modal', function(e) {
                var registrationFileId = $(e.relatedTarget).data('file_id');
		var rowId = $(e.relatedTarget).data('row_id');
                var fullUrl = "{{ url('admin/registration-file') }}" + '/' + registrationFileId; 

		$("body").on('click', '#js-submit-filefield-modal', function() {

			$.ajax({
				type: 'GET',
				url: fullUrl,
			        success: function() {
					$('#confirmDeleteFile').modal('hide');		
	          			$('#' + rowId).fadeOut(600, function () {
						$(this).remove();
					});
				}
			});
		});
            });
            {{-- /Trigger Modal delete --}}
        </script>
    @endsection
@endif
