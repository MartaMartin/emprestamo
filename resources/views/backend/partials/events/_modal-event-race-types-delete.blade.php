{{-- Modal delete --}}
@if(count($event->eventRaceTypes))
    <div class="modal modal-danger fade" id="confirmDeleteRacetype">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Eliminar modalidad del evento</h4>
                </div>
                <div class="modal-body">
                    <p>¿Estás seguro que quieres eliminar la modalidad seleccionada de este evento?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-outline" id="js-submit-racetype-modal">Eliminar modalidad</button>
                </div>{{-- /.modal-footer --}}
            </div>{{-- /.modal-content --}}
        </div>{{-- /.modal-dialog --}}
    </div>{{-- /.modal --}}
    {{-- /Modal delete --}}


    @section('specific-footer-js')
        @parent
        <script>
            {{-- Trigger Modal delete --}}
            $('#confirmDeleteRacetype').on('show.bs.modal', function(e) {
                var registrationRaceTypeId = $(e.relatedTarget).data('race_type_id');
		var rowId = $(e.relatedTarget).data('row_id');
                var fullUrl = "{{ url('admin/eventRaceType') }}" + '/' + registrationRaceTypeId + '/delete';
		$("body").on('click', '#js-submit-racetype-modal', function() {

			$.ajax({
				type: 'GET',
				url: fullUrl,
			        success: function() {
					$('#confirmDeleteRacetype').modal('hide');		
	          			$('#' + rowId).fadeOut(600, function () {
						$(this).remove();
					});
				}
			});
		});
            });
            {{-- /Trigger Modal delete --}}
        </script>
    @endsection
@endif
