{{--|| Event Organization and Federation ||--}}
<div class="box-header with-border">
    <h3 class="box-title">{{ _('Nueva categoría') }} </h3>
</div>{{-- /.box-header --}}

<div class="box-body" id="dynamic-categories">

    <div class="row">
        {{--||| Event btn-add-category |||--}}
        <div class="col-xs-12 col-md-12">
            <div class="form-group">
		@can ('admin_edit', $event)
                <p>Haga uso de esta sección para Añadir una <strong>Categoría</strong>. Siempre podrá ir a la sección <a href="#">Gestionar Categorías</a></p>
		@endcan
                <button class="btn-add-category btn btn-success" type="button"><i class="fa fa-plus"></i> Añadir Categoría</button>
            </div>{{-- /.form-group --}}
        </div>{{-- /.col-md-6 --}}
        {{--||| /Event btn-add-category |||--}}
    </div>

    {{--||| Dynamic content |||--}}
    <div class="categories-create-wrap">

    </div>{{-- /.categories-create-wrap --}}
    {{--||| Dynamic content |||--}}

    <div class="row btn-add-category-create-bottom">
        {{--||| Event btn-add-category |||--}}
        <div class="col-xs-12 col-md-6">
            <div class="form-group">
                <button class="btn-add-category btn btn-success" type="button"><i class="fa fa-plus"></i> Añadir Categoría</button>
            </div>{{-- /.form-group --}}
        </div>{{-- /.col-md-6 --}}
        {{--||| /Event btn-add-category |||--}}
    </div>

</div>{{-- /.box-body --}}
{{--|| /Event Organization and Federation ||--}}

@section('specific-footer-js')
    @parent
    <script>
        $(document).ready(function() {
            var max_fields         = 5; //maximum input boxes allowed
            var categories_create_wrapper = $(".categories-create-wrap"); //Fields wrapper
            var dynamic_categories_create = $("#dynamic-categories"); //Add button ID
            var race_type_select = "{{ Form::select('event_race_types\[\]', $event_race_types, null, 
					['class' => 'form-control', 'placeholder' => '-- Seleccionar Modalidad --', 'required']
		) }}";
            var category_select = "{{ Form::select('event_categories\[\]', $categories, null, 
					['class' => 'form-control', 'placeholder' => '-- Seleccionar Categoría --', 'required']
		) }}";
            var category_start_time = "{{ Form::date('categories_start_times\[\]', null,['class' => 'form-control']) }}";
            var category_base_price = "{{ Form::number('categories_base_prices\[\]', null,
				['class' => 'form-control', 'step' => '0.01', 'min' => '0']
		) }}";

            race_type_select = $('<div/>').html(race_type_select).text(); // Convert to propper HTML for append
            category_select = $('<div/>').html(category_select).text(); // Convert to propper HTML for append
            category_start_time = $('<div/>').html(category_start_time).text(); // Convert to propper HTML for append
            category_base_price = $('<div/>').html(category_base_price).text(); // Convert to propper HTML for append

            var x = 0; //initlal text box count

            $('.btn-add-category-create-bottom').hide(); // Hide del button bottom "Añadir campo de texto"

            $(dynamic_categories_create).on('click', '.btn-add-category', function(e){ //on add input button click
                e.preventDefault();
                if(x < max_fields){ //max input box allowed
                    x++; //text box increment
                    $(categories_create_wrapper).append(
                        '<div class="container-row container-dynamic">' +
                            '<div class="row">' +
                                '<div class="col-xs-12 col-md-6">' +
                                    '<div class="form-group">' +
                                        '<label for="event_categories[]">Categoría</label>' +
                                        '<div class="input-group">' +
                                            '<span class="input-group-addon"><i class="fa fa-tag"></i></span>' +
					 	category_select +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                                '<div class="col-xs-12 col-md-6">' +
                                    '<div class="form-group">' +
                                        '<label for="event_race_types[]">Modalidad</label>' +
                                        '<div class="input-group">' +
                                            '<span class="input-group-addon"><i class="fa fa-tag"></i></span>' +
					 	race_type_select +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                                '<div class="col-xs-6 col-md-3">' +
                                    '<div class="form-group">' +
                                        '<label for="categories_start_times[]">Hora de salida</label>' +
                                        '<div class="input-group">' +
                                            '<span class="input-group-addon"><i class="fa fa-flag-checkered"></i></span>' +
						category_start_time +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                                '<div class="col-xs-6 col-md-3">' +
                                    '<div class="form-group">' +
                                        '<label for="categories_base_prices[]">Precio base</label>' +
                                        '<div class="input-group">' +
                                            '<span class="input-group-addon"><i class="fa fa-eur"></i></span>' +
						category_base_price +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                            '<div class="row">' +
                                '<div class="col-xs-12 col-md-6">' +
                                    '<div class="form-group">' +
                                        '<button class="btn-remove-category btn btn-danger" type="button"><i class="fa fa-minus"></i> Eliminar Categoría</button>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>'
                    ); //add input box

                    // Mostramos botón Añadir campos en footer de Form
                    if(x > 0) {
                        $('.btn-add-category-create-bottom').show();
                    }
                }
            });

            $(categories_create_wrapper).on("click",".btn-remove-category", function(e){ //user click on remove text
                e.preventDefault();
                $(this).parent('div').parent('div').parent('div').parent('div').remove();
                x--;
                // Ocultamos botón Añadir campos en footer de Form
                if(x == 0) {
                    $('.btn-add-category-create-bottom').hide();
                }
            });

        });
    </script>
@endsection
