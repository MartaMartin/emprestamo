{{--|| Event Race Type ||--}}
@if(count($event->eventCategories))
    <div class="box-header with-border">
        <h3 class="box-title">{{ _('Categorías del evento') }} </h3>
    </div>{{-- /.box-header --}}

    <div class="box-body" id="existing-event-race-types">
@endif

    @foreach($event->eventCategories as $event_category)
        <div class="container-row container-existing" id="js-category-row-{{ $event_category->id }}">
        {!! Form::hidden('event_categories_id[]', $event_category->id) !!}
            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <div class="form-group">
                        {!! Form::label('event_race_types[]', _('Categoría')) !!}
                        <div class="input-group">
				{!! Form::select('event_categories[]', 
					$categories, $event_category->categories_id, 
					['class' => 'form-control', 'placeholder' => '-- Seleccionar Categoría --']) 
				!!}
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="form-group">
                        {!! Form::label('event_race_types[]', _('Modalidad')) !!}
                        <div class="input-group">
				{!! Form::select('event_race_types[]', 
					$event_race_types, $event_category->event_race_types_id, 
					['class' => 'form-control', 'placeholder' => '-- Seleccionar Modalidad --']) 
				!!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
            	<div class="col-xs-4 col-md-3">
                	<div class="form-group">
                        <label for="categories_start_times[]">
				<span class="admin-label-switch">Hora de salida</span> 
            			{!! Form::date('categories_start_times[]', $event_category->start_time, ['class' => 'form-control']) !!}
                        </div>
                 </div>
                 <div class="col-xs-4 col-md-3">
                       <div class="form-group">
                       <label for="event_race_types_team_members_min[]">Precio base</label>
                                <div class="input-group">
                                      <span class="input-group-addon"><i class="fa fa-minus-circle"></i></span>
            				{!! Form::number('categories_base_prices[]', $event_category->start_time, 
						['class' => 'form-control', 'step' => '0.01', 'min' => '0']
					) !!}
                                 </div>
                        </div>
                  </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <div class="form-group">
                        <button type="button" data-category_id="{{ $event_category->id }}" 
				data-row_id="js-category-row-{{ $event_category->id }}"
				class="btn-remove-racetype btn btn-danger" 
				data-toggle="modal" 
				data-target="#confirmDeleteCategory">
				<i class="fa fa-minus"></i> Eliminar categoría
			</button>
                    </div>
                </div>
            </div>
        </div>
    @endforeach

@if(count($event->eventCategories))
    </div>{{-- /.box-body --}}
    {{--|| /Event Existing Registration textfields ||--}}

    <div class="box-footer">
        <button type="submit" class="btn btn-primary">Actualizar categorías existentes</button>
    </div>{{-- /.box-footer --}}

@endif
