{{--|| Event Organization and Federation ||--}}
<div class="box-header with-border">
    <h3 class="box-title">{{ _('Dependencias') }}</h3>
</div>{{-- /.box-header --}}

<div class="box-body">

    <div class="row">

        {{--||| Event organizations_id |||--}}
        <div class="col-xs-12 col-md-6">
            <div class="form-group">
                {!! Form::label('organizations_id', _('Organizador')) !!}
                {!! Form::select('organizations_id', $organizations, $event->organizations_id, [
									    'id' => 'js-organizations-select',
                                                                            'class' => 'form-control',
                                                                            'placeholder' => '-- Sin Organizador --'
                                                                            ]) !!}
            </div>{{-- /.form-group --}}
        </div>{{-- /.col-xs-12 .col-md-6 --}}
        {{--||| /Event organizations_id |||--}}

        {{--||| Event federations_id |||--}}
        <div class="col-xs-12 col-md-6">
            <div class="form-group">
                {!! Form::label('federations_id', _('Federación')) !!}
                {!! Form::select('federations_id', $federations, $event->federations_id, [
                                                                            'class' => 'form-control',
                                                                            'placeholder' => '-- Sin Federación --'
                                                                            ]) !!}
            </div>{{-- /.form-group --}}
        </div>{{-- /.col-xs-12 .col-md-6 --}}
        {{--||| /Event federations_id |||--}}

    </div>{{-- /.row --}}

</div>{{-- /.box-body --}}
{{--|| /Event Organization and Federation ||--}}

{{--|| Event Circuit ||--}}
<div class="box-header with-border">
    <h3 class="box-title">{{ _('Circuitos') }}</h3>
</div>{{-- /.box-header --}}

<div class="box-body">

    <div class="row">

        {{--||| Event circuits_id |||--}}
        <div class="col-xs-12 col-md-10">
            <div class="form-group">
                {!! Form::label('circuits_id', _('Circuito')) !!}
                {!! Form::select(
			'circuits_id', 
			isset($circuits[$event->organizations_id])?$circuits[$event->organizations_id]:[], 
			$event->circuits_id, 
			[
				'id' => 'js-circuits-select',
                        	'class' => 'form-control',
                                'placeholder' => '-- Sin Circuito --'
                        ]
		     ) 
   		  !!}
            </div>{{-- /.form-group --}}
        </div>{{-- /.col-xs-12 .col-md-6 --}}
        {{--||| /Event circuits_id |||--}}

        {{--||| Event circuit_score |||--}}
        <div class="col-xs-12 col-md-2">
            <div class="form-group">
		<input type="hidden" name="circuit_score" value="0"/>
                <label for="circuit_score"><span class="admin-label-switch">{!! _('Clasifica en circuito') !!}</span> 
		<input id="circuits_score_checkbox" name="circuit_score" type="checkbox" data-size="normal" data-on-color="primary" data-off-color="danger" data-on-text="SÍ" data-off-text="NO" value="1"></label>

            </div>
            {{--/.form-group--}}
        </div>
        {{--/.col-xs-12 .col-md-6--}}
        {{--||| /Event circuit_score |||--}}

    </div>{{-- /.row --}}

</div>{{-- /.box-body --}}
{{--|| /Event Circuit ||--}}

{{--| Event Status and Type |--}}
<div class="box-header with-border">
    <h3 class="box-title">{{ _('Estado evento') }}</h3>
</div>{{-- /.box-header --}}

<div class="box-body">

    {{--|| Status and Type ||--}}
    <div class="row">

        {{--||| Event status |||--}}
        <div class="col-md-6">
	    @can ('admin_edit', $event)
            <div class="form-group">
                {!! Form::label('status', _('Estado evento')) !!}
                {!! Form::select('status', [
                                            'active' => 'Activo',
                                            'deactivate' => 'Desactivado',
                                            'open_inscriptions' => 'Inscripciones abiertas',
                                            'closed_inscriptions' => 'Inscripciones cerradas',
                                            'draft' => 'Borrador'
                                            ],
                                            $event->status, [
                                                            'class' => 'form-control',
                                                            'required'
                                                            ])
                !!}
            </div>{{-- /.form-group --}}
	    @endcan
        </div>{{-- /.col-md-6 --}}
        {{--||| /Event status |||--}}

        {{--||| Event type |||--}}
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('type', _('Tipo evento')) !!}
                {!! Form::select('type', [
                                            'internal' => 'Inscripción interna',
                                            'external' => 'Inscripción externa'
                                            ],
                                            $event->type, [
                                                            'class' => 'form-control',
                                                            'required'
                                                            ])
                !!}
            </div>{{-- /.form-group --}}
        </div>{{-- /.col-md-6 --}}
        {{--||| /Event type |||--}}

    </div>{{-- /.row --}}
    {{--|| /Status and Type ||--}}

</div>{{-- /.box-body --}}
{{--| /Event Status and Type |--}}

{{--| External Event |--}}
<div class="box-header with-border">
    <h3 class="box-title">{{ _('Inscripciones externas') }}</h3>
</div>{{-- /.box-header --}}

<div class="box-body">

    {{--|| URL Registration and Results ||--}}
    <div class="row">

        {{--||| External url_registration |||--}}
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('url_registration', _('URL registro externa')) !!}
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-external-link"></i>
                    </span>
                    {!! Form::text('url_registration', $event->url_registration,
                                            [
                                                'class' => 'form-control',
                                                'placeholder' => _('URL registro externa')
                                            ])
                    !!}
                </div>{{-- /.input-group --}}
            </div>{{-- /.form-group --}}
        </div>{{-- /.col-md-6 --}}
        {{--||| /External url_registration |||--}}

        {{--||| External url_results |||--}}
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('url_results', _('URL resultados externa')) !!}
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-external-link-square"></i>
                    </span>
                    {!! Form::text('url_results', $event->url_results,
                                            [
                                                'class' => 'form-control',
                                                'placeholder' => _('URL resultados externa')
                                            ])
                    !!}
                </div>{{-- /.input-group --}}
            </div>{{-- /.form-group --}}
        </div>{{-- /.col-md-6 --}}
        {{--||| /External url_results |||--}}

    </div>{{-- /.row --}}
    {{--|| /URL Registration and Results ||--}}

</div>{{-- /.box-body --}}
{{--| /External Event |--}}

@section('specific-footer-js')
    @parent
    <script>
        var status_switch = "{!! $event->circuit_score ? true : false !!}";
        $("#circuits_score_checkbox").bootstrapSwitch('state', status_switch, true);
//        $("[name='circuit_score']").on('switchChange.bootstrapSwitch', function(event, state) {
//            console.log(this);  // DOM element
//            console.log(event); // jQuery event
//            console.log(state); // true | false
//        });
    </script>
@endsection
