{{--| General Event details |--}}
<div class="box-header with-border">
    <h3 class="box-title">{{ _('Datos generales') }}</h3>
</div>{{-- /.box-header --}}

<div class="box-body">

    {{--|| Edition and name ||--}}
    <div class="row">

        {{--||| Event edition |||--}}
        <div class="col-md-2">
            <div class="form-group">
                {!! Form::label('edition', _('Edición')) !!}
                {!! Form::number('edition', $event->edition,
                                            [
                                                'class' => 'form-control',
                                                'max' => 999
                                            ])
                !!}
            </div>{{-- /.form-group --}}
        </div>{{-- /.col-md-6 --}}
        {{--||| /Event edition |||--}}

        {{--||| Event name |||--}}
        <div class="col-md-10">
            <div class="form-group">
                {!! Form::label('name', _('Nombre del evento')) !!}
                {!! Form::text('name', $event->name,
                                            [
                                                'class' => 'form-control',
                                                'placeholder' => _('Nombre del evento'),
                                                'required'
                                            ])
                !!}
            </div>{{-- /.form-group --}}
        </div>{{-- /.col-md-6 --}}
        {{--||| /Event name |||--}}

    </div>{{-- /.row --}}
    {{--|| /Edition and name ||--}}

    {{--|| Seats and dates ||--}}
    <div class="row">

        {{--||| Event seats |||--}}
        <div class="col-md-2">
            <div class="form-group">
                {!! Form::label('seats', _('Límite de Inscripciones')) !!}
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-group"></i>
                    </span>
                    {!! Form::number('seats', $event->seats,
                                            [
                                                'class' => 'form-control',
                                                'placeholder' => _('Plazas evento'),
                                                'max' => 999999,
                                                'required'
                                            ])
                    !!}
                </div>{{-- /.input-group --}}
            </div>{{-- /.form-group --}}
        </div>{{-- /.col-md-2 --}}
        {{--||| /Event seats |||--}}

        {{--||| Event start_time |||--}}
        {{--WIP--}}
        <div class="col-md-10">
            <div class="form-group">
                {!! Form::label('start_time', _('Fecha del evento')) !!}
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </span>
                    {!! Form::date('start_time', ($event->start_time ? $event->start_time->format('Y-m-d') : null),
                                                [
                                                    'class' => 'form-control',
                                                    'data-inputmask' => "'alias': 'dd/mm/yyyy'",
                                                    'required'

                                                ])
                    !!}
                </div>{{-- /.input-group --}}
            </div>{{-- /.form-group --}}
        </div>{{-- /.col-md-10 --}}
        {{--WIP--}}
        {{--||| /Event start_time |||--}}

    </div>{{-- /.row --}}
    {{--|| /Seats and dates ||--}}

</div>{{-- /.box-body --}}
{{--| /General Event details |--}}

{{--| Location Event details |--}}
<div class="box-header with-border">
    <h3 class="box-title">{{ _('Localización evento') }}</h3>
</div>{{-- /.box-header --}}

<div class="box-body">

    {{--|| City and state ||--}}
    <div class="row">

        {{--||| Event city |||--}}
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('city', _('Ciudad/Población')) !!}
                {!! Form::text('city', $event->city,
                                            [
                                                'class' => 'form-control',
                                                'placeholder' => _('Ciudad/Población evento'),
                                                'required'
                                            ])
                !!}
            </div>{{-- /.form-group --}}
        </div>{{-- /.col-md-6 --}}
        {{--||| /Event city |||--}}

        {{--||| Event state |||--}}
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('state', _('Provincia')) !!}
                {!! Form::select('state', $country_states, $event->state, [
                                                                            'class' => 'form-control',
                                                                            'required'
                                                                            ]) !!}
            </div>{{-- /.form-group --}}
        </div>{{-- /.col-md-6 --}}
        {{--||| /Event state |||--}}

    </div>{{-- /.row --}}
    {{--|| /City and state ||--}}

    {{--|| Start and Finish location ||--}}
    <div class="row">

        {{--||| Event start_location |||--}}
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('start_location', _('Lugar de inicio')) !!}
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-map"></i>
                    </span>
                    {!! Form::text('start_location', $event->start_location,
                                            [
                                                'class' => 'form-control',
                                                'placeholder' => _('Lugar de inicio evento'),
                                                'required'
                                            ])
                    !!}
                </div>{{-- /.input-group --}}
            </div>{{-- /.form-group --}}
        </div>{{-- /.col-md-6 --}}
        {{--||| /Event start_location |||--}}

        {{--||| Event finish_location |||--}}
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('finish_location', _('Lugar de fin')) !!}
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-map-o"></i>
                    </span>
                    {!! Form::text('finish_location', $event->finish_location,
                                            [
                                                'class' => 'form-control',
                                                'placeholder' => _('Lugar de fin evento'),
                                                'required'
                                            ])
                    !!}
                </div>{{-- /.input-group --}}
            </div>{{-- /.form-group --}}
        </div>{{-- /.col-md-6 --}}
        {{--||| /Event finish_location |||--}}

    </div>{{-- /.row --}}
    {{--|| /Start and Finish location ||--}}

</div>{{-- /.box-body --}}
{{--| /Location Event details |--}}

{{--| Registration dates block |--}}
<div class="box-header with-border">
    <h3 class="box-title">{{ _('Fechas inscripciones') }}</h3>
</div>{{-- /.box-header --}}

<div class="box-body">

    {{--|| Registration Start and End time ||--}}
    <div class="row">

        {{--||| Event registration_start_time |||--}}
        {{--WIP--}}
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('registration_start_time', _('Apertura inscripciones')) !!}
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-calendar-check-o"></i>
                    </span>
                    {!! Form::date('registration_start_time', ($event->registration_start_time ? $event->registration_start_time->format('Y-m-d') : null),
                                                [
                                                    'class' => 'form-control',
                                                    'data-inputmask' => "'alias': 'dd/mm/yyyy'",
                                                    'required'

                                                ])
                    !!}
                </div>{{-- /.input-group --}}
            </div>{{-- /.form-group --}}
        </div>{{-- /.col-md-6 --}}
        {{--WIP--}}
        {{--||| /Event registration_start_time |||--}}

        {{--||| Event registration_end_time |||--}}
        {{--WIP--}}
        <div class="col-md-6">

            <div class="form-group">
                {!! Form::label('registration_end_time', _('Cierre inscripciones')) !!}
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-calendar-times-o"></i>
                    </span>
                    {!! Form::date( 'registration_end_time', ($event->registration_end_time ? $event->registration_end_time->format('Y-m-d') : null),
                                                [
                                                    'class' => 'form-control',
                                                    'data-inputmask' => "'alias': 'dd/mm/yyyy'",
                                                    'required'

                                                ])
                    !!}
                </div>{{-- /.input-group --}}
            </div>{{-- /.form-group --}}
        </div>{{-- /.col-md-6 --}}
        {{--WIP--}}
        {{--||| /Event registration_end_time |||--}}

    </div>{{-- /.row --}}
    {{--|| /Registration Start and End time ||--}}


    {{--|| Early registration Start and End time ||--}}
    <div class="row">

        {{--||| Event early_registration_start_time |||--}}
        {{--WIP--}}
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('early_registration_start_time', _('Apertura temprana inscripciones')) !!}
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-calendar-check-o"></i>
                    </span>
                    {!! Form::date( 'early_registration_start_time', ($event->early_registration_start_time ? $event->early_registration_start_time->format('Y-m-d') : null),
                                                [
                                                    'class' => 'form-control',
                                                    'data-inputmask' => "'alias': 'dd/mm/yyyy'",
                                                    'required'

                                                ])
                    !!}
                </div>{{-- /.input-group --}}
            </div>{{-- /.form-group --}}
        </div>{{-- /.col-md-6 --}}
        {{--WIP--}}
        {{--||| /Event early_registration_start_time |||--}}

        {{--||| Event early_registration_end_time |||--}}
        {{--WIP--}}
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('early_registration_end_time', _('Cierre apertura temprana inscripciones')) !!}
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-calendar-times-o"></i>
                    </span>
                    {!! Form::date( 'early_registration_end_time', ($event->early_registration_end_time ? $event->early_registration_end_time->format('Y-m-d') : null),
                                                [
                                                    'class' => 'form-control',
                                                    'data-inputmask' => "'alias': 'dd/mm/yyyy'",
                                                    'required'

                                                ])
                    !!}

                </div>{{-- /.input-group --}}
            </div>{{-- /.form-group --}}
        </div>{{-- /.col-md-6 --}}
        {{--WIP--}}
        {{--||| /Event early_registration_end_time |||--}}

    </div>{{-- /.row --}}
    {{--|| /Early registration Start and End time ||--}}

    {{--|| Early registration Start and End time ||--}}
    <div class="row">

        {{--||| Event early_registration_start_time |||--}}
        {{--WIP--}}
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('late_registration_end_time', _('Cierre tardío inscripciones')) !!}
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-calendar-times-o"></i>
                    </span>
                    {!! Form::date( 'late_registration_end_time', ($event->late_registration_end_time ? $event->late_registration_end_time->format('Y-m-d') : null),
                                                [
                                                    'class' => 'form-control',
                                                    'data-inputmask' => "'alias': 'dd/mm/yyyy'",
                                                    'required'

                                                ])
                    !!}
                </div>{{-- /.input-group --}}
            </div>{{-- /.form-group --}}
        </div>{{-- /.col-md-6 --}}
        {{--WIP--}}
        {{--||| /Event late_registration_end_time |||--}}

    </div>{{-- /.row --}}
    {{--|| /Late registration End time ||--}}

</div>{{-- /.box-body --}}
{{--| /Registration dates block |--}}