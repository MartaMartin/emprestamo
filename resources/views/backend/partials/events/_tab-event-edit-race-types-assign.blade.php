{{--|| Event Race Type ||--}}
<div class="box-header with-border">
    <h3 class="box-title">{{ _('Asignar modalidad a evento') }} </h3>
</div>{{-- /.box-header --}}

<div class="box-body" id="dynamic-event-race-types-assign">

    <div class="row">
        {{--||| Event btn-add-race-type |||--}}
        <div class="col-xs-12 col-md-12">
            <div class="form-group">
		@can ('admin_edit', $event)
                <p>Haga uso de esta sección para <strong>Asignar</strong> una <strong>Modalidad</strong> al evento. Siempre podrá ir a la sección <a href="#">Gestionar Modalidades</a> para gestionar las Modalidades existentes.</p>
		@endcan
                <button class="btn-add-race-type btn btn-success" type="button"><i class="fa fa-plus"></i> Asignar Modalidad</button>
            </div>{{-- /.form-group --}}
        </div>{{-- /.col-md-6 --}}
        {{--||| /Event btn-add-race-type |||--}}
    </div>

    {{--||| Dynamic content |||--}}
    <div class="event-race-types-assign-wrap">

    </div>{{-- /.event-race-types-assign-wrap --}}
    {{--||| Dynamic content |||--}}

    <div class="row btn-add-race-type-assign-bottom">
        {{--||| Event btn-add-race-type |||--}}
        <div class="col-xs-12 col-md-6">
            <div class="form-group">
                <button class="btn-add-race-type btn btn-success" type="button"><i class="fa fa-plus"></i> Asignar Modalidad</button>
            </div>{{-- /.form-group --}}
        </div>{{-- /.col-md-6 --}}
        {{--||| /Event btn-add-race-type |||--}}
    </div>

</div>{{-- /.box-body --}}
{{--|| /Event Race Type ||--}}

@section('specific-footer-js')
    @parent
    <script>
        $(document).ready(function() {
            var max_fields                 = 5; //maximum input boxes allowed
            var event_race_types_assign_wrapper  = $(".event-race-types-assign-wrap"); //Fields wrapper
            var dynamic_event_race_types_assign  = $("#dynamic-event-race-types-assign"); //Add button ID
            var race_type_id           = "{{ Form::hidden('event_race_types_id\[\]', '', ['class' => 'form-control']) }}";
            var race_type_select           = "{{ Form::select('event_race_types\[\]', $race_types, null, ['class' => 'form-control', 'placeholder' => '-- Seleccionar Modalidad --']) }}";
            var race_type_start_time_select = "{{ Form::date('event_race_types_start_time\[\]', null,['class' => 'form-control','required']) }}";
            race_type_select = $('<div/>').html(race_type_select).text(); // Convert to propper HTML for append
            race_type_id = $('<div/>').html(race_type_id).text(); // Convert to propper HTML for append
            race_type_start_time_select = $('<div/>').html(race_type_start_time_select).text(); // Convert to propper HTML for append

            var x = 0; //initlal text box count
            var y = 0; // Counter para IDs

            $('.btn-add-race-type-assign-bottom').hide(); // Hide del button bottom "Añadir campo de texto"

            $(dynamic_event_race_types_assign).on('click', '.btn-add-race-type', function(e){ //on add input button click
                e.preventDefault();
                if(x < max_fields){ //max input box allowed
                    x++; //text box increment
                    y++;
                    $(event_race_types_assign_wrapper).append(
			' ' + race_type_id +
                        '<div class="container-row container-dynamic">' +
                            '<div class="row">' +
                                '<div class="col-xs-12 col-md-4">' +
                                    '<div class="form-group">' +
                                        '<label for="event_race_types[]">Modalidad</label>' +
                                        race_type_select +
                                    '</div>' +
                                '</div>' +
                                '<div class="col-xs-12 col-md-4">' +
                                    '<div class="form-group">' +
                                        '<label for="event_race_types_start_time[]">Hora de salida</label>' +
                                        '<div class="input-group">' +
                                            '<span class="input-group-addon"><i class="fa fa-calendar"></i></span>' +
                                            race_type_start_time_select +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                                '<div class="col-xs-6 col-md-2">' +
                                    '<div class="form-group">' +
                                        '<label for="event_race_types_seats[]">Nº Plazas</label>' +
                                        '<div class="input-group">' +
                                            '<span class="input-group-addon"><i class="fa fa-users"></i></span>' +
                                            '<input type="number" class="form-control" name="event_race_types_seats[]" step="0" min="0">' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                                '<div class="col-xs-6 col-md-2">' +
                                    '<div class="form-group">' +
                                        '<label for="event_race_types_base_price[]">Precio modalidad</label>' +
                                        '<div class="input-group">' +
                                            '<span class="input-group-addon"><i class="fa fa-eur"></i></span>' +
                                            '<input type="number" class="form-control" name="event_race_types_base_price[]" step="0.01" min="0">' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +

                            '<div class="row">' +
                                '<div class="col-xs-4 col-md-2">' +
                                    '<div class="form-group">' +
                                        '<label for="event_race_types_team_registration[]"><span class="admin-label-switch">Registro por equipos</span> <input name="event_race_types_team_registration[]" type="hidden" value="0"/> <input name="event_race_types_team_registration[]" type="checkbox" data-size="normal" data-on-color="primary" data-off-color="danger" data-on-text="SÍ" data-off-text="NO" value="1" id="dynamic-team-registration-check-' + y + '"></label>' +
                                    '</div>' +
                                '</div>' +

                                '<div class="col-xs-4 col-md-2">' +
                                    '<div class="form-group">' +
                                        '<label for="event_race_types_team_members_min[]">Nº min. equipo</label>' +
                                        '<div class="input-group">' +
                                            '<span class="input-group-addon"><i class="fa fa-minus-circle"></i></span>' +
                                            '<input type="number" class="form-control" name="event_race_types_team_members_min[]" step="0" min="0">' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +

                                '<div class="col-xs-4 col-md-2">' +
                                    '<div class="form-group">' +
                                        '<label for="event_race_types_team_members_max[]">Nº max. equipo</label>' +
                                        '<div class="input-group">' +
                                            '<span class="input-group-addon"><i class="fa fa-plus-circle"></i></span>' +
                                            '<input type="number" class="form-control" name="event_race_types_team_members_max[]" step="0" min="0">' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +

                            '</div>' +
                            '<div class="row">' +
                                '<div class="col-xs-12 col-md-6">' +
                                    '<div class="form-group">' +
                                        '<button class="btn-remove-race-type btn btn-danger" type="button"><i class="fa fa-minus"></i> Eliminar Modalidad</button>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>'
                    ); //add input box

                    $("#dynamic-team-registration-check-"+y).bootstrapSwitch('state', false, true);

                    // Mostramos botón Añadir campos en footer de Form
                    if(x > 0) {
                        $('.btn-add-race-type-assign-bottom').show();
                    }
                }
            });

            $(event_race_types_assign_wrapper).on("click",".btn-remove-race-type", function(e){ //user click on remove text
                e.preventDefault();
                $(this).parent('div').parent('div').parent('div').parent('div').remove();
                x--;
                // Ocultamos botón Añadir campos en footer de Form
                if(x == 0) {
                    $('.btn-add-race-type-assign-bottom').hide();
                }
            });

        });
    </script>
@endsection
