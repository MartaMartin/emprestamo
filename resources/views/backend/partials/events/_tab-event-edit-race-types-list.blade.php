{{--|| Event Race Type ||--}}
@if(count($event->eventRaceTypes))
    <div class="box-header with-border">
        <h3 class="box-title">{{ _('Modalidades del evento') }} </h3>
    </div>{{-- /.box-header --}}

    <div class="box-body" id="existing-event-race-types">
@endif

    @foreach($event->eventRaceTypes as $event_race_type)
        <div class="container-row container-existing" id="js-race-type-row-{{ $event_race_type->id }}">
        {!! Form::hidden('event_race_types_id[]', $event_race_type->id) !!}
            <div class="row">
                <div class="col-xs-12 col-md-4">
                    <div class="form-group">
                        {!! Form::label('event_race_types[]', _('Modalidad')) !!}
                        <div class="input-group">
				{!! Form::select('event_race_types[]', 
					$race_types, $event_race_type->race_types_id, 
					['class' => 'form-control', 'placeholder' => '-- Seleccionar Modalidad --']) 
				!!}
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-4">
                    <div class="form-group">
                        {!! Form::label('event_race_types_start_time[]', _('Hora de salida')) !!}
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>

            			{!! Form::date('event_race_types_start_time[]', $event_race_type->start_time ,
					['class' => 'form-control','required']) !!}
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-2">
                    <div class="form-group">
                        {!! Form::label('event_race_types_seats[]', _('Nº de plazas')) !!}
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-users"></i></span>
				{!! Form::number('event_race_types_seats[]', $event_race_type->seats,
					['class' => 'form-control', 'step' => 0, 'min' => 0])
				!!}
				
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-2">
                    <div class="form-group">
                        {!! Form::label('event_race_types_base_price[]', _('Precio modalidad')) !!}
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-eur"></i></span>
				{!! Form::number('event_race_types_base_price[]', $event_race_type->base_price,
					['class' => 'form-control', 'step' => 0.01, 'min' => 0])
				!!}
				
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
            	<div class="col-xs-4 col-md-2">
                	<div class="form-group">
                        <label for="event_race_types_team_registration[]">
				<span class="admin-label-switch">Registro por equipos</span> 
				{!! Form::hidden('event_race_types_team_registration[]', 0) !!}
				{!! Form::checkbox('event_race_types_team_registration[]', 1, $event_race_type->team_registration,
					['data-size' => 'normal', 'data-on-color' => 'primary', 'data-off-color' => 'danger',
					'data-on-text' => 'SÍ', 'data-off-text' => 'NO', 
					'id' => 'existing-team-registration-check-' . $event_race_type->id ]
				) !!}
                        </div>
                 </div>
                 <div class="col-xs-4 col-md-2">
                       <div class="form-group">
                       <label for="event_race_types_team_members_min[]">Nº min. equipo</label>
                                <div class="input-group">
                                      <span class="input-group-addon"><i class="fa fa-minus-circle"></i></span>
					{!! Form::number('event_race_types_team_members_min[]', $event_race_type->team_members_min,
						['class' => 'form-control', 'step' => 0, 'min' => 0])
					!!}

                                 </div>
                        </div>
                  </div>
                 <div class="col-xs-4 col-md-2">
                       <div class="form-group">
                       <label for="event_race_types_team_members_min[]">Nº max. equipo</label>
                                <div class="input-group">
                                      <span class="input-group-addon"><i class="fa fa-minus-circle"></i></span>
					{!! Form::number('event_race_types_team_members_max[]', $event_race_type->team_members_max,
						['class' => 'form-control', 'step' => 0, 'min' => 0])
					!!}

                                 </div>
                        </div>
                  </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <div class="form-group">
                        <button type="button" data-race_type_id="{{ $event_race_type->id }}" 
				data-row_id="js-race-type-row-{{ $event_race_type->id }}"
				class="btn-remove-racetype btn btn-danger" 
				data-toggle="modal" 
				data-target="#confirmDeleteRacetype">
				<i class="fa fa-minus"></i> Eliminar modalidad
			</button>
                    </div>
                </div>
            </div>
        </div>
    @endforeach

@if(count($event->eventRaceTypes))
    </div>{{-- /.box-body --}}
    {{--|| /Event Existing Registration textfields ||--}}

    <div class="box-footer">
        <button type="submit" class="btn btn-primary">Actualizar modalidades existentes</button>
    </div>{{-- /.box-footer --}}

@section('specific-footer-js')
    @parent
    <script>
        @foreach($event->eventRaceTypes as $event_race_type)
            $("#existing-team-registration-check-"+"{!! $event_race_type->id !!}").bootstrapSwitch('state', "{!! $event_race_type->team_registration !!}", true);
        @endforeach
    </script>
@endsection
@endif
