{{--|| Event Existing Registration files ||--}}
@if(count($event->registrationFiles))
    <div class="box-header with-border">
        <h3 class="box-title">{{ _('Archivos en Inscripción evento') }} </h3>
    </div>{{-- /.box-header --}}

    <div class="box-body" id="existing-files">
@endif

    @foreach($event->registrationFiles as $registration_file)

        <div class="container-row container-existing" id="js-file-row-{{$registration_file->id}}">
        {!! Form::hidden('registration_files_id[]', $registration_file->id) !!}
            <div class="row">
                <div class="col-xs-12 col-md-4">
                    <div class="form-group">
                        <label for="registration_files_name[]">Etiqueta del archivo</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-tag"></i></span>
                            {!! Form::text('registration_files_name[]', $registration_file->name,
                                                        [
                                                            'class' => 'form-control',
                                                            'placeholder' => _('Etiqueta del archivo en el registro'),
                                                            'required'
                                                        ])
                            !!}
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-2">
                    <div class="form-group">
                        {!! Form::label('registration_files_weight[]', _('Posición en formulario')) !!}
                        {!! Form::selectRange('registration_files_weight[]', -10, 10, $registration_file->weight, ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-xs-6 col-md-2">
                    <div class="form-group">
                        <label for="registration_files_mandatory[]"><span class="admin-label-switch">Archivo obligatorio</span>
                            {!! Form::hidden("registration_files_mandatory[]", '0') !!}
                            <input name="registration_files_mandatory[]" type="checkbox" data-size="normal" data-on-color="primary" data-off-color="danger" data-on-text="SÍ" data-off-text="NO" value="1" id="existing-file-check1-{{ $registration_file->id }}">
                        </label>
                    </div>
                </div>
                <div class="col-xs-6 col-md-2">
                    <div class="form-group">
                        <label for="registration_files_bib_mandatory[]"><span class="admin-label-switch">Requerido BIB</span>
                            {!! Form::hidden("registration_files_bib_mandatory[]", '0') !!}
                            <input name="registration_files_bib_mandatory[]" type="checkbox" data-size="normal" data-on-color="primary" data-off-color="danger" data-on-text="SÍ" data-off-text="NO" value="1" id="existing-file-check2-{{ $registration_file->id }}">
                        </label>
                    </div>
                </div>
                <div class="col-xs-6 col-md-2">
                    <div class="form-group">
                        <label for="registration_files_start_mandatory[]"><span class="admin-label-switch">Requerido comienzo</span>
                            {!! Form::hidden("registration_files_start_mandatory[]", '0') !!}
                            <input name="registration_files_start_mandatory[]" type="checkbox" data-size="normal" data-on-color="primary" data-off-color="danger" data-on-text="SÍ" data-off-text="NO" value="1" id="existing-file-check3-{{ $registration_file->id }}">
                        </label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-8">
                    <div class="form-group">
                        <label for="registration_files_description[]">Descripción del archivo</label>
                        <textarea class="form-control" placeholder="Texto explicativo del archivo en el formulario de registro" name="registration_files_description[]" rows="2" required>{{ $registration_file->description }}</textarea>
                    </div>
                </div>
                <div class="col-xs-12 col-md-4">
                    <div class="form-group">
                        <label for="registration_files_race_type[]">Modalidad</label>
                        {{--race_type_select +--}}
                        {!! Form::select('registration_files_race_type[]', $event_race_types, $registration_file->event_race_types_id, [
                            'class' => 'form-control',
                            'placeholder' => '-- Seleccionar Modalidad --',
                            'required']) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <div class="form-group">
                        <button type="button" data-file_id="{{ $registration_file->id }}" class="btn-remove-file btn btn-danger" data-toggle="modal" data-target="#confirmDeleteFile" data-row_id="js-file-row-{{$registration_file->id}}"><i class="fa fa-minus"></i> Eliminar archivo</button>
                    </div>
                </div>
            </div>
        </div>
    @endforeach

@if(count($event->registrationFiles))
    </div>{{-- /.box-body --}}
    {{--|| /Event Existing Registration files ||--}}

    <div class="box-footer">
        <button type="submit" class="btn btn-primary">Actualizar campos archivos existentes</button>
    </div>{{-- /.box-footer --}}

    @section('specific-footer-js')
        @parent
        <script>
            @foreach($event->registrationFiles as $registration_file)
                $("#existing-file-check1-"+"{!! $registration_file->id !!}").bootstrapSwitch('state', "{!! $registration_file->required_for_registration !!}", true);
                $("#existing-file-check2-"+"{!! $registration_file->id !!}").bootstrapSwitch('state', "{!! $registration_file->required_for_bib_number !!}", true);
                $("#existing-file-check3-"+"{!! $registration_file->id !!}").bootstrapSwitch('state', "{!! $registration_file->required_for_start !!}", true);
            @endforeach
        </script>
    @endsection
@endif
