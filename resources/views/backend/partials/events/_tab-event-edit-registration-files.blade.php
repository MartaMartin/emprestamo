{{--|| Event Registration files ||--}}
<div class="box-header with-border">
    <h3 class="box-title">{{ _('Añadir nuevos Archivos') }} </h3>
</div>{{-- /.box-header --}}

<div class="box-body" id="dynamic-files">

    <div class="row">
        {{--||| Event files btn-add-file |||--}}
        <div class="col-xs-12 col-md-12">
            <div class="form-group">
                <p>Haga uso de esta sección para Añadir, Modificar o Eliminar <strong>nuevos archivos</strong> visibles en las formularios de Inscripción.</p>
                <button class="btn-add-file btn btn-success" type="button"><i class="fa fa-plus"></i> Añadir archivo</button>
            </div>{{-- /.form-group --}}
        </div>{{-- /.col-md-6 --}}
        {{--||| /Event files btn-add-file |||--}}
    </div>

    {{--||| Dynamic content |||--}}
    <div class="files-wrap">

    </div>{{-- /.input_fields_wrap --}}
    {{--||| /Dynamic content |||--}}

    <div class="row btn-add-file-bottom">
        {{--||| Event files btn-add-file |||--}}
        <div class="col-xs-12 col-md-6">
            <div class="form-group">
                <button class="btn-add-file btn btn-success" type="button"><i class="fa fa-plus"></i> Añadir archivo</button>
            </div>{{-- /.form-group --}}
        </div>{{-- /.col-md-6 --}}
        {{--||| /Event files btn-add-file |||--}}
    </div>

</div>{{-- /.box-body --}}
{{--|| /Event Registration files ||--}}

@section('specific-footer-js')
    @parent
    <script>
        $(document).ready(function() {
            var max_fields         = 10; //maximum input boxes allowed
            var files_wrapper = $(".files-wrap"); //Fields wrapper
            var dynamic_files = $("#dynamic-files"); //Add button ID
            var weight_files_select      = "{{ Form::selectRange('registration_files_weight\[\]', -10, 10, 0, ['class' => 'form-control']) }}";
            var race_type_select   = "{{ Form::select('registration_files_race_type\[\]', $event_race_types, null, ['class' => 'form-control', 'placeholder' => '-- Seleccionar Modalidad --', 'required']) }}";
            weight_files_select = $('<div/>').html(weight_files_select).text(); // Convert to propper HTML for append
            race_type_select = $('<div/>').html(race_type_select).text(); // Convert to propper HTML for append

            var x = 0; //initlal text box count
            var y = 0; // Counter para IDs

            $('.btn-add-file-bottom').hide(); // Hide del button bottom "Añadir campo de texto"

            $(dynamic_files).on('click', '.btn-add-file', function(e){ //on add input button click
                e.preventDefault();
                if(x < max_fields){ //max input box allowed
                    x++; //text box increment
                    y++;
                    $(files_wrapper).append(
                        '<div class="container-row container-dynamic">' +
                            '<div class="row">' +
                                '<div class="col-xs-12 col-md-4">' +
                                    '<div class="form-group">' +
                                        '<label for="registration_files_name[]">Etiqueta del archivo</label>' +
                                        '<div class="input-group">' +
                                            '<span class="input-group-addon"><i class="fa fa-tag"></i></span>' +
                                            '<input type="text" class="form-control" placeholder="Etiqueta del archivo en el registro" name="registration_files_name[]" required>' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                                '<div class="col-xs-12 col-md-2">' +
                                    '<div class="form-group">' +
                                        '<label for="registration_files_weight[]">Posición en formulario</label>' +
                                        weight_files_select +
                                    '</div>' +
                                '</div>' +
                                '<div class="col-xs-6 col-md-2">' +
                                    '<div class="form-group">' +
                                        '<label for="registration_files_mandatory[]"><span class="admin-label-switch">Archivo obligatorio</span>' +
                                        '<input type="hidden" name="registration_files_mandatory[]" value="0">' +
                                        '<input name="registration_files_mandatory[]" type="checkbox" data-size="normal" data-on-color="primary" data-off-color="danger" data-on-text="SÍ" data-off-text="NO" value="1" id="dynamic-file-check1-' + y + '"></label>' +
                                    '</div>' +
                                '</div>' +
                                '<div class="col-xs-6 col-md-2">' +
                                    '<div class="form-group">' +
                                        '<label for="registration_files_bib_mandatory[]"><span class="admin-label-switch">Requerido BIB</span>' +
                                        '<input type="hidden" name="registration_files_bib_mandatory[]" value="0">' +
                                        '<input name="registration_files_bib_mandatory[]" type="checkbox" data-size="normal" data-on-color="primary" data-off-color="danger" data-on-text="SÍ" data-off-text="NO" value="1" id="dynamic-file-check2-' + y + '"></label>' +
                                    '</div>' +
                                '</div>' +
                                '<div class="col-xs-6 col-md-2">' +
                                    '<div class="form-group">' +
                                        '<label for="registration_files_start_mandatory[]"><span class="admin-label-switch">Requerido comienzo</span>' +
                                        '<input type="hidden" name="registration_files_start_mandatory[]" value="0">' +
                                        '<input name="registration_files_start_mandatory[]" type="checkbox" data-size="normal" data-on-color="primary" data-off-color="danger" data-on-text="SÍ" data-off-text="NO" value="1" id="dynamic-file-check3-' + y + '"></label>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                            '<div class="row">' +
                                '<div class="col-xs-12 col-md-8">' +
                                    '<div class="form-group">' +
                                        '<label for="registration_files_description[]">Descripción del archivo</label>' +
                                        '<textarea class="form-control" placeholder="Texto explicativo del archivo en el formulario de registro" name="registration_files_description[]" rows="5" required></textarea>' +
                                    '</div>' +
                                '</div>' +
                                '<div class="col-xs-12 col-md-4">' +
                                    '<div class="form-group">' +
                                        '<label for="registration_files_race_type[]">Modalidad</label>' +
                                        race_type_select +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                            '<div class="row">' +
                                '<div class="col-xs-12 col-md-6">' +
                                    '<div class="form-group">' +
                                        '<button class="btn-remove-file btn btn-danger" type="button"><i class="fa fa-minus"></i> Eliminar archivo</button>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>'
                    ); //add input box

                    $("#dynamic-file-check1-"+y).bootstrapSwitch('state', false, true);
                    $("#dynamic-file-check2-"+y).bootstrapSwitch('state', false, true);
                    $("#dynamic-file-check3-"+y).bootstrapSwitch('state', false, true);

                    // Mostramos botón Añadir campos en footer de Form
                    if(x > 0) {
                        $('.btn-add-file-bottom').show();
                    }
                }
            });

            $(files_wrapper).on("click",".btn-remove-file", function(e){ //user click on remove text
                e.preventDefault();
                $(this).parent('div').parent('div').parent('div').parent('div').remove();
                x--;
                // Ocultamos botón Añadir campos en footer de Form
                if(x == 0) {
                    $('.btn-add-file-bottom').hide();
                }
            });

        });
    </script>
@endsection