{{--|| Event Existing Registration textfields ||--}}
@if(count($event->registrationTextfields))
    <div class="box-header with-border">
        <h3 class="box-title">{{ _('Campos de texto en Inscripción evento') }} </h3>
    </div>{{-- /.box-header --}}

    <div class="box-body" id="existing-textfields">
@endif

    @foreach($event->registrationTextfields as $registration_textfield)
        <div class="container-row container-existing" id="js-textfield-row-{{$registration_textfield->id}}">
        {!! Form::hidden('registration_textfields_id[]', $registration_textfield->id) !!}
            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <div class="form-group">
                        {!! Form::label('registration_textfields_name[]', _('Etiqueta del archivo')) !!}
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-tag"></i></span>
                            {!! Form::text('registration_textfields_name[]', $registration_textfield->name,
                                                        [
                                                            'class' => 'form-control',
                                                            'placeholder' => _('Etiqueta del archivo en el registro'),
                                                            'required'
                                                        ])
                            !!}
                        </div>
                    </div>
                </div>
                <div class="col-xs-4 col-md-2">
                    <div class="form-group">
                        {!! Form::label('registration_textfields_length[]', _('Nº máximo de caracteres')) !!}
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-keyboard-o"></i></span>
                            {!! Form::number('registration_textfields_length[]', $registration_textfield->max_length,
                                        [
                                            'class' => 'form-control',
                                            'min'   => '0',
                                            'required'
                                        ])
                            !!}
                        </div>
                    </div>
                </div>
                <div class="col-xs-4 col-md-2">
                    <div class="form-group">
                        {!! Form::label('registration_textfields_weight[]', _('Posición en formulario')) !!}
                        {!! Form::selectRange('registration_textfields_weight[]', -10, 10, $registration_textfield->weight, ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-xs-4 col-md-2">
                    <div class="form-group">
                        <label for="registration_textfields_mandatory_{{ $registration_textfield->id }}"><span class="admin-label-switch">Campo obligatorio</span>
			    <input name="registration_textfields_mandatory[]" type="hidden"/>
                            <input name="registration_textfields_mandatory[]" type="checkbox" data-size="normal" data-on-color="primary" data-off-color="danger" data-on-text="SÍ" data-off-text="NO" value="1" id="existing-textfield-check-{{ $registration_textfield->id }}">
                        </label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-8">
                    <div class="form-group">
                        <label for="registration_textfields_description[]">Descripción del campo de texto</label>
                        <textarea class="form-control" placeholder="Texto explicativo del campo de texto en el formulario de registro" name="registration_textfields_description[]" rows="2" required>{{ $registration_textfield->description }}</textarea>
                    </div>
                </div>
                <div class="col-xs-12 col-md-4">
                    <div class="form-group">
                        <label for="registration_textfields_race_type[]">Modalidad</label>
                        {{--race_type_select +--}}
                        {!! Form::select('registration_textfields_race_type[]', $event_race_types, $registration_textfield->event_race_types_id, [
                            'class' => 'form-control',
                            'placeholder' => '-- Seleccionar Modalidad --',
                            'required']) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <div class="form-group">
                        <button type="button" data-textfield_id="{{ $registration_textfield->id }}" class="btn-remove-file btn btn-danger" data-toggle="modal" data-target="#confirmDeleteTxtField" data-row_id="js-textfield-row-{{$registration_textfield->id}}"><i class="fa fa-minus"></i> Eliminar campo de texto</button>
                    </div>
                </div>
            </div>
        </div>
    @endforeach

@if(count($event->registrationTextfields))
    </div>{{-- /.box-body --}}
    {{--|| /Event Existing Registration textfields ||--}}

    <div class="box-footer">
        <button type="submit" class="btn btn-primary">Actualizar campos de texto existentes</button>
    </div>{{-- /.box-footer --}}

    @section('specific-footer-js')
        @parent
        <script>
            @foreach($event->registrationTextfields as $registration_textfields)
                $("#existing-textfield-check-"+"{!! $registration_textfields->id !!}").bootstrapSwitch('state', "{!! $registration_textfields->mandatory !!}", true);
            @endforeach
        </script>
    @endsection
@endif
