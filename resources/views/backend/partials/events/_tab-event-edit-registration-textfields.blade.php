{{--|| Event Organization and Federation ||--}}
<div class="box-header with-border">
    <h3 class="box-title">{{ _('Añadir nuevos Campos de texto') }} </h3>
</div>{{-- /.box-header --}}

<div class="box-body" id="dynamic-textfields">

    <div class="row">
        {{--||| Event btn-add-textfield |||--}}
        <div class="col-xs-12 col-md-12">
            <div class="form-group">
                <p>Haga uso de esta sección para Añadir, Modificar o Eliminar <strong>nuevos campos de texto</strong> visibles en las formularios de Inscripción.</p>
                <button class="btn-add-textfield btn btn-success" type="button"><i class="fa fa-plus"></i> Añadir campo de texto</button>
            </div>{{-- /.form-group --}}
        </div>{{-- /.col-md-6 --}}
        {{--||| /Event btn-add-textfield |||--}}
    </div>

    <div class="textfields-wrap">

    </div>{{-- /.input_fields_wrap --}}

    <div class="row btn-add-textfield-bottom">
        {{--||| Event btn-add-textfield |||--}}
        <div class="col-xs-12 col-md-6">
            <div class="form-group">
                <button class="btn-add-textfield btn btn-success" type="button"><i class="fa fa-plus"></i> Añadir campo de texto</button>
            </div>{{-- /.form-group --}}
        </div>{{-- /.col-md-6 --}}
        {{--||| /Event btn-add-textfield |||--}}
    </div>

</div>{{-- /.box-body --}}
{{--|| /Event Organization and Federation ||--}}

@section('specific-footer-js')
    @parent
    <script>
        $(document).ready(function() {
            var max_fields         = 10; //maximum input boxes allowed
            var textfields_wrapper = $(".textfields-wrap"); //Fields wrapper
            var dynamic_textfields = $("#dynamic-textfields"); //Add button ID
            var weight_textfields_select      = "{{ Form::selectRange('registration_textfields_weight\[\]', -10, 10, 0, ['class' => 'form-control']) }}";
            var race_type_select   = "{{ Form::select('registration_textfields_race_type\[\]', $event_race_types, null, ['class' => 'form-control', 'placeholder' => '-- Seleccionar Modalidad --', 'required']) }}";
            weight_textfields_select = $('<div/>').html(weight_textfields_select).text(); // Convert to propper HTML for append
            race_type_select = $('<div/>').html(race_type_select).text(); // Convert to propper HTML for append

            var x = 0; //initlal text box count
            var y = 0; // Counter para IDs

            $('.btn-add-textfield-bottom').hide(); // Hide del button bottom "Añadir campo de texto"

            $(dynamic_textfields).on('click', '.btn-add-textfield', function(e){ //on add input button click
                e.preventDefault();
                if(x < max_fields){ //max input box allowed
                    x++; //text box increment
                    y++;
                    $(textfields_wrapper).append(
                        '<div class="container-row container-dynamic">' +
                            '<div class="row">' +
                                '<div class="col-xs-12 col-md-6">' +
                                    '<div class="form-group">' +
                                        '<label for="registration_textfields_name[]">Etiqueta del campo</label>' +
                                        '<div class="input-group">' +
                                            '<span class="input-group-addon"><i class="fa fa-tag"></i></span>' +
                                            '<input type="text" class="form-control" placeholder="Etiqueta del campo en el registro" name="registration_textfields_name[]" required>' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +

                                '<div class="col-xs-6 col-md-2">' +
                                    '<div class="form-group">' +
                                        '<label for="registration_textfields_length[]">Nº máximo de caracteres</label>' +
                                        '<div class="input-group">' +
                                            '<span class="input-group-addon"><i class="fa fa-keyboard-o"></i></span>' +
                                            '<input type="number" class="form-control" name="registration_textfields_length[]" min="0" required>' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                                '<div class="col-xs-12 col-md-2">' +
                                    '<div class="form-group">' +
                                        '<label for="registration_textfields_weight[]">Posición en formulario</label>' +
                                        weight_textfields_select +
                                    '</div>' +
                                '</div>' +
                                '<div class="col-xs-6 col-md-2">' +
                                    '<div class="form-group">' +
                                        '<label for="registration_textfields_mandatory[]"><span class="admin-label-switch">Campo obligatorio</span>' +
                                        '<input type="hidden" name="registration_textfields_mandatory[]" value="0">' +
                                        '<input name="registration_textfields_mandatory[]" type="checkbox" data-size="normal" data-on-color="primary" data-off-color="danger" data-on-text="SÍ" data-off-text="NO" value="1" id="dynamic-textfield-check-' + y + '"></label>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                            '<div class="row">' +
                                '<div class="col-xs-12 col-md-8">' +
                                    '<div class="form-group">' +
                                        '<label for="registration_textfields_description[]">Descripción del campo</label>' +
                                        '<textarea class="form-control" placeholder="Texto explicativo del campo en el formulario de registro" name="registration_textfields_description[]" rows="5" required></textarea>' +
                                    '</div>' +
                                '</div>' +
                                '<div class="col-xs-12 col-md-4">' +
                                    '<div class="form-group">' +
                                        '<label for="registration_textfields_race_type[]">Modalidad</label>' +
                                        race_type_select +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                            '<div class="row">' +
                                '<div class="col-xs-12 col-md-6">' +
                                    '<div class="form-group">' +
                                        '<button class="btn-remove-textfield btn btn-danger" type="button"><i class="fa fa-minus"></i> Eliminar campo de texto</button>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>'
                    ); //add input box

                    $("#dynamic-textfield-check-"+y).bootstrapSwitch('state', false, true);

                    // Mostramos botón Añadir campos en footer de Form
                    if(x > 0) {
                        $('.btn-add-textfield-bottom').show();
                    }
                }
            });

            $(textfields_wrapper).on("click",".btn-remove-textfield", function(e){ //user click on remove text
                e.preventDefault();
                $(this).parent('div').parent('div').parent('div').parent('div').remove();
                x--;
                // Ocultamos botón Añadir campos en footer de Form
                if(x == 0) {
                    $('.btn-add-textfield-bottom').hide();
                }
            });

        });
    </script>
@endsection