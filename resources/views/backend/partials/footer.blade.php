<footer class="main-footer">

    {{-- To the right --}}
    <div class="pull-right hidden-xs">
        <a href="http://artvisual.net/" target="_blank">We made this web with <span class="cuore">❤</span></a>
    </div>

    {{-- Default to the left --}}
    <strong>Copyright &copy; {{ date('Y') }} <a href="{{ route('index') }}" target="_blank">Emprestamo</a>.</strong> Todos los derechos reservados.

</footer>{{-- /.main-footer --}}
