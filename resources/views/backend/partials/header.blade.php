{{-- Main Header --}}
<header class="main-header">

    {{-- Logo --}}
    <a href="{{ route('admin.index') }}" class="logo">
        {{-- mini logo for sidebar mini 50x50 pixels --}}
        <span class="logo-mini"><b>EMP</b></span>

        {{-- logo for regular state and mobile devices --}}
        <span class="logo-lg">
            <b>Emprestamo</b>

            @if ($notifications->where('read', 0)->count())
                <div class="small_circle">
                    {{ $notifications->where('read', 0)->count() }}
                </div>
            @endif
        </span>
    </a>


    {{-- Header Navbar --}}
    <nav class="navbar navbar-static-top" role="navigation">
        {{-- Sidebar toggle button--}}
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Navegación</span>
        </a>
        {{-- Navbar Right Menu --}}
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                {{-- User Account Menu --}}
                <li class="dropdown user user-menu">
                    {{-- Menu Toggle Button --}}
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        {{-- The user image in the navbar--}}
                        <img src="{{ asset('assets/img/placeholder.jpg') }}" class="user-image" alt="User Image"/>
                        {{-- hidden-xs hides the username on small devices so only the image appears. --}}
                        <span class="hidden-xs">{{ Auth::user()->name }} {{ Auth::user()->family_name }}</span>
                    </a>
                    <ul class="dropdown-menu">
                        {{-- The user image in the menu --}}
                        <li class="user-header">
                            <img src="{{ asset('/assets/img/placeholder.jpg') }}" class="img-circle" alt="User Image" />
                            <p>
                                {{ Auth::user()->name }} {{ Auth::user()->family_name }}
                                <small>Emprestamo {{ Auth::user()->role }}</small>
                            </p>
                        </li>
                        {{-- Menu Body --}}
                        {{-- Menu Footer--}}
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="{{ route('admin.users.edit', Auth::user()->id) }}" class="btn btn-default btn-flat">Perfil</a>

                                {{--<a class="btn btn-sm btn-success" href="{{ route('admin.users.edit', $user->id) }}"><i class="fa fa-pencil"></i> {{ _('Editar cuenta') }}</a>--}}
                            </div>
                            <div class="pull-right">
                                <a href="{{ route('auth.logout') }}" class="btn btn-default btn-flat">Desconectar</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
