@if (session('message'))
    <div class="callout callout-success lead">
        <h4><i class="fa fa-thumbs-up"></i> Realizado con éxito</h4>
        <p>
            {{ session('message') }}
        </p>
    </div>
@endif