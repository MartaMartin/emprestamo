{{-- Left side column. contains the sidebar --}}
<aside class="main-sidebar">

    {{-- Sidebar: style can be found in sidebar.less --}}
    <section class="sidebar">

        {{-- Sidebar user panel (optional) --}}
        <div class="user-panel">

            <div class="pull-left image">
				<img src="{{ asset('/assets/img/placeholder.jpg') }}" class="img-circle" alt="User Image" />
            </div>{{-- /.pull-left .image --}}

            <div class="pull-left info">
                <p>{{ Auth::user()->name }} {{ Auth::user()->family_name }}</p>
                {{-- Status --}}
                <a href="{{ route('admin.users.edit', Auth::user()->id) }}"><i class="fa fa-circle {{ Auth::user()->role == 'admin' ? 'text-info' : 'text-success' }}"></i> {{ Auth::user()->role }}</a>
            </div>{{-- /.pull-left .info --}}

        </div>{{-- /.user-panel --}}

        {{-- Search form (Optional) --}}
        {{--<form action="#" method="get" class="sidebar-form">--}}

            {{--<div class="input-group">--}}

                {{--<input type="text" name="q" class="form-control" placeholder="Buscar..."/>--}}
                {{--<span class="input-group-btn">--}}
                  {{--<button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>--}}
                {{--</span>--}}

            {{--</div>--}}{{-- /.input-group --}}

        {{--</form>--}}{{-- /.sidebar-form --}}

	    @can ('index', new \App\Models\User)
        <ul class="sidebar-menu">

            <li class="header">ADMINISTRACIÓN</li>

            @foreach (config('admin.sections_aside') as $section_aside)
                @if(Auth::user()->role == 'admin' || $section_aside['has_level'] == Auth::user()->role || $section_aside['has_level'] == 'all')
                    @if($section_aside['has_subs'])

                            <li class="has-subs">

                                <a href="#">
                                <i class="{{ $section_aside['classes'] }}"></i> <span>{{ $section_aside['label'] }}</span> <i class="fa fa-angle-left pull-right"></i>
                                </a>

                                <ul class="treeview-menu">

                                    @foreach($section_aside['subs_aside'] as $sub_aside)

                                        <li @if(Route::currentRouteName() == $sub_aside['route'] && (empty($sub_aside['params']) || !array_diff(Route::getCurrentRoute()->parameters(), $sub_aside['params'])))
                                                class="active"
                                        @endif>

                                            <a href="{{ route($sub_aside['route'], !empty($sub_aside['params']) ? $sub_aside['params'] : []) }}">
                                                <i class="{{ $sub_aside['classes'] }}"></i> <span>{{ $sub_aside['label'] }}</span>
                                            </a>

                                        </li>
                                    @endforeach

                                </ul>{{-- /.treeview-menu --}}

                            </li>

                    @else

                        <li @if(Route::currentRouteName() == $section_aside['route'] && (empty($sub_aside['params']) || array_diff(Route::getCurrentRoute()->parameters(), $section_aside['params'])))
                            class="active" @endif>

                            <a href="{{ route($section_aside['route']) }}">
                                <i class="{{ $section_aside['classes'] }}"></i> <span>{{ $section_aside['label'] }}</span></i>
                            </a>

                        </li>

                    @endif
                @endif

            @endforeach

        </ul>{{-- /.sidebar-menu --}}
		@endcan

        <ul class="sidebar-menu">

            <li class="header">ANALISTA</li>

            @foreach (config('analyst.sections_aside') as $section_aside)
                @if(Auth::user()->role == 'analyst' || $section_aside['has_level'] == Auth::user()->role || $section_aside['has_level'] == 'all')
                    @if($section_aside['has_subs'])

                            <li class="has-subs">

                                <a href="#">
                                <i class="{{ $section_aside['classes'] }}"></i> <span>{{ $section_aside['label'] }}</span> <i class="fa fa-angle-left pull-right"></i>
                                </a>

                                <ul class="treeview-menu">

                                    @foreach($section_aside['subs_aside'] as $sub_aside)

                                        <li @if(Route::currentRouteName() == $sub_aside['route'] && (empty($sub_aside['params']) || !array_diff(Route::getCurrentRoute()->parameters(), $sub_aside['params'])))
                                                class="active"
                                        @endif>

                                            <a href="{{ route($sub_aside['route'], !empty($sub_aside['params']) ? $sub_aside['params'] : []) }}">
                                                <i class="{{ $sub_aside['classes'] }}"></i> <span>{{ $sub_aside['label'] }}</span>
                                            </a>

                                        </li>
                                    @endforeach

                                </ul>{{-- /.treeview-menu --}}

                            </li>

                    @else

                        <li @if(Route::currentRouteName() == $section_aside['route'] && (empty($sub_aside['params']) || array_diff(Route::getCurrentRoute()->parameters(), $section_aside['params'])))
                            class="active" @endif>

                            <a href="{{ route($section_aside['route']) }}">
                                <i class="{{ $section_aside['classes'] }}"></i> <span>{{ $section_aside['label'] }}</span></i>
                            </a>

                        </li>

                    @endif
                @endif

            @endforeach

        </ul>{{-- /.sidebar-menu --}}

    </section>{{-- /.sidebar --}}

</aside>{{-- /.main-sidebar --}}
