@if (session('status'))
    <div class="callout callout-info lead">
        <h4><i class="fa fa-thumbs-up"></i> Realizado con éxito</h4>
        <p>
            {{ session('status') }}
        </p>
    </div>
@endif