@if(Session::has('success'))
    <div class="callout callout-success lead">
        <h4><i class="fa fa-thumbs-up"></i> Realizado con éxito</h4>
        <p>
            {{ Session::get('success') }}
        </p>
    </div>
@endif