{{-- Modal delete --}} 
@if(count($documents))
    <div class="modal modal-danger fade" id="confirmDeleteDocument">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Eliminar documento del usuario</h4>
                </div>
                <div class="modal-body">
                    <p>¿Estás seguro que quieres eliminar el documento adjunto al usuario?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cerrar</button>
                    <button type="submit" id="js-submit-document-modal" class="btn btn-outline">Eliminar documento</button>
                </div>{{-- /.modal-footer --}}
            </div>{{-- /.modal-content --}}
        </div>{{-- /.modal-dialog --}}
    </div>{{-- /.modal --}}
    {{-- /Modal delete --}}


    @section('specific-footer-js')
        @parent
        <script>
            {{-- Trigger Modal delete --}}
            $('#confirmDeleteDocument').on('show.bs.modal', function(e) {
                var registrationDocumentId = $(e.relatedTarget).data('document_id');
				var rowId = $(e.relatedTarget).data('row_id');
                var fullUrl = "{{ url('admin/documentos') }}" + '/' + registrationDocumentId + '/delete'; 

				$("body").on('click', '#js-submit-document-modal', function() {

					$.ajax({
						type: 'GET',
						url: fullUrl,
						success: function() {
							$('#confirmDeleteDocument').modal('hide');		
							$('#' + rowId).fadeOut(600, function () {
								$(this).remove();
							});
						}
					});
				});
			});
            {{-- /Trigger Modal delete --}}
        </script>
    @endsection
@endif
