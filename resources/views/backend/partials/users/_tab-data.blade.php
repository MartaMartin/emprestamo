<div class="box-header with-border">
	<h3 class="box-title">{{ _('Datos Personales') }}</h3>
</div>{{-- /.box-header --}}

{{-- form start --}}
{!! Form::model($user,
		[
		'method' => 'PUT',
		'route' => [
			'admin.users.update',
			$user->id
		],
		'role' => 'form'
	])
!!}

<div class="box-body">

	<div class="row">

		<div class="col-md-6">

			<div class="form-group">

				{!! Form::label('name', _('Nombre')) !!}
				{!! Form::text('name', $user->name,
					[
						'class' => 'form-control',
						'placeholder' => _('Nombre usuario'),
						'required'
					])
				!!}

			</div>{{-- /.form-group --}}

		</div>{{-- /.col-md-6 --}}

		<div class="col-md-6">

			<div class="form-group">

				{!! Form::label('family_name', _('Apellidos')) !!}
				{!! Form::text('family_name', $user->family_name,
					[
						'class' => 'form-control',
						'placeholder' => _('Apellidos usuario'),
						'required'
					])
				!!}

			</div>{{-- /.form-group --}}

		</div>{{-- /.col-md-6 --}}

	</div>{{-- /.row --}}

	<div class="row">

		<div class="col-md-6">

			<div class="form-group">

				{!! Form::label('email', _('Email')) !!}

				<div class="input-group">
					<span class="input-group-addon">
						<i class="fa fa-envelope"></i>
					</span>
					{!! Form::email('email', $user->email,
						[
							'class' => 'form-control remove_autocomplete',
							'placeholder' => _('Email usuario'),
							'readonly' => true,
							'required'
						])
					!!}
				</div>{{-- /.input-group --}}

			</div>{{-- /.form-group --}}

		</div>{{-- /.col-md-6 --}}

		<div class="col-md-6">

			<div class="form-group">

				{!! Form::label('password', _('Contraseña')) !!}

				<div class="input-group">
					<span class="input-group-addon">
						<i class="fa fa-lock"></i>
					</span>
					{!! Form::password('password',
						[
							'class' => 'form-control remove_autocomplete',
							'placeholder' => _('Contraseña usuario'),
							'readonly' => true
						])
					!!}
				</div>{{-- /.input-group --}}

			</div>{{-- /.form-group --}}

		</div>{{-- /.col-md-6 --}}

	</div>{{-- /.row --}}

	<div class="row">

		<div class="col-md-6">

			<div class="form-group">

				{!! Form::label('real_id_type', _('Tipo identificación')) !!}

				{!! Form::select('real_id_type', [
					'dni' => 'DNI',
					'cif' => 'CIF',
					'nie' => 'NIE'
				],
				$user->real_id_type, ['class' => 'form-control']) !!}

			</div>{{-- /.form-group --}}

		</div>{{-- /.col-md-6 --}}

		<div class="col-md-6">

			<div class="form-group">

				{!! Form::label('real_id_number', _('Número identificación')) !!}
				{!! Form::text('real_id_number', $user->real_id_number,
					[
						'class' => 'form-control',
						'placeholder' => _('Número identificación'),
						'required'
					])
				!!}

			</div>{{-- /.form-group --}}

		</div>{{-- /.col-md-6 --}}

	</div>{{-- /.row --}}

</div>{{-- /.box-body --}}


<div class="box-header with-border">
	<h3 class="box-title">{{ _('Permisos usuario') }}</h3>
</div>{{-- /.box-header --}}

<div class="box-body">

	<div class="row">

		<div class="col-md-6">

			<div class="form-group">

				{!! Form::label('role', _('Tipo usuario')) !!}

				{!! Form::select('role', $roles,
				$user->role, ['class' => 'form-control']) !!}

			</div>{{-- /.form-group --}}

		</div>{{-- /.col-md-6 --}}

		<div class="col-md-6">

			<div class="form-group">

                <label>
                    {!! Form::checkbox('dni_verified', 1, $user->dni_verified) !!}
                    DNI verificado
                </label>

			</div>{{-- /.form-group --}}

		</div>{{-- /.col-md-6 --}}

	</div>{{-- /.row --}}

</div>{{-- /.box-body --}}

<div class="box-footer">
	<button type="submit" class="btn btn-primary">Actualizar usuario</button>
</div>{{-- /.box-footer --}}

{!! Form::close() !!}{{-- /.form --}}
