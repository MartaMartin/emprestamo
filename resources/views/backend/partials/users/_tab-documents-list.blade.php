@if (count($documents))
	{{-- form start --}}
	{!! Form::open([
		'files' => true,
		'route' => 'admin.documents.store',
		'role' => 'form'
	])
	!!}

	{!! Form::hidden('password', 'pass_inicial') !!}

		{{--|| User Document ||--}}
		<div class="box-header with-border">
			<h3 class="box-title">{{ _('Actualizar documentos del usuario') }} </h3>
		</div>{{-- /.box-header --}}

		<div class="box-body">

			<input type="hidden" name="foreign_id" value="{{ $user->id }}" />
			<input type="hidden" name="model" value="User" />

			@foreach ($documents as $document)
				<div class="container-row container-existing" id="js-document-row-{{$document->id}}">
					<input type="hidden" name="ids[]" value="{{ $document->id }}" />
					<div class="row">
						<div class="col-xs-12 col-md-8">
							<div class="form-group">
								<label for="user_document_names[]">Nombre del documento</label>
								{!! Form::text('user_document_names[]', $document->name, ['class' => 'form-control', 'required']) !!}
							</div>
						</div>
						<div class="col-xs-12 col-md-4">
							<div class="form-group">
								<label for="user_document_files[]">Fichero</label>
								<div class="input-group">
									{!! Form::file('user_document_files[]', ['class' => 'form-control', 'required']) !!}
								</div>
								<a target="_blank" href="{{ AssetUrl::get('Document', $document->type, $document->filename, 0, 0) }}">{{ _('Ver fichero') . ' ' . $document->name }}</a>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-xs-12 col-md-6">
							<div class="form-group">
								<button type="button" data-document_id="{{ $document->id }}" class="btn-remove-document btn btn-danger" data-toggle="modal" data-target="#confirmDeleteDocument" data-row_id="js-document-row-{{$document->id}}"><i class="fa fa-minus"></i> Eliminar documento</button>
							</div>
						</div>
					</div>
				</div>
			@endforeach

			<div class="box-footer">
				<button type="submit" class="btn btn-primary">{{ _('Actualizar ficheros') }}</button>
			</div>{{-- /.box-footer --}}

		</div>{{-- /.box-body --}}
		{{--|| /User Document ||--}}

	{!! Form::close() !!}{{-- /.form --}}
	@include('backend.partials.users._modal-documents-delete')
@endif
