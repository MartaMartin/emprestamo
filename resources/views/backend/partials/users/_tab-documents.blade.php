{{-- form start --}}
{!! Form::open([
	'files' => true,
	'route' => 'admin.documents.store',
	'role' => 'form'
])
!!}

{!! Form::hidden('password', 'pass_inicial') !!}

	{{--|| User Document ||--}}
	<div class="box-header with-border">
		<h3 class="box-title">{{ _('Subir nuevos documentos del usuario') }} </h3>
	</div>{{-- /.box-header --}}

	<div class="box-body" id="dynamic-user-documents-assign">

		<input type="hidden" name="foreign_id" value="{{ $user->id }}" />
		<input type="hidden" name="model" value="User" />

		<div class="row">
			{{--||| User btn-add-document |||--}}
			<div class="col-xs-12 col-md-12">
				<div class="form-group">
					<button class="btn-add-document btn btn-success" type="button"><i class="fa fa-plus"></i>Añadir documento</button>
				</div>{{-- /.form-group --}}
			</div>{{-- /.col-md-6 --}}
			{{--||| /User btn-add-document |||--}}
		</div>

		{{--||| Dynamic content |||--}}
		<div class="user-documents-assign-wrap">

		</div>{{-- /.user-documents-assign-wrap --}}
		{{--||| Dynamic content |||--}}

		<div class="row btn-add-document-assign-bottom">
			{{--||| User btn-add-document |||--}}
			<div class="col-xs-12 col-md-6">
				<div class="form-group">
					<button class="btn-add-document btn btn-success" type="button"><i class="fa fa-plus"></i>Añadir documento</button>
				</div>{{-- /.form-group --}}
			</div>{{-- /.col-md-6 --}}
			{{--||| /User btn-add-document |||--}}
		</div>

		<div class="box-footer">
			<button type="submit" class="btn btn-primary">{{ _('Subir ficheros') }}</button>
		</div>{{-- /.box-footer --}}

	</div>{{-- /.box-body --}}
	{{--|| /User Document ||--}}

	@section('specific-footer-js')
		@parent
		<script>
			$(document).ready(function() {
				var max_fields = 10; //maximum input boxes allowed
				var user_documents_assign_wrapper  = $(".user-documents-assign-wrap"); //Fields wrapper
				var dynamic_user_documents_assign  = $("#dynamic-user-documents-assign"); //Add button ID

				var name_input  = "{{ Form::text('user_document_names\[\]', null, ['class' => 'form-control', 'required']) }}";
				var file = "{{ Form::file('user_document_files\[\]', ['class' => 'form-control', 'required']) }}";
				// var weight_select = "{{ Form::selectRange('user_document_weights\[\]', -10, 10, 0, ['class' => 'form-control']) }}";

				name_input = $('<div/>').html(name_input).text(); // Convert to propper HTML for append
				file = $('<div/>').html(file).text(); // Convert to propper HTML for append
				// weight_select = $('<div/>').html(weight_select).text(); // Convert to propper HTML for append

				var x = 0; //initlal text box count

				$('.btn-add-document-assign-bottom').hide(); // Hide del button bottom "Añadir campo de texto"

				$(dynamic_user_documents_assign).on('click', '.btn-add-document', function(e){ //on add input button click
					e.preventDefault();
					if(x < max_fields){ //max input box allowed
						x++; //text box increment
						$(user_documents_assign_wrapper).append(
							'<input type="hidden" name="ids[]" value="" />' +
							'<div class="container-row container-dynamic">' +
								'<div class="row">' +
									'<div class="col-xs-12 col-md-8">' +
										'<div class="form-group">' +
											'<label for="user_document_names[]">Nombre del documento</label>' +
												name_input +
										'</div>' +
									'</div>' +
									'<div class="col-xs-12 col-md-4">' +
										'<div class="form-group">' +
											'<label for="user_document_files[]">Fichero</label>' +
											'<div class="input-group">' +
												file +
											'</div>' +
										'</div>' +
									'</div>' +
								'</div>' +

								'<div class="row">' +
									'<div class="col-xs-12 col-md-6">' +
										'<div class="form-group">' +
											'<button class="btn-remove-document btn btn-danger" type="button"><i class="fa fa-minus"></i> Eliminar Documento</button>' +
										'</div>' +
									'</div>' +
								'</div>' +
							'</div>'
						); //add input box

						// Mostramos botón Añadir campos en footer de Form
						if(x > 0) {
							$('.btn-add-document-assign-bottom').show();
						}
					}
				});

				$(user_documents_assign_wrapper).on("click",".btn-remove-document", function(e){ //user click on remove text
					e.preventDefault();
					$(this).parent('div').parent('div').parent('div').parent('div').remove();
					x--;
					// Ocultamos botón Añadir campos en footer de Form
					if(x == 0) {
						$('.btn-add-document-assign-bottom').hide();
					}
				});

			});
		</script>
	@endsection

{!! Form::close() !!}{{-- /.form --}}
