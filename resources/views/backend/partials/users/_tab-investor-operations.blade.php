<div class="box-body">
	<table class="table table-responsive table-condensed">
		<thead>
			<tr>
				<th>{{ _('Operación') }}</th>
				<th>{{ _('Estado') }}</th>
				<th>{{ _('Invertido') }}</th>
				<th>{{ _('Fecha de operación') }}</th>
				<th>{{ _('Tipo de interés') }}</th>
				<th>{{ _('Plazo') }}</th>
				<th>{{ _('Calificación') }}</th>
				<th>{{ _('Fecha devolución') }}</th>
				<th>{{ _('Retorno') }}</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($operations as $operation)
			<tr>
                <td><a href="{{ route('admin.operations.show', [$operation->id]) }}">{{ $operation->invoice->name }}</a></td>
				<td>{{ $status[$operation->status] }}</td>
				<td>{{ number_format($operation->invested, 2, ',', '.') }}€</td>
				<td>{{ Functions::shortDate($operation->created_at) }}</td>
				<td>{{ $operation->fee }}%</td>
                <td>{{ round((strtotime($operation->invoice->expires) - strtotime($operation->accepted_date ? $operation->accepted_date : $operation->created_at)) / 3600 / 24) }} días</td>
                <td>{{ $operation->calification + 1 }}</td>
				<td>{{ $operation->closed_date ? Functions::shortDate($operation->closed_date) : '--' }}</td>
                <td>{{ number_format($operation->earned + $operation->invested, 2, ',', '.') }}€</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>
