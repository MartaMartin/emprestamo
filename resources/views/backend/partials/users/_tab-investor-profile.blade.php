{{-- form start --}}
{!! Form::model($investor,
	[
	'method' => 'PUT',
	'route' => [
		'admin.investors.update',
		$investor->id
	],
	'role' => 'form'
	])
!!}

{!! Form::hidden('password', 'pass_inicial') !!}

<div class="box-body">

	<div class="row">

		<div class="col-md-4">

			<div class="form-group">

				{!! Form::label('type', _('Tipo de inversor')) !!}
				{!! Form::select('type', [
					0 => _('No acreditado'),
					1 => _('Acreditado'),
				],
				$investor->type, ['class' => 'form-control']) !!}

			</div>{{-- /.form-group --}}

		</div>{{-- /.col-md-4 --}}

		<div class="col-md-4">

			<div class="form-group">

				{!! Form::label('auto', _('Inversión automática')) !!}
				{!! Form::select('auto', [
					0 => _('No'),
					1 => _('Sí'),
				],
				$investor->auto, ['class' => 'form-control']) !!}

			</div>{{-- /.form-group --}}

		</div>{{-- /.col-md-4 --}}

		<div class="col-md-4">

			<div class="form-group">

				{!! Form::label('max_days', _('Máximo de días de espera para el retorno de la inversión')) !!}
				{!! Form::number('max_days', $investor->max_days,
					[
						'class' => 'form-control',
						'placeholder' => _('Días de espera')
					])
				!!}

			</div>{{-- /.form-group --}}

		</div>{{-- /.col-md-4 --}}

	</div>{{-- /.row --}}

	<div class="row">

		<div class="col-md-4">

			<div class="form-group">

				{!! Form::label('max_invest', _('Importe máximo de inversión')) !!}
				{!! Form::number('max_invest', $investor->max_invest,
					[
						'class' => 'form-control',
						'placeholder' => _('Importe')
					])
				!!}

			</div>{{-- /.form-group --}}

		</div>{{-- /.col-md-4 --}}

		<!--
		<div class="col-md-4">

			<div class="form-group">

				{!! Form::label('min_invest', _('Importe mínimo de inversión')) !!}
				{!! Form::number('min_invest', $investor->min_invest,
					[
						'class' => 'form-control',
						'placeholder' => _('Importe'),
						'required'
					])
				!!}

			</div>{{-- /.form-group --}}

		</div>{{-- /.col-md-4 --}}
		-->

		<div class="col-md-4">

			<div class="form-group">

				{!! Form::label('fee', _('Interés')) !!}
				{!! Form::number('fee', $investor->fee,
					[
						'class' => 'form-control',
						'placeholder' => _('Interés en %')
					])
				!!}

			</div>{{-- /.form-group --}}

		</div>{{-- /.col-md-4 --}}

		<div class="col-md-4">

			<div class="form-group">

				{!! Form::label('wallet', _('Wallet')) !!}
				{!! Form::number('wallet', $investor->wallet,
					[
						'class' => 'form-control',
						'placeholder' => _('Saldo en la cartera'),
                        'disabled'
					])
				!!}

			</div>{{-- /.form-group --}}

		</div>{{-- /.col-md-4 --}}

	</div>{{-- /.row --}}

</div>{{-- /.box-body --}}

<div class="box-footer">
	<button type="submit" class="btn btn-primary">Actualizar inversor</button>
</div>{{-- /.box-footer --}}

{!! Form::close() !!}{{-- /.form --}}
