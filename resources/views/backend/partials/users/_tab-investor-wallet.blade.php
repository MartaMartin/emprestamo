@if (count($transfers))
    <!-- BEGIN TABLAS -->
    <div class="box-body">
        <table class="table table-responsive table-condensed">
            <thead>
                <th class="text-left">Tipo de transferencia</th>
                <th class="text-left">Operación</th>
                <th class="text-left">Referencia</th>
                <th class="text-left">Estado</th>
                <th class="text-right">Cantidad</th>
                <th class="text-center">Fecha</th>
            </thead>
            <tbody>
                @foreach ($transfers as $transfer)
                    <tr class="show_subtransfers" style="cursor: pointer;" var="row_{{ $transfer->id }}">
                        <td class="morado text-left">
                            @if (count($transfer->subtransfers) > 1)
                                <span class="selector">&#x25B8;</span>
                            @endif
                            {{ $transfer_types[$transfer->type] }}
                        </td>
                        <td class="morado text-left">
                            @if ($transfer->operation)
                                <a href="{{ route('admin.operations.show', $transfer->operation->id) }}">{{ $transfer->operation->invoice->name }}</a>
                            @endif
                        </td>
                        <td class="morado text-left">
                            {{ $transfer->ref }}
                        </td>
                        <td class="morado text-left">
                            {{ $transfer->active ? _('Validada') : _('Pendiente') }}
                        </td>
                        <td class="text-right" style="color: {{ $transfer->type == 1 || $transfer->type == 2 || $transfer->type == 5 ? 'red' : 'green' }};">
                            {{ ($transfer->type == 1 || $transfer->type == 2 || $transfer->type == 5 ? '-' : '') . number_format($transfer->quantity, 2, ',', '.') }}€
                        </td>
                        <td class="text-center">
                            {{ Functions::beautyTime($transfer->created_at) }}
                            {{ Functions::shortDate($transfer->created_at) }}
                        </td>
                    </tr>
                    @if (count($transfer->subtransfers) > 1)
                        <tr><td colspan="10">
                            <table class="table table-responsive table-condensed hidden row_{{ $transfer->id }}">
                                <th class="text-left">Tipo de transferencia</th>
                                <th class="text-left">Operación</th>
                                <th class="text-left">Referencia</th>
                                <th class="text-left">Estado</th>
                                <th class="text-right">Cantidad</th>
                                <th class="text-center">Fecha</th>
                            @foreach ($transfer->subtransfers as $subtransfer)
                            <tr>
                                <td class="morado text-left">
                                    {{ $transfer_types[$subtransfer->type] }}
                                </td>
                                <td class="morado text-left">
                                    @if ($subtransfer->operation)
                                        {{ $subtransfer->operation->invoice->name }}
                                    @endif
                                </td>
                                <td class="morado text-left">
                                    {{ $subtransfer->ref }}
                                </td>
                                <td class="morado text-left">
                                    {{ $subtransfer->active ? _('Validada') : _('Pendiente') }}
                                </td>
                                <td class="text-right" style="color: {{ $subtransfer->type == 1 || $subtransfer->type == 2 || $subtransfer->type == 5 ? 'red' : 'green' }};">
                                    {{ ($subtransfer->type == 1 || $subtransfer->type == 2 || $subtransfer->type == 5 ? '-' : '') . number_format($subtransfer->original_quantity, 2, ',', '.') }}€
                                </td>
                                <td class="text-center">
                                    {{ Functions::beautyTime($subtransfer->created_at) }}
                                    {{ Functions::shortDate($subtransfer->created_at) }}
                                </td>
                            </tr>
                            @endforeach
                        </table>
                        </td></tr>
                    @endif
                @endforeach
            </tbody>
        </table>
    </div>
@endif

<div class="box-body">
    <div class="col-md-2">
        <div class="well bg-blanco margTop10">
            <div class="row">
                <div class="col-md-12">
                    <div class="regular15 negro">
                        <table style="border: none !important;">
                            <tbody>
                                @foreach ($donutgraphs['invested'] as $aux)
                                    <tr class="jqplot-table-legend">
                                        <td class="jqplot-table-legend" style="text-align:center;padding-right: 10px; padding-top:0;">
                                            <div>
                                                <div class="jqplot-table-legend-swatch" style="border-color:{{ $aux[2] }};"></div>
                                            </div>
                                        </td>
                                        <td class="jqplot-table-legend" style="padding-top:0;">{{ $aux[0] }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-10">
        <div class="col-md-6" style="text-align: center; margin-top: 20px;">
            <h4>{{ _('Total invertido') }}: {{ number_format($totals['invested'], 2, ',', '.') }}€</h4>
            <div id="invested_donut"></div>
        </div>
        <div class="col-md-6" style="text-align: center; margin-top: 20px;">
            <h4>{{ _('Interés medio') }}: {{ round($totals['fee']) }}%</h4>
            <div id="fee_donut"></div>
        </div>
        <div class="col-md-6" style="text-align: center; margin-top: 20px;">
            <h4>{{ _('Total devuelto') }}: {{ number_format($totals['returned'], 2, ',', '.') }}€</h4>
            <div id="returned_donut"></div>
        </div>
        <div class="col-md-6" style="text-align: center; margin-top: 20px;">
            <h4>{{ _('Total beneficio') }}: {{ number_format($totals['only_earned'], 2, ',', '.') }}€</h4>
            <div id="earned_donut"></div>
        </div>
    </div>
</div>

@section('specific-header-css')
<link  href="/assets/css/jquery.jqplot.css" rel="stylesheet" type="text/css" />
<link  href="/assets/css/amcharts.css" rel="stylesheet" type="text/css" />
<link  href="/assets/css/export.css" rel="stylesheet" type="text/css" />
<style>
/*************************/
/*	GRAFICAS */
/*************************/

.jqplot-donut-series.jqplot-data-label {
	color: #ffffff;
}

.jqplot-target {
	border: none !important;
}

#chartdiv {
	width	: 100%;
	height	: 500px;
	background: url(/assets/img/logograf.png) no-repeat center 40% / 60%;
}

a[href="http://www.amcharts.com/javascript-charts/"] {
	display: none !important;
}

.amcharts-graph-wallet {
	stroke-linejoin: round;
	stroke-linecap: round;
	stroke-dasharray: 500%;
	stroke-dasharray: 0 \0/;    /* fixes IE prob */
	stroke-dashoffset: 0 \0/;   /* fixes IE prob */
	-webkit-animation: am-draw 40s;
	animation: am-draw 40s;
}
@-webkit-keyframes am-draw {
	0% {
		stroke-dashoffset: 500%;
	}
	100% {
		stroke-dashoffset: 0%;
	}
}
@keyframes am-draw {
	0% {
		stroke-dashoffset: 500%;
	}
	100% {
		stroke-dashoffset: 0%;
	}
}
</style>
@endsection

@section('specific-footer-js')
<script type="text/javascript" src="/assets/scripts/private.js"></script>
<script type="text/javascript" src="/assets/plugins/graficas/amcharts.js"></script>
<script type="text/javascript" src="/assets/plugins/graficas/jquery.jqplot.min.js"></script>
<script type="text/javascript" src="/assets/plugins/graficas/jqplot.donutRenderer.js"></script>
<script>
	var invested_data = [
	@if ($totals['invested'])
		@foreach ($donutgraphs['invested'] as $row)
		    ['{{ $row[0] }}', {{ number_format($row[1] * 100 / $totals['invested'], 2, '.', '') }}, '{{ $row[2] }}'],
		@endforeach
	@endif
	];

	var fee_data = [
	@if (count($donutgraphs['fee']))
		@foreach ($donutgraphs['fee'] as $row)
		    ['{{ $row[0] }}', {{ number_format($row[1] * 100 / ($totals['fee'] * count($donutgraphs['fee'])), 2, '.', '') }}, '{{ $row[2] }}'],
		@endforeach
		];
	@endif

	var returned_data = [
	@if ($totals['returned'])
		@foreach ($donutgraphs['returned'] as $row)
		    ['{{ $row[0] }}', {{ number_format($row[1] * 100 / $totals['returned'], 2, '.', '') }}, '{{ $row[2] }}'],
		@endforeach
	@endif
	];

	var earned_data = [
	@if ($totals['only_earned'])
		@foreach ($donutgraphs['earned'] as $row)
		    ['{{ $row[0] }}', {{ number_format($row[1] * 100 / $totals['only_earned'], 2, '.', '') }}, '{{ $row[2] }}'],
		@endforeach
	@endif
	];

    Private.donuts();

    $('.show_subtransfers').click(function() {
        $('.' + $(this).attr('var')).toggleClass('hidden');

        if ($(this).find('.selector').html() == '▾') {
            $(this).find('.selector').html('&#x25B8;');
        } else {
            $(this).find('.selector').html('&#x25BE;');
        }
    });
</script>
@endsection
