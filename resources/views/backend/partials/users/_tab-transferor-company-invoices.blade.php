@if ($transferor_company->invoices->count())
    <!-- BEGIN TABLAS -->
    <div class="box-body">
        <table class="table table-responsive table-condensed">
            <thead>
					<th>Nombre</th>
					<th>Interés</th>
					<th style="text-align: right;">Precio</th>
					<th style="text-align: right;">Recaudado</th>
					<th style="text-align: center;">Estado</th>
					<th style="text-align: center;">Fecha</th>
					<th style="text-align: center;">Archivos</th>
            </thead>
            <tbody>
				@foreach ($transferor_company->invoices as $invoice)
					<tr>
						<td>
							<a href="{{ route('admin.invoices.edit', $invoice->id) }}">{{ $invoice->name }}</a>
						</td>
						<td>
							@if (count($invoice->operations))
                                {{ number_format($invoice->operations[0]->fee, 2, ',', '.') }}%
                            @endif
						</td>
						<td style="text-align: right;">
							{{ number_format($invoice->price, 2, ',', '.') }}
						</td>
						<td style="text-align: right;">
							@if (count($invoice->operations))
                                {{ number_format($invoice->operations[0]->getCurrentInvested(), 2, ',', '.') }}
                            @endif
						</td>
						<td style="text-align: center;">
							@if (count($invoice->operations))
								{{ $status[$invoice->operations[0]->status] }}
							@else
								{{ $status[0] }}
							@endif
						</td>
						<td style="text-align: center;">
							@if (count($invoice->operations))
								@if ($invoice->operations[0]->status == 3)
                                    Expira: {{ Functions::beautyDate($invoice->expires) }}
                                @else
                                    @if ($invoice->operations[0]->closed_date)
                                        {{ Functions::beautyDateTime($invoice->operations[0]->closed_date) }}
                                    @endif
                                @endif
                            @else
                                Expira: {{ Functions::beautyDate($invoice->expires) }}
                            @endif
						</td>
						<td>
                            <ul class="no-style">
							@foreach ($invoice->documents as $document)
								@if ($document->type == 'image')
                                    <li><a href="{{ AssetUrl::get('Document', $document->type, $document->filename, 0, 0) }}" target="_blank"><i class="fa fa-file"></i> {{ $document->name }}</a></li>
								@else
									<li><a href="{{ AssetUrl::get('Document', $document->type, $document->filename) }}" target="_blank"><i class="fa fa-file"></i> {{ $document->name }}</a></li>
								@endif
							@endforeach
                            </ul>
						</td>
					</tr>
				@endforeach
            </tbody>
        </table>
    </div>
@endif
