{{-- form start --}}
{!! Form::model($transferor_company,
	[
	'method' => 'PUT',
	'route' => [
		'admin.transferor_companies.update',
		$transferor_company->id
	],
	'role' => 'form'
	])
!!}

{!! Form::hidden('password', 'pass_inicial') !!}

<div class="box-body">

	<div class="row">

		<div class="col-md-6">

			<div class="form-group">

				{!! Form::label('name', _('Nombre de la empresa')) !!}
				{!! Form::text('name', $transferor_company->name,
					[
						'class' => 'form-control',
						'placeholder' => _('Nombre de la empresa'),
						'required'
					])
				!!}

			</div>{{-- /.form-group --}}

		</div>{{-- /.col-md-6 --}}

		<div class="col-md-6">

			<div class="form-group">

                {!! Form::label('total_debt', _('Saldo vivo')) . ' (Actual: ' . number_format($transferor_company->debt, 2, ',', '.') . '€)' !!}
				{!! Form::text('total_debt', $transferor_company->total_debt,
					[
						'class' => 'form-control',
						'placeholder' => _('Saldo vivo máximo'),
						'required'
					])
				!!}

			</div>{{-- /.form-group --}}

		</div>{{-- /.col-md-6 --}}


	</div>{{-- /.row --}}

</div>{{-- /.box-body --}}

<div class="box-footer">
	<button type="submit" class="btn btn-primary">Actualizar empresa cedente</button>
</div>{{-- /.box-footer --}}

{!! Form::close() !!}{{-- /.form --}}
