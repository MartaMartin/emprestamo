<div class="box-body">
    <div class="circle">
        Nº operaciones: {{ $operations }}
    </div>
    <div class="circle">
        Nº operaciones fallidas: {{ $failed_operations }}
    </div>
    <div class="circle">
        Nº operaciones con éxito: {{ $success_operations }}
    </div>
    <div class="circle">
        Saldo vivo: {{ number_format($transferor_company->debt, 2, ',', '.') }}&nbsp;€
    </div>
</div>
