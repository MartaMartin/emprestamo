@extends('backend.layouts.dashboard')

@section('content')

    {{-- //////// News title ///////// --}}
    <?php $page_title = 'Editar variable'  ?>
    {{-- //////// News title ///////// --}}

    {{-- *** Create settings row *** --}}
    <div class="row">

        <div class="col-xs-12">

            <div class="box box-primary">

                {{-- form start --}}
				{!! Form::model($setting,
					[
					'method' => 'PUT',
					'route' => [
						'admin.settings.update',
						$setting->id
					],
					'role' => 'form'
					])
                !!}

                {!! Form::hidden('password', 'pass_inicial') !!}

                <div class="box-body">

                    <div class="row">

                        <div class="col-md-4">

							<div class="form-group">

								{!! Form::label('name', _('Nombre de la variable')) !!}
								{!! Form::text('name', $setting->name,
									[
										'class' => 'form-control',
										'placeholder' => _('Nombre de la variable'),
										'required'
									])
								!!}

							</div>{{-- /.form-group --}}

						</div>{{-- /.col-md-4 --}}

                        <div class="col-md-4">

							<div class="form-group">

								{!! Form::label('key', _('Clave de la variable (Sin espacios, ni tildes, etc...)')) !!}
								{!! Form::text('key', $setting->key,
									[
										'class' => 'form-control',
										'placeholder' => _('Ej: porcentaje_comision, minimo_inversion, etc...'),
										'required',
										'disabled' => true
									])
								!!}

							</div>{{-- /.form-group --}}

						</div>{{-- /.col-md-4 --}}

                        <div class="col-md-4">

							<div class="form-group">

								{!! Form::label('value', _('Valor de la variable')) !!}
								{!! Form::text('value', $setting->value,
									[
										'class' => 'form-control',
										'placeholder' => _('Valor de la variable'),
										'required'
									])
								!!}

							</div>{{-- /.form-group --}}

						</div>{{-- /.col-md-4 --}}

                    </div>{{-- /.row --}}

                </div>{{-- /.box-body --}}

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Actualizar variable</button>
                </div>{{-- /.box-footer --}}

                {!! Form::close() !!}{{-- /.form --}}

            </div>{{-- /.box --}}

        </div>{{-- /.col --}}

    </div>{{-- /.row --}}
    {{-- /*** Create settings row *** --}}

@endsection
