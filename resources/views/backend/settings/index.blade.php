@extends('backend.layouts.dashboard')

@section('content')

    {{-- //////// News title ///////// --}}
    <?php $page_title = 'Listado de variables'  ?>
    {{-- //////// News title ///////// --}}

    <div class="row">

        <div class="col-xs-12">

            <div class="box">

                <div class="box-header">
                    <h3 class="box-title">{{ _('Variables') }}</h3>
                </div>{{-- /.box-header --}}

                <div class="box-body">

                    {!! $setting_list->render() !!}

                    <table id="settings" class="table table-bordered table-striped data-table">

                        <thead>

                        <tr>
                            <th>Nombre</th>
                            <th>Clave</th>
                            <th>Valor</th>
                            <th class="no-sort">Acciones</th>
                        </tr>

                        </thead>

                        <tbody>

                        @foreach($setting_list as $setting)
                            <tr>
                                <td>{{ $setting->name }}</td>
                                <td>{{ $setting->key }}</td>
                                <td>{{ $setting->value }}</td>
                                <td class="text-right">
                                    @can ('edit', new \App\Models\Setting)
                                        <a class="btn btn-sm btn-success" href="{{ route('admin.settings.edit', $setting->id) }}">
                                            <i class="fa fa-pencil"></i> {{ _('Editar variable') }}
                                        </a>
                                    @endcan

                                    @can ('destroy', new \App\Models\Setting)
									<button type="button" data-settings_id="{{ $setting->id }}"
										data-settings_title="{{ $setting->name }}"
										class="btn btn-danger btn-sm" data-toggle="modal" data-target="#confirmDelete">
                                        <i class="fa fa-trash"></i> {{ _('Eliminar') }}
                                     </button>
                                     @endcan

                                </td>

                            </tr>
                        @endforeach

                        </tbody>

                        <tfoot>

                        <tr>
                            <th>Nombre</th>
                            <th>Clave</th>
                            <th>Valor</th>
                            <th>Acciones</th>
                        </tr>

                        </tfoot>

                    </table>

                    {!! $setting_list->render() !!}

                </div>{{-- /.box-body --}}

            </div>{{-- /.box --}}

        </div>{{-- /.col --}}

    </div>{{-- /.row --}}

    <div class="row">

        <div class="col-xs-12">
	    @can ('create', new \App\Models\Setting)
            <div class="box">

                <div class="box-header">
                    <h3 class="box-title">{{ _('Nuevas variables') }}</h3>
                </div>{{-- /.box-header --}}

                <div class="box-body">

                    <a class="btn btn-success" href="{{ route('admin.settings.create') }}"><i class="fa fa-plus"></i> Crear variable</a>

                </div>{{-- /.box-body --}}

            </div>{{-- /.box --}}
	    @endcan

        </div>{{-- /.col --}}

    </div>{{-- /.row --}}

    {{-- Modal delete --}}
    <div class="modal modal-danger fade" id="confirmDelete">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Eliminar variable</h4>
                </div>
                <div class="modal-body">
                    <p>¿Estás seguro que quieres eliminar la siguiente variable?</p>
                    <p>
                        <span class="txt-settings-name"></span>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cerrar</button>
                    {!! Form::open(['method' => 'DELETE', 'id'=>'delForm']) !!}
                        <button type="submit" class="btn btn-outline">Eliminar variable</button>
                    {!! Form::close() !!}
                </div>{{-- /.modal-footer --}}
            </div>{{-- /.modal-content --}}
        </div>{{-- /.modal-dialog --}}
    </div>{{-- /.modal --}}
    {{-- /Modal delete --}}
@endsection


@section('specific-footer-js')

    <script>
        {{-- Trigger Modal elete --}}
        $('#confirmDelete').on('show.bs.modal', function(e) {

            var settingsId = $(e.relatedTarget).data('settings_id');
            var settingsName = $(e.relatedTarget).data('settings_name');
            var fullUrl = "{{ url('admin/configuraciones') }}" + '/' + settingsId;

            $("#confirmDelete .txt-settings-name").text(settingsName);
            $("#delForm").attr('action', fullUrl);
        });
    </script>
@endsection
