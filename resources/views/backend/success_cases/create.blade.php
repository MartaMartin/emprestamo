@extends('backend.layouts.dashboard')

@section('content')

    {{-- //////// News title ///////// --}}
    <?php $page_title = 'Crear caso de éxito'  ?>
    {{-- //////// News title ///////// --}}

    {{-- *** Create success_cases row *** --}}
    <div class="row">

        <div class="col-xs-12">

            <div class="box box-primary">

                {{-- form start --}}
                {!! Form::open([
					'files' => true,
					'route' => 'admin.success_cases.store',
					'role' => 'form'
				])
                !!}

                {!! Form::hidden('password', 'pass_inicial') !!}

                <div class="box-body">

                    <div class="row">

                        <div class="col-md-4">

							<div class="form-group">

								{!! Form::label('title', _('Título del caso de éxito')) !!}
								{!! Form::text('title', null,
									[
										'class' => 'form-control',
										'placeholder' => _('Título del caso de éxito'),
										'required'
									])
								!!}

							</div>{{-- /.form-group --}}

						</div>{{-- /.col-md-4 --}}

                        <div class="col-md-4">

							<div class="form-group">

								{!! Form::label('subtitle', _('Subtítulo del caso de éxito')) !!}
								{!! Form::text('subtitle', null,
									[
										'class' => 'form-control',
										'placeholder' => _('Subtítulo del caso de éxito'),
										'required'
									])
								!!}

							</div>{{-- /.form-group --}}

						</div>{{-- /.col-md-4 --}}

                        <div class="col-md-4">

							<div class="form-group">

								{!! Form::label('filename', _('Imagen adjunta')) !!}
								{!! Form::file('filename', null,
									[
										'class' => 'form-control',
										'required'
									])
								!!}

							</div>{{-- /.form-group --}}

						</div>{{-- /.col-md-4 --}}

                    </div>{{-- /.row --}}

                    <div class="row">

                        <div class="col-md-6">

							<div class="form-group">

								{!! Form::label('name', _('Nombre del contacto')) !!}
								{!! Form::text('name', null,
									[
										'class' => 'form-control',
										'placeholder' => _('Nombre del contacto'),
										'required'
									])
								!!}

							</div>{{-- /.form-group --}}

						</div>{{-- /.col-md-6 --}}

                        <div class="col-md-6">

							<div class="form-group">

								{!! Form::label('company_name', _('Nombre de la empresa')) !!}
								{!! Form::text('company_name', null,
									[
										'class' => 'form-control',
										'placeholder' => _('Nombre de la empresa'),
										'required'
									])
								!!}

							</div>{{-- /.form-group --}}

						</div>{{-- /.col-md-6 --}}

                    </div>{{-- /.row --}}

                    <div class="row">

                        <div class="col-md-12">

							<div class="form-group">

								{!! Form::label('description', _('Descripción del caso de éxito')) !!}
								{!! Form::textarea('description', null,
									[
										'class' => 'form-control',
										'placeholder' => _('Descripción'),
										'required'
									])
								!!}

							</div>{{-- /.form-group --}}

						</div>{{-- /.col-md-12 --}}

                    </div>{{-- /.row --}}

                </div>{{-- /.box-body --}}

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Crear caso de éxito</button>
                </div>{{-- /.box-footer --}}

                {!! Form::close() !!}{{-- /.form --}}

            </div>{{-- /.box --}}

        </div>{{-- /.col --}}

    </div>{{-- /.row --}}
    {{-- /*** Create success_cases row *** --}}

@endsection
