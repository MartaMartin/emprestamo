@extends('backend.layouts.dashboard')

@section('content')

    {{-- //////// News title ///////// --}}
    <?php $page_title = 'Listado de casos de éxito'  ?>
    {{-- //////// News title ///////// --}}

    <div class="row">

        <div class="col-xs-12">

            <div class="box">

                <div class="box-header">
                    <h3 class="box-title">{{ _('Casos de éxito') }}</h3>
                </div>{{-- /.box-header --}}

                <div class="box-body">

                    {!! $success_cases->render() !!}

                    <table id="success_cases" class="table table-bordered table-striped data-table">

                        <thead>

                        <tr>
                            <th>{{ _('Imagen') }}</th>
                            <th>Nombre</th>
                            <th class="no-sort">Acciones</th>
                        </tr>

                        </thead>

                        <tbody>

                        @foreach($success_cases as $success_case)
                            <tr>
                                <td><img src="{{ AssetUrl::get('SuccessCase', 'image', $success_case->filename, 50, 50) }}" /></td>
                                <td>{{ $success_case->name }}</td>
                                <td class="text-right">
                                    @can ('edit', new \App\Models\SuccessCase)
                                        <a class="btn btn-sm btn-success" href="{{ route('admin.success_cases.edit', $success_case->id) }}">
                                            <i class="fa fa-pencil"></i> {{ _('Editar caso de éxito') }}
                                        </a>
                                    @endcan

                                    @can ('destroy', new \App\Models\SuccessCase)
									<button type="button" data-success_cases_id="{{ $success_case->id }}"
										data-success_cases_title="{{ $success_case->name }}"
										class="btn btn-danger btn-sm" data-toggle="modal" data-target="#confirmDelete">
                                        <i class="fa fa-trash"></i> {{ _('Eliminar') }}
                                     </button>
                                     @endcan

                                </td>

                            </tr>
                        @endforeach

                        </tbody>

                        <tfoot>

                        <tr>
                            <th>{{ _('Imagen') }}</th>
                            <th>Nombre</th>
                            <th>Acciones</th>
                        </tr>

                        </tfoot>

                    </table>

                    {!! $success_cases->render() !!}

                </div>{{-- /.box-body --}}

            </div>{{-- /.box --}}

        </div>{{-- /.col --}}

    </div>{{-- /.row --}}

    <div class="row">

        <div class="col-xs-12">
	    @can ('create', new \App\Models\SuccessCase)
            <div class="box">

                <div class="box-header">
                    <h3 class="box-title">{{ _('Nuevos casos de éxito') }}</h3>
                </div>{{-- /.box-header --}}

                <div class="box-body">

                    <a class="btn btn-success" href="{{ route('admin.success_cases.create') }}"><i class="fa fa-plus"></i> Crear caso de éxito</a>

                </div>{{-- /.box-body --}}

            </div>{{-- /.box --}}
	    @endcan

        </div>{{-- /.col --}}

    </div>{{-- /.row --}}

    {{-- Modal delete --}}
    <div class="modal modal-danger fade" id="confirmDelete">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Eliminar caso de éxito</h4>
                </div>
                <div class="modal-body">
                    <p>¿Estás seguro que quieres eliminar la siguiente caso de éxito?</p>
                    <p>
                        <span class="txt-success_cases-name"></span>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cerrar</button>
                    {!! Form::open(['method' => 'DELETE', 'id'=>'delForm']) !!}
                        <button type="submit" class="btn btn-outline">Eliminar caso de éxito</button>
                    {!! Form::close() !!}
                </div>{{-- /.modal-footer --}}
            </div>{{-- /.modal-content --}}
        </div>{{-- /.modal-dialog --}}
    </div>{{-- /.modal --}}
    {{-- /Modal delete --}}
@endsection


@section('specific-footer-js')

    <script>
        {{-- Trigger Modal elete --}}
        $('#confirmDelete').on('show.bs.modal', function(e) {

            var success_casesId = $(e.relatedTarget).data('success_cases_id');
            var success_casesName = $(e.relatedTarget).data('success_cases_name');
            var fullUrl = "{{ url('admin/caso-de-exito') }}" + '/' + success_casesId;

            $("#confirmDelete .txt-success_cases-name").text(success_casesName);
            $("#delForm").attr('action', fullUrl);
        });
    </script>
@endsection
