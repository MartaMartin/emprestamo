@extends('backend.layouts.dashboard')

@section('content')

    {{-- //////// News title ///////// --}}
    <?php $page_title = 'Listado de etiquetas'  ?>
    {{-- //////// News title ///////// --}}

    <div class="row">

        <div class="col-xs-12">

            <div class="box">

                <div class="box-header">
                    <h3 class="box-title">{{ _('Etiquetas') }}</h3>
                </div>{{-- /.box-header --}}

                <div class="box-body">

                    {!! $tags->render() !!}

                    <table id="tags" class="table table-bordered table-striped data-table">

                        <thead>

                        <tr>
                            <th>Nombre</th>
                            <th class="no-sort">Acciones</th>
                        </tr>

                        </thead>

                        <tbody>

                        @foreach($tags as $tag)
                            <tr>
                                <td>{{ $tag->name }}</td>
                                <td class="text-right">
                                    @can ('edit', new \App\Models\Tag)
                                        <a class="btn btn-sm btn-success" href="{{ route('admin.tags.edit', $tag->id) }}">
                                            <i class="fa fa-pencil"></i> {{ _('Editar etiqueta') }}
                                        </a>
                                    @endcan

                                    @can ('destroy', new \App\Models\Tag)
									<button type="button" data-tags_id="{{ $tag->id }}"
										data-tags_title="{{ $tag->name }}"
										class="btn btn-danger btn-sm" data-toggle="modal" data-target="#confirmDelete">
                                        <i class="fa fa-trash"></i> {{ _('Eliminar') }}
                                     </button>
                                     @endcan

                                </td>

                            </tr>
                        @endforeach

                        </tbody>

                        <tfoot>

                        <tr>
                            <th>Nombre</th>
                            <th>Acciones</th>
                        </tr>

                        </tfoot>

                    </table>

                    {!! $tags->render() !!}

                </div>{{-- /.box-body --}}

            </div>{{-- /.box --}}

        </div>{{-- /.col --}}

    </div>{{-- /.row --}}

    <div class="row">

        <div class="col-xs-12">
	    @can ('create', new \App\Models\Tag)
            <div class="box">

                <div class="box-header">
                    <h3 class="box-title">{{ _('Nuevas etiquetas') }}</h3>
                </div>{{-- /.box-header --}}

                <div class="box-body">

                    <a class="btn btn-success" href="{{ route('admin.tags.create') }}"><i class="fa fa-plus"></i> Crear etiqueta</a>

                </div>{{-- /.box-body --}}

            </div>{{-- /.box --}}
	    @endcan

        </div>{{-- /.col --}}

    </div>{{-- /.row --}}

    {{-- Modal delete --}}
    <div class="modal modal-danger fade" id="confirmDelete">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Eliminar etiqueta</h4>
                </div>
                <div class="modal-body">
                    <p>¿Estás seguro que quieres eliminar la siguiente etiqueta?</p>
                    <p>
                        <span class="txt-tags-name"></span>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cerrar</button>
                    {!! Form::open(['method' => 'DELETE', 'id'=>'delForm']) !!}
                        <button type="submit" class="btn btn-outline">Eliminar etiqueta</button>
                    {!! Form::close() !!}
                </div>{{-- /.modal-footer --}}
            </div>{{-- /.modal-content --}}
        </div>{{-- /.modal-dialog --}}
    </div>{{-- /.modal --}}
    {{-- /Modal delete --}}
@endsection


@section('specific-footer-js')

    <script>
        {{-- Trigger Modal elete --}}
        $('#confirmDelete').on('show.bs.modal', function(e) {

            var tagsId = $(e.relatedTarget).data('tags_id');
            var tagsName = $(e.relatedTarget).data('tags_name');
            var fullUrl = "{{ url('admin/etiqueta') }}" + '/' + tagsId;

            $("#confirmDelete .txt-tags-name").text(tagsName);
            $("#delForm").attr('action', fullUrl);
        });
    </script>
@endsection
