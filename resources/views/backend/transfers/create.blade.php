@extends('backend.layouts.dashboard')

@section('content')
	<script src="{{ asset('/vendors/ckeditor/ckeditor.js') }}"></script>

    {{-- //////// Page title ///////// --}}
    <?php $page_title = 'Crear página'  ?>
    {{-- //////// Page title ///////// --}}

    {{-- *** Create page row *** --}}
    <div class="row">

        <div class="col-xs-12">

            <div class="box box-primary">

                {{-- form start --}}
                {!! Form::open([
					'route' => 'admin.pages.store',
					'role' => 'form'
				])
                !!}

                {!! Form::hidden('password', 'pass_inicial') !!}

                <div class="box-body">

                    <div class="row">

                        <div class="col-md-12">

							<div class="form-group">

								{!! Form::label('title', _('Título de la página')) !!}
								{!! Form::text('title', null,
									[
										'class' => 'form-control',
										'placeholder' => _('Título de la página'),
										'required'
									])
								!!}

							</div>{{-- /.form-group --}}

						</div>{{-- /.col-md-12 --}}

                    </div>{{-- /.row --}}

                    <div class="row">

                        <div class="col-md-12">

							<div class="form-group">

								{!! Form::label('content', _('Contenido')) !!}
								{!! Form::textarea('content', null,
									[
										'class' => 'form-control ckeditor',
										'placeholder' => _('Contenido de la página'),
										'required'
									])
								!!}

							</div>{{-- /.form-group --}}

						</div>{{-- /.col-md-12 --}}

                    </div>{{-- /.row --}}

                </div>{{-- /.box-body --}}

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Crear página</button>
                </div>{{-- /.box-footer --}}

                {!! Form::close() !!}{{-- /.form --}}

            </div>{{-- /.box --}}

        </div>{{-- /.col --}}

    </div>{{-- /.row --}}
    {{-- /*** Create page row *** --}}

@endsection
