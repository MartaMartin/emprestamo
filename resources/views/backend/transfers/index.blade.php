@extends('backend.layouts.dashboard')

@section('content')

    {{-- //////// Transfer title ///////// --}}
    <?php $transfer_title = 'Listado de transferencias'  ?>
    {{-- //////// Transfer title ///////// --}}

    <div class="row">

        <div class="col-xs-12">

            <div class="box">

                <div class="box-header">
                    <h3 class="box-title">{{ _('Transferencias') }}</h3>
                </div>{{-- /.box-header --}}

                <div class="box-body">

                    {!! $transfers->render() !!}

                    <table id="transfers" class="table table-bordered table-striped data-table">

                        <thead>

                        <tr>
                            <th>Inversor</th>
                            <th>Operación</th>
                            <th>Cantidad</th>
                            <th>Referencia</th>
                            <th>Tipo</th>
                            <th>Validada</th>
                            <th class="no-sort">Acciones</th>
                        </tr>

                        </thead>

                        <tbody>

                        @foreach($transfers as $transfer)
                            <tr>
                                @if ($transfer->investor)
                                    <td><a href="{{ route('admin.users.edit', $transfer->investor->user_id) }}">{{ $transfer->investor->user->full_name }}</a></td>
                                @else
                                    <td>Emprestamo</td>
                                @endif
                                @if ($transfer->operation)
                                    <td><a href="{{ route('admin.operations.show', [$transfer->operation->id]) }}">{{ $transfer->operation->invoice->name }}</a></td>
                                @else
                                    <td>-</td>
                                @endif
								<td>{{ number_format($transfer->quantity, 2, ',', '.') }}€</td>
								<td>{{ $transfer->ref }}</td>
								<td>{{ $types[$transfer->type] }}</td>
								<td>{{ $transfer->active ? _('Validada') : _('Inactiva') }}</td>
                                <td class="text-right">
									@if (!$transfer->active)
										@can ('edit', new \App\Models\Transfer)
											<a class="btn btn-sm btn-success" href="{{ route('admin.transfers.edit', $transfer->id) }}">
												<i class="fa fa-pencil"></i> {{ _('Validar') }}
											</a>
											<a class="btn btn-sm btn-danger" href="{{ route('admin.transfers.edit', [$transfer->id, true]) }}">
												<i class="fa fa-trash"></i> {{ _('Rechazar') }}
											</a>
										@endcan
									@endif
                                </td>
                            </tr>
                        @endforeach

                        </tbody>

                        <tfoot>

                        <tr>
                            <th>Inversor</th>
                            <th>Operación</th>
                            <th>Cantidad</th>
                            <th>Tipo</th>
                            <th>Validada</th>
                            <th class="no-sort">Acciones</th>
                        </tr>

                        </tfoot>

                    </table>

                    {!! $transfers->render() !!}

                </div>{{-- /.box-body --}}

            </div>{{-- /.box --}}

        </div>{{-- /.col --}}

    </div>{{-- /.row --}}

@endsection
