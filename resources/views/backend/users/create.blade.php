@extends('backend.layouts.dashboard')

@section('content')

    {{-- //////// Page title ///////// --}}
    <?php $page_title = 'Crear usuario'  ?>
    {{-- //////// Page title ///////// --}}

    {{-- *** Create user row *** --}}
    <div class="row">

        <div class="col-xs-12">

            <div class="box box-primary">

                <div class="box-header with-border">
                    <h3 class="box-title">{{ _('Datos Personales') }}</h3>
                </div>{{-- /.box-header --}}

                {{-- form start --}}
                {!! Form::open([
					'route' => 'admin.users.store',
					'role' => 'form'
					])
                !!}

                <div class="box-body">

                    <div class="row">

                        <div class="col-md-6">

                            <div class="form-group">

                                {!! Form::label('name', _('Nombre')) !!}
                                {!! Form::text('name', null,
									[
										'class' => 'form-control',
										'placeholder' => _('Nombre usuario'),
										'required'
									])
                                !!}

                            </div>{{-- /.form-group --}}

                        </div>{{-- /.col-md-6 --}}

                        <div class="col-md-6">

                            <div class="form-group">

                                {!! Form::label('family_name', _('Apellidos')) !!}
                                {!! Form::text('family_name', null,
									[
										'class' => 'form-control',
										'placeholder' => _('Apellidos usuario'),
										'required'
									])
                                !!}

                            </div>{{-- /.form-group --}}

                        </div>{{-- /.col-md-6 --}}

                    </div>{{-- /.row --}}

					<div class="row">

						<div class="col-md-6">

							<div class="form-group">

								{!! Form::label('email', _('Email')) !!}

								<div class="input-group">
									<span class="input-group-addon">
										<i class="fa fa-envelope"></i>
									</span>
									{!! Form::email('email', null,
										[
											'class' => 'form-control',
											'placeholder' => _('Email usuario'),
											'required'
										])
									!!}
								</div>{{-- /.input-group --}}

							</div>{{-- /.form-group --}}

						</div>{{-- /.col-md-6 --}}

						<div class="col-md-6">

							<div class="form-group">

								{!! Form::label('password', _('Contraseña')) !!}

								<div class="input-group">
									<span class="input-group-addon">
										<i class="fa fa-lock"></i>
									</span>
									{!! Form::password('password',
										[
											'class' => 'form-control',
											'placeholder' => _('Contraseña usuario'),
											'required'
										])
									!!}
								</div>{{-- /.input-group --}}

							</div>{{-- /.form-group --}}

						</div>{{-- /.col-md-6 --}}

					</div>{{-- /.row --}}

                    <div class="row">

                        <div class="col-md-6">

                            <div class="form-group">

                                {!! Form::label('real_id_type', _('Tipo identificación')) !!}

                                {!! Form::select('real_id_type', [
									'dni' => 'DNI',
									'cif' => 'CIF',
									'nie' => 'NIE'
								],
								null , ['class' => 'form-control']) !!}

                            </div>{{-- /.form-group --}}

                        </div>{{-- /.col-md-6 --}}

                        <div class="col-md-6">

                            <div class="form-group">

                                {!! Form::label('real_id_number', _('Número identificación')) !!}
                                {!! Form::text('real_id_number', null,
									[
										'class' => 'form-control',
										'placeholder' => _('Número identificación'),
										'required'
									])
                                !!}

                            </div>{{-- /.form-group --}}

                        </div>{{-- /.col-md-6 --}}

                    </div>{{-- /.row --}}

                </div>{{-- /.box-body --}}


                <div class="box-header with-border">
                    <h3 class="box-title">{{ _('Permisos usuario') }}</h3>
                </div>{{-- /.box-header --}}

                <div class="box-body">

                    <div class="row">

                        <div class="col-md-12">

                            <div class="form-group">

                                {!! Form::label('role', _('Tipo usuario')) !!}

                                {!! Form::select('role', $roles,
								null, ['class' => 'form-control']) !!}

                            </div>{{-- /.form-group --}}

                        </div>{{-- /.col-md-12 --}}

                    </div>{{-- /.row --}}

                </div>{{-- /.box-body --}}

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Crear usuario</button>
                </div>{{-- /.box-footer --}}

                {!! Form::close() !!}{{-- /.form --}}

            </div>{{-- /.box --}}

        </div>{{-- /.col --}}

    </div>{{-- /.row --}}
    {{-- /*** Create user row *** --}}

@endsection
