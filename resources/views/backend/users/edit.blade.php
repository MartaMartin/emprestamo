@extends('backend.layouts.dashboard')

@section('page_title', 'Editar usuario ' . $user->full_name)

@section('content')

    {{-- *** Create user row *** --}}
    <div class="row">

        <div class="col-xs-12">

			{{-- Tabs Contentainer --}}
            <div class="nav-tabs-custom">

				@if ($user->role == 'investor' || $user->role == 'debtor_company' || $user->role == 'transferor_company')
					<ul class="nav nav-tabs nav-justified">
						<li class="active"><a href="#data" data-toggle="tab">{{ _('Datos usuario') }}</a></li>
						<li><a href="#profile" data-toggle="tab">{{ _('Perfil') }}</a></li>
						<li><a href="#documents" data-toggle="tab">{{ _('Ficheros') }}</a></li>
					</ul>{{-- /.nav .nav-tabs --}}
				@endif

				{{--| Tabs Content |--}}
                <div class="tab-content">

					<div class="tab-pane active" id="data">
						@include('backend.partials.users._tab-data')
					</div>
					<div class="tab-pane" id="profile">
						@if ($user->role == 'investor')
							@include('backend.partials.users._tab-investor-profile')
						@endif
						@if ($user->role == 'debtor_company')
							@include('backend.partials.users._tab-debtor-company-profile')
						@endif
						@if ($user->role == 'transferor_company')
							@include('backend.partials.users._tab-transferor-company-profile')
						@endif
					</div>
					<div class="tab-pane" id="documents">
						@if ($user->role == 'investor' || $user->role == 'debtor_company' || $user->role == 'transferor_company')
							@include('backend.partials.users._tab-documents-list')
							@include('backend.partials.users._tab-documents')
						@endif
					</div>

					{{-- /Tab data --}}

				</div>
				{{-- /Tabs content --}}

            </div>{{-- /.box --}}

            @if ($user->role == 'investor')
                <h4>Operaciones</h4>
                <div class="nav-tabs-custom">
                    @include('backend.partials.users._tab-investor-operations')
                </div>{{-- /.box --}}
                @if (!empty($rejected_operations))
                    <h4>Operaciones rechazadas</h4>
                    <div class="nav-tabs-custom">
                        @include('backend.partials.users._tab-investor-rejected-operations')
                    </div>{{-- /.box --}}
                @endif
                <div class="nav-tabs-custom">
                    @include('backend.partials.users._tab-investor-wallet')
                </div>{{-- /.box --}}
            @endif
            @if ($user->role == 'transferor_company')
                <div class="nav-tabs-custom">
                    @include('backend.partials.users._tab-transferor-company-stats')
                </div>{{-- /.box --}}
                <div class="nav-tabs-custom">
                    @include('backend.partials.users._tab-transferor-company-invoices')
                </div>{{-- /.box --}}
            @endif
            @if ($user->role == 'debtor_company')
                <div class="nav-tabs-custom">
                    @include('backend.partials.users._tab-debtor-company-stats')
                </div>{{-- /.box --}}
                <div class="nav-tabs-custom">
                    @include('backend.partials.users._tab-transferor-company-invoices', ['transferor_company' => $debtor_company])
                </div>{{-- /.box --}}
            @endif

        </div>{{-- /.col --}}

    </div>{{-- /.row --}}
    {{-- /*** Create user row *** --}}

@endsection
