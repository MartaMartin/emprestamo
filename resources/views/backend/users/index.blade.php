@extends('backend.layouts.dashboard')

@section('content')

    {{-- //////// Page title ///////// --}}
    <?php $page_title = 'Listado de usuarios'  ?>
    {{-- //////// Page title ///////// --}}

    <div class="row">

        <div class="col-xs-12">

            <div class="box">

                <div class="box-header">
                    <h3 class="box-title">{{ _('Usuarios') }} {{ $type ? ('de tipo ' . $roles[$type]) : '' }}</h3>
                </div>{{-- /.box-header --}}

                @if ($type == 'transferor_company')
                    <div class="clearfix">
                        <div class="circle">
                            Total empresas:<br />{{ $companies }}
                        </div>
                        <div class="circle">
                            Nº solicitudes:<br />{{ $requests }}
                        </div>
                        <div class="circle">
                            Nº solicitudes aceptadas:<br />{{ $approved_requests }}
                        </div>
                        <div class="circle">
                            Nº solicitudes aceptadas por la empresa:<br />{{ $accepted_requests }}
                        </div>
                        <div class="circle">
                            Nº solicitudes descartadas:<br />{{ $rejected_requests }}
                        </div>
                        <div class="circle">
                            Nº solicitudes rechazadas:<br />{{ $cancelled_requests }}
                        </div>
                        <div class="circle">
                            Nº operaciones financiadas:<br />{{ $completed_requests }}
                        </div>
                        <div class="circle">
                            Capital total financiado:<br />{{ number_format($total_money, 2, ',', '.') }}€
                        </div>
                        <div class="circle">
                            Dinero financiado automáticamente:<br />{{ number_format($automatic_money, 2, ',', '.') }}€
                        </div>
                        <div class="circle">
                            Nº operaciones fallidas:<br />{{ $failed_operations }}
                        </div>
                        <div class="circle">
                            Dinero operaciones fallidas:<br />{{ number_format($failed_money, 2, ',', '.') }}€
                        </div>
                        <div class="circle">
                            Nº operaciones con ronda abierta:<br />{{ $accepted_requests }}
                        </div>
                    </div>
                @endif
                @if ($type == 'investor')
                    <div class="clearfix">
                        <div class="circle">
                            Total invesrores:<br />{{ $users->total() }}
                        </div>
                        <div class="circle">
                            Invesrores acreditados:<br />{{ $acredited}}
                        </div>
                        <div class="circle">
                            Invesrores no acreditados:<br />{{ $not_acredited }}
                        </div>
                        <div class="circle">
                            Invesrores con dinero en el wallet:<br />{{ $with_wallet }}
                        </div>
                        <div class="circle">
                            Dinero total en wallets:<br />{{ number_format($total_wallet, 2, ',', '.') }}&nbsp;€
                        </div>
                        <div class="circle">
                            Dinero total automatizado en wallets:<br />{{ $total_automatic_wallet }}
                        </div>
                    </div>
                @endif

                <div class="box-body">

                    {!! $users->render() !!}

                    <table id="users" class="table table-bordered table-striped data-table">

                        <thead>

                            <tr>
                                <th>Nombre</th>
                                <th>Apellidos</th>
                                <th>Tipo</th>
                                <th>Email</th>
								<th>DNI</th>
								<th>DNI verificado</th>
                                <th>Estado</th>
                                <th class="no-sort">Acciones</th>
                            </tr>

                        </thead>

                        <tbody>

                            @foreach($users as $user)
                                <tr>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->family_name }}</td>
                                    <td>{{ $roles[$user->role] }}</td>
                                    <td>{{ $user->email }}</td>
									<td>{{ $user->real_id_number }}</td>
									<td>{{ $user->dni_verified ? _('Sí') : _('No') }}</td>
                                    <td class="text-center">

                                        @if($user->status)

                                            <button type="button" data-user_id="{{ $user->id }}" data-user_name="{{ $user->name }}" data-user_family_name="{{ $user->family_name }}" data-user_email="{{ $user->email }}" class="btn btn-info btn-sm" data-toggle="modal" data-target="#confirmUnban"><i class="fa fa-plus-circle"></i> {{ _('Desbloquear') }}</button>

                                        @else

                                            <button type="button" data-user_id="{{ $user->id }}" data-user_name="{{ $user->name }}" data-user_family_name="{{ $user->family_name }}" data-user_email="{{ $user->email }}" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#confirmBan"><i class="fa fa-minus-circle"></i> {{ _('Bloquear') }}</button>

                                        @endif

                                    </td>
                                    <td class="text-right">

                                        <a class="btn btn-sm btn-success" href="{{ route('admin.users.edit', $user->id) }}"><i class="fa fa-pencil"></i> {{ _('Editar cuenta') }}</a>

										{{-- 
										@if ($user->path)
											<a class="btn btn-sm btn-primary" href="{{ route($user->path, $user->id) }}"><i class="fa fa-pencil"></i> {{ $user->role_name }}</a>
										@endif

										<a class="btn btn-sm btn-primary" href="{{ route('admin.documents.index', $user->id) }}"><i class="fa fa-pencil"></i> {{ _('Documentación') }}</a>

                                        <button type="button" data-user_name="{{ $user->name }}" data-user_family_name="{{ $user->family_name }}" data-user_email="{{ $user->email }}" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#confirmPassword"><i class="fa fa-mail-forward"></i> {{ _('Restablecer contraseña') }}</button>
										--}}

                                        <button type="button" data-user_id="{{ $user->id }}" data-user_name="{{ $user->name }}" data-user_family_name="{{ $user->family_name }}" data-user_email="{{ $user->email }}" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#confirmDelete"><i class="fa fa-trash"></i> {{ _('Eliminar') }}</button>

                                    </td>

                                </tr>
                            @endforeach

                        </tbody>

                        <tfoot>

                            <tr>
                                <th>Nombre</th>
                                <th>Apellidos</th>
                                <th>Email</th>
                                <th>Estado</th>
                                <th>Acciones</th>
                            </tr>

                        </tfoot>

                    </table>

                    {!! $users->render() !!}

                </div>{{-- /.box-body --}}

            </div>{{-- /.box --}}

        </div>{{-- /.col --}}

    </div>{{-- /.row --}}

    <div class="row">

        <div class="col-xs-12">

            <div class="box">

                <div class="box-header">
                    <h3 class="box-title">{{ _('Nuevos usuarios') }}</h3>
                </div>{{-- /.box-header --}}

                <div class="box-body">

                    <a class="btn btn-success" href="{{ route('admin.users.create') }}"><i class="fa fa-plus"></i> Crear usuario</a>

                </div>{{-- /.box-body --}}

            </div>{{-- /.box --}}

        </div>{{-- /.col --}}

    </div>{{-- /.row --}}





    {{-- Modal password reset --}}
    <div class="modal modal-primary fade" id="confirmPassword">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Restablecer contraseña</h4>
                </div>
                <div class="modal-body">
                    <p>¿Estás seguro que quieres restablecer la contraseña del siguiente usuario?</p>
                    <p>
                        <span class="txt-user-name"></span> <span class="txt-user-family-name"></span>
                        <br>
                        <span class="txt-user-email"></span>
                        <br>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cerrar</button>
                    {!! Form::open(['id'=>'passForm']) !!}
                        {!! Form::hidden('email', null) !!}
                        <button type="submit" class="btn btn-outline">Restablecer contraseña</button>
                    {!! Form::close() !!}
                </div>{{-- /.modal-footer --}}
            </div>{{-- /.modal-content --}}
        </div>{{-- /.modal-dialog --}}
    </div>{{-- /.modal --}}
    {{-- /Modal password reset --}}

    {{-- Modal delete --}}
    <div class="modal modal-danger fade" id="confirmDelete">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Eliminar usuario</h4>
                </div>
                <div class="modal-body">
                    <p>¿Estás seguro que quieres eliminar al siguiente usuario?</p>
                    <p>
                        <span class="txt-user-name"></span> <span class="txt-user-family-name"></span>
                        <br>
                        <span class="txt-user-email"></span>
                        <br>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cerrar</button>
                    {!! Form::open(['method' => 'DELETE', 'id'=>'delForm']) !!}
                        <button type="submit" class="btn btn-outline">Eliminar usuario</button>
                    {!! Form::close() !!}
                </div>{{-- /.modal-footer --}}
            </div>{{-- /.modal-content --}}
        </div>{{-- /.modal-dialog --}}
    </div>{{-- /.modal --}}
    {{-- /Modal delete --}}

    {{-- Modal ban --}}
    <div class="modal modal-warning fade" id="confirmBan">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Bloquear usuario</h4>
                </div>
                <div class="modal-body">
                    <p class="modal-txt">¿Estás seguro que quieres bloquear al siguiente usuario?</p>
                    <p>
                        <span class="txt-user-name"></span> <span class="txt-user-family-name"></span>
                        <br>
                        <span class="txt-user-email"></span>
                        <br>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cerrar</button>
                    {!! Form::open(['id'=>'banForm']) !!}
                        <button type="submit" class="btn btn-outline modal-btn">Bloquear usuario</button>
                    {!! Form::close() !!}
                </div>{{-- /.modal-footer --}}
            </div>{{-- /.modal-content --}}
        </div>{{-- /.modal-dialog --}}
    </div>{{-- /.modal --}}
    {{-- /Modal ban --}}

    {{-- Modal unban --}}
    <div class="modal modal-info fade" id="confirmUnban">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Desbloquear usuario</h4>
                </div>
                <div class="modal-body">
                    <p class="modal-txt">¿Estás seguro que quieres desbloquear al siguiente usuario?</p>
                    <p>
                        <span class="txt-user-name"></span> <span class="txt-user-family-name"></span>
                        <br>
                        <span class="txt-user-email"></span>
                        <br>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cerrar</button>
                    {!! Form::open(['id'=>'unbanForm']) !!}
                        <button type="submit" class="btn btn-outline modal-btn">Desbloquear usuario</button>
                    {!! Form::close() !!}
                </div>{{-- /.modal-footer --}}
            </div>{{-- /.modal-content --}}
        </div>{{-- /.modal-dialog --}}
    </div>{{-- /.modal --}}
    {{-- /Modal ban --}}

@endsection

@section('specific-footer-js')

    <script>
        {{-- Trigger Modal delete --}}
        $('#confirmDelete').on('show.bs.modal', function(e) {

            var userId = $(e.relatedTarget).data('user_id');
            var userName = $(e.relatedTarget).data('user_name');
            var userFamilyName = $(e.relatedTarget).data('user_family_name');
            var userEmail = $(e.relatedTarget).data('user_email');
            var fullUrl = "{{ url('admin/usuario') }}" + '/' + userId;

            $('#confirmDelete .txt-user-name').text( userName );
            $('#confirmDelete .txt-user-family-name').text( userFamilyName );
            $('#confirmDelete .txt-user-email').text( userEmail );
            $('#delForm').attr('action', fullUrl);
        });
        {{-- /Trigger Modal delete --}}

        {{-- Trigger Modal password --}}
        $('#confirmPassword').on('show.bs.modal', function(e) {

            var userName = $(e.relatedTarget).data('user_name');
            var userFamilyName = $(e.relatedTarget).data('user_family_name');
            var userEmail = $(e.relatedTarget).data('user_email');
            var fullUrl = '{{ url('password/email') }}';

            $('#confirmPassword .txt-user-name').text(userName);
            $('#confirmPassword .txt-user-family-name').text(userFamilyName);
            $('#confirmPassword .txt-user-email').text(userEmail);
            $('#passForm').attr('action', fullUrl);
            $('#passForm input:hidden[name=email]').val(userEmail);
        });
        {{-- /Trigger Modal password --}}

        {{-- Trigger Modal ban --}}
        $('#confirmBan').on('show.bs.modal', function(e) {

            var userId = $(e.relatedTarget).data('user_id');
            var userName = $(e.relatedTarget).data('user_name');
            var userFamilyName = $(e.relatedTarget).data('user_family_name');
            var userEmail = $(e.relatedTarget).data('user_email');
            var fullUrl = '{{ url('admin/usuario') }}' + '/' + userId + '/ban';

            $('#confirmBan .txt-user-name').text(userName);
            $('#confirmBan .txt-user-family-name').text(userFamilyName);
            $('#confirmBan .txt-user-email').text(userEmail);
            $('#banForm').attr('action', fullUrl);
        });
        {{-- /Trigger Modal ban --}}

        {{-- Trigger Modal unban --}}
        $('#confirmUnban').on('show.bs.modal', function(e) {

            var userId = $(e.relatedTarget).data('user_id');
            var userName = $(e.relatedTarget).data('user_name');
            var userFamilyName = $(e.relatedTarget).data('user_family_name');
            var userEmail = $(e.relatedTarget).data('user_email');
            var fullUrl = '{{ url('admin/usuario') }}' + '/' + userId + '/unban';

            $('#confirmUnban .txt-user-name').text(userName);
            $('#confirmUnban .txt-user-family-name').text(userFamilyName);
            $('#confirmUnban .txt-user-email').text(userEmail);
            $('#unbanForm').attr('action', fullUrl);
        });
        {{-- /Trigger Modal unban --}}

    </script>

@endsection
