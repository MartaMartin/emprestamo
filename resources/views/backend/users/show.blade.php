@extends('layout')

@section('content')
    <div class="page-header">
        <h1>Users / Show </h1>
    </div>


    <div class="row">
        <div class="col-md-12">

            <form action="#">
                <div class="form-group">
                    <label for="nome">ID</label>
                    <p class="form-control-static">{{$user->id}}</p>
                </div>
                <div class="form-group">
                     <label for="email">EMAIL</label>
                     <p class="form-control-static">{{$user->email}}</p>
                </div>
                    <div class="form-group">
                     <label for="password">PASSWORD</label>
                     <p class="form-control-static">{{$user->password}}</p>
                </div>
                    <div class="form-group">
                     <label for="name">NAME</label>
                     <p class="form-control-static">{{$user->name}}</p>
                </div>
                    <div class="form-group">
                     <label for="family_name">FAMILY_NAME</label>
                     <p class="form-control-static">{{$user->family_name}}</p>
                </div>
                    <div class="form-group">
                     <label for="dob">DOB</label>
                     <p class="form-control-static">{{$user->dob}}</p>
                </div>
                    <div class="form-group">
                     <label for="sex">SEX</label>
                     <p class="form-control-static">{{$user->sex}}</p>
                </div>
                    <div class="form-group">
                     <label for="status">STATUS</label>
                     <p class="form-control-static">{{$user->status}}</p>
                </div>
                    <div class="form-group">
                     <label for="role">ROLE</label>
                     <p class="form-control-static">{{$user->role}}</p>
                </div>
                    <div class="form-group">
                     <label for="real_id_type">REAL_ID_TYPE</label>
                     <p class="form-control-static">{{$user->real_id_type}}</p>
                </div>
                    <div class="form-group">
                     <label for="real_id_number">REAL_ID_NUMBER</label>
                     <p class="form-control-static">{{$user->real_id_number}}</p>
                </div>
                    <div class="form-group">
                     <label for="city">CITY</label>
                     <p class="form-control-static">{{$user->city}}</p>
                </div>
                    <div class="form-group">
                     <label for="address">ADDRESS</label>
                     <p class="form-control-static">{{$user->address}}</p>
                </div>
                    <div class="form-group">
                     <label for="postcode">POSTCODE</label>
                     <p class="form-control-static">{{$user->postcode}}</p>
                </div>
                    <div class="form-group">
                     <label for="state">STATE</label>
                     <p class="form-control-static">{{$user->state}}</p>
                </div>
                    <div class="form-group">
                     <label for="country">COUNTRY</label>
                     <p class="form-control-static">{{$user->country}}</p>
                </div>
                    <div class="form-group">
                     <label for="phone">PHONE</label>
                     <p class="form-control-static">{{$user->phone}}</p>
                </div>
            </form>



            <a class="btn btn-default" href="{{ route('admin.users.index') }}">Back</a>
            <a class="btn btn-warning" href="{{ route('admin.users.edit', $user->id) }}">Edit</a>
            <form action="#/$user->id" method="DELETE" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };"><button class="btn btn-danger" type="submit">Delete</button></form>
        </div>
    </div>


@endsection