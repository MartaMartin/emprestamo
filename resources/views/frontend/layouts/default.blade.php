<!DOCTYPE html>
<html lang="{{ Config::get('app.locale') }}">
    <head>
        <title>Emprestamo - @yield('title')</title>
        @include('frontend.partials.metatags')
        @include('frontend.partials.css-core')
    </head>

    <body class="@yield('body-classes')" id="@yield('pagina')">

        {{-- BEGIN MODALS LOGIN / REG --}}
        @include('frontend.partials.modal-auth')
        {{-- END MODALS LOGIN / REG --}}

        {{-- BEGIN MENU TOP --}}
        @include('frontend.partials.menu-top')
        {{-- END MENU TOP --}}

        {{-- BEGIN MAIN CONTENT --}}
        @yield('content')
        {{-- END MAIN CONTENT --}}

        {{-- BEGIN FOOTER --}}
        @include('frontend.partials.footer')
        {{-- END FOOTER --}}

        {{-- BEGIN CORE JAVASCRIPTS --}}
        @include('frontend.partials.js-core')
        {{-- END CORE JAVASCRIPTS --}}

    </body>

</html>
