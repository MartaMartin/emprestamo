@section('pagina','blog')								{{-- id de la pagina para el body --}}
@section('title','Blog')           			 			{{-- titulo de la pagina --}}
@section('blog_act','active')                    		{{-- activacion menu top --}}
@section('content')                               		{{-- contendido pagina --}}
<!-- BEGIN DESTACADAS -->
<div class="section bg-blanco" id="destacados">
	<div class="container">
		<div class="latobold12 text-uppercase">
			blog emprestamo
		</div>
		<div class="row margTop40" id="contNot">
			@foreach ($news as $i => $n)
				@if ($i <= 1)
					<div class="col-md-6 col-sm-12 col-xs-12 margBot40">
						<a href="{{ route('news.show', [$n->slug]) }}"><div class="imagen" style="background: url({{ AssetUrl::get('Document', 'image', $n->image, 500, 400) }}) no-repeat center center / cover"></div></a>
						<div class="latobold12 margTop30"><span class="naranja text-uppercase">{{ $n->category->name  }}</span> <span class="negro">| {{ Functions::shortDate($n->created_at) }}</span></div>
						<div class="latoligth34 margTop20"><a href="{{ route('news.show', [$n->slug]) }}" class="negro">{{ $n->title }}</a></div>
					</div>
				@else
					<div class="col-md-4 margBot40">
						<a href="{{ route('news.show', [$n->slug]) }}"><div class="imagenmed" style="background: url({{ AssetUrl::get('Document', 'image', $n->image, 500, 400) }}) no-repeat center center / cover"></div></a>
						<div class="latobold12 margTop30"><span class="naranja text-uppercase">{{ $n->category->name }}</span> <span class="negro">| {{ Functions::shortDate($n->created_at) }}</span></div>
						<div class="latoligth34 margTop20"><a href="{{ route('news.show', [$n->slug]) }}" class="negro">{{ $n->title }}</a></div>
					</div>
				@endif
			@endforeach
		</div>
		<!-- END LISTADO NOTICIAS -->

		<div class="row text-center margTop30">
			<a id="loadMoreNews" class="btn btn-transparente naranja text-uppercase" href="javascript:;"> cargar más noticias</a>
		</div>
	</div>
</div>
<!-- END NOTICIAS RESTO -->
@endsection

@extends('frontend.layouts.default')
