@section('pagina','blog-det')							{{-- id de la pagina para el body --}}
@section('title',$news->title)           			 			{{-- titulo de la pagina --}}
@section('blog_act','active')                    		{{-- activacion menu top --}}


@section('scripts_pagina')								{{-- js plugins inscluidos en la pagina (indept de los core) --}}
<script type="text/javascript" src="/assets/plugins/social/jssocials.min.js"></script>
<script type="text/javascript" src="/assets/scripts/blog.js"></script>
@endsection

@section('css_pagina')									{{-- css especifico de la pagina --}}
<link href="/assets/css/jssocials.css" rel="stylesheet" type="text/css">
<link href="/assets/css/jssocials-theme-flat.css" rel="stylesheet" type="text/css">
@endsection

@section('js_pagina')									{{-- script de la pagina --}}
@parent
Blog.init();
@endsection

@section('content')                               		{{-- contendido pagina --}}
<!-- BEGIN NOTICIA -->
<div class="section bg-blanco animatedParent animateOnce" id="noticia">
	<div class="container">
		<div class="latobold12 text-uppercase">
            blog emprestamo | {{ $news->title }}
		</div>
		<div class="row margTop40">
			<div class="col-md-8 col-md-push-2 col-sm-12 col-xs-12 margBot40 animated fadeIn">
				<div class="imagen" style="background: url({{ AssetUrl::get('Document', 'image', $news->image, 675, 450) }}) no-repeat center center / cover"></div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 col-md-push-3 fechacat">
                <div class="latobold12 titular"><span class="naranja text-uppercase">{{ $news->category->name }}</span> <span class="negro">| {{ Functions::shortDate($news->created_at) }}</span></div>

				<!-- BEGIN DATOS NOTICIA -->
                <div class="latoligth34 margTop20">{{ $news->title }}</div>
				<div class="latoligth15 margTop20">
                    {!! $news->content !!}
				</div>
				<div class="latoligth34 text-center margTop60">Comparte la noticia</div>
				<!-- END DATOS NOTICIA -->

				<!-- BEGIN SOCIAL SHARE -->
				<div class="margTop20 text-center">
					<div id="social"></div>
				</div>
				<!-- END SOCIAL SHARE -->

			</div>
		</div>
		<div class="row margTop90">
			<div class="latobold16 text-center text-uppercase margBot30">noticias relacionadas</div>
            @foreach ($news->related_news as $n)
                <div class="col-md-4 margBot40">
                    <a href="{{ route('news.show', [$n->slug]) }}"><div class="imagenmed" style="background: url({{ AssetUrl::get('Document', 'image', $n->image, 675, 450) }}) no-repeat center center / cover"></div></a>
                    <div class="latobold12 margTop30"><span class="naranja text-uppercase">{{ $n->category->name }}</span> <span class="negro">| {{ Functions::shortDate($n->created_at) }}</span></div>
                    <div class="latoligth21 margTop20"><a href="{{ url('/blog/view') }}" class="negro">{{ $n->title }}</a></div>
                </div>
            @endforeach
		</div>
	</div>
</div>
<!-- END NOTICIA -->
@endsection

@extends('frontend.layouts.default')
