@section('pagina','casos-exito-inv')						{{-- id de la pagina para el body --}}
@section('title','Casos de éxito Inversores')           	{{-- titulo de la pagina --}}
@section('casos-ex-inv','active')                    		{{-- activacion menu top --}}
@section('content')                           				{{-- contendido pagina --}}
<!-- BEGIN CONOCE EXPERIENCIA -->
<div id="casos" class="section blanco animatedParent animateOnce" style="background: url(/assets/img/fondos/casoscalc.jpg) no-repeat center bottom / cover;">
	<div class="filtro-img"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-push-2 text-center">

				<div class="row">
					<div class="col-md-12">
						<p class="text-inverse text-uppercase latobold12 animated growIn">casos de éxito</p>
						<h1 class="text-inverse text-uppercase black margTop30 animated bounceInLeft">inversiones de éxito</h1>
					</div>
				</div>
				<div class="row ">
					<div class="col-md-6 col-md-push-3">
						<p class="text-inverse latoligth21 margTop20 animated growIn hidden-sm hidden-xs">Solicita un préstamo a inversores y te aseguramos en dinero en menos de dos días</p>
					</div>
				</div>


			</div>
		</div>
	</div>
</div>
<!-- END CONOCE EXPERIENCIA -->

<!-- BEGIN CASOS -->
<div class="section bg-blanco margTop30" id="casoslist">
	<div class="container ">
		<div class="fichacasoex margBot50">
			<div class="row animatedParent animateOnce">
				<div class="col-md-12 imagen animated bounceIn" style="background:url(/assets/img/testimonial/testimonial-3.jpg) center center / cover">
				</div>
			</div>
			<div class="clr margTop30"></div>
			<div class="row animatedParent animateOnce">
				<div class="col-md-6 animated bounceInLeft">
					<div class="regular34">Nombre Inversor / empresa</div>
					<div class="regular18 margTop20">
						Morbi nec consectetur nibh. Duis finibus arcu metus, ut egestas magna congue in.
					</div>
				</div>
				<div class="col-md-6 animated growIn">
					<div class="latobold12 amarillo text-uppercase">Laura Moss | Telecomunicaciones</div>
					<div class="latoligth15 margTop20">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi nec consectetur nibh. Duis finibus arcu metus, ut egestas magna congue in. Nam mollis porttitor tortor eget consectetur. Cconsectetur adipiscing elit. Morbi nec consectetur nibh. Duis finibus arcu metus, ut egestas magna congue in. Nam mollis porttitor tortor eget consectetur.</div>
				</div>
			</div>
		</div>
		<div class="fichacasoex margBot50">
			<div class="row animatedParent animateOnce">
				<div class="col-md-12 imagen animated bounceIn" style="background:url(/assets/img/testimonial/testimonial-2.jpg) center center / cover">
				</div>
			</div>
			<div class="clr margTop30"></div>
			<div class="row animatedParent animateOnce">
				<div class="col-md-6 animated bounceInLeft">
					<div class="regular34">Nombre Inversor / empresa</div>
					<div class="regular18 margTop20">
						Morbi nec consectetur nibh. Duis finibus arcu metus, ut egestas magna congue in.
					</div>
				</div>
				<div class="col-md-6 animated growIn">
					<div class="latobold12 amarillo text-uppercase">Laura Moss | Telecomunicaciones</div>
					<div class="latoligth15 margTop20">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi nec consectetur nibh. Duis finibus arcu metus, ut egestas magna congue in. Nam mollis porttitor tortor eget consectetur. Cconsectetur adipiscing elit. Morbi nec consectetur nibh. Duis finibus arcu metus, ut egestas magna congue in. Nam mollis porttitor tortor eget consectetur.</div>
				</div>
			</div>
		</div>
		<div class="fichacasoex margBot50">
			<div class="row animatedParent animateOnce">
				<div class="col-md-12 imagen animated bounceIn" style="background:url(/assets/img/testimonial/testimonial-1.jpg) center center / cover">
				</div>
			</div>
			<div class="clr margTop30"></div>
			<div class="row animatedParent animateOnce">
				<div class="col-md-6 animated bounceInLeft">
					<div class="regular34">Nombre Inversor / empresa</div>
					<div class="regular18 margTop20">
						Morbi nec consectetur nibh. Duis finibus arcu metus, ut egestas magna congue in.
					</div>
				</div>
				<div class="col-md-6 animated growIn">
					<div class="latobold12 amarillo text-uppercase">Laura Moss | Telecomunicaciones</div>
					<div class="latoligth15 margTop20">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi nec consectetur nibh. Duis finibus arcu metus, ut egestas magna congue in. Nam mollis porttitor tortor eget consectetur. Cconsectetur adipiscing elit. Morbi nec consectetur nibh. Duis finibus arcu metus, ut egestas magna congue in. Nam mollis porttitor tortor eget consectetur.</div>
				</div>
			</div>
		</div>

	</div>
</div>
<!-- END CASOS -->

<!-- BEGIN CUADRADO SUGERENCIA -->
<div class="section bg-gris-claro animatedParent animateOnce text-center" id="suger">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-md-push-4 col-sm-12 conshadow bg-blanco animated growIn cuadrado">
				<div class="row">
					<div class="col-md-12">
						<h1 class="light hidden-sm hidden-xs">¿Quieres probar wallet?</h1>
						<h1 class="light hidden-lg hidden-md">Solicita un préstamo</h1>
						<h1 class="light hidden-md hidden-lg">Únete a Emprestamo</h1>
						<p class="paddBot20 hidden-sm hidden-xs">Proceso sencillo. Tarifas 100% transparentes, con precios muy competitivos</p>
					</div>
				</div>
				<div class="clr"></div>
				<div class="row">
					<div class="clr"></div>
					<div class="col-md-6 col-md-push-3">
						<a class="btn noradio bg-amarillo text-uppercase blanco margTop20"  href="{{ url('/registro-empresa') }}">regístrte ahora</a>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
<!-- END CUADRADO SUGERENCIA -->
@endsection
@extends('frontend.layouts.default')