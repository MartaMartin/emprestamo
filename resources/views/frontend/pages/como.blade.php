@section('pagina','como')								{{-- id de la pagina para el body --}}
@section('title','Como Funciona')            			{{-- titulo de la pagina --}}
@section('como_act','active')                    		{{-- activacion menu top --}}
@section('content')										{{-- contendido pagina --}}
<!-- BEGIN SLIDER PORTADA -->
<div class="section" id="slidercomo" style="background: url(/assets/img/fondos/como.jpg) no-repeat center bottom / cover;">
	<div class="filtro-img light"></div>
	<div class="container animatedParent animateOnce margTop30" >
		<div class="row">
			<div class="col-md-12 text-center">
				<p class="text-inverse text-uppercase latobold12 animated fadeInDown">Cómo funciona</p>
				<h1 class="text-inverse text-uppercase black margTop30 animated growIn">Operar a través de EMPRESTAMO es muy sencillo</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 col-md-offset-3 text-center">
				<p class="text-inverse latoligth21 margTop20 animated fadeIn wordspa2">Si tienes una factura pendiente de cobrar o necesitas la inyección puntual de capital para acometer un nuevo servicio a uno de tus clientes, ¡Emprestamo es tu solución!</p>
			</div>
		</div>
	</div>

</div>
<!-- END SLIDER PORTADA -->

<!-- BEGIN PASOS -->
<div class="section bg-blanco" id="pasos">
	<div class="container">
		<div class="col-md-8 col-md-push-2">
            <h3>Si eres una EMPRESA</h3>
			<ul class="pasoscomo margTop30 animatedParent animateOnce ul-company" data-sequence="200">
				<li data-id="1" class="animated growIn">
					<div class="latoligth34">Crea</div>
                    <p>1. CREA una nueva operación de préstamo desde tu PERFIL DE USUARIO, Nuestros Analistas de Riesgos analizarán en detalle la operación propuesta, para asegurarse de que cumple con los criterios de CALIDAD requeridos y, si es así, darán luz verde a la misma.</p>
				</li>
				<li data-id="2" class="animated growIn">
					<div class="latoligth34">Acepta</div>
					<p>2. ACEPTA la oferta realizada por nuestros analistas con el coste para tu operación</p>
				</li>
				<li data-id="3" class="animated growIn">
					<div class="latoligth34">Publicamos</div>
					<p>3. Una vez tu operación ha sido aceptada por nuestras Analistas de Riesgos, PUBLICAMOS tu operación ante nuestra Red de Inversores</p>
				</li>
				<li data-id="4" class="animated growIn">
					<div class="latoligth34">Recibe</div>
					<p>4. RECIBE tu préstamo en menos de 48 horas</p>
				</li>
			</ul>
            ¡Así de fácil! Una vez realice el cobro la empresa deudora, desde EMPRESTAMO nos encargaremos de hacer llegar a nuestros inversores sus respectivas cantidades (Capital principal + Intereses).
            ¡Tú no tendrás que ocuparte de nada más que de atender a tus nuevos clientes!
		</div>
	</div>
</div>
<!-- END PASOS -->

<!-- BEGIN PASOS -->
<div class="section bg-blanco" id="pasos2">
	<div class="container">
		<div class="col-md-8 col-md-push-2">
            <h3>Si eres un INVERSOR</h3>
            Ahora puedes prestar capital a compañías solventes de la mano de EMPRESTAMO, de forma segura y sin riesgos, pues todas las operaciones propuestas en la plataforma están cubiertas por una aseguradora de primer nivel.
			<ul class="pasoscomo margTop30 animatedParent animateOnce ul-investor" data-sequence="200">
				<li data-id="1" class="animated growIn">
					<div class="latoligth34">Regístrate</div>
                    <p>1. REGÍSTRATE como inversor desde aquí</p>
				</li>
				<li data-id="2" class="animated growIn">
					<div class="latoligth34">Selecciona</div>
					<p>2. SELECCIONA tus criterios de inversión</p>
				</li>
				<li data-id="3" class="animated growIn">
					<div class="latoligth34">Controla</div>
					<p>3. CONTROLA todos tus movimientos de dinero así como los retornos esperados desde tu Wallet</p>
				</li>
				<li data-id="4" class="animated growIn">
					<div class="latoligth34">Invierte</div>
					<p>4. INVIERTE en compañías solventes mediante operaciones aseguradas</p>
				</li>
				<li data-id="5" class="animated growIn">
					<div class="latoligth34">Recibe</div>
					<p>5. RECIBE tus retornos en tu wallet</p>
				</li>
			</ul>
            ¡Así de fácil! Una vez realice el cobro la empresa deudora, desde EMPRESTAMO nos encargaremos de hacer llegar a nuestros inversores sus respectivas cantidades (Capital principal + Intereses).
            ¡Tú no tendrás que ocuparte de nada más que de atender a tus nuevos clientes!
		</div>
	</div>
</div>
<!-- END PASOS -->

<!-- BEGIN CUADRADOS SUGERENCIAS -->
<div class="section bg-gris-claro animatedParent animateOnce" id="suger">
	<div class="container">
		<div class="row">
			<div class="col-md-5 col-sm-12 conshadow bg-blanco animated growIn cuadrado text-center">
				<div class="row">
					<div class="col-md-12">
						<h1 class="light margTop60">Solicita tu préstamo</h1>
						<p class="paddBot20 font16 wordspa5 margTop20 hidden-sm hidden-xs">Proceso sencillo. Tarifas 100% transparentes,<br>con precios muy competitivos</p>
					</div>
				</div>
				<div class="clr"></div>
				<div class="row">
					<div class="clr"></div>
					<a class="btn noradio bg-amarillo text-uppercase blanco margTop20" href="{{ url('/registro-empresa') }}">¿Quieres un prestamo?</a>
				</div>
			</div>


			<div class="col-md-5 col-sm-12 col-md-push-2 conshadow bg-blanco animated growIn cuadrado text-center">
				<div class="row">
					<div class="col-md-12">
						<h1 class="light margTop60 hidden-sm hidden-xs">¿Quieres probar el wallet?</h1>
						<h1 class="light margTop60 hidden-lg hidden-md">Únete a emprestamo</h1>
						<p class="paddBot20 font16 wordspa5 margTop20 hidden-sm hidden-xs">Obtén altas rentabilidades a corto plazo,<br> superiores al 8% anual</p>
					</div>
				</div>
				<div class="clr"></div>
				<div class="row">
					<div class="clr"></div>
					<a class="btn noradio bg-morado text-uppercase blanco margTop20" href="{{ url('/registro-inversor') }}">regístrate ahora</a>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END CUADRADOS SUGERENCIAS -->
@endsection

@extends('frontend.layouts.default')

