@section('pagina','empresas')								{{-- id de la pagina para el body --}}
@section('title','Empresas')           			 			{{-- titulo de la pagina --}}
@section('empresa_act','active')                    		{{-- activacion menu top --}}


@section('scripts_pagina')									{{-- js plugins inscluidos en la pagina (indept de los core) --}}
<script type="text/javascript" src="/assets/plugins/slider/ion.rangeSlider.min.js"></script>
<script type="text/javascript" src="/assets/scripts/empresas.js"></script>
@endsection

@section('css_pagina')										{{-- css especifico de la pagina --}}
	<link href="/assets/css/ion.rangeSlider.css" rel="stylesheet" type="text/css">
	<link href="/assets/css/ion.rangeSlider.skinModern.css" rel="stylesheet" type="text/css">
@endsection

@section('js_pagina')										{{-- script de la pagina --}}
@parent
	Empresas.init();
@endsection

@section('content')                               			{{-- contendido pagina --}}
<!-- BEGIN CABECERA SLOGAN -->
<div class="section" id="slogan">
	<div class="container">
		<div class="row">
			<div class="col-md-5 col-sm-12 col-xs-12 izq">
				<div class="amarillo text-uppercase">Empresas</div>
				<div class="negro hidden-sm hidden-xs">Consíguelo, de manera sencilla</div>
				<div class="negro hidden-lg hidden-md">Obtén un préstamo</div>
			</div>
			<div class="col-md-5 col-md-push-1 col-sm-12 col-xs-12 drxa">
                EMPRESTAMO ofrece la posibilidad de adelantar sus facturas a cualquier empresa con una  factura aprobada y pendiente de cobrar por un servicio prestado a una empresa solvente.
			</div>
		</div>
	</div>
</div>
<!-- END CABECERA SLOGAN -->

<!-- BEGIN INFO DATOS -->
<div class="section bg-amarillo animatedParent blanco text-center" id="icodatos">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<img src="/assets/img/extra/ico-papel-white.png">
				<div class="latoligth15 margTop5">1. ¿Te ves obligado a esperar a cobrar una factura aprobada y pendiente por un servicio prestado a una empresa solvente?</div>
			</div>
			<div class="col-md-3">
				<img src="/assets/img/extra/ico-rueda-white.png">
				<div class="latoligth21 margTop5">2. EMPRESTAMO te ofrece préstamo a corto plazo, a precios competitivos</div>
			</div>
			<div class="col-md-3">
				<img src="/assets/img/extra/ico-stats-white.png">
				<div class="latoligth21 margTop5">3. Nuestros préstamos no computan en CIRBE</div>
			</div>
			<div class="col-md-3">
				<img src="/assets/img/extra/ico-cartera-white.png">
				<div class="latoligth21 margTop5">4. Sin comisiones de estudio, sin comisiones de no disponibilidad, </div>
			</div>
		</div>
	</div>
</div>
<!-- END INFO DATOS -->

<!-- BEGIN CALCULADORA -->
<div class="section bg-blanco margTop30 animatedParent animateOnce" id="calculadora">
	<div class="container ">
		<div class="col-md-6 col-sm-12 col-xs-12 bg-gris-claro bord-gris" id="minicalc">
			<div class="latobold12 text-uppercase margBot30">importe a financiar</div>
			<!-- creamos input que será y almacenará el valor del slider  -->
			<input id="slidr" name="slidr">
			<div class="latobold12 text-uppercase margTop20">dias de vencimiento</div>
			<!-- creamos botones con valores de los dias en el atributo data-dias para almacenearlo con click:js en input dias -->
			<div class="btn-group latobold12 margTop10 text-left" role="group" aria-label="Basic example">
				<button type="button" class="btn sinradio" data-dias="30">0-30</button>
				<button type="button" class="btn sinradio" data-dias="60">31-60</button>
				<button type="button" class="btn sinradio" data-dias="90">61-90</button>
				<button type="button" class="btn sinradio" data-dias="120">91-120</button>
				<button type="button" class="btn sinradio" data-dias="150">121-150</button>
				<button type="button" class="btn sinradio" data-dias="180">151-180</button>
				<!-- creamos input donde almacenaremos el valor de los dias. Por defecto el primero: 30 -->
				<input type="hidden" name="dias" id="dias" value="30">
			</div>
			<div class="row">
				<div class="col-md-4 margTop20">
					<div class="latobold12 text-uppercase">recibe hasta</div>
					<!-- mostraremos el valor de la financiación -->
					<div class="amarillo latobold16 margTop10" id="hasta"></div>
				</div>
				<div class="col-md-8 margTop20">
					<div class="latobold12 text-uppercase">coste financiación</div>
					<!-- mostraremos el valor del coste de la financiación -->
					<div class="amarillo latobold16 margTop10" id="coste">65€</div>
				</div>
			</div>
		</div>
		<div class="col-md-5 col-md-push-1 animated growIn hidden-sm hidden-xs">
			<h2 class="regular34">Calcula tu préstamo</h2>
			<div class="regular18">Tu prestamo en menos de 48 horas</div>
			<div class="latoligth15 margTop30">Las cantidades mostradas son orientativas ya que el coste exacto de la financiación depende del análisis de riesgo realizado por nuestro equipo. Para saber el coste de financiación puedes solicitar un presupuesto sin ningún compromiso.</div>
			<div class="latoblack12 naranja text-uppercase margTop30">Solicita un préstamo</div>
			<div class="latoligth15 margTop30">Proceso sencillo. Tarifas 100% transparentes, con precios muy competitivos</div>
		</div>
	</div>
</div>
<!-- END CALCULADORA -->

<!-- BEGIN CONOCE EXPERIENCIA -->
<div id="casos" class="section blanco margTop30 animatedParent animateOnce" style="background: url(/assets/img/fondos/casoscalc.jpg) no-repeat fixed center bottom / cover;">
	<div class="filtro-img"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-push-2 text-center">

				<div class="row">
					<div class="col-md-12">
						<p class="text-inverse text-uppercase latobold12 animated bounceInRight">casos de éxito</p>
						<h1 class="text-inverse text-uppercase black margTop30 animated bounceInLeft">conoce la expriencia</h1>
					</div>
				</div>
				<div class="row ">
					<div class="col-md-12">
						<p class="text-inverse latoligth21 margTop20 animated bounceInRight hidden-sm hidden-xs">Ellos forman parte de Emprestamos y quieren compartir su experiencia</p>
						<a class="btn btn-lg btn-transparente margTop20 text-center animated growIn text-uppercase" href="{{ url('/casos-de-exito-empresa') }}">Ver todos los casos</a>
					</div>
				</div>


			</div>
		</div>
	</div>
</div>
<!-- END CONOCE EXPERIENCIA -->

<!-- BEGIN REQUISITOS -->
<div class="section bg-blanco" id="requisitos">
	<div class="container text-center">
		<h1 class="light margTop40">Requisitos para empresas</h1>
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 margTop50">
				<div class="text-center"><img src="/assets/img/extra/client-nac.png"></div>
				<div class="latoblack14 text-uppercase">clientes nacionales</div>
				<div class="latoligth15 margTop20">Lorem ipsum ad his scripta blandit partiendo, eum fastidii accumsan euripidis in, eum liber hendrerit an. Qui ut wisi vocibus suscipiantur, quo dicit ridens inciderint id. Quo mundi lobortis reformidans eu, legimus senserit definiebas an eos.</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 margTop50">
				<div class="text-center"><img src="/assets/img/extra/client-intnac.png"></div>
				<div class="latoblack14 text-uppercase">clientes internacionales</div>
				<div class="latoligth15 margTop20">Lorem ipsum ad his scripta blandit partiendo, eum fastidii accumsan euripidis in, eum liber hendrerit an. Qui ut wisi vocibus suscipiantur, quo dicit ridens inciderint id. Quo mundi lobortis reformidans eu, legimus senserit definiebas an eos.</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 margTop50">
				<div class="text-center"><img src="/assets/img/extra/client-pub.png"></div>
				<div class="latoblack14 text-uppercase">administración pública</div>
				<div class="latoligth15 margTop20">Lorem ipsum ad his scripta blandit partiendo, eum fastidii accumsan euripidis in, eum liber hendrerit an. Qui ut wisi vocibus suscipiantur, quo dicit ridens inciderint id. Quo mundi lobortis reformidans eu, legimus senserit definiebas an eos.</div>
			</div>
		</div>
	</div>
	<div class="clr margTop20"></div>
</div>
<!-- END REQUISITOS -->

<!-- BEGIN CUADRADO SUGERENCIA -->
<div class="section bg-gris-claro animatedParent animateOnce text-center" id="suger">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-md-push-4 col-sm-12 conshadow bg-blanco animated growIn cuadrado">
				<div class="row">
					<div class="col-md-12">
						<h1 class="light hidden-xs hidden-sm">Solicita un préstamo</h1>
						<h1 class="light hidden-lg hidden-md">Solicita un préstamo</h1>
						<p class="paddBot20 hidden-sm hidden-xs">Proceso sencillo. Tarifas 100% transparentes, con precios muy competitivos</p>
					</div>
				</div>
				<div class="clr"></div>
				<div class="row">
					<div class="clr"></div>
					<div class="col-md-6 col-md-push-3">
						<a class="btn noradio bg-amarillo text-uppercase blanco margTop20" href="{{ url('/registro-empersa')  }}">regístrte ahora</a>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
<!-- END CUADRADO SUGERENCIA -->
@endsection

@extends('frontend.layouts.default')
