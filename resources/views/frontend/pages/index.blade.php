@section('pagina','home')								{{-- id de la pagina para el body --}}
@section('title','Bienvenidos')            				{{-- titulo de la pagina --}}
@section('index_act','active')                    		{{-- activacion menu top --}}
@section('content')										{{-- contendido pagina --}}
	<!-- BEGIN SLIDER PORTADA -->
	<div id="sliderportada">
		<div id="slider" class="carousel slide">
			<!-- BEGIN BOTONERA SLIDER -->
			<ol class="carousel-indicators">
				<li data-target="#slider" data-slide-to="0" class="active"></li>
				<li data-target="#slider" data-slide-to="1"></li>
				<li data-target="#slider" data-slide-to="2"></li>
				<li data-target="#slider" data-slide-to="3"></li>
			</ol>
			<!-- END BOTONERA SLIDER -->

			<!-- BEGIN SLIDES -->
			<div class="carousel-inner animatedParent" role="listbox">
				<div class="item active" style="background: url(/assets/img/fondos/fondoindex.jpg) no-repeat center center / cover;">
					<div class="filtro-img"></div>
					<div class="container" >
						<div class="row">
							<div class="col-md-12 text-center">
								<p class="text-inverse text-uppercase latobold12 animated fadeInDown">Solicita un préstamo</p>
								<h1 class="text-inverse text-uppercase black margTop30 animated growIn">Tu prestamo en menos de 48 h</h1>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-md-offset-3 text-center">
								<p class="text-inverse latoligth21 margTop20 animated fadeIn">Solicita un préstamo a inversores y te aseguramos el dinero en menos de
									dos días</p>
								<a class="btn btn-lg btn-transparente margTop20 animated fadeIn" href="{{ url('/como-funciona')  }}">Cómo funciona</a>
							</div>
						</div>
					</div>
				</div>

				<div class="item" style="background: url(/assets/img/fondos/fondoindex.jpg) no-repeat center center / cover;">
					<div class="filtro-img"></div>
					<div class="container" >
						<div class="row">
							<div class="col-md-12 text-center">
								<p class="text-inverse text-uppercase latobold12 animated fadeInDown">Solicita un préstamo</p>
								<h1 class="text-inverse text-uppercase black margTop30 animated growIn">Tu prestamo en menos de 48 h</h1>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-md-offset-3 text-center">
								<p class="text-inverse latoligth21 margTop20 animated fadeIn">Solicita un préstamo a inversores y te aseguramos el dinero en menos de
									dos días</p>
								<a class="btn btn-lg btn-transparente margTop20 animated fadeIn" href="{{ url('/como-funciona')  }}">Cómo funciona</a>
							</div>
						</div>
					</div>
				</div>

				<div class="item" style="background: url(/assets/img/fondos/fondoindex.jpg) no-repeat center center / cover;">
					<div class="filtro-img"></div>
					<div class="container" >
						<div class="row">
							<div class="col-md-12 text-center">
								<p class="text-inverse text-uppercase latobold12 animated fadeInDown">Solicita un préstamo</p>
								<h1 class="text-inverse text-uppercase black margTop30 animated growIn">Tu prestamo en menos de 48 h</h1>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-md-offset-3 text-center">
								<p class="text-inverse latoligth21 margTop20 animated fadeIn">Solicita un préstamo a inversores y te aseguramos el dinero en menos de
									dos días</p>
								<a class="btn btn-lg btn-transparente margTop20 animated fadeIn" href="{{ url('/como-funciona')  }}">Cómo funciona</a>
							</div>
						</div>
					</div>
				</div>

				<div class="item" style="background: url(/assets/img/fondos/fondoindex.jpg) no-repeat center center / cover;">
					<div class="filtro-img"></div>
					<div class="container" >
						<div class="row">
							<div class="col-md-12 text-center">
								<p class="text-inverse text-uppercase latobold12 animated fadeInDown">Solicita un préstamo</p>
								<h1 class="text-inverse text-uppercase black margTop30 animated growIn">Tu prestamo en menos de 48 h</h1>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-md-offset-3 text-center">
								<p class="text-inverse latoligth21 margTop20 animated fadeIn">Solicita un préstamo a inversores y te aseguramos el dinero en menos de
									dos días</p>
								<a class="btn btn-lg btn-transparente margTop20 animated fadeIn" href="{{ url('/como-funciona')  }}">Cómo funciona</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END SLIDES -->

		</div>
		<!--
		<div class="cover">
		</div>
		-->
	</div>
	<!-- END SLIDER PORTADA -->

	<!-- BEGIN CUADRADOS SUGERENCIAS -->
	<div class="section bg-gris-claro animatedParent animateOnce" id="suger">
		<div class="container">
			<div class="row">
				<div class="col-md-5 col-sm-12 conshadow bg-blanco animated growIn cuadrado">
					<div class="row">
						<div class="col-md-12">
							<h1 class="light">PYME</h1>
							<p class="paddBot20">Obtén un préstamo a las mejores condiciones del mercado</p>
							<ul class="empresa">
								<li>¿Necesitas soluciones de financiación flexibles y rápidas para tus picos de tesorería?</li>
								<li>Solicita un préstamo a nuestros inversores y podrás tener el dinero en menos de 48 horas: ¡contamos con la mayor Red de Inversores privados de España!</li>
                                <li>Ofrecemos las condiciones más competitivas del mercado ¡sin avales! – calcula tu préstamo <a href="{{ url('/empresas') }}">AQUÍ</a></li>
							</ul>
						</div>
					</div>
					<div class="clr"></div>
					<div class="row">
						<div class="clr"></div>
						<div class="col-md-6 col-sm-6 col-xs-6 pull-left">
							<a class="btn noradio bg-amarillo text-uppercase blanco margTop20" href="{{ url('/registro-empresa') }}">¿Quieres un prestamo?</a>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-6 text-center">
							<div class="latoblack12 amarillo text-uppercase margTop40 tipocuenta">Pymes</div>
						</div>

					</div>
				</div>


				<div class="col-md-5 col-sm-12 col-md-push-2 conshadow bg-blanco animated growIn cuadrado">
					<div class="row">
						<div class="col-md-12">
							<h1 class="light">INVERSOR</h1>
							<p class="paddBot20">Obtén un retorno seguro a tu inversión</p>
							<ul class="inversor">
								<li>¿Quieres invertir en operaciones garantizadas, con rentabilidades de hasta el 15% anual?</li>
                                <li>Averigua qué operaciones tenemos en curso y comienza a invertir <a href="{{ url('/registro-inversor') }}">AHORA</a></li>
							</ul>
						</div>
					</div>
					<div class="clr"></div>
					<div class="row">
						<div class="clr"></div>
						<div class="col-md-6 col-sm-6 col-xs-6 pull-left">
							<a class="btn noradio bg-morado text-uppercase blanco margTop20" href="{{ url('/registro-inversor') }}">únete a emprestamo</a>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-6 text-center">
							<div class="latoblack12 amarillo text-uppercase margTop40 tipocuenta">Inversores</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END CUADRADOS SUGERENCIAS -->

	<!-- BEGIN INFO DATOS -->
	<div class="section bg-gris-claro animatedParent animateOnce hidden-sm hidden-xs" id="icodatos">
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<div class="pull-left"><img src="/assets/img/extra/ico-cartera.png"></div>
					<div class="latoligth15 margTop20 paddLef20">Cartera de inversión (1.000.000€)</div>
				</div>
				<div class="col-md-3">
					<div class="pull-left"><img src="/assets/img/extra/ico-papel.png"></div>
					<div class="latoligth15 margTop20 paddLef20">Préstamo medio (20.000€)</div>
				</div>
				<div class="col-md-3">
					<div class="pull-left"><img src="/assets/img/extra/ico-rueda.png"></div>
					<div class="latoligth15 margTop20 paddLef20">Tiempo medio de devolución (60 días)</div>
				</div>
				<div class="col-md-3">
					<div class="pull-left"><img src="/assets/img/extra/ico-stats.png"></div>
					<div class="latoligth15 margTop20 paddLef20">Tipo Medio de interés (2,5%)</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END INFO DATOS -->

	<!-- BEGIN WALLET -->
	<div id="wallet" class="section blanco animatedParent animateOnce" style="background: url(/assets/img/fondos/wallet.jpg) no-repeat fixed center center;">
		<div class="filtro-img"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-6">

					<div class="row">
						<div class="col-md-12">
							<h1 class="text-inverse text-uppercase black margTop30 animated bounceInLeft">Tu wallet</h1>
							<p class="text-inverse text-uppercase latobold12 animated bounceInRight">Acceso total a tus operaciones</p>
						</div>
					</div>
					<div class="row  hidden-sm hidden-xs">
						<div class="col-md-12">
							<p class="text-inverse latoligth21 margTop20 animated bounceInRight">Tanto si eres EMPRESA como si eres INVERSOR, a través de tu PANEL DE USUARIO podrás acceder y gestionar tus operaciones en tiempo real, desde cualquier dispositivo, de manera sencilla y segura.</p>
							<a class="btn btn-lg btn-transparente margTop20 text-center animated growIn" href="{{ url('/como-funciona')  }}">Cómo funciona</a>
						</div>
					</div>


				</div>
				<div class="col-md-5 col-md-push-1 animatedParent animateOnce">
					<div class="bg-blanco fullwidth negro animated bounceIn hidden-sm hidden-xs" id="rentab">
						<div class="row">
							<div class="col-md-8 pull-left">
								<p class="latobold16 text-uppercase">rentabilidad anual</p>
								<p class="latoligth15">Superior al</p>
							</div>
							<div class="col-md-4 pull-right">
								<h1 class="black morado text-right margRig10"><img src="/assets/img/extra/ico-flecha-up.png"/> <span class="porcent">8</span>%</h1>
							</div>
						</div>
					</div>
					<div class="ipad animated bounceInDown"></div>
					<div class="clr"></div>
				</div>
			</div>
		</div>
	</div>
	<!-- END WALLET -->

	<!-- BEGIN SLIDER USUARIOS -->
	<div class="section  hidden-sm hidden-xs">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1 class="text-center text-uppercase latobold12">Ellos ya forman parte</h1>
					<div class="barramorada"></div>
				</div>
			</div>
			<div class="row margTop30">
				<div id="sliderusers" class="carousel slide">
					<!-- BEGIN SLIDES -->
					<div class="carousel-inner" role="listbox">

						@foreach ($success_cases as $k => $success_case)
							@if ($k % 3 == 0)
							<div class="item @if ($k == 0) active @endif">
								<div class="container" >
									<div class="row">
							@endif
                                        <a href="{{ route('success_cases.show', [$success_case->id]) }}">
                                            <div class="col-md-4">
                                                <div class="row contenedor">
                                                    <img src="{{ AssetUrl::get('SuccessCase', 'image', $success_case->filename, 800, 500) }}" class="img-responsive">
                                                    <div class="filtro-img light"></div>
                                                    <blockquote class="slide">
                                                        <p>{{ $success_case->title }}</p>
                                                    </blockquote>
                                                </div>
                                                <div class="clr"></div>
                                                <div class="row">
                                                    <div class="latobold12 margTop20 text-uppercase morado">{{ $success_case->name }} | {{ $success_case->company_name }}</div>
                                                </div>
                                            </div>
                                        </a>
							@if ($k % 3 == 2)
									</div>
								</div>
							</div>
							@endif
						@endforeach

						@if ($num % 3 != 0)
									</div>
								</div>
							</div>
						@endif
					</div>
					<!-- END SLIDES -->

					<!-- BEGIN BOTONERA SLIDER -->
					<ol class="carousel-indicators">
						@for ($i = 0; $i < $pages; $i ++)
							<li data-target="#sliderusers" data-slide-to="{{ $i }}" class="@if ($i == 0) active @endif"></li>
						@endfor
					</ol>
					<!-- END BOTONERA SLIDER -->

				</div>
			</div>
		</div>
	</div>
	<!-- END SLIDER USUARIOS -->
@endsection

@extends('frontend.layouts.default')

<?php
//var_dump($success_cases);
?>

{{--<a href="{{ route('auth.login') }}">{{ _('Login') }}</a>
<a href="{{ route('auth.register') }}">{{ _('Register') }}</a>--}}

