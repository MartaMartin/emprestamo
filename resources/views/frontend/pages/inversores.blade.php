@section('pagina','inversores')								{{-- id de la pagina para el body --}}
@section('title','Inversores')           			 		{{-- titulo de la pagina --}}
@section('inversores_act','active')                    		{{-- activacion menu top --}}
@section('content')                               			{{-- contendido pagina --}}
<!-- BEGIN CABECERA SLOGAN -->
<div class="section" id="slogan">
	<div class="container">
		<div class="row">
			<div class="col-md-5 c col-sm-12 col-xs-12 izq">
				<div class="morado text-uppercase">Inversores</div>
				<div class="negro">Invierte en proyectos</div>
			</div>
			<div class="col-md-5 col-md-push-1 col-sm-12 col-xs-12 drxa">
                Ahora puedes prestar capital a compañías solventes de la mano de EMPRESTAMO, de forma segura y sin riesgos, pues todas las operaciones propuestas en la plataforma están cubiertas por una aseguradora de primer nivel.
			</div>
		</div>
	</div>
</div>
<!-- END CABECERA SLOGAN -->

<!-- BEGIN INFO DATOS -->
<div class="section bg-morado animatedParent blanco text-center" id="icodatos">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<img src="/assets/img/extra/ico-papel-white.png">
				<div class="latoligth21 margTop5">Préstamos a corto plazo. Operaciones a 30- 60- 90 días.</div>
			</div>
			<div class="col-md-3">
				<img src="/assets/img/extra/ico-rueda-white.png">
				<div class="latoligth21 margTop5">Altas rentabilidades superiores al 8% anual.</div>
			</div>
			<div class="col-md-3">
				<img src="/assets/img/extra/ico-stats-white.png">
				<div class="latoligth21 margTop5">Riesgo limitado: Operaciones aseguradas</div>
			</div>
			<div class="col-md-3">
				<img src="/assets/img/extra/ico-cartera-white.png">
				<div class="latoligth21 margTop5">Máximo control, flexibilidad y transparencia desde tu Wallet</div>
			</div>
		</div>
	</div>
</div>
<!-- END INFO DATOS -->

<!-- BEGIN BOXES -->
<div class="section bg-blanco margTop30" id="boxes">
	<div class="container ">
		<div class="row animatedParent animateOnce">
			<div class="col-md-7 text-center animated bounceInLeft">
				<div class="row">
					<div class="col-md-10 bg-grisb">
						<img src="/assets/img/extra/portatil.png" class="pc img-responsive">
					</div>
				</div>
			</div>
			<div class="col-md-5 animated growIn">
				<div class="regular34">Tu wallet</div>
                <div class="latoligth21 margTop20">
                    <p>Obtén retorno con intereses a tus inversiones en sólo 30 días, 60 días o 90 días.</p>
                    <p>Controla todos tus movimientos de dinero así como los retornos esperados desde tu Wallet.</p>
                    <p>Establece tus criterios de inversión y automatiza tus preferencias.</p>
                </div>
			</div>
		</div>
		<div class="clr margTop60"></div>
		<div class="row animatedParent animateOnce">
			<div class="col-md-5 animated bounceInLeft">
				<div class="regular34">Las operaciones</div>
				<div class="latoligth21 margTop20">Los inversores adelantan el dinero de una factura presentada por una pyme. EMPRESTAMO se encarga de cobrar la factura junto con los intereses pactados y de hacer llegar el dinero a los inversores en los plazos pactados. ¿Y si la empresa no paga la factura? Ejecutamos el seguro y se devuelve el capital prestado a los inversores. ¡ESTO ES EMPRESTAMO!</div>
			</div>
			<div class="col-md-7 animated growIn">
				<img src="/assets/img/extra/boxes.jpg" class="img-responsive">
			</div>
		</div>
		<div class="clr margTop60"></div>
		<div class="row animatedParent animateOnce">
			<div class="col-md-7 text-center animated bounceInLeft">
				<div class="row">
					<div class="col-md-10 bg-grisb">
						<img src="/assets/img/extra/portatil.png" class="pc img-responsive">
					</div>
				</div>
			</div>
			<div class="col-md-5 animated growIn">
				<div class="regular34">Altas rentabilidades</div>
				<div class="latoligth21 margTop20">Apoya la creación de empleo y favorece la riqueza de nuestro entorno prestando capital privado a pymes solventes que necesitan adelantar el cobro de una factura mientras realizan el servicio contratado.</div>
				<div class="latoligth21 margTop20">Sencillo, con retorno de lo invertido en días, buenas rentabilidades y sin riesgos. ¿Qué más se puede pedir? ¡EMPRESTAMO es el lugar donde las finanzas particulares y la rentabilidad a corto plazo sin riesgo se encuentran!</div>
			</div>
		</div>
	</div>
</div>
<!-- END BOXES -->

<!-- BEGIN CONOCE EXPERIENCIA -->
<div id="casos" class="section blanco margTop30 animatedParent animateOnce" style="background: url(/assets/img/fondos/casos.jpg) no-repeat fixed center bottom / cover;">
	<div class="filtro-img"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-push-2 text-center">

				<div class="row">
					<div class="col-md-12">
						<p class="text-inverse text-uppercase latobold12 animated bounceInRight">casos de éxito</p>
						<h1 class="text-inverse text-uppercase black margTop30 animated bounceInLeft">conoce la expriencia</h1>
					</div>
				</div>
				<div class="row ">
					<div class="col-md-12">
						<p class="text-inverse latoligth21 margTop20 animated bounceInRight hidden-sm hidden-xs">Ellos forman parte de Emprestamos y quieren compartir su experiencia</p>
						<a class="btn btn-lg btn-transparente margTop20 text-center animated growIn text-uppercase" href="{{ url('/casos-de-exito-inversores') }}">Ver todos los casos</a>
					</div>
				</div>


			</div>
		</div>
	</div>
</div>
<!-- END CONOCE EXPERIENCIA -->

<!-- BEGIN CUADRADO SUGERENCIA -->
<div class="section bg-gris-claro animatedParent animateOnce text-center" id="suger">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-md-push-4 col-sm-12 conshadow bg-blanco animated growIn cuadrado">
				<div class="row">
					<div class="col-md-12">
						<h1 class="light hidden-sm hidden-xs">¿Quieres probar wallet?</h1>
						<h1 class="light hidden-md hidden-lg">Únete a Emprestamo</h1>
						<p class="paddBot20 hidden-sm hidden-xs">Obtén altas rentabilidades a corto plazo, superiores al 8% anual</p>
					</div>
				</div>
				<div class="clr"></div>
				<div class="row">
					<div class="clr"></div>
					<div class="col-md-6 col-md-push-3">
						<a class="btn noradio bg-morado text-uppercase blanco margTop20" href="{{ url('/registro-inversor') }}">regístrte ahora</a>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
<!-- END CUADRADO SUGERENCIA -->
@endsection

@extends('frontend.layouts.default')
