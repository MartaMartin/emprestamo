


{{-- REGISTRO USUARIO --}}
<section class="myc-intranet-panel clearfix">
	<h3 class="panel-title">Nuevo usuario</h3>
	<hr class="myc-hr-separator">
	<div class="row">
		<div class="col-sm-4 panel-picto-wrapper">
			<img src="{{asset('img/intranet-picto-registro.svg')}}">
			<p class="help-block">Texto con las ventajas de registrarse en MyChip... Lorem ipsum dolor sit amet pallentesque aliquam.</p>
		</div>
		<div class="col-sm-8">

			{{-- <form method="POST" action="{{ url('auth/register') }}" class="myc-form"> --}}

			{!! Form::open([
			  'url' => 'auth/register',
			  'class' => 'myc-form'
			]) !!}
			<div class="form-group">
				<label for="name">Nombre</label>
				<input type="text" class="form-control" name="name" value="{{ old('name') }}">
			</div>

			<div class="form-group">
				<label for="email">E-mail</label>
				<input type="email" class="form-control" name="email" value="{{ old('email') }}">
			</div>

			<div class="form-group">
				<label for="password">Contraseña</label>
				<input type="password" class="form-control" name="password">
			</div>

			<div class="form-group">
				<label for="password_confirmation">Repite la contraseña</label>
				<input type="password" class="form-control" name="password_confirmation">
			</div>

			<div class="form-group">
				<button class="btn myc-btn-intranet pull-right" type="submit">Registrarse</button>
			</div>

			{!! Form::close() !!}

		</div> {{-- /.col-sm-8 --}}
	</div> {{-- /.row --}}
</section>