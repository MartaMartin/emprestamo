{{-- BEGIN CSS CORE --}}
<link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="/assets/css/animaciones.css" rel="stylesheet" type="text/css">
<!--[if lte IE 9]>
<link href='/assets/css/animaciones-ie-fix.css' rel='stylesheet' type="text/css">
<![endif]-->
<link href="/assets/css/style.css" rel="stylesheet" type="text/css">
<link href="/assets/css/select2.min.css" rel="stylesheet" type="text/css">
<link href="/assets/css/uniform.default.css" rel="stylesheet" type="text/css">
<link href="/assets/plugins/toastr/toastr.min.css" rel="stylesheet" type="text/css">
<link href="/assets/plugins/datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css">

{{-- BEGIN CSS PAGINA --}}
@yield('css_pagina')
{{-- END CSS PAGINA --}}

{{-- BEGIN CSS CORE --}}
<link href="/assets/css/app.css" rel="stylesheet" type="text/css">
{{-- END CSS CORE --}}