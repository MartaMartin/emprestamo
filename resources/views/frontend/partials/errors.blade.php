@if(count($errors) > 0)
	@foreach ($errors->all() as $error)
		<input type="hidden" class="error" value="{{ $error }}" />
	@endforeach
	<script type="text/javascript">
		$(document).ready(function(){
			var errors = $('input[class="error"]');

			$.each(errors, function(e){
				var outmsg = $(this).val();
				App.ui_msg("error",outmsg);
			});
		});
	</script>
@endif


