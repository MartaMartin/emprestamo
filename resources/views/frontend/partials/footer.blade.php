<footer class="section animatedParent">
	<div class="container">
		<div class="row animated fadeIn"  id="menufooter">
			<div class="col-sm-3 sinleft">
				<h1 class="latobold12">Funcionamiento básico</h1>
				<ul class="sinleft">
					<li><a href="{{ url('/como-funciona') }}">Cómo funciona</a></li>
					<li><a href="{{ url('/empresas') }}">Cómo funciona - empresas</a></li>
					<li><a href="{{ url('/inversores') }}">Cómo funciona - inversores</a></li>
				</ul>
			</div>
			<div class="col-sm-3">
				<h1 class="latobold12">Condiciones generales</h1>
				<ul class="sinleft">
					<li><a href="#">Preguntas Frecuentes (FAQ)</a></li>
					<li><a href="#">Servicios y tarifas emprendedor</a></li>
					<li><a href="#">Servicios y tarifas inversor</a></li>
				</ul>
			</div>
			<div class="col-sm-3">
				<h1 class="latobold12">Información de interés</h1>
				<ul class="sinleft">
					<li><a href="#">Información básica para el cliente</a></li>
					<li><a href="#">Reglamento defensa cliente</a></li>
					<li><a href="#">Reglamento interno de conducta</a></li>
					<li><a href="#">Política de privacidad</a></li>
					<li><a href="#">Política de cookies</a></li>
					<li><a href="#">Sugerencias, quejas y reclamaciones</a></li>
					<li><a href="{{ route('success_cases.index') }}">Historias de éxito</a></li>
				</ul>
			</div>
			<div class="col-sm-3 sinright">
				<h1 class="latobold12">Sobre Nosotros</h1>
				<ul class="sinleft">
					<li><a href="#">Quiénes somos</a></li>
					<li><a href="#">Premios y reconocimientos</a></li>
					<li><a href="#">Prensa</a></li>
					<li><a href="#">Contacto</a></li>
				</ul>
			</div>
		</div>
		<div class="row borders animated fadeIn" id="submenufooter">
			<ul class="sinleft">
				<li><a href="#">Enlace</a></li>
				<li><a href="#">Enlace</a></li>
			</ul>
		</div>
		<div class="row latoligth13 opacity50 blanco margTop20 hidden-sm hidden-xs">
			<div class="col-md-6 pull-left sinleft">&copy; Emprestamo <?php echo date('Y');?></div>
			<div class="col-md-6 text-right sinright">Design with <i class="fa fa-heart"></i> by <a class="latoligth13 blanco" href="http://artvisual.es" target="_blank">Artvisual</a></div>
		</div>
	</div>
</footer>
