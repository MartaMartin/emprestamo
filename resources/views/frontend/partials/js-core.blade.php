{{-- BEGIN CORE JAVASCRIPTS --}}
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script type="text/javascript" src="//netdna.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/assets/plugins/animaciones/css3-animate-it.js"></script>
<script type="text/javascript" src="/assets/plugins/uniform/jquery.uniform.min.js"></script>
<script type="text/javascript" src="/assets/plugins/toastr/toastr.min.js"></script>
<script type="text/javascript" src="/assets/plugins/select2/select2.min.js"></script>
<!-- BEGIN VALIDACIONES -->
<script type="text/javascript" src="/assets/plugins/validator/jquery.validate.min.js"></script>
<script type="text/javascript" src="/assets/plugins/validator/localization/messages_es.js"></script>
<script type="text/javascript" src="/assets/plugins/validator/additional-methods.js"></script>
<script type="text/javascript" src="/assets/scripts/validaciones.js"></script>
<!-- END VALIDACIONES -->
<script type="text/javascript" src="/assets/plugins/moment/moment.min.js"></script>
<script type="text/javascript" src="/assets/plugins/moment/locales.js"></script>
<script type="text/javascript" src="/assets/plugins/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>

<script type="text/javascript" src="/assets/scripts/app.js"></script>
{{-- END CORE JAVASCRIPTS --}}

{{-- BEGIN PAGE JAVASCRIPTS PLUGINS --}}
@yield('scripts_pagina')
{{-- END PAGE JAVASCRIPTS PLUGINS --}}

{{-- BEGIN CORE JAVASCRIPTS --}}
<script type="text/javascript">
	$(function() {
		App.init();
		{{-- BEGIN PAGE SCRIPTS --}}
		@yield('js_pagina')
		{{-- END PAGE SCRIPTS --}}
	});
</script>
{{-- END CORE JAVASCRIPTS --}}

{{--BEGIN UI MESSAGES--}}
@include('frontend.partials.message')
@include('frontend.partials.errors')
{{--END UI MESSAGES--}}