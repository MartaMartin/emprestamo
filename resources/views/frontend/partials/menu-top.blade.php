{{-- BEGIN MENU TOP --}}
<div id="menutop" class="navbar-top">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-target="#navbar-mobile">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<div id="logo"><a href="/"><img src="/assets/img/logo.png" /></a></div>
		</div>
		<!-- BEGIN MENU PC -->
		<div class="collapse navbar-collapse hidden-sm hidden-xs" id="navbar-pc">
			<ul class="nav navbar-nav navbar-right text-uppercase latoligth12">
				<li class="@yield('index_act')">
					<a href="/">Home</a>
				</li>
				<li class="@yield('empresa_act')">
					<a href="{{ url('/empresas')  }}">Empresas</a>
				</li>
				<li class="@yield('inversores_act')">
					<a href="{{ url('/inversores')  }}">Inversores</a>
				</li>
				<li class="@yield('como_act')">
					<a href="{{ url('/como-funciona')  }}">Cómo funciona</a>
				</li>
				<li class="@yield('blog_act')">
					<a href="/blog">Blog</a>
				</li>
				@if (Auth::check())
					<li class="@yield('login_act')">
						@if (Auth::user()->role == 'admin' || Auth::user()->role == 'analyst')
							<a href="{{ route('admin.index') }}" class="text-lowercase naranja text-capitalize">Panel de administración</a>
						@else
							<a href="{{ route('private.users.index') }}" class="text-lowercase naranja text-capitalize">Área privada</a>
						@endif
					</li>
					<li class="@yield('login_act')">
						<a href="{{ route('auth.logout') }}" class="text-lowercase naranja text-capitalize">Desconectarse</a>
					</li>
				@else
					<li class="@yield('login_act')">
						<a href="{{ route('auth.login') }}" class="text-lowercase naranja text-capitalize">Login</a>
					</li>
					<li class="@yield('reg_act')">
						<a href="javascript:;" data-target="#modal-reg" data-toggle="modal" class="text-lowercase naranja text-capitalize">Registro</a>
					</li>
				@endif
			</ul>
		</div>
		<!-- END MENU PC -->

		<!-- BEGIN MENU MOVIL -->
		<div class="fade oculto collapse navbar-collapse bg-blanco hidden-md hidden-lg" id="navbar-mobile">
			<div class="container bg-morado-light full-height full-width">
				<div class="close pull-left latobold16" data-target="#navbar-mobile">x</div>
				<ul class="nav navbar-nav navbar-right text-uppercase latoligth30">
					<li class="@yield('index_act')">
						<a href="/">Home</a>
					</li>
					<li class="@yield('empresa_act')">
						<a href="{{ url('/empresas')  }}">Empresas</a>
					</li>
					<li class="@yield('inversores_act')">
						<a href="{{ url('/inversores')  }}">Inversores</a>
					</li>
					<li class="@yield('como_act')">
						<a href="{{ url('/como-funciona')  }}">cómo funciona</a>
					</li>
					<li class="@yield('blog_act')">
						<a href="/blog">blog</a>
					</li>
					<li class="">
						&nbsp;
					</li>
					@if (Auth::check())
						<li class="@yield('login_act')">
							<a href="{{ route('private.users.index') }}" class="text-lowercase naranja text-capitalize">Área privada</a>
						</li>
						<li class="@yield('login_act')">
							<a href="{{ route('auth.logout') }}" class="text-lowercase naranja text-capitalize">Desconectarse</a>
						</li>
					@else
						<li class="@yield('login_act')">
							<a href="{{ route('auth.login') }}" class="text-lowercase naranja text-capitalize">Login</a>
						</li>
						<li class="">
							<a href="#" data-target="#modal-reg" data-toggle="modal" class="text-lowercase text-capitalize">Registro</a>
						</li>
					@endif
				</ul>
			</div>
		</div>
		<!-- END MENU MOVIL -->
	</div>
</div>
{{-- END MENU TOP --}}
