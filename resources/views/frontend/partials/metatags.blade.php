{{-- BEGIN META TAGS --}}
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<meta name="apple-mobile-web-app-title" content="Título (entre 8 y 12 caracteres)">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="mobile-web-app-capable" content="yes">
<meta name="format-detection" content="telephone=no (=yes Si queremos habilitar llamadas desde links tipo tel:XXX XXX XXX en dispositivos móviles)">

<!-- Mobile Internet Explorer ClearType Technology -->
<!--[if IEMobile]>
<meta http-equiv="cleartype" content="on">
<![endif]-->

<meta name="keywords" content="Palabras clave (máximo 20 simple y/o longtail)">
<meta name="description" content="Descripción (máximo 200 caracteres)">
<meta name="author" content="Artvisual Comunicación Digital">
<meta name="twitter:card" content="Sumario Twitter Card (máximo 140 caracteres)" />
<meta name="twitter:site" content="@cuenta_cliente" />
<meta name="twitter:creator" content="@artvisual" />
<meta property="twitter:image" content="URL_imagen_twitter (Mínimo 120x120 y máximo peso 1Mb/Existe la posibilidad de emplear sólo og:image, respetando siempre las características expuestas)" />
<meta property="og:url" content="URL_contenido" />
<meta property="og:title" content="Título (máximo 40 caracteres)" />
<meta property="og:description" content="Descripción OG (máximo 300 caracteres en Post y 110 caracteres en Comment)" />
<meta property="og:image" content="URL_imagen_OG (Facebook mínimo 200x200)" />
{{-- END META TAGS--}}

{{-- BEGIN FAVICON --}}
<link rel="icon" href="/favicon/favicon.ico">
<link rel="apple-touch-icon" sizes="57x57" href="/favicon/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/favicon/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/favicon/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/favicon/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/favicon/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/favicon/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/favicon/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon-180x180.png">
<link rel="icon" type="image/png" href="/favicon/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="/favicon/android-chrome-192x192.png" sizes="192x192">
<link rel="icon" type="image/png" href="/favicon/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="/favicon/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="/favicon/manifest.json">
<link rel="mask-icon" href="/favicon/safari-pinned-tab.svg" color="#5bbad5">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="msapplication-TileImage" content="/favicon/mstile-144x144.png">
<meta name="theme-color" content="#ffffff">
{{-- END FAVICON --}}
