{{-- BEGIN MODALES --}}
<div id="modales" class=animatedParent>
	<!-- BEGIN MODAL REG -->
	<div class="modal fade animated growIn" tabindex="-1" role="dialog" id="modal-reg">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="latobold24 blanco text-uppercase text-center">registrate como</h4>
				</div>
				<div class="modal-body text-center">
					<div class="row">
						<div class="col-md-6 col-sm-12 col-xs-12">
							<a href="{{ url('/registro-empresa') }}" class="btn fullwidth noradio bg-amarillo text-uppercase blanco margTop20">empresa</a>
						</div>
						<div class="col-md-6 col-sm-12 col-xs-12">
							<a href="{{ url('/registro-inversor') }}" class="btn fullwidth noradio bg-morado text-uppercase blanco margTop20">inversor</a>
						</div>
						<div class="clr"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END MODAL REG -->
</div>
<!-- END MODALES -->