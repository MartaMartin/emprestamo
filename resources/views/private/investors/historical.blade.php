@extends('private.layouts.default')
@section('private_historical', 'active')
@section('content')

	{{-- form start --}}
	{!! Form::open([
		'route' => 'private.investors.historical',
		'role' => 'form'
	])
	!!}

	<table class="table table-responsive table-condensed">
		<thead>
			<tr>
                <th></th>
				<th>{{ _('Operación') }}</th>
				<th>{{ _('Estado') }}</th>
				<th>{{ _('Invertido') }}</th>
				<th>{{ _('Fecha de operación') }}</th>
				<th>{{ _('Tipo de interés') }}</th>
				<th>{{ _('Plazo') }}</th>
				<th>{{ _('Fecha devolución') }}</th>
				<th>{{ _('Retorno') }}</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($operations as $operation)
			<tr>
                <td>{!! Form::checkbox('ids[]', $operation->id) !!}</td>
                <td><a href="{{ route('private.invoices.view', [$operation->invoice->id]) }}">{{ $operation->invoice->name }}</a></td>
				<td>{{ $status[$operation->status] }}</td>
				<td style="text-align: center;">{{ number_format($operation->invested, 2, ',', '.') }}€</td>
				<td>{{ Functions::shortDate($operation->created_at) }}</td>
				<td style="text-align: center;">{{ $operation->fee }}%</td>
                <td>{{ round((strtotime($operation->invoice->expires) - strtotime($operation->accepted_date ? $operation->accepted_date : $operation->created_at)) / 3600 / 24) }} días</td>
				<td>{{ $operation->closed_date ? Functions::shortDate($operation->closed_date) : '--' }}</td>
                <td style="text-align: center;">{{ number_format($operation->earned + $operation->invested, 2, ',', '.') }}€</td>
			</tr>
			@endforeach
		</tbody>
		<tfoot>
			<tr>
                <td>Total&nbsp;({{ $operations->count() }})</td>
                <td></td>
				<td style="text-align: center;">{{ number_format($totals['invested'], 2, ',', '.') }}€</td>
				<td></td>
				<td></td>
				<td style="text-align: center;">{{ number_format($totals['fee'], 2, ',', '.') }} %</td>
                <td></td>
				<td></td>
				<td style="text-align: center;">{{ number_format($totals['earned'], 2, ',', '.') }} €</td>
			</tr>
			<tr>
				<td colspan="9" style="text-align: center;">Beneficio total: {{ number_format($totals['earned'] - $totals['invested'], 2, ',', '.') }} €</td>
			</tr>
		</tfoot>
	</table>

    {!! Form::select('status', [null => _('ESTADO: Cualquiera')] + $status, $status_selected, ['class' => 'select2 form_control col-md-4 pull-left']) !!}

    <button class="btn col-md-4 blanco sinradio bg-morado alargado text-uppercase latoblack12 pull-right" type="submit">{{ _('Consultar') }}</button>

    <div class="col-md-6" style="text-align: center; margin-top: 20px;">
        <h4>{{ _('Total invertido') }}: {{ number_format($totals['invested'], 2, ',', '.') }}€</h4>
        <div id="invested_donut"></div>
    </div>
    <div class="col-md-6" style="text-align: center; margin-top: 20px;">
        <h4>{{ _('Interés medio') }}: {{ round($totals['fee']) }}%</h4>
        <div id="fee_donut"></div>
    </div>
    <div class="col-md-6" style="text-align: center; margin-top: 20px;">
        <h4>{{ _('Total devuelto') }}: {{ number_format($totals['returned'], 2, ',', '.') }}€</h4>
        <div id="returned_donut"></div>
    </div>
    <div class="col-md-6" style="text-align: center; margin-top: 20px;">
        <h4>{{ _('Total beneficio') }}: {{ number_format($totals['only_earned'], 2, ',', '.') }}€</h4>
        <div id="earned_donut"></div>
    </div>

	{!! Form::close() !!}{{-- /.form --}}

@endsection

@section('specific-js')
<script>
	var invested_data = [
	@if ($totals['invested'])
		@foreach ($donutgraphs['invested'] as $row)
		    ['{{ $row[0] }}', {{ number_format($row[1] * 100 / $totals['invested'], 2, '.', '') }}, '{{ $row[2] }}'],
		@endforeach
	@endif
	];

	var fee_data = [
	@if (count($donutgraphs['fee']))
		@foreach ($donutgraphs['fee'] as $row)
		    ['{{ $row[0] }}', {{ number_format($row[1] * 100 / ($totals['fee'] * count($donutgraphs['fee'])), 2, '.', '') }}, '{{ $row[2] }}'],
		@endforeach
		];
	@endif

	var returned_data = [
	@if ($totals['returned'])
		@foreach ($donutgraphs['returned'] as $row)
		    ['{{ $row[0] }}', {{ number_format($row[1] * 100 / $totals['returned'], 2, '.', '') }}, '{{ $row[2] }}'],
		@endforeach
	@endif
	];

	var earned_data = [
	@if ($totals['only_earned'])
		@foreach ($donutgraphs['earned'] as $row)
		    ['{{ $row[0] }}', {{ number_format($row[1] * 100 / $totals['only_earned'], 2, '.', '') }}, '{{ $row[2] }}'],
		@endforeach
	@endif
	];

    Private.donuts();
</script>
@endsection
