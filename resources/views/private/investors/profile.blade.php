@extends('private.layouts.default')
@section('private_profile', 'active')
@section('js_pagina','validateInvestorProfile();')
@section('content')

{{-- form start --}}
{!! Form::model($user,
		[
		'id' => "frmInvProf",
		'files' => true,
		'method' => 'PUT',
		'route' => [
			'private.investors.update'
		],
		'role' => 'form'
	])
!!}

	<div class="row">
		<!-- BEGIN PORTLET-->
		<div class="portlet box">
			<div class="portlet-title">
				<div class="tools">
					<a class="panel-full-screen text-muted" href="javascript:;" data-toggle="tooltip" title="Pantalla Completa"><i class="fa fa-expand text-active"></i></a>
					<a class="panel-toggle text-muted" data-toggle="tooltip" href="javascript:;" title="Esconder/Mostrar"><i class="fa fa-chevron-down text-active"></i></a>
				</div>
				Mis datos
			</div>
			<div class="portlet-body">
				<div class="row">
					<div class="col-md-6 margTop10">
						{!! Form::label('name', _('Nombre')) !!}
						{!! Form::text('name', $user->name,
							[
								'class' => 'form-control',
								'placeholder' => _('Nombre usuario'),
								'required'
							])
						!!}
					</div>
					<div class="col-md-6 margTop10">
						{!! Form::label('family_name', _('Apellidos')) !!}
						{!! Form::text('family_name', $user->family_name,
							[
								'class' => 'form-control',
								'placeholder' => _('Apellidos usuario'),
								'required'
							])
						!!}
					</div>
					<div class="clr"></div>
					<div class="col-md-6 margTop10">
						{!! Form::label('email', _('Email')) !!}
						{!! Form::email('email', $user->email,
							[
								'class' => 'form-control',
								'placeholder' => _('Email usuario'),
								'required'
							])
						!!}
					</div>
					<div class="col-md-6 margTop10">
						{!! Form::label('password', _('Contraseña')) !!}
						{!! Form::password('password',
							[
								'class' => 'form-control remove_autocomplete',
								'placeholder' => _('Contraseña usuario'),
								'readonly' => true
							])
						!!}
					</div>
					<div class="clr"></div>
					<div class="col-md-12 clr">
						<hr>
					</div>
					<div class="col-md-6 margTop10">
						{!! Form::label('real_id_type', _('Tipo identificación')) !!}
						{!! Form::select('real_id_type', [
							'dni' => 'DNI',
							'cif' => 'CIF',
							'nie' => 'NIE'
						],
						$user->real_id_type, ['class' => 'form-control select2']) !!}
					</div>
					<div class="col-md-6 margTop10">
						{!! Form::label('real_id_number', _('Número identificación')) !!}
						{!! Form::text('real_id_number', $user->real_id_number,
							[
								'class' => 'form-control',
								'placeholder' => _('Número identificación'),
								'required'
							])
						!!}
					</div>
					<div class="clr"></div>
					<div class="col-md-6 margTop10">
						{!! Form::label('investor_type', _('Tipo de inversor')) !!}
						{!! Form::select('investor_type', [
							0 => _('No acreditado'),
							1 => _('Acreditado'),
						],
						$investor->type, ['class' => 'form-control select2']) !!}
					</div>
                    @if (!$investor->user->dni_verified)
                        <div class="col-md-6 margTop10">
                            {!! Form::label('investor_type', _('DNI')) !!}

                            @if ($document)
                                <a target="_blank" href="{{ AssetUrl::get('Document', $document->type, $document->filename, 0, 0) }}"><i class="fa fa-file"></i> Ver archivo</a>
                            @endif
                            <div class="sinleft">
                                <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                    <div class="form-control" data-trigger="fileinput">
                                        <span class="fileinput-filename"></span>
                                    </div>
                                    <span class="input-group-addon btn btn-transparente sinradio morado fino text-uppercase latoblack12 btn-file">
                                        <span class="fileinput-new text-uppercase"> Subir archivo</span>
                                        <span class="fileinput-exists margRig10">Cambiar</span>
                                        {!! Form::file('filename') !!}
                                    </span>
                                    <a href="#" class="input-group-addon btn btn-transparente sinradio morado fino fileinput-exists latoblack12" data-dismiss="fileinput">Eliminar</a>
                                </div>
                            </div>
                        </div>
                    @endif
					<div class="clr"></div>
                    @if ($user->investor->type == 1)
                        <div class="col-md-12">
                            <hr>
                        </div>
                        <div class="col-md-12">
                            <h4>Inversiones automáticas</h4>
                            <p>Si no eres un inversor acreditado, no podrás realizar inversiones automáticas</p>
                        </div>
                        <div class="col-md-6 margTop10">
                            {!! Form::label('investor_auto', _('Inversión automática')) !!}
                            {!! Form::select('investor_auto', [
                                0 => _('No'),
                                1 => _('Sí'),
                            ],
                            $investor->auto, ['class' => 'form-control select2']) !!}
                        </div>
                        <div class="col-md-6 margTop10">
                            {!! Form::label('investor_max_invest', _('Importe máximo de inversión')) !!}
                            <div class="input-group">
                                {!! Form::number('investor_max_invest', $investor->max_invest,
                                    [
                                        'class' => 'form-control',
                                        'placeholder' => _('Importe')
                                    ])
                                !!}
                                <span class="input-group-addon">€</span>
                            </div>
                        </div>
                        <div class="clr"></div>
                        <div class="col-md-6 margTop10">
                            {!! Form::label('investor_calification', _('Calificación de riesgo')) !!} <i class="fa fa-info-circle text-active" title="Mejor calificación por riesgo = 10" data-toggle="tooltip" data-target="top"></i>
                            {!! Form::select('investor_calification', [0 => _('Cualquiera'), 1, 2, 3, 4, 5, 6, 7, 8, 9, 10], $investor->calification,
                                [
                                    'class' => 'form-control select2'
                                ])
                            !!}
                        </div>
                        {{--
                        <div class="col-md-6 margTop10">
                            {!! Form::label('investor_min_invest', _('Importe mínimo de inversión')) !!}
                            {!! Form::number('investor_min_invest', $investor->min_invest ? $investor->min_invest : 0,
                                [
                                    'class' => 'form-control',
                                    'placeholder' => _('Cualquiera')
                                ])
                            !!}
                        </div>
                        --}}
                        <div class="col-md-6 margTop10">
                            {!! Form::label('investor_fee', _('Interés mínimo anual')) !!}
                            <div class="input-group">
                                {!! Form::number('investor_fee', $investor->fee,
                                    [
                                        'class' => 'form-control',
                                        'placeholder' => _('Cualquiera')
                                    ])
                                !!}
                                <span class="input-group-addon">%</span>
                            </div>
                        </div>
                        <div class="col-md-6 margTop10">
                            {!! Form::label('investor_max_days', _('Máximo de días de espera para el retorno de la inversión')) !!}
                            {!! Form::number('investor_max_days', (int)$investor->max_days,
                                [
                                    'class' => 'form-control',
					'step' => '1',
                                    'placeholder' => _('Cualquiera')
                                ])
                            !!}
                        </div>
                        <div class="col-md-12">
                            <hr>
                        </div>
                    @endif
				</div>
			</div>
			<div class="portlet-footer margTop20">
				<button type="submit" class="btn blanco sinradio bg-morado alargado text-uppercase latoblack12 center-block"> Aceptar </button>
			</div>
		</div>
		<!-- END PORTLET-->
	</div>

{!! Form::close() !!}{{-- /.form --}}

@endsection
