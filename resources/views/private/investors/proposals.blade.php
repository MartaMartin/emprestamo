@extends('private.layouts.default')
@section('private_proposals', 'active')
@section('content')

<div class="row">

    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#index" aria-controls="index" role="tab" data-toggle="tab">{{ _('Propuestas pendientes') }}</a></li>
        <li role="presentation"><a href="#rejected" aria-controls="rejected" role="tab" data-toggle="tab">{{ _('Rechazadas') }}</a></li>
    </ul>

    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="index">
            @include('private.partials.proposals', ['operations' => $operations])
        </div>
        <div role="tabpanel" class="tab-pane" id="rejected">
            @include('private.partials.proposals', ['operations' => $operations_rejected])
        </div>
	</div>

	<ul>
	</ul>

@endsection
