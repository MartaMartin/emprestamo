@extends('private.layouts.default')
@section('private_wallet', 'active')
@section('js_pagina','validateInvWallet();')
@section('content')

	{{-- form start --}}
	{!! Form::model($user,
			[
			'id' => 'frmInvWallet',
			'method' => 'PUT',
			'route' => [
                'private.transfers.store',
                !empty($substract) ? true : false
			],
			'role' => 'form'
		])
	!!}

	<div class="row">
		<!-- BEGIN PORTLET-->
		<div class="portlet box">
			<div class="portlet-title">
				<div class="tools">
					<a class="panel-full-screen text-muted" href="javascript:;" data-toggle="tooltip" title="Pantalla Completa"><i class="fa fa-expand text-active"></i></a>
					<a class="panel-toggle text-muted" data-toggle="tooltip" href="javascript:;" title="Esconder/Mostrar"><i class="fa fa-chevron-down text-active"></i></a>
				</div>
				Mi wallet
			</div>
			<div class="portlet-body">
				<div class="row">
					<div class="col-md-6 margTop10">
                        @if (!empty($substract))
                            {!! Form::label('quantity', _('Cantidad a retirar')) !!}
                        @else
                            {!! Form::label('quantity', _('Cantidad a ingresar')) !!}
                        @endif
						<div class="input-group">
							{!! Form::text('quantity', null,
								[
									'class' => 'form-control',
									'placeholder' => 1000,
									'required'
								])
							!!}
							<span class="input-group-addon">€</span>
						</div>
					</div>
                    {{--
					<div class="col-md-6 margTop10">
						{!! Form::label('type', _('Inversión automática')) !!}
						{!! Form::select('type', $transfer_types,
						null, ['class' => 'form-control select2']) !!}
					</div>
					<div class="clr"></div>
					<div class="col-md-6 margTop10">
						{!! Form::label('operation_id', _('Operación (Sólo si es inversión o ganancia)')) !!}
						{!! Form::select('operation_id', ['' => _('Sin operación')] + $operations,
						null, ['class' => 'form-control select2']) !!}
					</div>
                    --}}
					<div class="col-md-12">
						<hr>
					</div>
				</div>
			</div>
            @if (!empty($substract))
                <div class="col-md-10 col-md-offset-1">
                    <p>Introduce la cantidad a retirar y se te ingresará en la cuenta.</p>
                </div>
            @else
                <div class="col-md-10 col-md-offset-1">
                    <p>Introduce la cantidad a ingresar y se generará una referencia para que la adjuntes cuando realices la transferencia bancaria, hasta que no se valide la transferencia, no podrás contar con esa cantidad en tu wallet.</p>
                    <p>Número de cuenta: </p>
                </div>
            @endif
			<div class="portlet-footer margTop20">
                @if (!empty($substract))
                    <button type="submit" class="btn blanco sinradio bg-morado alargado text-uppercase latoblack12 center-block"> Retirar dinero</button>
                @else
                    <button type="submit" class="btn blanco sinradio bg-morado alargado text-uppercase latoblack12 center-block"> Ingresar dinero</button>
                @endif
			</div>
		</div>
		<!-- END PORTLET-->
	</div>

	{!! Form::close() !!}{{-- /.form --}}

	@if (count($transfers))
		<div class="margTop20"></div>

		<!-- BEGIN TABLAS -->
		<div class="row">
			<table class="table table-responsive table-condensed">
				<thead>
					<th class="text-left" width="30%">Tipo de transferencia</th>
					<th class="text-left" width="15%">Operación</th>
					<th class="text-left" width="20%">Referencia</th>
					<th class="text-left" width="20%">Estado</th>
					<th class="text-right" width="20%">Cantidad</th>
					<th class="text-center" width="35%">Fecha</th>
				</thead>
				<tbody>
					@foreach ($transfers as $transfer)
                        <tr class="show_subtransfers" style="cursor: pointer;" var="row_{{ $transfer->id }}">
							<td class="morado text-left">
                                @if (count($transfer->subtransfers) > 1)
                                    <span class="selector">&#x25B8;</span>
                                @endif
								{{ $transfer_types[$transfer->type] }}
							</td>
							<td class="morado text-left">
                                @if ($transfer->operation)
                                    {{ $transfer->operation->invoice->name }}
                                @endif
							</td>
							<td class="morado text-left">
								{{ $transfer->ref }}
							</td>
							<td class="morado text-left">
								{{ $transfer->active ? _('Validada') : _('Pendiente') }}
							</td>
							<td class="text-right" style="color: {{ $transfer->type == 1 || $transfer->type == 2 || $transfer->type == 5 ? 'red' : 'green' }};">
                                {{ ($transfer->type == 1 || $transfer->type == 2 || $transfer->type == 5 ? '-' : '') . number_format($transfer->quantity, 2, ',', '.') }}€
							</td>
							<td class="text-center">
								{{ Functions::beautyTime($transfer->created_at) }}
								{{ Functions::shortDate($transfer->created_at) }}
							</td>
						</tr>
                        @if (count($transfer->subtransfers) > 1)
                            <tr><td colspan="10">
                                <table class="table table-responsive table-condensed hidden row_{{ $transfer->id }}">
                                    <th class="text-left" width="30%">Tipo de transferencia</th>
                                    <th class="text-left" width="15%">Operación</th>
                                    <th class="text-left" width="20%">Referencia</th>
                                    <th class="text-left" width="20%">Estado</th>
                                    <th class="text-right" width="20%">Cantidad</th>
                                    <th class="text-center" width="35%">Fecha</th>
                                @foreach ($transfer->subtransfers as $subtransfer)
                                <tr>
                                    <td class="morado text-left">
                                        {{ $transfer_types[$subtransfer->type] }}
                                    </td>
                                    <td class="morado text-left">
                                        @if ($subtransfer->operation)
                                            {{ $subtransfer->operation->invoice->name }}
                                        @endif
                                    </td>
                                    <td class="morado text-left">
                                        {{ $subtransfer->ref }}
                                    </td>
                                    <td class="morado text-left">
                                        {{ $subtransfer->active ? _('Validada') : _('Pendiente') }}
                                    </td>
                                    <td class="text-right" style="color: {{ $subtransfer->type == 1 || $subtransfer->type == 2 || $subtransfer->type == 5 ? 'red' : 'green' }};">
                                        {{ ($subtransfer->type == 1 || $subtransfer->type == 2 || $subtransfer->type == 5 ? '-' : '') . number_format($subtransfer->original_quantity, 2, ',', '.') }}€
                                    </td>
                                    <td class="text-center">
                                        {{ Functions::beautyTime($subtransfer->created_at) }}
                                        {{ Functions::shortDate($subtransfer->created_at) }}
                                    </td>
                                </tr>
                                @endforeach
                            </table>
                            </td></tr>
                        @endif
					@endforeach
				</tbody>
			</table>
		</div>
		<!-- END TABLAS -->

		<div class="row">
			<!-- BEGIN PORTLET-->
			<div class="portlet box">
				<div class="portlet-title">
					<div class="tools">
						<a class="panel-full-screen text-muted" href="javascript:;" data-toggle="tooltip" title="Pantalla Completa"><i class="fa fa-expand text-active"></i></a>
						<a class="panel-toggle text-muted" data-toggle="tooltip" href="javascript:;" title="Esconder/Mostrar"><i class="fa fa-chevron-down text-active"></i></a>
					</div>
					Gráfica Wallet
				</div>
				<div class="portlet-body">
					<!-- BEGIN TABLA GRAFICA -->
					<div class="row margTop20">
						<div id="chartdiv"></div>
					</div>
					<!-- END TABLA GRAFICA -->
				</div>
			</div>
			<!-- END PORTLET-->
		</div>
	@endif

@endsection

@section ('specific-js')

<script type="text/javascript">

	var datosGrafica = [];
	var amount = 0;
    var i = 0;

	@foreach ($graphs as $graph)
		@if ($graph['type'] == 0 || $graph['type'] == 3 || $graph['type'] == 4)
			amount += {{ str_replace(',', '.', $graph['quantity']) }};
		@else
			amount -= {{ str_replace(',', '.', $graph['quantity']) }};
		@endif

        var name = '{{ $transfer_types[$graph['type']] }}';
        @if ($graph['created_at'] > date('Y-m-d H:i:s'))
            var color = '<span style="color: #481B65;">+ {{ number_format($graph['quantity'], 2, ',', '.') }}€</span>';
            name += ' (pendiente)';
            var dash = 5;
            datosGrafica[i - 1]['dashLength'] = dash;
        @else
            var color = '{!! ($graph['type'] == 1 || $graph['type'] == 2 || $graph['type'] == 5) ? '<span style=\"color: red;\">-' . number_format($graph['quantity'], 2, ',', '.') . '€</span>' : '<span style=\"color: green;\">+' . number_format($graph['quantity'], 2, ',', '.') . '€</span>' !!}';
            var dash = 1;
        @endif
		datosGrafica.push({
			"date": "{{ date('d/m/Y H:i', strtotime($graph['created_at'])) }}",
			"cantidad": amount,
			"nomProyecto": name,
			"total": "{{ number_format($graph['quantity'], 2, ',', '.')}}",
            "trans": color,
            "dashLength": dash,
		});
        i ++;
	@endforeach

    $('.show_subtransfers').click(function() {
        $('.' + $(this).attr('var')).toggleClass('hidden');

        if ($(this).find('.selector').html() == '▾') {
            $(this).find('.selector').html('&#x25B8;');
        } else {
            $(this).find('.selector').html('&#x25BE;');
        }
    });
</script>

@endsection
