@extends('private.layouts.default')

@section('content')

	<div class="row">
		<!-- BEGIN PORTLET-->
		<div class="portlet box">
			<div class="portlet-title">
				<div class="tools">
					<a class="panel-full-screen text-muted" href="javascript:;" data-toggle="tooltip" title="Pantalla Completa"><i class="fa fa-expand text-active"></i></a>
					<a class="panel-toggle text-muted" data-toggle="tooltip" href="javascript:;" title="Esconder/Mostrar"><i class="fa fa-chevron-down text-active"></i></a>
				</div>
				{{ $invoice->name }}
				@if (count($invoice->operations))
					(<strong>Estado</strong>: {{ $operation_status[$invoice->operations[0]->status] }})
				@else
					(<strong>Estado</strong>: {{ $operation_status[0] }})
				@endif
			</div>
			<div class="portlet-body in">
				<p><strong>Precio</strong>: {{ number_format($invoice->price, 2, ',', '.') }} €</p>
				<p><strong>Recaudado</strong>: ({{ number_format($invoice->invested, 2, ',', '.') }} €)</p>
                <p><strong>Fecha de devolución</strong>: {{ Functions::beautyDate($invoice->expires) }}</p>
                <p><strong>Plazo: {{ round((strtotime($invoice->expires) - strtotime($invoice->operations[0]->accepted_date ? $invoice->operations[0]->accepted_date : $invoice->operations[0]->created_at)) / 3600 / 24) }} días</td>
				@if (count($invoice->operations))
					<p><strong>Interés</strong>: {{ $invoice->operations[0]->fee }}%</p>
					<p><strong>Calificación</strong>: {{ $invoice->operations[0]->calification + 1 }}</p>
					<p><strong>Empresa cedente</strong>: {{ $invoice->operations[0]->invoice->transferorCompany->name }}</p>
                    @if (Auth::user()->role == 'transferor_company')
                        <p><strong>Empresa deudora</strong>: {{ $invoice->operations[0]->invoice->debtorCompany->name }}</p>
                    @endif
				@endif
                @if (Auth::user()->role == 'transferor_company')
                    <p><strong>Documentos</strong>:</p>
                    @foreach ($invoice->documents as $document)
                        @if ($document->type == 'image')
                            - <a href="{{ AssetUrl::get('Document', $document->type, $document->filename, 0, 0) }}" target="_blank">{{ $document->name }}</a>
                        @else
                            - <a href="{{ AssetUrl::get('Document', $document->type, $document->filename) }}" target="_blank">{{ $document->name }}</a>
                        @endif
                    @endforeach
                    <p><strong>Descripción</strong>:</p>
                    <p>{{ $invoice->description }}</p>
                @endif
			</div>
		</div>
	</div>

	@if (count($invoice->operations))
		<div class="margTop20"></div>

		<div class="row">
			<!-- BEGIN PORTLET-->
			<div class="portlet box">
				<div class="portlet-title">
					<div class="tools">
						<a class="panel-full-screen text-muted" href="javascript:;" data-toggle="tooltip" title="Pantalla Completa"><i class="fa fa-expand text-active"></i></a>
						<a class="panel-toggle text-muted" data-toggle="tooltip" href="javascript:;" title="Esconder/Mostrar"><i class="fa fa-chevron-down text-active"></i></a>
					</div>
					Operaciones
				</div>
				<div class="portlet-body in">
					@if ($invoice->operations[0]->status == 1)
						<a class="btn blanco sinradio bg-morado alargado text-uppercase margTop5 latoblack12 center-block" href="{{ route('private.operations.accept-proposal', [$invoice->operations[0]->id]) }}">{{ _('Aceptar la oferta y las condiciones del contrato') }}</a>
						<a class="btn blanco sinradio bg-gris text-uppercase margTop5 latoblack12 center-block" href="{{ route('private.operations.reject-proposal', [$invoice->operations[0]->id]) }}">{{ _('Rechazar la oferta') }}</a>
					@elseif (in_array($invoice->operations[0]->status, [1, 3, 5, 6]))
						<table class="table table-responsive table-condensed">
							<thead>
								<tr>
									<th>{{ _('Inversor') }}</th>
									<th>{{ _('Tipo') }}</th>
									<th>{{ _('Cantidad') }}</th>
									<th>{{ _('Fecha') }}</th>
								</tr>
							</thead>
							<tbody>
								@if (count($invoice->operations) > 0)
                                    @foreach ($invoice->operations[0]->transfersGrouped as $type => $investors)
                                        @foreach ($investors as $investor)
                                            @if (Auth::user()->role == 'transferor_company' || $investor['investor'] && Auth::user()->id == $investor['investor']->user->id)
                                                <tr>
                                                    <td>
                                                        @if (!empty($investor['investor']))
                                                            {{ $investor['investor']->user->full_name }}
                                                        @else
                                                            {{ _('Emprestamo') }}
                                                        @endif
                                                    </td>
                                                    <td>
                                                        {{ $transfer_types[$type] }}
                                                    </td>
                                                    <td class="text-right" style="color: {{ $type == 1 || $type == 2 || $type == 5 ? 'red' : 'green' }};">
                                                        {{ ($type == 1 || $type == 2 || $type == 5 ? '-' : '') . number_format($investor['quantity'], 2, ',', '.') }}€
                                                    </td>
                                                    <td>
                                                        {{ Functions::beautyDateTime($investor['date']) }}
                                                    </td>
                                                </tr>
                                            @endif
                                        @endforeach
									@endforeach
								@else
									<tr>
										<td colspan="3">{{ _('No hay operaciones') }}</td>
									</tr>
								@endif
							</tbody>
						</table>
					@endif
				</div>
			</div>
		</div>
	@endif

    @if ($notifications->where('params', '["' . $invoice->id . '"]')->count())
		<div class="margTop20"></div>

		<div class="row">
			<!-- BEGIN PORTLET-->
			<div class="portlet box">
				<div class="portlet-title">
					<div class="tools">
						<a class="panel-full-screen text-muted" href="javascript:;" data-toggle="tooltip" title="Pantalla Completa"><i class="fa fa-expand text-active"></i></a>
						<a class="panel-toggle text-muted" data-toggle="tooltip" href="javascript:;" title="Esconder/Mostrar"><i class="fa fa-chevron-down text-active"></i></a>
					</div>
					Notificaciones
				</div>
				<div class="portlet-body in">
                    <table class="table table-responsive table-condensed">
                        <thead>
                            <tr>
                                <th>{{ _('Notificación') }}</th>
                                <th>{{ _('Fecha') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($own_notifications as $notification)
                            <tr>
                                <th>{{ $notification->message }}</th>
                                <th>{{ Functions::beautyDateTime($notification->created_at) }}</th>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    @endif

@endsection
