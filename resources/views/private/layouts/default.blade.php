<!DOCTYPE html>
<html lang="{{ Config::get('app.locale') }}">
    <head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Área privada</title>

        @include('frontend.partials.metatags')
        @include('frontend.partials.css-core')

		<!-- BEGIN CSS PAGINA -->
		<link href="/assets/css/jasny-bootstrap.css" rel="stylesheet" type="text/css">
		<link  href="/assets/css/jquery.jqplot.css" rel="stylesheet" type="text/css" />

		<link  href="/assets/css/amcharts.css" rel="stylesheet" type="text/css" />
		<link  href="/assets/css/export.css" rel="stylesheet" type="text/css" />
		<link  href="/assets/css/select2.min.css" rel="stylesheet" type="text/css" />
		<!-- END CSS PAGINA -->

    </head>

	<body id="modulos" class="bg-gris-claro">

		<!-- BEGIN LOADER -->
		<div id="loading"><img src="/assets/img/logo.gif" class="vcenter"/></div>
		<!-- END LOADER -->

        {{-- BEGIN MODALS LOGIN / REG --}}
        @include('frontend.partials.modal-auth')
        {{-- END MODALS LOGIN / REG --}}

        {{-- BEGIN MENU TOP --}}
        @include('frontend.partials.menu-top')
        {{-- END MENU TOP --}}

		@if ($user->role == 'investor')
			@include('private.partials._investor-menu')
		@else
			@include('private.partials._transferor-company-menu')
		@endif

		<div class="section">
			<div class="container">
				@if ($user->role == 'investor')
					<div class="col-md-3 sinleft aside">
						@include('private.partials.modules._sidebar')
					</div>
					<div class="col-md-9">
				@else
					<div class="col-md-12">
				@endif

{{--                @include('private.partials.errors')
                    @include('private.partials.message')--}}

					@yield('content')
					@if (false)
						@include('private.partials.modules._table')
					@endif
					</div>
			</div>
		</div>

        {{-- BEGIN FOOTER --}}
        @include('frontend.partials.footer')
        {{-- END FOOTER --}}

        {{-- BEGIN CORE JAVASCRIPTS --}}
        @include('frontend.partials.js-core')
        {{-- END CORE JAVASCRIPTS --}}

		<!-- BEGIN JAVASCRIPT PAGINA -->
		<script type="text/javascript" src="/assets/plugins/jansy/jasny-bootstrap.min.js"></script>
		<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
		<script type="text/javascript" src="/assets/plugins/counterup/jquery.counterup.min.js"></script>

		<script type="text/javascript" src="/assets/plugins/graficas/amcharts.js"></script>

		<script type="text/javascript" src="/assets/plugins/graficas/jquery.jqplot.min.js"></script>
		<script type="text/javascript" src="/assets/plugins/graficas/jqplot.donutRenderer.js"></script>

		<script type="text/javascript" src="/assets/plugins/graficas/es.js"></script>
		<script type="text/javascript" src="/assets/plugins/graficas/serial.js"></script>
		<script type="text/javascript" src="/assets/plugins/graficas/themes/light.js"></script>
		<script type="text/javascript" src="/assets/plugins/graficas/export.js"></script>

		<script type="text/javascript" src="/assets/plugins/select2/select2.full.min.js"></script>

        <script type="text/javascript">
            $(function () {

				setTimeout(function() {
					$('.remove_autocomplete').each(function() {
						$(this).attr('readonly', false);
					})
				}, 300); // Si lo ejecuto en el momento, no va

            });
        </script>

		<script type="text/javascript" src="/assets/scripts/private.js"></script>
		<!-- END JAVASCRIPT PAGINA -->

		<!-- BEGIN JAVASCRIPTS CORE -->
		<script type="text/javascript">
			$(function() {
				Private.init();
			});
		</script>
		<!-- END JAVASCRIPTS CORE -->

        @yield('specific-js')

    </body>

</html>
