@extends('private.layouts.default')
@section('private_notifications', 'active')
@section('content')

	<table class="table table-responsive table-condensed">
		<thead>
			<tr>
				<th>{{ _('Notificación') }}</th>
				<th>{{ _('Fecha') }}</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($notifications as $notification)
				<tr>
					<td>
						@if ($notification->url)
							@if ($notification->params)
								<a href="{{ route($notification->url, [implode(',', json_decode($notification->params))]) }}">
							@else
								<a href="{{ route($notification->url) }}">
							@endif
						@endif

						@if ($notification->read)
							{{ $notification->message }}
						@else
							<strong>{{ $notification->message }}</strong>
						@endif

						@if ($notification->url)
							</a>
						@endif
					</td>
					<td>
						{{ Functions::beautyDateTime($notification->created_at) }}
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>

@endsection
