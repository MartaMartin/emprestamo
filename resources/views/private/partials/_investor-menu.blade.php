<!-- BEGIN SUBMENUBLACK -->
<div id="submenu" class="submenu inversor">
	<div class="container">
		<!-- BEGIN SUBMENU IZQ -->
		<ul class="pull-left menutop">
			<li class="@yield('private_index')">
				<a href="{{ route('private.users.index') }}" class="latoligth12">{{ _('Inicio') }}</a>
			</li>
			<li class="@yield('private_profile')">
				<a href="{{ route('private.investors.profile') }}" class="latoligth12">{{ _('Mis datos') }}</a>
			</li>
			<li class="@yield('private_wallet')">
				<a href="{{ route('private.investors.wallet') }}" class="latoligth12">{{ _('Wallet') }}</a>
			</li>
			<li class="@yield('private_proposals')">
				<a href="{{ route('private.investors.proposals') }}" class="latoligth12">{{ _('Propuestas') }}</a>
			</li>
			<li class="@yield('private_historical')">
				<a href="{{ route('private.investors.historical') }}" class="latoligth12">{{ _('Histórico') }}</a>
			</li>
			<li class="@yield('private_notifications')">
                <a href="{{ route('private.notifications.index') }}" class="latoligth12">
                    {{ _('Notificaciones') }}
                    @if ($notifications->where('read', 0)->count())
                        ({{ $notifications->where('read', 0)->count() }})
                    @endif
                </a>
			</li>
		</ul>
		<!-- END SUBMENU IZQ -->

		<!-- BEGIN MENUSALDO -->
		<div class="dropdown pull-right paddTop10">
			<span class="blanco opacity50 regular15"> Saldo Actual</span>
			<button id="menusaldo" data-close-others="false" data-delay="1000" data-hover="dropdown" data-toggle="dropdown" class="dropdown-toggle transparente"  type="button" >
				<span class="blanco latobold18"> {{ number_format($amount, 2, ',', '.') }}</span> <span class="blanco">€</span> <i class="fa fa-angle-down paddLef20 font18"></i>
                {{-- counter --}}
			</button>
			<ul role="menu" class="dropdown-menu" aria-labelledby="menusaldo">
                <li><a href="{{ route('private.investors.wallet') }}" class="regular15 morado">Ingresar dinero</a></li>
                <li class="divider"></li>
                <li><a href="{{ route('private.investors.wallet', [true]) }}" class="regular15 morado">Retirar dinero</a></li>
                <li class="divider"></li>
                <li><a href="{{ route('private.investors.proposals') }}">Invertir en proyectos</a></li>
			</ul>
		</div>
		<!-- END MENUSALDO -->
	</div>
</div>
<!-- END SUBMENUBLACK -->
