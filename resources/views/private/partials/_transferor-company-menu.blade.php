<!-- BEGIN SUBMENUBLACK -->
<div id="submenu" class="submenu inversor">
	<div class="container">
		<!-- BEGIN SUBMENU IZQ -->
		<ul class="pull-left menutop">
			<li class="@yield('private_index')">
				<a href="{{ route('private.users.index') }}" class="latoligth12">{{ _('Inicio') }}</a>
			</li>
			<li class="@yield('private_profile')">
				<a href="{{ route('private.transferor_companies.profile') }}" class="latoligth12">{{ _('Mis datos') }}</a>
			</li>
			<li class="@yield('private_invoices')">
				<a href="{{ route('private.transferor_companies.invoices') }}" class="latoligth12">{{ _('Préstamos') }}</a>
			</li>
			<li class="@yield('private_notifications')">
                <a href="{{ route('private.notifications.index') }}" class="latoligth12">
                    {{ _('Notificaciones') }}
                    @if ($notifications->where('read', 0)->count())
                        ({{ $notifications->where('read', 0)->count() }})
                    @endif
                </a>
			</li>
		</ul>
		<!-- END SUBMENU IZQ -->
	</div>
</div>
<!-- END SUBMENUBLACK -->
