<div class="morado regular34">
	Wallet
</div>
<div class="well bg-blanco margTop10">
	<div class="row">
		<div class="col-md-12">
			<div class="regular15 negro opacity50">
				Saldo Actual
			</div>
			<div class="negro latoblack34 margTop10">
				<h3 class="counter_">{{ number_format($amount, 2, ',', '.') }} €</h3>
                @if ($pending_earned)
                    <br />
                    Recaudación pendiente +<small class="counter_">{{ number_format($pending_earned, 2, ',', '.') }}</small><span> €</span>
                @endif
                @if ($pending_ingresed)
                    <br />
                    Ingresos pendientes +<small class="counter_">{{ number_format($pending_ingresed, 2, ',', '.') }}</small><span> €</span>
                @endif
                @if ($pending_substracted)
                    <br />
                    Retiradas pendientes <small class="counter_">{{ number_format($pending_substracted, 2, ',', '.') }}</small><span> €</span>
                @endif
			</div>

			<!-- BEGIN DROPDOWN -->
			<div class="dropdown margTop15">
				<li class="btn-group">
					<button id="btnacciones" data-close-others="false" data-delay="1000" data-hover="dropdown" data-toggle="dropdown" class="btn portlet box dropdown-toggle sinradio fullwidth bord-gris2 morado" type="button">
						<div class="pull-left">
							<span class="regular15">Quiero...</span>
						</div>
						<div class="pull-right paddTop1">
							<i class="fa fa-angle-down enmarcado morado"></i>
						</div>
					</button>
					<ul role="menu" class="dropdown-menu" aria-labelledby="btnacciones">
						<li><a href="{{ route('private.investors.wallet') }}" class="regular15 morado">Ingresar dinero</a></li>
						<li class="divider"></li>
						<li><a href="{{ route('private.investors.wallet', [true]) }}" class="regular15 morado">Retirar dinero</a></li>
                        <li class="divider"></li>
                        <li><a href="{{ route('private.investors.proposals') }}">Invertir en proyectos</a></li>
					</ul>
				</li>
			</div>
			<!-- END DROPDOWN -->
		</div>
	</div>
</div>
@if (!empty($donutgraphs))
    <div class="well bg-blanco margTop10">
        <div class="row">
            <div class="col-md-12">
                <div class="regular15 negro">
                    <table style="border: none !important;">
                        <tbody>
                            @foreach ($donutgraphs['invested'] as $aux)
                                <tr class="jqplot-table-legend">
                                    <td class="jqplot-table-legend" style="text-align:center;padding-top:0;">
                                        <div>
                                            <div class="jqplot-table-legend-swatch" style="border-color:{{ $aux[2] }};"></div>
                                        </div>
                                    </td>
                                    <td class="jqplot-table-legend" style="padding-top:0;">{{ $aux[0] }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endif
