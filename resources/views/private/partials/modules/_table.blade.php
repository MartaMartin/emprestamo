<!-- BEGIN MODULOS VARIOS -->

<div class="row regular34 morado margBot10">
	Historial <span class="latoligth21 negro">(movimiento de cuenta bancaria)</span>
</div>
<!-- BEGIN TABLAS -->
<div class="row">
	<table class="table table-responsive table-condensed">
		<thead>
			<th>
				ID (préstamo)
			</th>
			<th>
				fecha
			</th>
			<th>
				cantidad
			</th>
			<th>
				entradas y salidas
			</th>
		</thead>
		<tbody>
			<tr>
				<td class="morado">
					Dato
				</td>
				<td>
					Dato
				</td>
				<td>
					Dato
				</td>
				<td>
					Dato
				</td>
			</tr>
			<tr>
				<td class="morado">
					Dato
				</td>
				<td>
					Dato
				</td>
				<td>
					Dato
				</td>
				<td>
					Dato
				</td>
			</tr>
			<tr>
				<td class="morado">
					Dato
				</td>
				<td>
					Dato
				</td>
				<td>
					Dato
				</td>
				<td>
					Dato
				</td>
			</tr>
		</tbody>
	</table>
</div>
<!-- END TABLAS -->

<div class="clr"></div>

<div class="row">
	<!-- BEGIN PORTLET-->
	<div class="portlet box">
		<div class="portlet-title">
			<div class="tools">
				<a class="panel-full-screen text-muted" href="javascript:;" data-toggle="tooltip" title="Pantalla Completa"><i class="fa fa-expand text-active"></i></a>
				<a class="panel-toggle text-muted" data-toggle="tooltip" href="javascript:;" title="Esconder/Mostrar"><i class="fa fa-chevron-down text-active"></i></a>
			</div>
			Acreditación
		</div>
		<div class="portlet-body collapse in">
			<div class="row">
				<div class="col-md-6">
					<select class="form-control morado select2hid" data-placeholder="Seleccione una opción">
						<option selected="">Inversor acreditado</option>
						<option>Inversor acreditado B</option>
					</select>
				</div>
				<div class="col-md-6"></div>
				<div class="col-md-12">
					<hr>
				</div>
				<div class="col-md-12">
					<div class="latoligth21 clr margBot10">DNI</div>
					<!-- BEGIN FILE UPOAD -->
					<div class="sinleft">
						<div class="fileinput fileinput-new input-group" data-provides="fileinput">
							<div class="form-control" data-trigger="fileinput">
								<!--<i class="fa fa-file"></i>-->
								<span class="fileinput-filename"></span>
							</div>
							<span class="input-group-addon btn btn-transparente sinradio morado fino text-uppercase latoblack12 btn-file">
								<span class="fileinput-new text-uppercase"> Subir archivo</span>
								<span class="fileinput-exists margRig10">Cambiar</span>
								<input type="file" name="archivo">
							</span>
							<a href="#" class="input-group-addon btn btn-transparente sinradio morado fino fileinput-exists latoblack12" data-dismiss="fileinput">Eliminar</a>
						</div>
					</div>
					<!-- END FILE UPOAD -->

				</div>
				<div class="col-md-12">
					<hr>
				</div>
				<div class="col-md-12">
					<div class="latoligth21 clr margBot10">Test de idoneidad</div>
					<div class="regular15 negro margTop20">
						Label
					</div>
					<div class="clr margTop5"></div>
					<input type="text" name="" id="" value="" placeholder="Placeholder" class="form-control">

					<div class="regular15 negro margTop20">
						Label
					</div>
					<div class="clr margTop5"></div>
					<input type="text" name="" id="" value="" placeholder="Placeholder" class="form-control">

				</div>
			</div>
		</div>
		<div class="portlet-footer collapse in margTop20">
			<button class="btn blanco sinradio bg-morado alargado text-uppercase latoblack12 center-block"> Aceptar </button>
			<!--<a class="btn btn-transparente morado sinradio alargado text-uppercase latoblack12"> Ejemplo boton </a>-->
		</div>
	</div>
	<!-- END PORTLET-->
</div>

<div class="clr margTop20"></div>

<!-- BEGIN MODULO PORTLET -->
<div class="row">
	<!-- BEGIN PORTLET-->
	<div class="portlet box">
		<div class="portlet-title">
			<div class="tools">
				<a class="panel-full-screen text-muted" href="javascript:;" data-toggle="tooltip" title="Pantalla Completa"><i class="fa fa-expand text-active"></i></a>
				<a class="panel-toggle text-muted" data-toggle="tooltip" href="javascript:;" title="Esconder/Mostrar"><i class="fa fa-chevron-down text-active"></i></a>
			</div>
			Titulo del modulo
		</div>
		<div class="portlet-body collapse in">
			<div class="row">
				<div class="col-md-6">
					<div id="demodonut" class="jqplot-target"></div>
					<a class="jqplot-image-button btn blanco sinradio bg-morado text-uppercase latoblack12" data-target="demodonut.png" href="" download="donutGraph"><i class="fa fa-cloud-download"></i> Descargar Imagen</a>
					<div id="imagenGraf" class="hidden"></div>
				</div>
			</div>
		</div>
		<div class="portlet-footer collapse in margTop20">
			<a class="btn btn-transparente morado sinradio alargado text-uppercase latoblack12"> Ejemplo boton </a>
		</div>
	</div>
	<!-- END PORTLET-->

</div>
<!-- END MODULO PORTLET -->

<div class="clr margTop20"></div>

<!-- BEGIN TABS -->
<div class="row">
	<div class="topbordermorado fullwidth margTop38">&nbsp;</div>
	<!-- Nav tabs -->
	<ul class="nav nav-tabs" role="tablist">
		<li role="presentation" class="active"><a href="#activos" aria-controls="activos" role="tab" data-toggle="tab">Activos</a></li>
		<li role="presentation"><a href="#rechazados" aria-controls="rechazados" role="tab" data-toggle="tab">Rechazados / Activos</a></li>
	</ul>

	<!-- Tab panes -->
	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active" id="activos">
			<!-- BEGIN PORTLET-->
			<div class="portlet box">
				<div class="portlet-title">
					<div class="tools">
						<a class="panel-full-screen text-muted" href="javascript:;" data-toggle="tooltip" title="Pantalla Completa"><i class="fa fa-expand text-active"></i></a>
						<a class="panel-toggle text-muted" data-toggle="tooltip" href="javascript:;" title="Esconder/Mostrar"><i class="fa fa-chevron-down text-active"></i></a>
					</div>
					Propuestas inversión Activas
				</div>
				<div class="portlet-body collapse in">

					<div class="row">
						<div class="col-md-12">
							<div class="latoligth21">ID préstamo- Título</div>
							<div class="clr margTop20"></div>
							<div class="col-md-12 sinleft">
								<h4 class="inline regular15">Cantidad:</h4> <span class="inline negro opacity50">Valor</span>
							</div>
							<div class="clr paddTop5"></div>
							<div class="col-md-12 sinleft">
								<h4 class="inline regular15">Tipo de interés:</h4> <span class="inline negro opacity50">Valor</span>
							</div>
							<div class="clr paddTop5"></div>
							<div class="col-md-12 sinleft">
								<h4 class="inline regular15">Plazo:</h4> <span class="inline negro opacity50">Valor</span>
							</div>
							<div class="clr paddTop5"></div>
							<div class="col-md-12 sinleft">
								<h4 class="inline regular15">Deudor:</h4> <span class="inline negro opacity50">Valor</span>
							</div>
							<div class="clr paddTop5"></div>
							<div class="col-md-12 sinleft margTop20">
								<button class="btn btn-transparente morado sinradio alargado text-uppercase latoblack12"> invertir </button>
								<button class="btn btn-transparente morado sinradio alargado text-uppercase latoblack12"> descartar </button>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<hr>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="latoligth21">ID préstamo- Título</div>
							<div class="clr margTop20"></div>
							<div class="col-md-12 sinleft">
								<h4 class="inline regular15">Cantidad:</h4> <span class="inline negro opacity50">Valor</span>
							</div>
							<div class="clr paddTop5"></div>
							<div class="col-md-12 sinleft">
								<h4 class="inline regular15">Tipo de interés:</h4> <span class="inline negro opacity50">Valor</span>
							</div>
							<div class="clr paddTop5"></div>
							<div class="col-md-12 sinleft">
								<h4 class="inline regular15">Plazo:</h4> <span class="inline negro opacity50">Valor</span>
							</div>
							<div class="clr paddTop5"></div>
							<div class="col-md-12 sinleft">
								<h4 class="inline regular15">Deudor:</h4> <span class="inline negro opacity50">Valor</span>
							</div>
							<div class="clr paddTop5"></div>
							<div class="col-md-12 sinleft margTop20">
								<button class="btn btn-transparente morado sinradio alargado text-uppercase latoblack12"> invertir </button>
								<button class="btn btn-transparente morado sinradio alargado text-uppercase latoblack12"> descartar </button>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<hr>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="latoligth21">ID préstamo- Título</div>
							<div class="clr margTop20"></div>
							<div class="col-md-12 sinleft">
								<h4 class="inline regular15">Cantidad:</h4> <span class="inline negro opacity50">Valor</span>
							</div>
							<div class="clr paddTop5"></div>
							<div class="col-md-12 sinleft">
								<h4 class="inline regular15">Tipo de interés:</h4> <span class="inline negro opacity50">Valor</span>
							</div>
							<div class="clr paddTop5"></div>
							<div class="col-md-12 sinleft">
								<h4 class="inline regular15">Plazo:</h4> <span class="inline negro opacity50">Valor</span>
							</div>
							<div class="clr paddTop5"></div>
							<div class="col-md-12 sinleft">
								<h4 class="inline regular15">Deudor:</h4> <span class="inline negro opacity50">Valor</span>
							</div>
							<div class="clr paddTop5"></div>
							<div class="col-md-12 sinleft margTop20">
								<button class="btn btn-transparente morado sinradio alargado text-uppercase latoblack12"> invertir </button>
								<button class="btn btn-transparente morado sinradio alargado text-uppercase latoblack12"> descartar </button>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<hr>
						</div>
					</div>

				</div>
				<div class="portlet-footer collapse in">

				</div>
			</div>
			<!-- END PORTLET-->
		</div>
		<div role="tabpanel" class="tab-pane" id="rechazados">

			<!-- BEGIN PORTLET-->
			<div class="portlet box">
				<div class="portlet-title">
					<div class="tools">
						<a class="panel-full-screen text-muted" href="javascript:;" data-toggle="tooltip" title="Pantalla Completa"><i class="fa fa-expand text-active"></i></a>
						<a class="panel-toggle text-muted" data-toggle="tooltip" href="javascript:;" title="Esconder/Mostrar"><i class="fa fa-chevron-down text-active"></i></a>
					</div>
					Propuestas inversión Rechazadas
				</div>
				<div class="portlet-body collapse in">

					<div class="row">
						<div class="col-md-12">
							<div class="latoligth21">ID préstamo RECHAZADO- Título</div>
							<div class="clr margTop20"></div>
							<div class="col-md-12 sinleft">
								<h4 class="inline regular15">Cantidad:</h4> <span class="inline negro opacity50">Valor</span>
							</div>
							<div class="clr paddTop5"></div>
							<div class="col-md-12 sinleft">
								<h4 class="inline regular15">Tipo de interés:</h4> <span class="inline negro opacity50">Valor</span>
							</div>
							<div class="clr paddTop5"></div>
							<div class="col-md-12 sinleft">
								<h4 class="inline regular15">Plazo:</h4> <span class="inline negro opacity50">Valor</span>
							</div>
							<div class="clr paddTop5"></div>
							<div class="col-md-12 sinleft">
								<h4 class="inline regular15">Deudor:</h4> <span class="inline negro opacity50">Valor</span>
							</div>
							<div class="clr paddTop5"></div>
							<div class="col-md-12 sinleft margTop20">
								<button class="btn btn-transparente morado sinradio alargado text-uppercase latoblack12"> invertir </button>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<hr>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="latoligth21">ID préstamo RECHAZADO- Título</div>
							<div class="clr margTop20"></div>
							<div class="col-md-12 sinleft">
								<h4 class="inline regular15">Cantidad:</h4> <span class="inline negro opacity50">Valor</span>
							</div>
							<div class="clr paddTop5"></div>
							<div class="col-md-12 sinleft">
								<h4 class="inline regular15">Tipo de interés:</h4> <span class="inline negro opacity50">Valor</span>
							</div>
							<div class="clr paddTop5"></div>
							<div class="col-md-12 sinleft">
								<h4 class="inline regular15">Plazo:</h4> <span class="inline negro opacity50">Valor</span>
							</div>
							<div class="clr paddTop5"></div>
							<div class="col-md-12 sinleft">
								<h4 class="inline regular15">Deudor:</h4> <span class="inline negro opacity50">Valor</span>
							</div>
							<div class="clr paddTop5"></div>
							<div class="col-md-12 sinleft margTop20">
								<button class="btn btn-transparente morado sinradio alargado text-uppercase latoblack12"> invertir </button>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<hr>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="latoligth21">ID préstamo RECHAZADO- Título</div>
							<div class="clr margTop20"></div>
							<div class="col-md-12 sinleft">
								<h4 class="inline regular15">Cantidad:</h4> <span class="inline negro opacity50">Valor</span>
							</div>
							<div class="clr paddTop5"></div>
							<div class="col-md-12 sinleft">
								<h4 class="inline regular15">Tipo de interés:</h4> <span class="inline negro opacity50">Valor</span>
							</div>
							<div class="clr paddTop5"></div>
							<div class="col-md-12 sinleft">
								<h4 class="inline regular15">Plazo:</h4> <span class="inline negro opacity50">Valor</span>
							</div>
							<div class="clr paddTop5"></div>
							<div class="col-md-12 sinleft">
								<h4 class="inline regular15">Deudor:</h4> <span class="inline negro opacity50">Valor</span>
							</div>
							<div class="clr paddTop5"></div>
							<div class="col-md-12 sinleft margTop20">
								<button class="btn btn-transparente morado sinradio alargado text-uppercase latoblack12"> invertir </button>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<hr>
						</div>
					</div>

				</div>
				<div class="portlet-footer collapse in">

				</div>
			</div>
			<!-- END PORTLET-->
		</div>
	</div>
</div>
<!-- END TABS -->

<div class="clr margTop20"></div>

<!-- BEGIN TABLAS -->
<div class="row">
	<table class="table table-responsive table-condensed" id="tblmsj">
		<thead>
			<th>
				<input type="checkbox" class="group-checkable" data-set="#tblmsj .checkboxes"/>
			</th>
			<th>
				fecha
			</th>
			<th>
				descripción
			</th>
		</thead>
		<tbody>
			<tr>
				<td>
					<input type="checkbox" data-idmsj="2" class="checkboxes"/>
				</td>
				<td>
					24/03/2016
				</td>
				<td>
					Lorem ipsum ad his scripta blandit partiendo, eum fastidii accumsan euripidis in, eum liber hendrerit an. Qui ut wisi vocibus suscipiantur, quo dicit ridens inciderint id. Quo mundi lobortis reformidans eu, legimus senserit definiebas an eos. Eu sit tincidunt incorrupte definitionem, vis mutat affert percipit cu, eirmod consectetuer signiferumque eu per. In usu latine equidem dolores. Quo no falli viris intellegam, ut fugit veritus placerat per.
				</td>
			</tr>
			<tr>
				<td>
					<input type="checkbox" data-idmsj="3" class="checkboxes"/>
				</td>
				<td>
					25/03/2016
				</td>
				<td>
					Ius id vidit volumus mandamus, vide veritus democritum te nec, ei eos debet libris consulatu. No mei ferri graeco dicunt, ad cum veri accommodare. Sed at malis omnesque delicata, usu et iusto zzril meliore. Dicunt maiorum eloquentiam cum cu, sit summo dolor essent te. Ne quodsi nusquam legendos has, ea dicit voluptua eloquentiam pro, ad sit quas qualisque. Eos vocibus deserunt quaestio ei.
				</td>
			</tr>
			<tr>
				<td>
					<input type="checkbox" data-idmsj="4" class="checkboxes"/>
				</td>
				<td>
					26/03/2016
				</td>
				<td>
					Blandit incorrupte quaerendum in quo, nibh impedit id vis, vel no nullam semper audiam. Ei populo graeci consulatu mei, has ea stet modus phaedrum. Inani oblique ne has, duo et veritus detraxit. Tota ludus oratio ea mel, offendit persequeris ei vim. Eos dicat oratio partem ut, id cum ignota senserit intellegat. Sit inani ubique graecis ad, quando graecis liberavisse et cum, dicit option eruditi at duo. Homero salutatus suscipiantur eum id, tamquam voluptaria expetendis ad sed, nobis feugiat similique usu ex.
				</td>
			</tr>
		</tbody>
	</table>
	<button class="btn btn-transparente morado sinradio latoblack12" id="deletetblmsj" data-ref="#tblmsj">Eliminar</button>
</div>
<!-- END TABLAS -->

<div class="clr margTop20"></div>

<!-- BEGIN MODULO PORTLET -->
<div class="row">
	<!-- BEGIN PORTLET-->
	<div class="portlet box">
		<div class="portlet-title">
			<div class="tools">
				<a class="panel-full-screen text-muted" href="javascript:;" data-toggle="tooltip" title="Pantalla Completa"><i class="fa fa-expand text-active"></i></a>
				<a class="panel-toggle text-muted" data-toggle="tooltip" href="javascript:;" title="Esconder/Mostrar"><i class="fa fa-chevron-down text-active"></i></a>
			</div>
			Últimos Préstamos
		</div>
		<div class="portlet-body collapse in ultprest">
			<div class="row">
				<div class="col-md-12">
					<h3 class="pull-left">ID préstamo- Título</h3>
					<div class="pull-right">
						<a class="btn btn-transparente morado redondo margTop20" role="button" data-toggle="collapse" href="#prestamo123" aria-expanded="false" aria-controls="collapseExample">
							<i class="fa fa-close"></i>
						</a>
					</div>
					<div class="clr"></div>
					<div class="col-md-12 sinleft">
						<h4 class="inline regular15">Fecha solicitud:</h4> <span class="inline negro opacity50">Valor</span>
					</div>
					<div class="clr"></div>
					<div class="col-md-12 sinleft">
						<h4 class="inline regular15">Estado:</h4> <span class="inline negro opacity50">Valor</span>
					</div>
					<div class="collapse desplegable pull-left" id="prestamo123">
						<div class="col-md-12 sinleft">
							<h4 class="inline regular15">Fecha devolución préstamo:</h4> <span class="inline negro opacity50">Valor</span>
						</div>
						<div class="col-md-12 sinleft">
							<h4 class="inline regular15">Tipo de interés de la operación:</h4> <span class="inline negro opacity50">Valor</span>
						</div>
						<div class="col-md-12 sinleft">
							<h4 class="inline regular15">Dinero Adelantado (primer pago):</h4> <span class="inline negro opacity50">Valor</span>
						</div>
						<div class="col-md-12 sinleft">
							<h4 class="inline regular15">2º Pago (resto del capital):</h4> <span class="inline negro opacity50">Valor</span>
						</div>
						<div class="col-md-12 sinleft">
							<h4 class="inline regular15">Coste de la operación:</h4> <span class="inline negro opacity50">Valor</span>
						</div>
						<div class="col-md-12 sinleft">
							<h4 class="inline regular15">Cliente Deudor:</h4> <span class="inline negro opacity50">Valor</span>
						</div>
						<div class="col-md-12 sinleft">
							<h4>Anexos:</h4>
							<div class="col-md-12 sinleft">
								<h4 class="inline regular15">Titulo del documento solicitado:</h4> <span class="inline"><a href="javascript:;" class="naranja">Descargar</a> / Descartar</span>
							</div>
							<div class="col-md-12 sinleft">
								<h4 class="inline regular15">Titulo del documento solicitado:</h4> <span class="inline"><a href="javascript:;" class="naranja">Descargar</a> / Descartar</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<hr>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h3 class="pull-left">ID préstamo- Título</h3>
					<div class="pull-right">
						<a class="btn btn-transparente morado redondo margTop20" role="button" data-toggle="collapse" href="#prestamo124" aria-expanded="false" aria-controls="collapseExample">
							<i class="fa fa-close"></i>
						</a>
					</div>
					<div class="clr"></div>
					<div class="col-md-12 sinleft">
						<h4 class="inline regular15">Fecha solicitud:</h4> <span class="inline negro opacity50">Valor</span>
					</div>
					<div class="clr"></div>
					<div class="col-md-12 sinleft">
						<h4 class="inline regular15">Estado:</h4> <span class="inline negro opacity50">Valor</span>
					</div>
					<div class="collapse desplegable pull-left" id="prestamo124">
						<div class="col-md-12 sinleft">
							<h4 class="inline regular15">Fecha devolución préstamo:</h4> <span class="inline negro opacity50">Valor</span>
						</div>
						<div class="col-md-12 sinleft">
							<h4 class="inline regular15">Tipo de interés de la operación:</h4> <span class="inline negro opacity50">Valor</span>
						</div>
						<div class="col-md-12 sinleft">
							<h4 class="inline regular15">Dinero Adelantado (primer pago):</h4> <span class="inline negro opacity50">Valor</span>
						</div>
						<div class="col-md-12 sinleft">
							<h4 class="inline regular15">2º Pago (resto del capital):</h4> <span class="inline negro opacity50">Valor</span>
						</div>
						<div class="col-md-12 sinleft">
							<h4 class="inline regular15">Coste de la operación:</h4> <span class="inline negro opacity50">Valor</span>
						</div>
						<div class="col-md-12 sinleft">
							<h4 class="inline regular15">Cliente Deudor:</h4> <span class="inline negro opacity50">Valor</span>
						</div>
						<div class="col-md-12 sinleft">
							<h4>Anexos:</h4>
							<div class="col-md-12 sinleft">
								<h4 class="inline regular15">Titulo del documento solicitado:</h4> <span class="inline"><a href="javascript:;" class="naranja">Descargar</a> / Descartar</span>
							</div>
							<div class="col-md-12 sinleft">
								<h4 class="inline regular15">Titulo del documento solicitado:</h4> <span class="inline"><a href="javascript:;" class="naranja">Descargar</a> / Descartar</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<hr>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h3 class="pull-left">ID préstamo- Título</h3>
					<div class="pull-right">
						<a class="btn btn-transparente morado redondo margTop20" role="button" data-toggle="collapse" href="#prestamo125" aria-expanded="false" aria-controls="collapseExample">
							<i class="fa fa-close"></i>
						</a>
					</div>
					<div class="clr"></div>
					<div class="col-md-12 sinleft">
						<h4 class="inline regular15">Fecha solicitud:</h4> <span class="inline negro opacity50">Valor</span>
					</div>
					<div class="clr"></div>
					<div class="col-md-12 sinleft">
						<h4 class="inline regular15">Estado:</h4> <span class="inline negro opacity50">Valor</span>
					</div>
					<div class="collapse desplegable pull-left" id="prestamo125">
						<div class="col-md-12 sinleft">
							<h4 class="inline regular15">Fecha devolución préstamo:</h4> <span class="inline negro opacity50">Valor</span>
						</div>
						<div class="col-md-12 sinleft">
							<h4 class="inline regular15">Tipo de interés de la operación:</h4> <span class="inline negro opacity50">Valor</span>
						</div>
						<div class="col-md-12 sinleft">
							<h4 class="inline regular15">Dinero Adelantado (primer pago):</h4> <span class="inline negro opacity50">Valor</span>
						</div>
						<div class="col-md-12 sinleft">
							<h4 class="inline regular15">2º Pago (resto del capital):</h4> <span class="inline negro opacity50">Valor</span>
						</div>
						<div class="col-md-12 sinleft">
							<h4 class="inline regular15">Coste de la operación:</h4> <span class="inline negro opacity50">Valor</span>
						</div>
						<div class="col-md-12 sinleft">
							<h4 class="inline regular15">Cliente Deudor:</h4> <span class="inline negro opacity50">Valor</span>
						</div>
						<div class="col-md-12 sinleft">
							<h4>Anexos:</h4>
							<div class="col-md-12 sinleft">
								<h4 class="inline regular15">Titulo del documento solicitado:</h4> <span class="inline"><a href="javascript:;" class="naranja">Descargar</a> / Descartar</span>
							</div>
							<div class="col-md-12 sinleft">
								<h4 class="inline regular15">Titulo del documento solicitado:</h4> <span class="inline"><a href="javascript:;" class="naranja">Descargar</a> / Descartar</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<hr>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h3 class="pull-left">ID préstamo- Título</h3>
					<div class="pull-right">
						<a class="btn btn-transparente morado redondo margTop20" role="button" data-toggle="collapse" href="#prestamo126" aria-expanded="false" aria-controls="collapseExample">
							<i class="fa fa-close"></i>
						</a>
					</div>
					<div class="clr"></div>
					<div class="col-md-12 sinleft">
						<h4 class="inline regular15">Fecha solicitud:</h4> <span class="inline negro opacity50">Valor</span>
					</div>
					<div class="clr"></div>
					<div class="col-md-12 sinleft">
						<h4 class="inline regular15">Estado:</h4> <span class="inline negro opacity50">Valor</span>
					</div>
					<div class="collapse desplegable pull-left" id="prestamo126">
						<div class="col-md-12 sinleft">
							<h4 class="inline regular15">Fecha devolución préstamo:</h4> <span class="inline negro opacity50">Valor</span>
						</div>
						<div class="col-md-12 sinleft">
							<h4 class="inline regular15">Tipo de interés de la operación:</h4> <span class="inline negro opacity50">Valor</span>
						</div>
						<div class="col-md-12 sinleft">
							<h4 class="inline regular15">Dinero Adelantado (primer pago):</h4> <span class="inline negro opacity50">Valor</span>
						</div>
						<div class="col-md-12 sinleft">
							<h4 class="inline regular15">2º Pago (resto del capital):</h4> <span class="inline negro opacity50">Valor</span>
						</div>
						<div class="col-md-12 sinleft">
							<h4 class="inline regular15">Coste de la operación:</h4> <span class="inline negro opacity50">Valor</span>
						</div>
						<div class="col-md-12 sinleft">
							<h4 class="inline regular15">Cliente Deudor:</h4> <span class="inline negro opacity50">Valor</span>
						</div>
						<div class="col-md-12 sinleft">
							<h4>Anexos:</h4>
							<div class="col-md-12 sinleft">
								<h4 class="inline regular15">Titulo del documento solicitado:</h4> <span class="inline"><a href="javascript:;" class="naranja">Descargar</a> / Descartar</span>
							</div>
							<div class="col-md-12 sinleft">
								<h4 class="inline regular15">Titulo del documento solicitado:</h4> <span class="inline"><a href="javascript:;" class="naranja">Descargar</a> / Descartar</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<hr>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h3 class="pull-left">ID préstamo- Título</h3>
					<div class="pull-right">
						<a class="btn btn-transparente morado redondo margTop20" role="button" data-toggle="collapse" href="#prestamo127" aria-expanded="false" aria-controls="collapseExample">
							<i class="fa fa-close"></i>
						</a>
					</div>
					<div class="clr"></div>
					<div class="col-md-12 sinleft">
						<h4 class="inline regular15">Fecha solicitud:</h4> <span class="inline negro opacity50">Valor</span>
					</div>
					<div class="clr"></div>
					<div class="col-md-12 sinleft">
						<h4 class="inline regular15">Estado:</h4> <span class="inline negro opacity50">Valor</span>
					</div>
					<div class="collapse desplegable pull-left" id="prestamo127">
						<div class="col-md-12 sinleft">
							<h4 class="inline regular15">Fecha devolución préstamo:</h4> <span class="inline negro opacity50">Valor</span>
						</div>
						<div class="col-md-12 sinleft">
							<h4 class="inline regular15">Tipo de interés de la operación:</h4> <span class="inline negro opacity50">Valor</span>
						</div>
						<div class="col-md-12 sinleft">
							<h4 class="inline regular15">Dinero Adelantado (primer pago):</h4> <span class="inline negro opacity50">Valor</span>
						</div>
						<div class="col-md-12 sinleft">
							<h4 class="inline regular15">2º Pago (resto del capital):</h4> <span class="inline negro opacity50">Valor</span>
						</div>
						<div class="col-md-12 sinleft">
							<h4 class="inline regular15">Coste de la operación:</h4> <span class="inline negro opacity50">Valor</span>
						</div>
						<div class="col-md-12 sinleft">
							<h4 class="inline regular15">Cliente Deudor:</h4> <span class="inline negro opacity50">Valor</span>
						</div>
						<div class="col-md-12 sinleft">
							<h4>Anexos:</h4>
							<div class="col-md-12 sinleft">
								<h4 class="inline regular15">Titulo del documento solicitado:</h4> <span class="inline"><a href="javascript:;" class="naranja">Descargar</a> / Descartar</span>
							</div>
							<div class="col-md-12 sinleft">
								<h4 class="inline regular15">Titulo del documento solicitado:</h4> <span class="inline"><a href="javascript:;" class="naranja">Descargar</a> / Descartar</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<hr>
				</div>
			</div>

		</div>
		<div class="portlet-footer collapse in margTop20">
			<!--<a class="btn btn-transparente morado sinradio alargado text-uppercase"> Ejemplo boton </a>-->
		</div>
	</div>
	<!-- END PORTLET-->

</div>
<!-- END MODULO PORTLET -->

<div class="clr margTop50"></div>

<!-- BEGIN criterios inversion auto -->
<div class="row regular34 morado margBot10">
	Criterios de inversión automática
</div>
<div class="row">
	<!-- BEGIN PORTLET-->
	<div class="portlet box">
		<div class="portlet-body collapse in">
			<div class="row">
				<div class="col-md-5">
					<div class="latoligth21 negro lineheight40">
						Inversión automática
					</div>
				</div>
				<div class="col-md-7">
					<div class="btn-group" role="group" aria-label="...">
						<button type="button" class="btn btn-gris">Permitir</button>
						<button type="button" class="btn btn-gris active">Denegar</button>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<hr>
				</div>
			</div>
			<div class="row">
				<div class="col-md-5">
					<div class="latoligth21 negro">
						Límite de inversión por factura
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="regular15 negro margTop20">
								Cantidad...
							</div>
							<div class="clr margTop5"></div>
							<input type="number" step="any" name="" id="" value="" placeholder="Cantidad" class="form-control">
						</div>
					</div>
				</div>
				<div class="col-md-7 black opacity50 regular15">
					Lorem ipsum ad his scripta blandit partiendo, eum fastidii accumsan euripidis in, eum liber hendrerit an. Qui ut wisi vocibus suscipiantur, quo dicit ridens inciderint id. Quo mundi lobortis reformidans eu, legimus senserit definiebas an eos.
				</div>
			</div>
		</div>
	</div>
	<!-- END PORTLET-->
	<button class="btn alargado noradio bg-morado text-uppercase blanco margTop20 latoblack12">Guardar Cambios</button>
</div>
<!-- END criterios inversion auto -->

<div class="clr margTop20"></div>

<div class="row">
	<!-- BEGIN PORTLET-->
	<div class="portlet box">
		<div class="portlet-title">
			<div class="tools">
				<a class="panel-full-screen text-muted" href="javascript:;" data-toggle="tooltip" title="Pantalla Completa"><i class="fa fa-expand text-active"></i></a>
				<a class="panel-toggle text-muted" data-toggle="tooltip" href="javascript:;" title="Esconder/Mostrar"><i class="fa fa-chevron-down text-active"></i></a>
			</div>
			Gráfica Wallet
		</div>
		<div class="portlet-body collapse in">
			<!-- BEGIN TABLA GRAFICA -->
			<div class="row margTop20">
				<div id="chartdiv"></div>
			</div>
			<!-- END TABLA GRAFICA -->
		</div>
		<div class="portlet-footer collapse in margTop20">
			<a class="btn btn-transparente morado sinradio alargado text-uppercase"> Ejemplo boton </a>
		</div>
	</div>
	<!-- END PORTLET-->
</div>

<!-- END MODULOS VARIOS -->
