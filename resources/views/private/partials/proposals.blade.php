<table class="table table-responsive table-condensed">
    <thead>
        <th>Operación</th>
        <th style="text-align: center;">Cantidad invertida</th>
        <th style="text-align: right;">Recaudado</th>
        <th style="text-align: center;">Empresa</th>
        <th style="text-align: center;">Calificación</th>
        <th style="text-align: center;">Estado</th>
        <th style="text-align: right;" width="15%">Opciones</th>
    </thead>
    <tbody>
        @foreach ($operations as $operation)
            <tr>
                <td class="morado">
                    <a class="amplia" role="button" data-toggle="collapse" href="#id_{{ $operation->id }}" aria-expanded="false" aria-controls="collapseExample">{{ $operation->invoice->name }}</a>
                    <br />
                    {{ number_format($operation->invoice->price, 2, ',', '.') }}€
                </td>
                <td style="text-align: center;">
                    {{ number_format($operation->invested, 2, ',', '.') }} €
                    <br />
                    ({{ $operation->fee }}% Interés)
                </td>
                <td style="text-align: right;">
                    {{ number_format($operation->getCurrentInvested(), 2, ',', '.') }}€
                </td>
                <td style="text-align: center;">
                    {{ $operation->invoice->transferorCompany->name }}
                    {{ $operation->invoice->debtorCompany->name }}
                </td>
                <td style="text-align: center;">
                    {{ $operation->calification }}
                </td>
                <td style="text-align: center;">
                    {{ $operation->statusName }}
                </td>
                <td>
                    @if ($operation->status == 3)
                        {!! Form::open([
                            'method' => 'post',
                            'route' => [
                                'private.operations.invest',
                                $operation->id
                            ]
                        ]) !!}
                            <div class="input-group">
                                {!! Form::number('amount', $operation->ticket, ['class' => 'form-control', 'step' => 0.01]) !!}
                                <span class="input-group-addon">€</span>
                            </div>
                            <button type="submit" class="btn blanco sinradio bg-morado alargado text-uppercase margTop5 latoblack12 center-block">Invertir</button>
                        {!! Form::close() !!}
                    @endif
                    @if (count($operation->transfers) == 0 && !$operation->invoice->rejectedInvoice)
                        <a class="btn blanco sinradio bg-gris text-uppercase margTop5 latoblack12 center-block" href="{{ route('private.operations.reject', $operation->invoice->id) }}">{{ _('Rechazar') }}</a>
                    @endif
                </td>
            </tr>
            <tr>
                <td colspan="10" style="height: 0px;">
                    <div class="collapse desplegable" id="id_{{ $operation->id }}">
                        <table class="table table-responsive table-condensed">
                            <thead>
                                <tr>
                                    <th>Tipo de transferencia</th>
                                    <th>Cantidad</th>
                                    <th>Fecha</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ($operation->transfersGrouped as $type => $investors)
                                @foreach ($investors as $investor_id => $i)
                                    @if ($investor_id == $investor->id)
                                        <tr>
                                            <td>
                                                {{ $transfer_types[$type] }}
                                            </td>
                                            <td>
                                                {{ number_format($i['quantity'], 2, ',', '.') }}
                                            </td>
                                            <td>
                                                {{ Functions::beautyDateTime($i['date']) }}
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
