@extends('private.layouts.default')
@section('private_invoices', 'active')
@section('js_pagina','validateCompanyInvoices();')
@section('content')
	{{-- form start --}}
	{!! Form::open(
			[
			'id' => 'frmCompInv',
			'files' => true,
			'method' => 'PUT',
			'route' => [
				'private.invoices.store'
			],
			'role' => 'form'
		])
	!!}

	{!! Form::hidden('transferor_company_id', $transferor_company->id) !!}

	<div class="row">
		<!-- BEGIN PORTLET-->
		<div class="portlet box">
			<div class="portlet-title">
				<div class="tools">
					<a class="panel-full-screen text-muted" href="javascript:;" data-toggle="tooltip" title="Pantalla Completa"><i class="fa fa-expand text-active"></i></a>
					<a class="panel-toggle text-muted" data-toggle="tooltip" href="javascript:;" title="Esconder/Mostrar"><i class="fa fa-chevron-down text-active"></i></a>
				</div>
				Presupuestos
			</div>
			<div class="portlet-body">
				<div class="row">
					<div class="col-md-4 margTop10">
						{!! Form::label('name', _('Nombre')) !!}
						{!! Form::text('name', null,
							[
								'class' => 'form-control',
								'placeholder' => _('Nombre del proyecto'),
								'required'
							])
						!!}
					</div>
					<div class="col-md-4 margTop10">
						{!! Form::label('price', _('Precio')) !!}
						<div class="input-group">
							{!! Form::text('price', null,
								[
									'class' => 'form-control',
									'placeholder' => 1000,
									'required'
								])
							!!}
							<span class="input-group-addon">€</span>
						</div>
					</div>

					<div class="col-md-4 margTop10">
						{!! Form::label('expires', _('Fecha Devolución Préstamo')) !!}
						<div class="input-group">
							{!! Form::text('expires', null,
								[
									'class' => 'form-control calendariolimitmin',
									'placeholder' => date('Y-m-d', strtotime('now + 1 month')),
									'required'
								])
							!!}
							<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						</div>
					</div>

					<div class="col-md-12 margTop10">
						{!! Form::label('description', _('Descripción de la operación')) !!}
                        {!! Form::textarea('description', null,
                            [
                                'class' => 'form-control',
                                'required'
                            ])
                        !!}
					</div>

					{{--
					<div class="col-md-6 margTop10">
						{!! Form::label('debtor_company_id', _('Empresa Deudora')) !!}
						{!! Form::select('debtor_company_id', $debtor_companies,
						null, ['class' => 'form-control']) !!}
					</div>
					--}}
					<div class="clr"></div>

					<div class="col-md-12">
						<hr>
						<h3>{{ _('Ficheros adjuntos') }} <a href="#" id="clone" class="margLef10 morado btn btn-transparente x-sm text-center text-uppercase">{{ _('Añadir adjunto') }} +</a></h3>
					</div>

					<div class="clr"></div>

					<div id="dynamic_list">
						<div id="clone_me" class="col-md-6 margTop10 clon hidden">
							{!! Form::label('names[]', _('Nombre del fichero')) !!}
							<a href="#" class="remove_item" item="1º">{{ _('Eliminar') }}</a>
							{!! Form::text('names[]', null,
								[
									'class' => 'form-control novalidate',
									'placeholder' => _('Nombre del fichero'),
									'required'
								])
							!!}
							<div class="sinleft margTop10">
								<div class="fileinput fileinput-new input-group" data-provides="fileinput">
									<div class="form-control" data-trigger="fileinput">
										<span class="fileinput-filename"></span>
									</div>
									<span class="input-group-addon btn btn-transparente sinradio morado fino text-uppercase latoblack12 btn-file">
										<span class="fileinput-new text-uppercase"> Subir archivo</span>
										<span class="fileinput-exists margRig10">Cambiar</span>
										{!! Form::file('filenames[]',['class'=>'novalidate']) !!}
									</span>
									<a href="#" class="input-group-addon btn btn-transparente sinradio morado fino fileinput-exists latoblack12" data-dismiss="fileinput">Eliminar</a>
								</div>
							</div>
						</div>
						{{--<div id="clone_me1" class="col-md-6 margTop10 clon">
							{!! Form::label('names[]', _('Nombre del fichero')) !!}
							<a href="#" class="remove_item" item="1º">{{ _('Eliminar') }}</a>
							{!! Form::text('names[]', null,
								[
									'class' => 'form-control',
									'placeholder' => _('Nombre del fichero'),
									'required'
								])
							!!}
							<div class="sinleft margTop10">
								<div class="fileinput fileinput-new input-group" data-provides="fileinput">
									<div class="form-control" data-trigger="fileinput">
										<span class="fileinput-filename"></span>
									</div>
									<span class="input-group-addon btn btn-transparente sinradio morado fino text-uppercase latoblack12 btn-file">
										<span class="fileinput-new text-uppercase"> Subir archivo</span>
										<span class="fileinput-exists margRig10">Cambiar</span>
										{!! Form::file('filenames[]',['class'=>'novalidate']) !!}
									</span>
									<a href="#" class="input-group-addon btn btn-transparente sinradio morado fino fileinput-exists latoblack12" data-dismiss="fileinput">Eliminar</a>
								</div>
							</div>
						</div>--}}
					</div>

					<div class="col-md-12">
						<hr>
					</div>
				</div>
			</div>
            <!--
            <div class="margTop10">
                <label style="text-align: center; display: block;">
                    {!! Form::checkbox('tos', 1, 0, ['required']) !!}
                    Acepto los términos del contrato
                </label>
			</div>
            -->
			<div class="portlet-footer margTop20">
				<button type="submit" class="btn blanco sinradio bg-morado alargado text-uppercase latoblack12 center-block"> Enviar presupuesto </button>
			</div>
		</div>
		<!-- END PORTLET-->
	</div>

	{!! Form::close() !!}{{-- /.form --}}

	<div class="margTop20"></div>

	@if (count($invoices))
	<div class="row">
		<table class="table table-responsive table-condensed">
			<thead>
				<tr>
					<th>Nombre</th>
					<th>Interés</th>
					<th style="text-align: right;">Precio</th>
					<th style="text-align: right;">Recaudado</th>
					<th style="text-align: center;">Estado</th>
					<th style="text-align: center;">Fecha</th>
					<th>Archivos</th>
				<tr>
			</thead>
			<tbody>
				@foreach ($invoices as $invoice)
					<tr>
						<td>
							<a href="{{ route('private.invoices.view', $invoice->id) }}">{{ $invoice->name }}</a>
						</td>
						<td>
							@if (count($invoice->operations))
                                {{ number_format($invoice->operations[0]->fee, 2, ',', '.') }}%
                            @endif
						</td>
						<td style="text-align: right;">
							{{ number_format($invoice->price, 2, ',', '.') }}
						</td>
						<td style="text-align: right;">
							@if (count($invoice->operations))
                                {{ number_format($invoice->operations[0]->getCurrentInvested(), 2, ',', '.') }}
                            @endif
						</td>
						<td style="text-align: center;">
							@if (count($invoice->operations))
								{{ $status[$invoice->operations[0]->status] }}
							@else
								{{ $status[0] }}
							@endif
						</td>
						<td style="text-align: center;">
							@if (count($invoice->operations))
								@if ($invoice->operations[0]->status == 3)
                                    Expira: {{ Functions::beautyDate($invoice->expires) }}
                                @else
                                    @if ($invoice->operations[0]->closed_date)
                                        {{ Functions::beautyDateTime($invoice->operations[0]->closed_date) }}
                                    @endif
                                @endif
                            @else
                                Expira: {{ Functions::beautyDate($invoice->expires) }}
                            @endif
						</td>
						<td>
                            <ul class="no-style">
							@foreach ($invoice->documents as $document)
								@if ($document->type == 'image')
                                    <li><a href="{{ AssetUrl::get('Document', $document->type, $document->filename, 0, 0) }}" target="_blank"><i class="fa fa-file"></i> {{ $document->name }}</a></li>
								@else
									<li><a href="{{ AssetUrl::get('Document', $document->type, $document->filename) }}" target="_blank"><i class="fa fa-file"></i> {{ $document->name }}</a></li>
								@endif
							@endforeach
                            </ul>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	@endif

@endsection
