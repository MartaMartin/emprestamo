@extends('private.layouts.default')
@section('private_profile', 'active')
@section('js_pagina','validateCompanyProfile();')
@section('content')

	{{-- form start --}}
	{!! Form::model($user,
			[
			'id' => 'frmCompProf',
			'files' => true,
			'method' => 'PUT',
			'route' => [
				'private.transferor_companies.update'
			],
			'role' => 'form'
		])
	!!}

	<div class="row">
		<!-- BEGIN PORTLET-->
		<div class="portlet box">
			<div class="portlet-title">
				<div class="tools">
					<a class="panel-full-screen text-muted" href="javascript:;" data-toggle="tooltip" title="Pantalla Completa"><i class="fa fa-expand text-active"></i></a>
					<a class="panel-toggle text-muted" data-toggle="tooltip" href="javascript:;" title="Esconder/Mostrar"><i class="fa fa-chevron-down text-active"></i></a>
				</div>
				Mis datos
			</div>
			<div class="portlet-body">
				<div class="row">
					<div class="col-md-6 margTop10">
						{!! Form::label('name', _('Nombre')) !!}
						{!! Form::text('name', $user->name,
							[
								'class' => 'form-control',
								'placeholder' => _('Nombre usuario'),
								'required'
							])
						!!}
					</div>
					<div class="col-md-6 margTop10">
						{!! Form::label('family_name', _('Apellidos')) !!}
						{!! Form::text('family_name', $user->family_name,
							[
								'class' => 'form-control',
								'placeholder' => _('Apellidos usuario'),
								'required'
							])
						!!}
					</div>
					<div class="clr"></div>
					<div class="col-md-6 margTop10">
						{!! Form::label('email', _('Email')) !!}
						{!! Form::email('email', $user->email,
							[
								'class' => 'form-control',
								'placeholder' => _('Email usuario'),
								'required'
							])
						!!}
					</div>
					<div class="col-md-6 margTop10">
						{!! Form::label('password', _('Contraseña')) !!}
						{!! Form::password('password',
							[
								'class' => 'form-control remove_autocomplete',
								'placeholder' => _('Contraseña usuario'),
								'readonly' => true
							])
						!!}
					</div>
					<div class="clr"></div>
					<div class="col-md-12">
						<hr>
					</div>
					<div class="clr"></div>
					<div class="col-md-6 margTop10">
						{!! Form::label('real_id_type', _('Tipo identificación')) !!}
						{!! Form::select('real_id_type', [
							'dni' => 'DNI',
							'cif' => 'CIF',
							'nie' => 'NIE'
						],
						$user->real_id_type, ['class' => 'form-control select2']) !!}
					</div>
					<div class="col-md-6 margTop10">
						{!! Form::label('real_id_number', _('Número identificación')) !!}
						{!! Form::text('real_id_number', $user->real_id_number,
							[
								'class' => 'form-control',
								'placeholder' => _('Número identificación'),
								'required'
							])
						!!}
					</div>
					<div class="clr"></div>
					<div class="col-md-6 margTop10">
						{!! Form::label('transferor_company_name', _('Nombre de la empresa')) !!}
						{!! Form::text('transferor_company_name', $transferor_company->name,
							[
								'class' => 'form-control',
								'placeholder' => _('Nombre de la empresa'),
								'required'
							])
						!!}
					</div>
                    @if (!$user->dni_verified)
                        <div class="col-md-6 margTop10">
                            {!! Form::label('', _('DNI')) !!}

                            @if ($document)
                                <a target="_blank" href="{{ AssetUrl::get('Document', $document->type, $document->filename, 0, 0) }}"><i class="fa fa-file"></i> Ver archivo</a>
                            @endif
                            <div class="sinleft">
                                <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                    <div class="form-control" data-trigger="fileinput">
                                        <span class="fileinput-filename"></span>
                                    </div>
                                    <span class="input-group-addon btn btn-transparente sinradio morado fino text-uppercase latoblack12 btn-file">
                                        <span class="fileinput-new text-uppercase"> Subir archivo</span>
                                        <span class="fileinput-exists margRig10">Cambiar</span>
                                        {!! Form::file('filename') !!}
                                    </span>
                                    <a href="#" class="input-group-addon btn btn-transparente sinradio morado fino fileinput-exists latoblack12" data-dismiss="fileinput">Eliminar</a>
                                </div>
                            </div>
                        </div>
                    @endif
					<div class="clr"></div>
					<div class="col-md-12">
						<hr>
					</div>
				</div>
			</div>
			<div class="portlet-footer margTop20">
				<button type="submit" class="btn blanco sinradio bg-morado alargado text-uppercase latoblack12 center-block"> Actualizar perfil </button>
			</div>
		</div>
		<!-- END PORTLET-->
	</div>

	{!! Form::close() !!}{{-- /.form --}}

@endsection
