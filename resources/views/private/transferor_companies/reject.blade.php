@extends('private.layouts.default')
@section('private_invoices', 'active')
@section('content')

	{{-- form start --}}
	{!! Form::open(
			[
			'files' => true,
			'route' => [
				'private.operations.reject-proposal-post',
				$id
			],
			'role' => 'form'
		])
	!!}

	<div class="row">
		<!-- BEGIN PORTLET-->
		<div class="portlet box">
			<div class="portlet-title">
				<div class="tools">
					<a class="panel-full-screen text-muted" href="javascript:;" data-toggle="tooltip" title="Pantalla Completa"><i class="fa fa-expand text-active"></i></a>
					<a class="panel-toggle text-muted" data-toggle="tooltip" href="javascript:;" title="Esconder/Mostrar"><i class="fa fa-chevron-down text-active"></i></a>
				</div>
				¿Qué motivo tienes para rechazar la propuesta?
			</div>
			<div class="portlet-body in">
				<div class="row">
					<div class="col-md-12 margTop10">
						{!! Form::label('reject_explain', _('Motivo')) !!}
						{!! Form::textarea('reject_explain', null,
							[
								'class' => 'form-control',
								'placeholder' => _('Descripción del motivo'),
								'required'
							])
						!!}
					</div>

					<div class="col-md-12">
						<hr>
					</div>
				</div>
			</div>
			<div class="portlet-footer in margTop20">
				<button type="submit" class="btn blanco sinradio bg-morado alargado text-uppercase latoblack12 center-block"> Rechazar propuesta </button>
			</div>
		</div>
		<!-- END PORTLET-->
	</div>

	{!! Form::close() !!}{{-- /.form --}}

@endsection
