@extends('private.layouts.default')
@section('private_index', 'active')
@section('content')

<div class="row">
	<!-- BEGIN PORTLET-->
	<div class="portlet box">
		<div class="portlet-title">
			<div class="tools">
				<a class="panel-full-screen text-muted" href="javascript:;" data-toggle="tooltip" title="Pantalla Completa"><i class="fa fa-expand text-active"></i></a>
				<a class="panel-toggle text-muted" data-toggle="tooltip" href="javascript:;" title="Esconder/Mostrar"><i class="fa fa-chevron-down text-active"></i></a>
			</div>
			@if ($user->role == 'investor')
				Introducción inversores
			@elseif ($user->role == 'transferor_company')
				Introducción empresas cedentes
			@endif
		</div>
		<div class="portlet-body">
			<div class="row">
				<div class="col-md-12">
					@if ($user->role == 'investor')
                        <p>Bienvenido a la mayor Red de Inversores privados de España a través de EMPRESTAMO.</p>
                        <p>¿Quieres cobrar ya tus facturas?</p>
                        <p>Completa el formulario de alta de tu Proyecto para que podamos analizarlo y presentarte una oferta de financiación en menos de 48 horas.</p>
					@endif

					@if ($user->role == 'transferor_company')
                        <p>Bienvenido a EMPRESTAMO, web líder en inversión colectiva mediante préstamos.</p>
                        <p>Obtén altas rentabilidades con operaciones a corto plazo.</p>
                        <p>Desde tu perfil podrás prestar capital a compañías solventes de la mano de EMPRESTAMO, de forma segura y sin riesgos, pues todas las operaciones propuestas en la plataforma están aseguradas.</p>
					@endif
				</div>
			</div>
		</div>
		<!-- END PORTLET-->
	</div>
</div>

@endsection
